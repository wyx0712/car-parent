/**
 * 
 */
package utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

/**
 * @author 王耀星
 * @date 2018年10月30日
 */
public class ExportWordWrapper extends ExportWord {

	public void exportWord(HttpServletResponse response) {

		try {
			response.setContentType("application/msword");
			response.addHeader("Content-Disposition",
					"attachment;filename=" + URLEncoder.encode("续期通知单", "UTF-8") + ".doc");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		try {
			createWord(response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
