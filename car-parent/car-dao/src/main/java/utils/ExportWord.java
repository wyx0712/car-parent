/**
 * 
 */
package utils;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

/**
 * @author 王耀星
 * @date 2018年10月30日
 */
public class ExportWord {

	// 生成续期通知单的方法
	public void createWord(OutputStream out) throws IOException {
		XWPFDocument doc = new XWPFDocument(); // 创建word文件
		XWPFParagraph p = doc.createParagraph(); // 新建段落
		p.setAlignment(ParagraphAlignment.CENTER);// 设置段落的对齐方式
		XWPFRun r = p.createRun();// 创建段落文本
		r.setFontSize(20);
		r.setText("中国平安车险有限公司");
		r.setTextPosition(35); // 设置行间距
		r.setBold(true);// 设置为粗体
		r.setColor("FF0000");// 设置颜色

		p = doc.createParagraph(); // 新建段落
		p.setAlignment(ParagraphAlignment.CENTER);
		r = p.createRun();
		r.setFontSize(16);
		r.setTextPosition(30); // 设置行间距
		r.setText("车险续期交费服务通知单");

		p = doc.createParagraph(); // 新建段落
		p.setAlignment(ParagraphAlignment.LEFT);
		r = p.createRun();
		r.setFontSize(12);
		r.setText("尊敬的_______先生/女士：");

		p = doc.createParagraph(); // 新建段落
		p.setAlignment(ParagraphAlignment.LEFT);
		p.setIndentationFirstLine(567);// 设置首行缩进，567约为1cm
		r = p.createRun();
		r.setFontSize(12);
		r.setText("您为了家庭幸福在本公司为车牌号______________的爱车投保的保单号码为___________________的车险，"
				+ "车险费为￥_________,现已到第______次交费时间，今有我公司专职保全员前往为您服务，为避"
				+ "免您因事繁忙忘记缴费，造成保单失效带来不必要的损失，请您于____年____月____日至____日" + "持本通知单到任一农业银行储蓄所(代收费窗口)交纳保险费，同时索取交费收据。");

		p = doc.createParagraph(); // 新建段落
		p.setAlignment(ParagraphAlignment.LEFT);
		r = p.createRun();
		r.setFontSize(11);
		r.setText("□咨询服务：如有咨询事项，请与专职展员________联系，以便您提供及时的保全服务。");

		p = doc.createParagraph(); // 新建段落
		p.setAlignment(ParagraphAlignment.LEFT);
		r = p.createRun();
		r.setFontSize(11);
		r.setText("□服务电话：总部：027-66668888\t地址：珞喻路特1号阳光大厦群楼二楼柜面中心");

		p = doc.createParagraph(); // 新建段落
		p.setAlignment(ParagraphAlignment.LEFT);
		p.setIndentationFirstLine(1350);
		r = p.createRun();
		r.setFontSize(11);
		r.setText("区部：________________手机:________________传呼：________________");

		p = doc.createParagraph(); // 新建段落
		p.setAlignment(ParagraphAlignment.LEFT);
		p.setIndentationFirstLine(567);
		r = p.createRun();
		r.setFontSize(12);
		r.setText("感谢您对我们工作的支持与合作。");

		p = doc.createParagraph(); // 新建段落
		p.setAlignment(ParagraphAlignment.RIGHT);
		r = p.createRun();
		r.setFontSize(12);
		r.setText("中国平安车险有限公司");

		p = doc.createParagraph(); // 新建段落
		p.setAlignment(ParagraphAlignment.RIGHT);
		p.setIndentationFirstLine(567);
		r = p.createRun();
		r.setFontSize(12);
		r.setText("年    月    日");

		// FileOutputStream out = new FileOutputStream("D:\\sample.doc");
		doc.write(out);
		out.close();

	}

}
