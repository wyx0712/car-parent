/**
 * 
 */
package utils;

import java.io.IOException;

/**
 * @author 王耀星
 * @date 2018年10月24日
 */
public class DatabaseBackupUtil {
	// 备份远程数据库
	public static void backup(String fileName) {
		// 数据库备份语句
		String str = "mysqldump -h 118.24.68.238 -uroot -proot car >D:/sqlbackup/" + fileName;
		// 创建运行时对象
		Runtime runtime = Runtime.getRuntime();
		// 调用cmd命令
		try {
			Process process = runtime.exec("cmd /C" + str);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 还原远程数据库
	public static void restore(String fileName) {
		// 数据库还原语句
		String str = "mysql -h 118.24.68.238 -uroot -proot car <D:/sqlbackup/" + fileName;
		// 创建运行时对象
		Runtime runtime = Runtime.getRuntime();
		// 调用cmd命令
		try {
			Process process = runtime.exec("cmd /C" + str);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
