package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.Dispatch;
import pojo.DispatchExample;

public interface DispatchMapper {
    int countByExample(DispatchExample example);

    int deleteByExample(DispatchExample example);

    int deleteByPrimaryKey(Integer dispatchId);

    int insert(Dispatch record);

    int insertSelective(Dispatch record);

    List<Dispatch> selectByExample(DispatchExample example);

    Dispatch selectByPrimaryKey(Integer dispatchId);

    int updateByExampleSelective(@Param("record") Dispatch record, @Param("example") DispatchExample example);

    int updateByExample(@Param("record") Dispatch record, @Param("example") DispatchExample example);

    int updateByPrimaryKeySelective(Dispatch record);

    int updateByPrimaryKey(Dispatch record);
}