package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.Recycle;
import pojo.RecycleExample;

public interface RecycleMapper {
    int countByExample(RecycleExample example);

    int deleteByExample(RecycleExample example);

    int deleteByPrimaryKey(Integer recycleId);

    int insert(Recycle record);

     int insertSelective(Recycle record);

    List<Recycle> selectByExample(RecycleExample example);

    Recycle selectByPrimaryKey(Integer recycleId);

    int updateByExampleSelective(@Param("record") Recycle record, @Param("example") RecycleExample example);

    int updateByExample(@Param("record") Recycle record, @Param("example") RecycleExample example);

    int updateByPrimaryKeySelective(Recycle record);

    int updateByPrimaryKey(Recycle record);
}