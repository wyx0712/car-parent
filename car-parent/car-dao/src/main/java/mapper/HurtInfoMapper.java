package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.HurtInfo;
import pojo.HurtInfoExample;

public interface HurtInfoMapper {
    int countByExample(HurtInfoExample example);

    int deleteByExample(HurtInfoExample example);

    int deleteByPrimaryKey(Integer hurtId);

    int insert(HurtInfo record);

    int insertSelective(HurtInfo record);

    List<HurtInfo> selectByExample(HurtInfoExample example);

    HurtInfo selectByPrimaryKey(Integer hurtId);

    int updateByExampleSelective(@Param("record") HurtInfo record, @Param("example") HurtInfoExample example);

    int updateByExample(@Param("record") HurtInfo record, @Param("example") HurtInfoExample example);

    int updateByPrimaryKeySelective(HurtInfo record);

    int updateByPrimaryKey(HurtInfo record);
}