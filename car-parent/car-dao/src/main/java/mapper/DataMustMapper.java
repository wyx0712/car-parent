package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.DataMust;
import pojo.DataMustExample;

public interface DataMustMapper {
    int countByExample(DataMustExample example);

    int deleteByExample(DataMustExample example);

    int deleteByPrimaryKey(Integer reportNo);

    int insert(DataMust record);

    int insertSelective(DataMust record);

    List<DataMust> selectByExample(DataMustExample example);

    DataMust selectByPrimaryKey(Integer reportNo);

    int updateByExampleSelective(@Param("record") DataMust record, @Param("example") DataMustExample example);

    int updateByExample(@Param("record") DataMust record, @Param("example") DataMustExample example);

    int updateByPrimaryKeySelective(DataMust record);

    int updateByPrimaryKey(DataMust record);
}