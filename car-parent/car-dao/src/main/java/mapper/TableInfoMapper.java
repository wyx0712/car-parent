package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.TableInfo;
import pojo.TableInfoExample;

public interface TableInfoMapper {
    int countByExample(TableInfoExample example);

    int deleteByExample(TableInfoExample example);

    int deleteByPrimaryKey(Integer tableId);

    int insert(TableInfo record);

    int insertSelective(TableInfo record);

    List<TableInfo> selectByExample(TableInfoExample example);

    TableInfo selectByPrimaryKey(Integer tableId);

    int updateByExampleSelective(@Param("record") TableInfo record, @Param("example") TableInfoExample example);

    int updateByExample(@Param("record") TableInfo record, @Param("example") TableInfoExample example);

    int updateByPrimaryKeySelective(TableInfo record);

    int updateByPrimaryKey(TableInfo record);
}