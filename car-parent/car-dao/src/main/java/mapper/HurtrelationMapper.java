package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.Hurtrelation;
import pojo.HurtrelationExample;

public interface HurtrelationMapper {
    int countByExample(HurtrelationExample example);

    int deleteByExample(HurtrelationExample example);

    int insert(Hurtrelation record);

    int insertSelective(Hurtrelation record);

    List<Hurtrelation> selectByExample(HurtrelationExample example);

    int updateByExampleSelective(@Param("record") Hurtrelation record, @Param("example") HurtrelationExample example);

    int updateByExample(@Param("record") Hurtrelation record, @Param("example") HurtrelationExample example);
}