package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.Lossinfo;
import pojo.LossinfoExample;

public interface LossinfoMapper {
    int countByExample(LossinfoExample example);

    int deleteByExample(LossinfoExample example);

    int deleteByPrimaryKey(Integer lossId);

    int insert(Lossinfo record);

    int insertSelective(Lossinfo record);

    List<Lossinfo> selectByExample(LossinfoExample example);

    Lossinfo selectByPrimaryKey(Integer lossId);

    int updateByExampleSelective(@Param("record") Lossinfo record, @Param("example") LossinfoExample example);

    int updateByExample(@Param("record") Lossinfo record, @Param("example") LossinfoExample example);

    int updateByPrimaryKeySelective(Lossinfo record);

    int updateByPrimaryKey(Lossinfo record);
}