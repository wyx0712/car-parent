package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.Policy;
import pojo.PolicyExample;

public interface PolicyMapper {
    int countByExample(PolicyExample example);

    int deleteByExample(PolicyExample example);

    int deleteByPrimaryKey(String policyId);

    int insert(Policy record);

    int insertSelective(Policy record);

    List<Policy> selectByExample(PolicyExample example);

    Policy selectByPrimaryKey(String policyId);

    int updateByExampleSelective(@Param("record") Policy record, @Param("example") PolicyExample example);

    int updateByExample(@Param("record") Policy record, @Param("example") PolicyExample example);

    int updateByPrimaryKeySelective(Policy record);

    int updateByPrimaryKey(Policy record);
}