package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.CarMessage;
import pojo.CarMessageExample;

public interface CarMessageMapper {
    int countByExample(CarMessageExample example);

    int deleteByExample(CarMessageExample example);

    int deleteByPrimaryKey(String carNo);

    int insert(CarMessage record);

    int insertSelective(CarMessage record);

    List<CarMessage> selectByExample(CarMessageExample example);

    CarMessage selectByPrimaryKey(String carNo);

    int updateByExampleSelective(@Param("record") CarMessage record, @Param("example") CarMessageExample example);

    int updateByExample(@Param("record") CarMessage record, @Param("example") CarMessageExample example);

    int updateByPrimaryKeySelective(CarMessage record);

    int updateByPrimaryKey(CarMessage record);
}