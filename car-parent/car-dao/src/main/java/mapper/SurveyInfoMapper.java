package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.SurveyInfo;
import pojo.SurveyInfoExample;

public interface SurveyInfoMapper {
    int countByExample(SurveyInfoExample example);

    int deleteByExample(SurveyInfoExample example);

    int deleteByPrimaryKey(Integer surveyId);

    int insert(SurveyInfo record);

    int insertSelective(SurveyInfo record);

    List<SurveyInfo> selectByExample(SurveyInfoExample example);

    SurveyInfo selectByPrimaryKey(Integer surveyId);

    int updateByExampleSelective(@Param("record") SurveyInfo record, @Param("example") SurveyInfoExample example);

    int updateByExample(@Param("record") SurveyInfo record, @Param("example") SurveyInfoExample example);

    int updateByPrimaryKeySelective(SurveyInfo record);

    int updateByPrimaryKey(SurveyInfo record);
}