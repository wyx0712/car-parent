package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.Carnotoparts;
import pojo.CarnotopartsExample;

public interface CarnotopartsMapper {
    int countByExample(CarnotopartsExample example);

    int deleteByExample(CarnotopartsExample example);

    int deleteByPrimaryKey(Integer carnotopartId);

    int insert(Carnotoparts record);

    int insertSelective(Carnotoparts record);

    List<Carnotoparts> selectByExample(CarnotopartsExample example);

    Carnotoparts selectByPrimaryKey(Integer carnotopartId);

    int updateByExampleSelective(@Param("record") Carnotoparts record, @Param("example") CarnotopartsExample example);

    int updateByExample(@Param("record") Carnotoparts record, @Param("example") CarnotopartsExample example);

    int updateByPrimaryKeySelective(Carnotoparts record);

    int updateByPrimaryKey(Carnotoparts record);
}