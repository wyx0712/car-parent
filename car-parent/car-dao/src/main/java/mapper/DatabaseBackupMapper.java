package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.DatabaseBackup;
import pojo.DatabaseBackupExample;

public interface DatabaseBackupMapper {
    int countByExample(DatabaseBackupExample example);

    int deleteByExample(DatabaseBackupExample example);

    int deleteByPrimaryKey(Integer backId);

    int insert(DatabaseBackup record);

    int insertSelective(DatabaseBackup record);

    List<DatabaseBackup> selectByExample(DatabaseBackupExample example);

    DatabaseBackup selectByPrimaryKey(Integer backId);

    int updateByExampleSelective(@Param("record") DatabaseBackup record, @Param("example") DatabaseBackupExample example);

    int updateByExample(@Param("record") DatabaseBackup record, @Param("example") DatabaseBackupExample example);

    int updateByPrimaryKeySelective(DatabaseBackup record);

    int updateByPrimaryKey(DatabaseBackup record);
}