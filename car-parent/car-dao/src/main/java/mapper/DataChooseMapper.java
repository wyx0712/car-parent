package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.DataChoose;
import pojo.DataChooseExample;

public interface DataChooseMapper {
    int countByExample(DataChooseExample example);

    int deleteByExample(DataChooseExample example);

    int deleteByPrimaryKey(Integer reportNo);

    int insert(DataChoose record);

    int insertSelective(DataChoose record);

    List<DataChoose> selectByExample(DataChooseExample example);

    DataChoose selectByPrimaryKey(Integer reportNo);

    int updateByExampleSelective(@Param("record") DataChoose record, @Param("example") DataChooseExample example);

    int updateByExample(@Param("record") DataChoose record, @Param("example") DataChooseExample example);

    int updateByPrimaryKeySelective(DataChoose record);

    int updateByPrimaryKey(DataChoose record);
}