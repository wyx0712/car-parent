package pojo;

import java.util.Date;

public class User {
	private Integer userId; // 用户(公司员工)编号

	private Integer tableId = 1001;// 表编号，默认值1001，不要改动

	private String userName;// 用户登录名

	private String realName;// 用户真实姓名

	private String password;// 登陆密码

	private String phone;// 手机号

	private Integer roleId;// 角色id

	private Integer jobState = 1;// 在职状态(1在职,0离职)

	private Date createTime;// 创建时间

	private Date updateTime;// 最后更新时间

	private Integer opId;// 最后操作人id

	private String back1;// 备用字段1

	private String back2;// 备用字段2

	private String back3;// 备用字段3

	private Role role;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName == null ? null : realName.trim();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password == null ? null : password.trim();
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone == null ? null : phone.trim();
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getJobState() {
		return jobState;
	}

	public void setJobState(Integer jobState) {
		this.jobState = jobState;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getOpId() {
		return opId;
	}

	public void setOpId(Integer opId) {
		this.opId = opId;
	}

	public String getBack1() {
		return back1;
	}

	public void setBack1(String back1) {
		this.back1 = back1 == null ? null : back1.trim();
	}

	public String getBack2() {
		return back2;
	}

	public void setBack2(String back2) {
		this.back2 = back2 == null ? null : back2.trim();
	}

	public String getBack3() {
		return back3;
	}

	public void setBack3(String back3) {
		this.back3 = back3 == null ? null : back3.trim();
	}
}