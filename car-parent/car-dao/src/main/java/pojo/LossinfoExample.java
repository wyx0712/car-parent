package pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LossinfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public LossinfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andLossIdIsNull() {
            addCriterion("loss_id is null");
            return (Criteria) this;
        }

        public Criteria andLossIdIsNotNull() {
            addCriterion("loss_id is not null");
            return (Criteria) this;
        }

        public Criteria andLossIdEqualTo(Integer value) {
            addCriterion("loss_id =", value, "lossId");
            return (Criteria) this;
        }

        public Criteria andLossIdNotEqualTo(Integer value) {
            addCriterion("loss_id <>", value, "lossId");
            return (Criteria) this;
        }

        public Criteria andLossIdGreaterThan(Integer value) {
            addCriterion("loss_id >", value, "lossId");
            return (Criteria) this;
        }

        public Criteria andLossIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("loss_id >=", value, "lossId");
            return (Criteria) this;
        }

        public Criteria andLossIdLessThan(Integer value) {
            addCriterion("loss_id <", value, "lossId");
            return (Criteria) this;
        }

        public Criteria andLossIdLessThanOrEqualTo(Integer value) {
            addCriterion("loss_id <=", value, "lossId");
            return (Criteria) this;
        }

        public Criteria andLossIdIn(List<Integer> values) {
            addCriterion("loss_id in", values, "lossId");
            return (Criteria) this;
        }

        public Criteria andLossIdNotIn(List<Integer> values) {
            addCriterion("loss_id not in", values, "lossId");
            return (Criteria) this;
        }

        public Criteria andLossIdBetween(Integer value1, Integer value2) {
            addCriterion("loss_id between", value1, value2, "lossId");
            return (Criteria) this;
        }

        public Criteria andLossIdNotBetween(Integer value1, Integer value2) {
            addCriterion("loss_id not between", value1, value2, "lossId");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNull() {
            addCriterion("table_id is null");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNotNull() {
            addCriterion("table_id is not null");
            return (Criteria) this;
        }

        public Criteria andTableIdEqualTo(Integer value) {
            addCriterion("table_id =", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotEqualTo(Integer value) {
            addCriterion("table_id <>", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThan(Integer value) {
            addCriterion("table_id >", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("table_id >=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThan(Integer value) {
            addCriterion("table_id <", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThanOrEqualTo(Integer value) {
            addCriterion("table_id <=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdIn(List<Integer> values) {
            addCriterion("table_id in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotIn(List<Integer> values) {
            addCriterion("table_id not in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdBetween(Integer value1, Integer value2) {
            addCriterion("table_id between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotBetween(Integer value1, Integer value2) {
            addCriterion("table_id not between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNull() {
            addCriterion("report_no is null");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNotNull() {
            addCriterion("report_no is not null");
            return (Criteria) this;
        }

        public Criteria andReportNoEqualTo(Integer value) {
            addCriterion("report_no =", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotEqualTo(Integer value) {
            addCriterion("report_no <>", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThan(Integer value) {
            addCriterion("report_no >", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("report_no >=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThan(Integer value) {
            addCriterion("report_no <", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThanOrEqualTo(Integer value) {
            addCriterion("report_no <=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoIn(List<Integer> values) {
            addCriterion("report_no in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotIn(List<Integer> values) {
            addCriterion("report_no not in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoBetween(Integer value1, Integer value2) {
            addCriterion("report_no between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotBetween(Integer value1, Integer value2) {
            addCriterion("report_no not between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andRepairShopIsNull() {
            addCriterion("repair_shop is null");
            return (Criteria) this;
        }

        public Criteria andRepairShopIsNotNull() {
            addCriterion("repair_shop is not null");
            return (Criteria) this;
        }

        public Criteria andRepairShopEqualTo(String value) {
            addCriterion("repair_shop =", value, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairShopNotEqualTo(String value) {
            addCriterion("repair_shop <>", value, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairShopGreaterThan(String value) {
            addCriterion("repair_shop >", value, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairShopGreaterThanOrEqualTo(String value) {
            addCriterion("repair_shop >=", value, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairShopLessThan(String value) {
            addCriterion("repair_shop <", value, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairShopLessThanOrEqualTo(String value) {
            addCriterion("repair_shop <=", value, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairShopLike(String value) {
            addCriterion("repair_shop like", value, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairShopNotLike(String value) {
            addCriterion("repair_shop not like", value, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairShopIn(List<String> values) {
            addCriterion("repair_shop in", values, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairShopNotIn(List<String> values) {
            addCriterion("repair_shop not in", values, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairShopBetween(String value1, String value2) {
            addCriterion("repair_shop between", value1, value2, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairShopNotBetween(String value1, String value2) {
            addCriterion("repair_shop not between", value1, value2, "repairShop");
            return (Criteria) this;
        }

        public Criteria andRepairTypeIsNull() {
            addCriterion("repair_type is null");
            return (Criteria) this;
        }

        public Criteria andRepairTypeIsNotNull() {
            addCriterion("repair_type is not null");
            return (Criteria) this;
        }

        public Criteria andRepairTypeEqualTo(String value) {
            addCriterion("repair_type =", value, "repairType");
            return (Criteria) this;
        }

        public Criteria andRepairTypeNotEqualTo(String value) {
            addCriterion("repair_type <>", value, "repairType");
            return (Criteria) this;
        }

        public Criteria andRepairTypeGreaterThan(String value) {
            addCriterion("repair_type >", value, "repairType");
            return (Criteria) this;
        }

        public Criteria andRepairTypeGreaterThanOrEqualTo(String value) {
            addCriterion("repair_type >=", value, "repairType");
            return (Criteria) this;
        }

        public Criteria andRepairTypeLessThan(String value) {
            addCriterion("repair_type <", value, "repairType");
            return (Criteria) this;
        }

        public Criteria andRepairTypeLessThanOrEqualTo(String value) {
            addCriterion("repair_type <=", value, "repairType");
            return (Criteria) this;
        }

        public Criteria andRepairTypeLike(String value) {
            addCriterion("repair_type like", value, "repairType");
            return (Criteria) this;
        }

        public Criteria andRepairTypeNotLike(String value) {
            addCriterion("repair_type not like", value, "repairType");
            return (Criteria) this;
        }

        public Criteria andRepairTypeIn(List<String> values) {
            addCriterion("repair_type in", values, "repairType");
            return (Criteria) this;
        }

        public Criteria andRepairTypeNotIn(List<String> values) {
            addCriterion("repair_type not in", values, "repairType");
            return (Criteria) this;
        }

        public Criteria andRepairTypeBetween(String value1, String value2) {
            addCriterion("repair_type between", value1, value2, "repairType");
            return (Criteria) this;
        }

        public Criteria andRepairTypeNotBetween(String value1, String value2) {
            addCriterion("repair_type not between", value1, value2, "repairType");
            return (Criteria) this;
        }

        public Criteria andCarOwnerIsNull() {
            addCriterion("car_owner is null");
            return (Criteria) this;
        }

        public Criteria andCarOwnerIsNotNull() {
            addCriterion("car_owner is not null");
            return (Criteria) this;
        }

        public Criteria andCarOwnerEqualTo(String value) {
            addCriterion("car_owner =", value, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarOwnerNotEqualTo(String value) {
            addCriterion("car_owner <>", value, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarOwnerGreaterThan(String value) {
            addCriterion("car_owner >", value, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarOwnerGreaterThanOrEqualTo(String value) {
            addCriterion("car_owner >=", value, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarOwnerLessThan(String value) {
            addCriterion("car_owner <", value, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarOwnerLessThanOrEqualTo(String value) {
            addCriterion("car_owner <=", value, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarOwnerLike(String value) {
            addCriterion("car_owner like", value, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarOwnerNotLike(String value) {
            addCriterion("car_owner not like", value, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarOwnerIn(List<String> values) {
            addCriterion("car_owner in", values, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarOwnerNotIn(List<String> values) {
            addCriterion("car_owner not in", values, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarOwnerBetween(String value1, String value2) {
            addCriterion("car_owner between", value1, value2, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarOwnerNotBetween(String value1, String value2) {
            addCriterion("car_owner not between", value1, value2, "carOwner");
            return (Criteria) this;
        }

        public Criteria andCarNoIsNull() {
            addCriterion("car_no is null");
            return (Criteria) this;
        }

        public Criteria andCarNoIsNotNull() {
            addCriterion("car_no is not null");
            return (Criteria) this;
        }

        public Criteria andCarNoEqualTo(String value) {
            addCriterion("car_no =", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotEqualTo(String value) {
            addCriterion("car_no <>", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoGreaterThan(String value) {
            addCriterion("car_no >", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoGreaterThanOrEqualTo(String value) {
            addCriterion("car_no >=", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLessThan(String value) {
            addCriterion("car_no <", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLessThanOrEqualTo(String value) {
            addCriterion("car_no <=", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLike(String value) {
            addCriterion("car_no like", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotLike(String value) {
            addCriterion("car_no not like", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoIn(List<String> values) {
            addCriterion("car_no in", values, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotIn(List<String> values) {
            addCriterion("car_no not in", values, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoBetween(String value1, String value2) {
            addCriterion("car_no between", value1, value2, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotBetween(String value1, String value2) {
            addCriterion("car_no not between", value1, value2, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarMoneyIsNull() {
            addCriterion("car_money is null");
            return (Criteria) this;
        }

        public Criteria andCarMoneyIsNotNull() {
            addCriterion("car_money is not null");
            return (Criteria) this;
        }

        public Criteria andCarMoneyEqualTo(Double value) {
            addCriterion("car_money =", value, "carMoney");
            return (Criteria) this;
        }

        public Criteria andCarMoneyNotEqualTo(Double value) {
            addCriterion("car_money <>", value, "carMoney");
            return (Criteria) this;
        }

        public Criteria andCarMoneyGreaterThan(Double value) {
            addCriterion("car_money >", value, "carMoney");
            return (Criteria) this;
        }

        public Criteria andCarMoneyGreaterThanOrEqualTo(Double value) {
            addCriterion("car_money >=", value, "carMoney");
            return (Criteria) this;
        }

        public Criteria andCarMoneyLessThan(Double value) {
            addCriterion("car_money <", value, "carMoney");
            return (Criteria) this;
        }

        public Criteria andCarMoneyLessThanOrEqualTo(Double value) {
            addCriterion("car_money <=", value, "carMoney");
            return (Criteria) this;
        }

        public Criteria andCarMoneyIn(List<Double> values) {
            addCriterion("car_money in", values, "carMoney");
            return (Criteria) this;
        }

        public Criteria andCarMoneyNotIn(List<Double> values) {
            addCriterion("car_money not in", values, "carMoney");
            return (Criteria) this;
        }

        public Criteria andCarMoneyBetween(Double value1, Double value2) {
            addCriterion("car_money between", value1, value2, "carMoney");
            return (Criteria) this;
        }

        public Criteria andCarMoneyNotBetween(Double value1, Double value2) {
            addCriterion("car_money not between", value1, value2, "carMoney");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("user_name is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("user_name is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("user_name =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("user_name <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("user_name >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_name >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("user_name <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("user_name <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("user_name like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("user_name not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("user_name in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("user_name not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("user_name between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("user_name not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andLossTimeIsNull() {
            addCriterion("loss_time is null");
            return (Criteria) this;
        }

        public Criteria andLossTimeIsNotNull() {
            addCriterion("loss_time is not null");
            return (Criteria) this;
        }

        public Criteria andLossTimeEqualTo(Date value) {
            addCriterion("loss_time =", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeNotEqualTo(Date value) {
            addCriterion("loss_time <>", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeGreaterThan(Date value) {
            addCriterion("loss_time >", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("loss_time >=", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeLessThan(Date value) {
            addCriterion("loss_time <", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeLessThanOrEqualTo(Date value) {
            addCriterion("loss_time <=", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeIn(List<Date> values) {
            addCriterion("loss_time in", values, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeNotIn(List<Date> values) {
            addCriterion("loss_time not in", values, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeBetween(Date value1, Date value2) {
            addCriterion("loss_time between", value1, value2, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeNotBetween(Date value1, Date value2) {
            addCriterion("loss_time not between", value1, value2, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossOpinionIsNull() {
            addCriterion("loss_opinion is null");
            return (Criteria) this;
        }

        public Criteria andLossOpinionIsNotNull() {
            addCriterion("loss_opinion is not null");
            return (Criteria) this;
        }

        public Criteria andLossOpinionEqualTo(String value) {
            addCriterion("loss_opinion =", value, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andLossOpinionNotEqualTo(String value) {
            addCriterion("loss_opinion <>", value, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andLossOpinionGreaterThan(String value) {
            addCriterion("loss_opinion >", value, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andLossOpinionGreaterThanOrEqualTo(String value) {
            addCriterion("loss_opinion >=", value, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andLossOpinionLessThan(String value) {
            addCriterion("loss_opinion <", value, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andLossOpinionLessThanOrEqualTo(String value) {
            addCriterion("loss_opinion <=", value, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andLossOpinionLike(String value) {
            addCriterion("loss_opinion like", value, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andLossOpinionNotLike(String value) {
            addCriterion("loss_opinion not like", value, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andLossOpinionIn(List<String> values) {
            addCriterion("loss_opinion in", values, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andLossOpinionNotIn(List<String> values) {
            addCriterion("loss_opinion not in", values, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andLossOpinionBetween(String value1, String value2) {
            addCriterion("loss_opinion between", value1, value2, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andLossOpinionNotBetween(String value1, String value2) {
            addCriterion("loss_opinion not between", value1, value2, "lossOpinion");
            return (Criteria) this;
        }

        public Criteria andPartsIdIsNull() {
            addCriterion("parts_id is null");
            return (Criteria) this;
        }

        public Criteria andPartsIdIsNotNull() {
            addCriterion("parts_id is not null");
            return (Criteria) this;
        }

        public Criteria andPartsIdEqualTo(Integer value) {
            addCriterion("parts_id =", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdNotEqualTo(Integer value) {
            addCriterion("parts_id <>", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdGreaterThan(Integer value) {
            addCriterion("parts_id >", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("parts_id >=", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdLessThan(Integer value) {
            addCriterion("parts_id <", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdLessThanOrEqualTo(Integer value) {
            addCriterion("parts_id <=", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdIn(List<Integer> values) {
            addCriterion("parts_id in", values, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdNotIn(List<Integer> values) {
            addCriterion("parts_id not in", values, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdBetween(Integer value1, Integer value2) {
            addCriterion("parts_id between", value1, value2, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdNotBetween(Integer value1, Integer value2) {
            addCriterion("parts_id not between", value1, value2, "partsId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNull() {
            addCriterion("load_id is null");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNotNull() {
            addCriterion("load_id is not null");
            return (Criteria) this;
        }

        public Criteria andLoadIdEqualTo(Integer value) {
            addCriterion("load_id =", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotEqualTo(Integer value) {
            addCriterion("load_id <>", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThan(Integer value) {
            addCriterion("load_id >", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("load_id >=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThan(Integer value) {
            addCriterion("load_id <", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThanOrEqualTo(Integer value) {
            addCriterion("load_id <=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdIn(List<Integer> values) {
            addCriterion("load_id in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotIn(List<Integer> values) {
            addCriterion("load_id not in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdBetween(Integer value1, Integer value2) {
            addCriterion("load_id between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotBetween(Integer value1, Integer value2) {
            addCriterion("load_id not between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andSignIsNull() {
            addCriterion("sign is null");
            return (Criteria) this;
        }

        public Criteria andSignIsNotNull() {
            addCriterion("sign is not null");
            return (Criteria) this;
        }

        public Criteria andSignEqualTo(String value) {
            addCriterion("sign =", value, "sign");
            return (Criteria) this;
        }

        public Criteria andSignNotEqualTo(String value) {
            addCriterion("sign <>", value, "sign");
            return (Criteria) this;
        }

        public Criteria andSignGreaterThan(String value) {
            addCriterion("sign >", value, "sign");
            return (Criteria) this;
        }

        public Criteria andSignGreaterThanOrEqualTo(String value) {
            addCriterion("sign >=", value, "sign");
            return (Criteria) this;
        }

        public Criteria andSignLessThan(String value) {
            addCriterion("sign <", value, "sign");
            return (Criteria) this;
        }

        public Criteria andSignLessThanOrEqualTo(String value) {
            addCriterion("sign <=", value, "sign");
            return (Criteria) this;
        }

        public Criteria andSignLike(String value) {
            addCriterion("sign like", value, "sign");
            return (Criteria) this;
        }

        public Criteria andSignNotLike(String value) {
            addCriterion("sign not like", value, "sign");
            return (Criteria) this;
        }

        public Criteria andSignIn(List<String> values) {
            addCriterion("sign in", values, "sign");
            return (Criteria) this;
        }

        public Criteria andSignNotIn(List<String> values) {
            addCriterion("sign not in", values, "sign");
            return (Criteria) this;
        }

        public Criteria andSignBetween(String value1, String value2) {
            addCriterion("sign between", value1, value2, "sign");
            return (Criteria) this;
        }

        public Criteria andSignNotBetween(String value1, String value2) {
            addCriterion("sign not between", value1, value2, "sign");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaIsNull() {
            addCriterion("return_idea is null");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaIsNotNull() {
            addCriterion("return_idea is not null");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaEqualTo(String value) {
            addCriterion("return_idea =", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaNotEqualTo(String value) {
            addCriterion("return_idea <>", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaGreaterThan(String value) {
            addCriterion("return_idea >", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaGreaterThanOrEqualTo(String value) {
            addCriterion("return_idea >=", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaLessThan(String value) {
            addCriterion("return_idea <", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaLessThanOrEqualTo(String value) {
            addCriterion("return_idea <=", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaLike(String value) {
            addCriterion("return_idea like", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaNotLike(String value) {
            addCriterion("return_idea not like", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaIn(List<String> values) {
            addCriterion("return_idea in", values, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaNotIn(List<String> values) {
            addCriterion("return_idea not in", values, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaBetween(String value1, String value2) {
            addCriterion("return_idea between", value1, value2, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaNotBetween(String value1, String value2) {
            addCriterion("return_idea not between", value1, value2, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andBack1IsNull() {
            addCriterion("back1 is null");
            return (Criteria) this;
        }

        public Criteria andBack1IsNotNull() {
            addCriterion("back1 is not null");
            return (Criteria) this;
        }

        public Criteria andBack1EqualTo(String value) {
            addCriterion("back1 =", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotEqualTo(String value) {
            addCriterion("back1 <>", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThan(String value) {
            addCriterion("back1 >", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThanOrEqualTo(String value) {
            addCriterion("back1 >=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThan(String value) {
            addCriterion("back1 <", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThanOrEqualTo(String value) {
            addCriterion("back1 <=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Like(String value) {
            addCriterion("back1 like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotLike(String value) {
            addCriterion("back1 not like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1In(List<String> values) {
            addCriterion("back1 in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotIn(List<String> values) {
            addCriterion("back1 not in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Between(String value1, String value2) {
            addCriterion("back1 between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotBetween(String value1, String value2) {
            addCriterion("back1 not between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack2IsNull() {
            addCriterion("back2 is null");
            return (Criteria) this;
        }

        public Criteria andBack2IsNotNull() {
            addCriterion("back2 is not null");
            return (Criteria) this;
        }

        public Criteria andBack2EqualTo(String value) {
            addCriterion("back2 =", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotEqualTo(String value) {
            addCriterion("back2 <>", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThan(String value) {
            addCriterion("back2 >", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThanOrEqualTo(String value) {
            addCriterion("back2 >=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThan(String value) {
            addCriterion("back2 <", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThanOrEqualTo(String value) {
            addCriterion("back2 <=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Like(String value) {
            addCriterion("back2 like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotLike(String value) {
            addCriterion("back2 not like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2In(List<String> values) {
            addCriterion("back2 in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotIn(List<String> values) {
            addCriterion("back2 not in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Between(String value1, String value2) {
            addCriterion("back2 between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotBetween(String value1, String value2) {
            addCriterion("back2 not between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack3IsNull() {
            addCriterion("back3 is null");
            return (Criteria) this;
        }

        public Criteria andBack3IsNotNull() {
            addCriterion("back3 is not null");
            return (Criteria) this;
        }

        public Criteria andBack3EqualTo(String value) {
            addCriterion("back3 =", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotEqualTo(String value) {
            addCriterion("back3 <>", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThan(String value) {
            addCriterion("back3 >", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThanOrEqualTo(String value) {
            addCriterion("back3 >=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThan(String value) {
            addCriterion("back3 <", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThanOrEqualTo(String value) {
            addCriterion("back3 <=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Like(String value) {
            addCriterion("back3 like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotLike(String value) {
            addCriterion("back3 not like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3In(List<String> values) {
            addCriterion("back3 in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotIn(List<String> values) {
            addCriterion("back3 not in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Between(String value1, String value2) {
            addCriterion("back3 between", value1, value2, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotBetween(String value1, String value2) {
            addCriterion("back3 not between", value1, value2, "back3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}