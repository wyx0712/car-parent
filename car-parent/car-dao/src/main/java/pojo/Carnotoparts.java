package pojo;

import java.util.Date;

public class Carnotoparts {
    private Integer carnotopartId;

    private Integer tableId;

    private Integer partsId;

    private String carNo;

    private String partsName;

    private Integer partsCount;

    private Double lossFee;

    private Double lossMoney;

    private Double lossPrices;

    private Date createTime;

    private Date updateTime;

    private Integer loadId;

    private String back1;

    private String back2;

    private String back3;

    public Integer getCarnotopartId() {
        return carnotopartId;
    }

    public void setCarnotopartId(Integer carnotopartId) {
        this.carnotopartId = carnotopartId;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public Integer getPartsId() {
        return partsId;
    }

    public void setPartsId(Integer partsId) {
        this.partsId = partsId;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo == null ? null : carNo.trim();
    }

    public String getPartsName() {
        return partsName;
    }

    public void setPartsName(String partsName) {
        this.partsName = partsName == null ? null : partsName.trim();
    }

    public Integer getPartsCount() {
        return partsCount;
    }

    public void setPartsCount(Integer partsCount) {
        this.partsCount = partsCount;
    }

    public Double getLossFee() {
        return lossFee;
    }

    public void setLossFee(Double lossFee) {
        this.lossFee = lossFee;
    }

    public Double getLossMoney() {
        return lossMoney;
    }

    public void setLossMoney(Double lossMoney) {
        this.lossMoney = lossMoney;
    }

    public Double getLossPrices() {
        return lossPrices;
    }

    public void setLossPrices(Double lossPrices) {
        this.lossPrices = lossPrices;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getLoadId() {
        return loadId;
    }

    public void setLoadId(Integer loadId) {
        this.loadId = loadId;
    }

    public String getBack1() {
        return back1;
    }

    public void setBack1(String back1) {
        this.back1 = back1 == null ? null : back1.trim();
    }

    public String getBack2() {
        return back2;
    }

    public void setBack2(String back2) {
        this.back2 = back2 == null ? null : back2.trim();
    }

    public String getBack3() {
        return back3;
    }

    public void setBack3(String back3) {
        this.back3 = back3 == null ? null : back3.trim();
    }
}