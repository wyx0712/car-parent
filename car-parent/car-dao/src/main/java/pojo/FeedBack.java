package pojo;

import java.util.Date;

public class FeedBack {
    private Integer feedBackId;

    private Integer tableId;

    private String customerName;

    private Date reportTime;

    private Integer customerTel;

    private String insuranceName;

    private String contentrs;

    private String customerRequest;

    private String contentrsHumen;

    private String contentrsPepole;

    private String receiver;

    private Date receiverDate;

    private Date createTime;

    private String beiyong2;

    private String beiyong3;

    private Date updateTime;

    private String state;

    private Integer loadId;

    private String beiyong1;

    public Integer getFeedBackId() {
        return feedBackId;
    }

    public void setFeedBackId(Integer feedBackId) {
        this.feedBackId = feedBackId;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName == null ? null : customerName.trim();
    }

    public Date getReportTime() {
        return reportTime;
    }

    public void setReportTime(Date reportTime) {
        this.reportTime = reportTime;
    }

    public Integer getCustomerTel() {
        return customerTel;
    }

    public void setCustomerTel(Integer customerTel) {
        this.customerTel = customerTel;
    }

    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName == null ? null : insuranceName.trim();
    }

    public String getContentrs() {
        return contentrs;
    }

    public void setContentrs(String contentrs) {
        this.contentrs = contentrs == null ? null : contentrs.trim();
    }

    public String getCustomerRequest() {
        return customerRequest;
    }

    public void setCustomerRequest(String customerRequest) {
        this.customerRequest = customerRequest == null ? null : customerRequest.trim();
    }

    public String getContentrsHumen() {
        return contentrsHumen;
    }

    public void setContentrsHumen(String contentrsHumen) {
        this.contentrsHumen = contentrsHumen == null ? null : contentrsHumen.trim();
    }

    public String getContentrsPepole() {
        return contentrsPepole;
    }

    public void setContentrsPepole(String contentrsPepole) {
        this.contentrsPepole = contentrsPepole == null ? null : contentrsPepole.trim();
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver == null ? null : receiver.trim();
    }

    public Date getReceiverDate() {
        return receiverDate;
    }

    public void setReceiverDate(Date receiverDate) {
        this.receiverDate = receiverDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getBeiyong2() {
        return beiyong2;
    }

    public void setBeiyong2(String beiyong2) {
        this.beiyong2 = beiyong2 == null ? null : beiyong2.trim();
    }

    public String getBeiyong3() {
        return beiyong3;
    }

    public void setBeiyong3(String beiyong3) {
        this.beiyong3 = beiyong3 == null ? null : beiyong3.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public Integer getLoadId() {
        return loadId;
    }

    public void setLoadId(Integer loadId) {
        this.loadId = loadId;
    }

    public String getBeiyong1() {
        return beiyong1;
    }

    public void setBeiyong1(String beiyong1) {
        this.beiyong1 = beiyong1 == null ? null : beiyong1.trim();
    }
}