package pojo;

import java.util.Date;

public class HurtInfo {
    private Integer hurtId;

    private Integer tableId;

    private Integer reportNo;

    private String injuredName;

    private String gender;

    private String age;

    private String censusRegister;

    private String phone;

    private String hospital;

    private String doctor;

    private String office;

    private String carNo;

    private Date cureTime;

    private Double lossFee;

    private String userName;

    private Date lossTime;

    private String illnessState;

    private Date createTime;

    private Date updateTime;

    private Integer loadId;

    private String back1;

    private String back2;

    private String back3;

    public Integer getHurtId() {
        return hurtId;
    }

    public void setHurtId(Integer hurtId) {
        this.hurtId = hurtId;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public Integer getReportNo() {
        return reportNo;
    }

    public void setReportNo(Integer reportNo) {
        this.reportNo = reportNo;
    }

    public String getInjuredName() {
        return injuredName;
    }

    public void setInjuredName(String injuredName) {
        this.injuredName = injuredName == null ? null : injuredName.trim();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age == null ? null : age.trim();
    }

    public String getCensusRegister() {
        return censusRegister;
    }

    public void setCensusRegister(String censusRegister) {
        this.censusRegister = censusRegister == null ? null : censusRegister.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital == null ? null : hospital.trim();
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor == null ? null : doctor.trim();
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office == null ? null : office.trim();
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo == null ? null : carNo.trim();
    }

    public Date getCureTime() {
        return cureTime;
    }

    public void setCureTime(Date cureTime) {
        this.cureTime = cureTime;
    }

    public Double getLossFee() {
        return lossFee;
    }

    public void setLossFee(Double lossFee) {
        this.lossFee = lossFee;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Date getLossTime() {
        return lossTime;
    }

    public void setLossTime(Date lossTime) {
        this.lossTime = lossTime;
    }

    public String getIllnessState() {
        return illnessState;
    }

    public void setIllnessState(String illnessState) {
        this.illnessState = illnessState == null ? null : illnessState.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getLoadId() {
        return loadId;
    }

    public void setLoadId(Integer loadId) {
        this.loadId = loadId;
    }

    public String getBack1() {
        return back1;
    }

    public void setBack1(String back1) {
        this.back1 = back1 == null ? null : back1.trim();
    }

    public String getBack2() {
        return back2;
    }

    public void setBack2(String back2) {
        this.back2 = back2 == null ? null : back2.trim();
    }

    public String getBack3() {
        return back3;
    }

    public void setBack3(String back3) {
        this.back3 = back3 == null ? null : back3.trim();
    }
}