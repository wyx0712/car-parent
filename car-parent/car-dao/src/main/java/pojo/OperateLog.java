package pojo;

import java.util.Date;

public class OperateLog {
	private Integer logId;

	private Integer tableId = 1007;

	private String opName;

	private String operation;

	private Date createDate;

	private String content;

	private String back1 = "1"; // 状态，1未回收，0已回收

	private String back2;

	private String back3;

	public Integer getLogId() {
		return logId;
	}

	public void setLogId(Integer logId) {
		this.logId = logId;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public String getOpName() {
		return opName;
	}

	public void setOpName(String opName) {
		this.opName = opName == null ? null : opName.trim();
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation == null ? null : operation.trim();
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content == null ? null : content.trim();
	}

	public String getBack1() {
		return back1;
	}

	public void setBack1(String back1) {
		this.back1 = back1 == null ? null : back1.trim();
	}

	public String getBack2() {
		return back2;
	}

	public void setBack2(String back2) {
		this.back2 = back2 == null ? null : back2.trim();
	}

	public String getBack3() {
		return back3;
	}

	public void setBack3(String back3) {
		this.back3 = back3 == null ? null : back3.trim();
	}
}