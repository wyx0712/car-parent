package pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CarnotopartsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CarnotopartsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCarnotopartIdIsNull() {
            addCriterion("carnotopart_id is null");
            return (Criteria) this;
        }

        public Criteria andCarnotopartIdIsNotNull() {
            addCriterion("carnotopart_id is not null");
            return (Criteria) this;
        }

        public Criteria andCarnotopartIdEqualTo(Integer value) {
            addCriterion("carnotopart_id =", value, "carnotopartId");
            return (Criteria) this;
        }

        public Criteria andCarnotopartIdNotEqualTo(Integer value) {
            addCriterion("carnotopart_id <>", value, "carnotopartId");
            return (Criteria) this;
        }

        public Criteria andCarnotopartIdGreaterThan(Integer value) {
            addCriterion("carnotopart_id >", value, "carnotopartId");
            return (Criteria) this;
        }

        public Criteria andCarnotopartIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("carnotopart_id >=", value, "carnotopartId");
            return (Criteria) this;
        }

        public Criteria andCarnotopartIdLessThan(Integer value) {
            addCriterion("carnotopart_id <", value, "carnotopartId");
            return (Criteria) this;
        }

        public Criteria andCarnotopartIdLessThanOrEqualTo(Integer value) {
            addCriterion("carnotopart_id <=", value, "carnotopartId");
            return (Criteria) this;
        }

        public Criteria andCarnotopartIdIn(List<Integer> values) {
            addCriterion("carnotopart_id in", values, "carnotopartId");
            return (Criteria) this;
        }

        public Criteria andCarnotopartIdNotIn(List<Integer> values) {
            addCriterion("carnotopart_id not in", values, "carnotopartId");
            return (Criteria) this;
        }

        public Criteria andCarnotopartIdBetween(Integer value1, Integer value2) {
            addCriterion("carnotopart_id between", value1, value2, "carnotopartId");
            return (Criteria) this;
        }

        public Criteria andCarnotopartIdNotBetween(Integer value1, Integer value2) {
            addCriterion("carnotopart_id not between", value1, value2, "carnotopartId");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNull() {
            addCriterion("table_id is null");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNotNull() {
            addCriterion("table_id is not null");
            return (Criteria) this;
        }

        public Criteria andTableIdEqualTo(Integer value) {
            addCriterion("table_id =", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotEqualTo(Integer value) {
            addCriterion("table_id <>", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThan(Integer value) {
            addCriterion("table_id >", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("table_id >=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThan(Integer value) {
            addCriterion("table_id <", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThanOrEqualTo(Integer value) {
            addCriterion("table_id <=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdIn(List<Integer> values) {
            addCriterion("table_id in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotIn(List<Integer> values) {
            addCriterion("table_id not in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdBetween(Integer value1, Integer value2) {
            addCriterion("table_id between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotBetween(Integer value1, Integer value2) {
            addCriterion("table_id not between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andPartsIdIsNull() {
            addCriterion("parts_id is null");
            return (Criteria) this;
        }

        public Criteria andPartsIdIsNotNull() {
            addCriterion("parts_id is not null");
            return (Criteria) this;
        }

        public Criteria andPartsIdEqualTo(Integer value) {
            addCriterion("parts_id =", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdNotEqualTo(Integer value) {
            addCriterion("parts_id <>", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdGreaterThan(Integer value) {
            addCriterion("parts_id >", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("parts_id >=", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdLessThan(Integer value) {
            addCriterion("parts_id <", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdLessThanOrEqualTo(Integer value) {
            addCriterion("parts_id <=", value, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdIn(List<Integer> values) {
            addCriterion("parts_id in", values, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdNotIn(List<Integer> values) {
            addCriterion("parts_id not in", values, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdBetween(Integer value1, Integer value2) {
            addCriterion("parts_id between", value1, value2, "partsId");
            return (Criteria) this;
        }

        public Criteria andPartsIdNotBetween(Integer value1, Integer value2) {
            addCriterion("parts_id not between", value1, value2, "partsId");
            return (Criteria) this;
        }

        public Criteria andCarNoIsNull() {
            addCriterion("car_no is null");
            return (Criteria) this;
        }

        public Criteria andCarNoIsNotNull() {
            addCriterion("car_no is not null");
            return (Criteria) this;
        }

        public Criteria andCarNoEqualTo(String value) {
            addCriterion("car_no =", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotEqualTo(String value) {
            addCriterion("car_no <>", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoGreaterThan(String value) {
            addCriterion("car_no >", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoGreaterThanOrEqualTo(String value) {
            addCriterion("car_no >=", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLessThan(String value) {
            addCriterion("car_no <", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLessThanOrEqualTo(String value) {
            addCriterion("car_no <=", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLike(String value) {
            addCriterion("car_no like", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotLike(String value) {
            addCriterion("car_no not like", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoIn(List<String> values) {
            addCriterion("car_no in", values, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotIn(List<String> values) {
            addCriterion("car_no not in", values, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoBetween(String value1, String value2) {
            addCriterion("car_no between", value1, value2, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotBetween(String value1, String value2) {
            addCriterion("car_no not between", value1, value2, "carNo");
            return (Criteria) this;
        }

        public Criteria andPartsNameIsNull() {
            addCriterion("parts_name is null");
            return (Criteria) this;
        }

        public Criteria andPartsNameIsNotNull() {
            addCriterion("parts_name is not null");
            return (Criteria) this;
        }

        public Criteria andPartsNameEqualTo(String value) {
            addCriterion("parts_name =", value, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsNameNotEqualTo(String value) {
            addCriterion("parts_name <>", value, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsNameGreaterThan(String value) {
            addCriterion("parts_name >", value, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsNameGreaterThanOrEqualTo(String value) {
            addCriterion("parts_name >=", value, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsNameLessThan(String value) {
            addCriterion("parts_name <", value, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsNameLessThanOrEqualTo(String value) {
            addCriterion("parts_name <=", value, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsNameLike(String value) {
            addCriterion("parts_name like", value, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsNameNotLike(String value) {
            addCriterion("parts_name not like", value, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsNameIn(List<String> values) {
            addCriterion("parts_name in", values, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsNameNotIn(List<String> values) {
            addCriterion("parts_name not in", values, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsNameBetween(String value1, String value2) {
            addCriterion("parts_name between", value1, value2, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsNameNotBetween(String value1, String value2) {
            addCriterion("parts_name not between", value1, value2, "partsName");
            return (Criteria) this;
        }

        public Criteria andPartsCountIsNull() {
            addCriterion("parts_count is null");
            return (Criteria) this;
        }

        public Criteria andPartsCountIsNotNull() {
            addCriterion("parts_count is not null");
            return (Criteria) this;
        }

        public Criteria andPartsCountEqualTo(Integer value) {
            addCriterion("parts_count =", value, "partsCount");
            return (Criteria) this;
        }

        public Criteria andPartsCountNotEqualTo(Integer value) {
            addCriterion("parts_count <>", value, "partsCount");
            return (Criteria) this;
        }

        public Criteria andPartsCountGreaterThan(Integer value) {
            addCriterion("parts_count >", value, "partsCount");
            return (Criteria) this;
        }

        public Criteria andPartsCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("parts_count >=", value, "partsCount");
            return (Criteria) this;
        }

        public Criteria andPartsCountLessThan(Integer value) {
            addCriterion("parts_count <", value, "partsCount");
            return (Criteria) this;
        }

        public Criteria andPartsCountLessThanOrEqualTo(Integer value) {
            addCriterion("parts_count <=", value, "partsCount");
            return (Criteria) this;
        }

        public Criteria andPartsCountIn(List<Integer> values) {
            addCriterion("parts_count in", values, "partsCount");
            return (Criteria) this;
        }

        public Criteria andPartsCountNotIn(List<Integer> values) {
            addCriterion("parts_count not in", values, "partsCount");
            return (Criteria) this;
        }

        public Criteria andPartsCountBetween(Integer value1, Integer value2) {
            addCriterion("parts_count between", value1, value2, "partsCount");
            return (Criteria) this;
        }

        public Criteria andPartsCountNotBetween(Integer value1, Integer value2) {
            addCriterion("parts_count not between", value1, value2, "partsCount");
            return (Criteria) this;
        }

        public Criteria andLossFeeIsNull() {
            addCriterion("loss_fee is null");
            return (Criteria) this;
        }

        public Criteria andLossFeeIsNotNull() {
            addCriterion("loss_fee is not null");
            return (Criteria) this;
        }

        public Criteria andLossFeeEqualTo(Double value) {
            addCriterion("loss_fee =", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeNotEqualTo(Double value) {
            addCriterion("loss_fee <>", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeGreaterThan(Double value) {
            addCriterion("loss_fee >", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeGreaterThanOrEqualTo(Double value) {
            addCriterion("loss_fee >=", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeLessThan(Double value) {
            addCriterion("loss_fee <", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeLessThanOrEqualTo(Double value) {
            addCriterion("loss_fee <=", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeIn(List<Double> values) {
            addCriterion("loss_fee in", values, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeNotIn(List<Double> values) {
            addCriterion("loss_fee not in", values, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeBetween(Double value1, Double value2) {
            addCriterion("loss_fee between", value1, value2, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeNotBetween(Double value1, Double value2) {
            addCriterion("loss_fee not between", value1, value2, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossMoneyIsNull() {
            addCriterion("loss_money is null");
            return (Criteria) this;
        }

        public Criteria andLossMoneyIsNotNull() {
            addCriterion("loss_money is not null");
            return (Criteria) this;
        }

        public Criteria andLossMoneyEqualTo(Double value) {
            addCriterion("loss_money =", value, "lossMoney");
            return (Criteria) this;
        }

        public Criteria andLossMoneyNotEqualTo(Double value) {
            addCriterion("loss_money <>", value, "lossMoney");
            return (Criteria) this;
        }

        public Criteria andLossMoneyGreaterThan(Double value) {
            addCriterion("loss_money >", value, "lossMoney");
            return (Criteria) this;
        }

        public Criteria andLossMoneyGreaterThanOrEqualTo(Double value) {
            addCriterion("loss_money >=", value, "lossMoney");
            return (Criteria) this;
        }

        public Criteria andLossMoneyLessThan(Double value) {
            addCriterion("loss_money <", value, "lossMoney");
            return (Criteria) this;
        }

        public Criteria andLossMoneyLessThanOrEqualTo(Double value) {
            addCriterion("loss_money <=", value, "lossMoney");
            return (Criteria) this;
        }

        public Criteria andLossMoneyIn(List<Double> values) {
            addCriterion("loss_money in", values, "lossMoney");
            return (Criteria) this;
        }

        public Criteria andLossMoneyNotIn(List<Double> values) {
            addCriterion("loss_money not in", values, "lossMoney");
            return (Criteria) this;
        }

        public Criteria andLossMoneyBetween(Double value1, Double value2) {
            addCriterion("loss_money between", value1, value2, "lossMoney");
            return (Criteria) this;
        }

        public Criteria andLossMoneyNotBetween(Double value1, Double value2) {
            addCriterion("loss_money not between", value1, value2, "lossMoney");
            return (Criteria) this;
        }

        public Criteria andLossPricesIsNull() {
            addCriterion("loss_prices is null");
            return (Criteria) this;
        }

        public Criteria andLossPricesIsNotNull() {
            addCriterion("loss_prices is not null");
            return (Criteria) this;
        }

        public Criteria andLossPricesEqualTo(Double value) {
            addCriterion("loss_prices =", value, "lossPrices");
            return (Criteria) this;
        }

        public Criteria andLossPricesNotEqualTo(Double value) {
            addCriterion("loss_prices <>", value, "lossPrices");
            return (Criteria) this;
        }

        public Criteria andLossPricesGreaterThan(Double value) {
            addCriterion("loss_prices >", value, "lossPrices");
            return (Criteria) this;
        }

        public Criteria andLossPricesGreaterThanOrEqualTo(Double value) {
            addCriterion("loss_prices >=", value, "lossPrices");
            return (Criteria) this;
        }

        public Criteria andLossPricesLessThan(Double value) {
            addCriterion("loss_prices <", value, "lossPrices");
            return (Criteria) this;
        }

        public Criteria andLossPricesLessThanOrEqualTo(Double value) {
            addCriterion("loss_prices <=", value, "lossPrices");
            return (Criteria) this;
        }

        public Criteria andLossPricesIn(List<Double> values) {
            addCriterion("loss_prices in", values, "lossPrices");
            return (Criteria) this;
        }

        public Criteria andLossPricesNotIn(List<Double> values) {
            addCriterion("loss_prices not in", values, "lossPrices");
            return (Criteria) this;
        }

        public Criteria andLossPricesBetween(Double value1, Double value2) {
            addCriterion("loss_prices between", value1, value2, "lossPrices");
            return (Criteria) this;
        }

        public Criteria andLossPricesNotBetween(Double value1, Double value2) {
            addCriterion("loss_prices not between", value1, value2, "lossPrices");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNull() {
            addCriterion("load_id is null");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNotNull() {
            addCriterion("load_id is not null");
            return (Criteria) this;
        }

        public Criteria andLoadIdEqualTo(Integer value) {
            addCriterion("load_id =", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotEqualTo(Integer value) {
            addCriterion("load_id <>", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThan(Integer value) {
            addCriterion("load_id >", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("load_id >=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThan(Integer value) {
            addCriterion("load_id <", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThanOrEqualTo(Integer value) {
            addCriterion("load_id <=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdIn(List<Integer> values) {
            addCriterion("load_id in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotIn(List<Integer> values) {
            addCriterion("load_id not in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdBetween(Integer value1, Integer value2) {
            addCriterion("load_id between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotBetween(Integer value1, Integer value2) {
            addCriterion("load_id not between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andBack1IsNull() {
            addCriterion("back1 is null");
            return (Criteria) this;
        }

        public Criteria andBack1IsNotNull() {
            addCriterion("back1 is not null");
            return (Criteria) this;
        }

        public Criteria andBack1EqualTo(String value) {
            addCriterion("back1 =", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotEqualTo(String value) {
            addCriterion("back1 <>", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThan(String value) {
            addCriterion("back1 >", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThanOrEqualTo(String value) {
            addCriterion("back1 >=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThan(String value) {
            addCriterion("back1 <", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThanOrEqualTo(String value) {
            addCriterion("back1 <=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Like(String value) {
            addCriterion("back1 like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotLike(String value) {
            addCriterion("back1 not like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1In(List<String> values) {
            addCriterion("back1 in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotIn(List<String> values) {
            addCriterion("back1 not in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Between(String value1, String value2) {
            addCriterion("back1 between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotBetween(String value1, String value2) {
            addCriterion("back1 not between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack2IsNull() {
            addCriterion("back2 is null");
            return (Criteria) this;
        }

        public Criteria andBack2IsNotNull() {
            addCriterion("back2 is not null");
            return (Criteria) this;
        }

        public Criteria andBack2EqualTo(String value) {
            addCriterion("back2 =", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotEqualTo(String value) {
            addCriterion("back2 <>", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThan(String value) {
            addCriterion("back2 >", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThanOrEqualTo(String value) {
            addCriterion("back2 >=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThan(String value) {
            addCriterion("back2 <", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThanOrEqualTo(String value) {
            addCriterion("back2 <=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Like(String value) {
            addCriterion("back2 like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotLike(String value) {
            addCriterion("back2 not like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2In(List<String> values) {
            addCriterion("back2 in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotIn(List<String> values) {
            addCriterion("back2 not in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Between(String value1, String value2) {
            addCriterion("back2 between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotBetween(String value1, String value2) {
            addCriterion("back2 not between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack3IsNull() {
            addCriterion("back3 is null");
            return (Criteria) this;
        }

        public Criteria andBack3IsNotNull() {
            addCriterion("back3 is not null");
            return (Criteria) this;
        }

        public Criteria andBack3EqualTo(String value) {
            addCriterion("back3 =", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotEqualTo(String value) {
            addCriterion("back3 <>", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThan(String value) {
            addCriterion("back3 >", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThanOrEqualTo(String value) {
            addCriterion("back3 >=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThan(String value) {
            addCriterion("back3 <", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThanOrEqualTo(String value) {
            addCriterion("back3 <=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Like(String value) {
            addCriterion("back3 like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotLike(String value) {
            addCriterion("back3 not like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3In(List<String> values) {
            addCriterion("back3 in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotIn(List<String> values) {
            addCriterion("back3 not in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Between(String value1, String value2) {
            addCriterion("back3 between", value1, value2, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotBetween(String value1, String value2) {
            addCriterion("back3 not between", value1, value2, "back3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}