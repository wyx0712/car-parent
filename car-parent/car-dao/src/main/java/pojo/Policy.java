package pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Policy {
	private String policyId; // 保单号

	private Integer tableId = 1003;// 表编号,默认1003不要更改

	private String carNo;// 车牌号

	private String applicant;// 投保人

	private String aIdcard;// 投保人身份证号

	private String aPhone;// 投保人手机号

	private String aAddress;// 投保人住址

	private String beneficiary;// 受益人

	private String bIdcard;// 受益人身份证号

	private String bPhone;// 受益人手机号

	private String bAdress;// 受益人住址

	private Double insuredAmount;// 保费

	private Double compensateAmount;// 保额
	
	@DateTimeFormat(pattern="yyyy-MM-dd hh-mm-ss")
	@JsonFormat(pattern="yyyy-MM-dd hh-mm-ss")
	private Date startDate;// 生效时间

	private Date endTime;// 结束时间

	private Integer insuranceId;// 险种

	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间

	private Integer loadId;// 操作人id

	private String back1;// 备用字段1

	private String back2;// 备用字段2

	private String back3;// 备用字段2

	public String getPolicyId() {
		return policyId;
	}

	public void setPolicyId(String policyId) {
		this.policyId = policyId == null ? null : policyId.trim();
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo == null ? null : carNo.trim();
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant == null ? null : applicant.trim();
	}

	public String getaIdcard() {
		return aIdcard;
	}
	
	public void setaIdcard(String aIdcard) {
		this.aIdcard = aIdcard == null ? null : aIdcard.trim();
	}

	public String getaPhone() {
		return aPhone;
	}

	public void setaPhone(String aPhone) {
		this.aPhone = aPhone == null ? null : aPhone.trim();
	}

	public String getaAddress() {
		return aAddress;
	}

	public void setaAddress(String aAddress) {
		this.aAddress = aAddress == null ? null : aAddress.trim();
	}

	public String getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(String beneficiary) {
		this.beneficiary = beneficiary == null ? null : beneficiary.trim();
	}

	public String getbIdcard() {
		return bIdcard;
	}

	public void setbIdcard(String bIdcard) {
		this.bIdcard = bIdcard == null ? null : bIdcard.trim();
	}

	public String getbPhone() {
		return bPhone;
	}

	public void setbPhone(String bPhone) {
		this.bPhone = bPhone == null ? null : bPhone.trim();
	}

	public String getbAdress() {
		return bAdress;
	}

	public void setbAdress(String bAdress) {
		this.bAdress = bAdress == null ? null : bAdress.trim();
	}

	public Double getInsuredAmount() {
		return insuredAmount;
	}

	public void setInsuredAmount(Double insuredAmount) {
		this.insuredAmount = insuredAmount;
	}

	public Double getCompensateAmount() {
		return compensateAmount;
	}

	public void setCompensateAmount(Double compensateAmount) {
		this.compensateAmount = compensateAmount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getInsuranceId() {
		return insuranceId;
	}

	public void setInsuranceId(Integer insuranceId) {
		this.insuranceId = insuranceId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getLoadId() {
		return loadId;
	}

	public void setLoadId(Integer loadId) {
		this.loadId = loadId;
	}

	public String getBack1() {
		return back1;
	}

	public void setBack1(String back1) {
		this.back1 = back1 == null ? null : back1.trim();
	}

	public String getBack2() {
		return back2;
	}

	public void setBack2(String back2) {
		this.back2 = back2 == null ? null : back2.trim();
	}

	public String getBack3() {
		return back3;
	}

	public void setBack3(String back3) {
		this.back3 = back3 == null ? null : back3.trim();
	}
	
	
	
	public String getAIdcard() {
		return aIdcard;
	}
	public String getAPhone() {
		return aPhone;
	}
	public String getAAddress() {
		return aAddress;
	}
	public String getBIdcard() {
		return bIdcard;
	}
	public String getBPhone() {
		return bPhone;
	}
	public String getBAdress() {
		return bAdress;
	}

}