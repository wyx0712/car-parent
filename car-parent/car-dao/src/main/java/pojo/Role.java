package pojo;

public class Role {
	private Integer roleId;

	private Integer tableId = 1002;

	private String roleName;

	private String back1 = "1";

	private String back2;

	private String back3;

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName == null ? null : roleName.trim();
	}

	public String getBack1() {
		return back1;
	}

	public void setBack1(String back1) {
		this.back1 = back1 == null ? null : back1.trim();
	}

	public String getBack2() {
		return back2;
	}

	public void setBack2(String back2) {
		this.back2 = back2 == null ? null : back2.trim();
	}

	public String getBack3() {
		return back3;
	}

	public void setBack3(String back3) {
		this.back3 = back3 == null ? null : back3.trim();
	}
}