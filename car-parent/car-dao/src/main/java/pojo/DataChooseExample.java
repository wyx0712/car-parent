package pojo;

import java.util.ArrayList;
import java.util.List;

public class DataChooseExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DataChooseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andReportNoIsNull() {
            addCriterion("report_no is null");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNotNull() {
            addCriterion("report_no is not null");
            return (Criteria) this;
        }

        public Criteria andReportNoEqualTo(Integer value) {
            addCriterion("report_no =", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotEqualTo(Integer value) {
            addCriterion("report_no <>", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThan(Integer value) {
            addCriterion("report_no >", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("report_no >=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThan(Integer value) {
            addCriterion("report_no <", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThanOrEqualTo(Integer value) {
            addCriterion("report_no <=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoIn(List<Integer> values) {
            addCriterion("report_no in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotIn(List<Integer> values) {
            addCriterion("report_no not in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoBetween(Integer value1, Integer value2) {
            addCriterion("report_no between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotBetween(Integer value1, Integer value2) {
            addCriterion("report_no not between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNull() {
            addCriterion("table_id is null");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNotNull() {
            addCriterion("table_id is not null");
            return (Criteria) this;
        }

        public Criteria andTableIdEqualTo(Integer value) {
            addCriterion("table_id =", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotEqualTo(Integer value) {
            addCriterion("table_id <>", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThan(Integer value) {
            addCriterion("table_id >", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("table_id >=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThan(Integer value) {
            addCriterion("table_id <", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThanOrEqualTo(Integer value) {
            addCriterion("table_id <=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdIn(List<Integer> values) {
            addCriterion("table_id in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotIn(List<Integer> values) {
            addCriterion("table_id not in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdBetween(Integer value1, Integer value2) {
            addCriterion("table_id between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotBetween(Integer value1, Integer value2) {
            addCriterion("table_id not between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andReparationMediationIsNull() {
            addCriterion("reparation_mediation is null");
            return (Criteria) this;
        }

        public Criteria andReparationMediationIsNotNull() {
            addCriterion("reparation_mediation is not null");
            return (Criteria) this;
        }

        public Criteria andReparationMediationEqualTo(String value) {
            addCriterion("reparation_mediation =", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationNotEqualTo(String value) {
            addCriterion("reparation_mediation <>", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationGreaterThan(String value) {
            addCriterion("reparation_mediation >", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationGreaterThanOrEqualTo(String value) {
            addCriterion("reparation_mediation >=", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationLessThan(String value) {
            addCriterion("reparation_mediation <", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationLessThanOrEqualTo(String value) {
            addCriterion("reparation_mediation <=", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationLike(String value) {
            addCriterion("reparation_mediation like", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationNotLike(String value) {
            addCriterion("reparation_mediation not like", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationIn(List<String> values) {
            addCriterion("reparation_mediation in", values, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationNotIn(List<String> values) {
            addCriterion("reparation_mediation not in", values, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationBetween(String value1, String value2) {
            addCriterion("reparation_mediation between", value1, value2, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationNotBetween(String value1, String value2) {
            addCriterion("reparation_mediation not between", value1, value2, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceIsNull() {
            addCriterion("repair_invoice is null");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceIsNotNull() {
            addCriterion("repair_invoice is not null");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceEqualTo(String value) {
            addCriterion("repair_invoice =", value, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceNotEqualTo(String value) {
            addCriterion("repair_invoice <>", value, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceGreaterThan(String value) {
            addCriterion("repair_invoice >", value, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceGreaterThanOrEqualTo(String value) {
            addCriterion("repair_invoice >=", value, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceLessThan(String value) {
            addCriterion("repair_invoice <", value, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceLessThanOrEqualTo(String value) {
            addCriterion("repair_invoice <=", value, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceLike(String value) {
            addCriterion("repair_invoice like", value, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceNotLike(String value) {
            addCriterion("repair_invoice not like", value, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceIn(List<String> values) {
            addCriterion("repair_invoice in", values, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceNotIn(List<String> values) {
            addCriterion("repair_invoice not in", values, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceBetween(String value1, String value2) {
            addCriterion("repair_invoice between", value1, value2, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andRepairInvoiceNotBetween(String value1, String value2) {
            addCriterion("repair_invoice not between", value1, value2, "repairInvoice");
            return (Criteria) this;
        }

        public Criteria andDriverVipIsNull() {
            addCriterion("driver_vip is null");
            return (Criteria) this;
        }

        public Criteria andDriverVipIsNotNull() {
            addCriterion("driver_vip is not null");
            return (Criteria) this;
        }

        public Criteria andDriverVipEqualTo(String value) {
            addCriterion("driver_vip =", value, "driverVip");
            return (Criteria) this;
        }

        public Criteria andDriverVipNotEqualTo(String value) {
            addCriterion("driver_vip <>", value, "driverVip");
            return (Criteria) this;
        }

        public Criteria andDriverVipGreaterThan(String value) {
            addCriterion("driver_vip >", value, "driverVip");
            return (Criteria) this;
        }

        public Criteria andDriverVipGreaterThanOrEqualTo(String value) {
            addCriterion("driver_vip >=", value, "driverVip");
            return (Criteria) this;
        }

        public Criteria andDriverVipLessThan(String value) {
            addCriterion("driver_vip <", value, "driverVip");
            return (Criteria) this;
        }

        public Criteria andDriverVipLessThanOrEqualTo(String value) {
            addCriterion("driver_vip <=", value, "driverVip");
            return (Criteria) this;
        }

        public Criteria andDriverVipLike(String value) {
            addCriterion("driver_vip like", value, "driverVip");
            return (Criteria) this;
        }

        public Criteria andDriverVipNotLike(String value) {
            addCriterion("driver_vip not like", value, "driverVip");
            return (Criteria) this;
        }

        public Criteria andDriverVipIn(List<String> values) {
            addCriterion("driver_vip in", values, "driverVip");
            return (Criteria) this;
        }

        public Criteria andDriverVipNotIn(List<String> values) {
            addCriterion("driver_vip not in", values, "driverVip");
            return (Criteria) this;
        }

        public Criteria andDriverVipBetween(String value1, String value2) {
            addCriterion("driver_vip between", value1, value2, "driverVip");
            return (Criteria) this;
        }

        public Criteria andDriverVipNotBetween(String value1, String value2) {
            addCriterion("driver_vip not between", value1, value2, "driverVip");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesIsNull() {
            addCriterion("medical_prices is null");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesIsNotNull() {
            addCriterion("medical_prices is not null");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesEqualTo(String value) {
            addCriterion("medical_prices =", value, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesNotEqualTo(String value) {
            addCriterion("medical_prices <>", value, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesGreaterThan(String value) {
            addCriterion("medical_prices >", value, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesGreaterThanOrEqualTo(String value) {
            addCriterion("medical_prices >=", value, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesLessThan(String value) {
            addCriterion("medical_prices <", value, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesLessThanOrEqualTo(String value) {
            addCriterion("medical_prices <=", value, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesLike(String value) {
            addCriterion("medical_prices like", value, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesNotLike(String value) {
            addCriterion("medical_prices not like", value, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesIn(List<String> values) {
            addCriterion("medical_prices in", values, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesNotIn(List<String> values) {
            addCriterion("medical_prices not in", values, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesBetween(String value1, String value2) {
            addCriterion("medical_prices between", value1, value2, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andMedicalPricesNotBetween(String value1, String value2) {
            addCriterion("medical_prices not between", value1, value2, "medicalPrices");
            return (Criteria) this;
        }

        public Criteria andDieCertificateIsNull() {
            addCriterion("die_certificate is null");
            return (Criteria) this;
        }

        public Criteria andDieCertificateIsNotNull() {
            addCriterion("die_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andDieCertificateEqualTo(String value) {
            addCriterion("die_certificate =", value, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDieCertificateNotEqualTo(String value) {
            addCriterion("die_certificate <>", value, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDieCertificateGreaterThan(String value) {
            addCriterion("die_certificate >", value, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDieCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("die_certificate >=", value, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDieCertificateLessThan(String value) {
            addCriterion("die_certificate <", value, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDieCertificateLessThanOrEqualTo(String value) {
            addCriterion("die_certificate <=", value, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDieCertificateLike(String value) {
            addCriterion("die_certificate like", value, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDieCertificateNotLike(String value) {
            addCriterion("die_certificate not like", value, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDieCertificateIn(List<String> values) {
            addCriterion("die_certificate in", values, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDieCertificateNotIn(List<String> values) {
            addCriterion("die_certificate not in", values, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDieCertificateBetween(String value1, String value2) {
            addCriterion("die_certificate between", value1, value2, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDieCertificateNotBetween(String value1, String value2) {
            addCriterion("die_certificate not between", value1, value2, "dieCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateIsNull() {
            addCriterion("disability_certificate is null");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateIsNotNull() {
            addCriterion("disability_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateEqualTo(String value) {
            addCriterion("disability_certificate =", value, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateNotEqualTo(String value) {
            addCriterion("disability_certificate <>", value, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateGreaterThan(String value) {
            addCriterion("disability_certificate >", value, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("disability_certificate >=", value, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateLessThan(String value) {
            addCriterion("disability_certificate <", value, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateLessThanOrEqualTo(String value) {
            addCriterion("disability_certificate <=", value, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateLike(String value) {
            addCriterion("disability_certificate like", value, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateNotLike(String value) {
            addCriterion("disability_certificate not like", value, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateIn(List<String> values) {
            addCriterion("disability_certificate in", values, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateNotIn(List<String> values) {
            addCriterion("disability_certificate not in", values, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateBetween(String value1, String value2) {
            addCriterion("disability_certificate between", value1, value2, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andDisabilityCertificateNotBetween(String value1, String value2) {
            addCriterion("disability_certificate not between", value1, value2, "disabilityCertificate");
            return (Criteria) this;
        }

        public Criteria andYanglufeiIsNull() {
            addCriterion("yanglufei is null");
            return (Criteria) this;
        }

        public Criteria andYanglufeiIsNotNull() {
            addCriterion("yanglufei is not null");
            return (Criteria) this;
        }

        public Criteria andYanglufeiEqualTo(String value) {
            addCriterion("yanglufei =", value, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andYanglufeiNotEqualTo(String value) {
            addCriterion("yanglufei <>", value, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andYanglufeiGreaterThan(String value) {
            addCriterion("yanglufei >", value, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andYanglufeiGreaterThanOrEqualTo(String value) {
            addCriterion("yanglufei >=", value, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andYanglufeiLessThan(String value) {
            addCriterion("yanglufei <", value, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andYanglufeiLessThanOrEqualTo(String value) {
            addCriterion("yanglufei <=", value, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andYanglufeiLike(String value) {
            addCriterion("yanglufei like", value, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andYanglufeiNotLike(String value) {
            addCriterion("yanglufei not like", value, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andYanglufeiIn(List<String> values) {
            addCriterion("yanglufei in", values, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andYanglufeiNotIn(List<String> values) {
            addCriterion("yanglufei not in", values, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andYanglufeiBetween(String value1, String value2) {
            addCriterion("yanglufei between", value1, value2, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andYanglufeiNotBetween(String value1, String value2) {
            addCriterion("yanglufei not between", value1, value2, "yanglufei");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateIsNull() {
            addCriterion("touqiekanpo_certificate is null");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateIsNotNull() {
            addCriterion("touqiekanpo_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateEqualTo(String value) {
            addCriterion("touqiekanpo_certificate =", value, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateNotEqualTo(String value) {
            addCriterion("touqiekanpo_certificate <>", value, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateGreaterThan(String value) {
            addCriterion("touqiekanpo_certificate >", value, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("touqiekanpo_certificate >=", value, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateLessThan(String value) {
            addCriterion("touqiekanpo_certificate <", value, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateLessThanOrEqualTo(String value) {
            addCriterion("touqiekanpo_certificate <=", value, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateLike(String value) {
            addCriterion("touqiekanpo_certificate like", value, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateNotLike(String value) {
            addCriterion("touqiekanpo_certificate not like", value, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateIn(List<String> values) {
            addCriterion("touqiekanpo_certificate in", values, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateNotIn(List<String> values) {
            addCriterion("touqiekanpo_certificate not in", values, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateBetween(String value1, String value2) {
            addCriterion("touqiekanpo_certificate between", value1, value2, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiekanpoCertificateNotBetween(String value1, String value2) {
            addCriterion("touqiekanpo_certificate not between", value1, value2, "touqiekanpoCertificate");
            return (Criteria) this;
        }

        public Criteria andCarKeyIsNull() {
            addCriterion("car_key is null");
            return (Criteria) this;
        }

        public Criteria andCarKeyIsNotNull() {
            addCriterion("car_key is not null");
            return (Criteria) this;
        }

        public Criteria andCarKeyEqualTo(String value) {
            addCriterion("car_key =", value, "carKey");
            return (Criteria) this;
        }

        public Criteria andCarKeyNotEqualTo(String value) {
            addCriterion("car_key <>", value, "carKey");
            return (Criteria) this;
        }

        public Criteria andCarKeyGreaterThan(String value) {
            addCriterion("car_key >", value, "carKey");
            return (Criteria) this;
        }

        public Criteria andCarKeyGreaterThanOrEqualTo(String value) {
            addCriterion("car_key >=", value, "carKey");
            return (Criteria) this;
        }

        public Criteria andCarKeyLessThan(String value) {
            addCriterion("car_key <", value, "carKey");
            return (Criteria) this;
        }

        public Criteria andCarKeyLessThanOrEqualTo(String value) {
            addCriterion("car_key <=", value, "carKey");
            return (Criteria) this;
        }

        public Criteria andCarKeyLike(String value) {
            addCriterion("car_key like", value, "carKey");
            return (Criteria) this;
        }

        public Criteria andCarKeyNotLike(String value) {
            addCriterion("car_key not like", value, "carKey");
            return (Criteria) this;
        }

        public Criteria andCarKeyIn(List<String> values) {
            addCriterion("car_key in", values, "carKey");
            return (Criteria) this;
        }

        public Criteria andCarKeyNotIn(List<String> values) {
            addCriterion("car_key not in", values, "carKey");
            return (Criteria) this;
        }

        public Criteria andCarKeyBetween(String value1, String value2) {
            addCriterion("car_key between", value1, value2, "carKey");
            return (Criteria) this;
        }

        public Criteria andCarKeyNotBetween(String value1, String value2) {
            addCriterion("car_key not between", value1, value2, "carKey");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateIsNull() {
            addCriterion("loss_vehicles_certificate is null");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateIsNotNull() {
            addCriterion("loss_vehicles_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateEqualTo(String value) {
            addCriterion("loss_vehicles_certificate =", value, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateNotEqualTo(String value) {
            addCriterion("loss_vehicles_certificate <>", value, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateGreaterThan(String value) {
            addCriterion("loss_vehicles_certificate >", value, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("loss_vehicles_certificate >=", value, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateLessThan(String value) {
            addCriterion("loss_vehicles_certificate <", value, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateLessThanOrEqualTo(String value) {
            addCriterion("loss_vehicles_certificate <=", value, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateLike(String value) {
            addCriterion("loss_vehicles_certificate like", value, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateNotLike(String value) {
            addCriterion("loss_vehicles_certificate not like", value, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateIn(List<String> values) {
            addCriterion("loss_vehicles_certificate in", values, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateNotIn(List<String> values) {
            addCriterion("loss_vehicles_certificate not in", values, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateBetween(String value1, String value2) {
            addCriterion("loss_vehicles_certificate between", value1, value2, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andLossVehiclesCertificateNotBetween(String value1, String value2) {
            addCriterion("loss_vehicles_certificate not between", value1, value2, "lossVehiclesCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateIsNull() {
            addCriterion("touqiefd_certificate is null");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateIsNotNull() {
            addCriterion("touqiefd_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateEqualTo(String value) {
            addCriterion("touqiefd_certificate =", value, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateNotEqualTo(String value) {
            addCriterion("touqiefd_certificate <>", value, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateGreaterThan(String value) {
            addCriterion("touqiefd_certificate >", value, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("touqiefd_certificate >=", value, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateLessThan(String value) {
            addCriterion("touqiefd_certificate <", value, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateLessThanOrEqualTo(String value) {
            addCriterion("touqiefd_certificate <=", value, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateLike(String value) {
            addCriterion("touqiefd_certificate like", value, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateNotLike(String value) {
            addCriterion("touqiefd_certificate not like", value, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateIn(List<String> values) {
            addCriterion("touqiefd_certificate in", values, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateNotIn(List<String> values) {
            addCriterion("touqiefd_certificate not in", values, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateBetween(String value1, String value2) {
            addCriterion("touqiefd_certificate between", value1, value2, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andTouqiefdCertificateNotBetween(String value1, String value2) {
            addCriterion("touqiefd_certificate not between", value1, value2, "touqiefdCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateIsNull() {
            addCriterion("wugong_certificate is null");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateIsNotNull() {
            addCriterion("wugong_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateEqualTo(String value) {
            addCriterion("wugong_certificate =", value, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateNotEqualTo(String value) {
            addCriterion("wugong_certificate <>", value, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateGreaterThan(String value) {
            addCriterion("wugong_certificate >", value, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("wugong_certificate >=", value, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateLessThan(String value) {
            addCriterion("wugong_certificate <", value, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateLessThanOrEqualTo(String value) {
            addCriterion("wugong_certificate <=", value, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateLike(String value) {
            addCriterion("wugong_certificate like", value, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateNotLike(String value) {
            addCriterion("wugong_certificate not like", value, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateIn(List<String> values) {
            addCriterion("wugong_certificate in", values, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateNotIn(List<String> values) {
            addCriterion("wugong_certificate not in", values, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateBetween(String value1, String value2) {
            addCriterion("wugong_certificate between", value1, value2, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andWugongCertificateNotBetween(String value1, String value2) {
            addCriterion("wugong_certificate not between", value1, value2, "wugongCertificate");
            return (Criteria) this;
        }

        public Criteria andAssignmentIsNull() {
            addCriterion("assignment is null");
            return (Criteria) this;
        }

        public Criteria andAssignmentIsNotNull() {
            addCriterion("assignment is not null");
            return (Criteria) this;
        }

        public Criteria andAssignmentEqualTo(String value) {
            addCriterion("assignment =", value, "assignment");
            return (Criteria) this;
        }

        public Criteria andAssignmentNotEqualTo(String value) {
            addCriterion("assignment <>", value, "assignment");
            return (Criteria) this;
        }

        public Criteria andAssignmentGreaterThan(String value) {
            addCriterion("assignment >", value, "assignment");
            return (Criteria) this;
        }

        public Criteria andAssignmentGreaterThanOrEqualTo(String value) {
            addCriterion("assignment >=", value, "assignment");
            return (Criteria) this;
        }

        public Criteria andAssignmentLessThan(String value) {
            addCriterion("assignment <", value, "assignment");
            return (Criteria) this;
        }

        public Criteria andAssignmentLessThanOrEqualTo(String value) {
            addCriterion("assignment <=", value, "assignment");
            return (Criteria) this;
        }

        public Criteria andAssignmentLike(String value) {
            addCriterion("assignment like", value, "assignment");
            return (Criteria) this;
        }

        public Criteria andAssignmentNotLike(String value) {
            addCriterion("assignment not like", value, "assignment");
            return (Criteria) this;
        }

        public Criteria andAssignmentIn(List<String> values) {
            addCriterion("assignment in", values, "assignment");
            return (Criteria) this;
        }

        public Criteria andAssignmentNotIn(List<String> values) {
            addCriterion("assignment not in", values, "assignment");
            return (Criteria) this;
        }

        public Criteria andAssignmentBetween(String value1, String value2) {
            addCriterion("assignment between", value1, value2, "assignment");
            return (Criteria) this;
        }

        public Criteria andAssignmentNotBetween(String value1, String value2) {
            addCriterion("assignment not between", value1, value2, "assignment");
            return (Criteria) this;
        }

        public Criteria andFireCertificateIsNull() {
            addCriterion("fire_certificate is null");
            return (Criteria) this;
        }

        public Criteria andFireCertificateIsNotNull() {
            addCriterion("fire_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andFireCertificateEqualTo(String value) {
            addCriterion("fire_certificate =", value, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andFireCertificateNotEqualTo(String value) {
            addCriterion("fire_certificate <>", value, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andFireCertificateGreaterThan(String value) {
            addCriterion("fire_certificate >", value, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andFireCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("fire_certificate >=", value, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andFireCertificateLessThan(String value) {
            addCriterion("fire_certificate <", value, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andFireCertificateLessThanOrEqualTo(String value) {
            addCriterion("fire_certificate <=", value, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andFireCertificateLike(String value) {
            addCriterion("fire_certificate like", value, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andFireCertificateNotLike(String value) {
            addCriterion("fire_certificate not like", value, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andFireCertificateIn(List<String> values) {
            addCriterion("fire_certificate in", values, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andFireCertificateNotIn(List<String> values) {
            addCriterion("fire_certificate not in", values, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andFireCertificateBetween(String value1, String value2) {
            addCriterion("fire_certificate between", value1, value2, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andFireCertificateNotBetween(String value1, String value2) {
            addCriterion("fire_certificate not between", value1, value2, "fireCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateIsNull() {
            addCriterion("rainstorm_certificate is null");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateIsNotNull() {
            addCriterion("rainstorm_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateEqualTo(String value) {
            addCriterion("rainstorm_certificate =", value, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateNotEqualTo(String value) {
            addCriterion("rainstorm_certificate <>", value, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateGreaterThan(String value) {
            addCriterion("rainstorm_certificate >", value, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("rainstorm_certificate >=", value, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateLessThan(String value) {
            addCriterion("rainstorm_certificate <", value, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateLessThanOrEqualTo(String value) {
            addCriterion("rainstorm_certificate <=", value, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateLike(String value) {
            addCriterion("rainstorm_certificate like", value, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateNotLike(String value) {
            addCriterion("rainstorm_certificate not like", value, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateIn(List<String> values) {
            addCriterion("rainstorm_certificate in", values, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateNotIn(List<String> values) {
            addCriterion("rainstorm_certificate not in", values, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateBetween(String value1, String value2) {
            addCriterion("rainstorm_certificate between", value1, value2, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andRainstormCertificateNotBetween(String value1, String value2) {
            addCriterion("rainstorm_certificate not between", value1, value2, "rainstormCertificate");
            return (Criteria) this;
        }

        public Criteria andBack1IsNull() {
            addCriterion("back1 is null");
            return (Criteria) this;
        }

        public Criteria andBack1IsNotNull() {
            addCriterion("back1 is not null");
            return (Criteria) this;
        }

        public Criteria andBack1EqualTo(String value) {
            addCriterion("back1 =", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotEqualTo(String value) {
            addCriterion("back1 <>", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThan(String value) {
            addCriterion("back1 >", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThanOrEqualTo(String value) {
            addCriterion("back1 >=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThan(String value) {
            addCriterion("back1 <", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThanOrEqualTo(String value) {
            addCriterion("back1 <=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Like(String value) {
            addCriterion("back1 like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotLike(String value) {
            addCriterion("back1 not like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1In(List<String> values) {
            addCriterion("back1 in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotIn(List<String> values) {
            addCriterion("back1 not in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Between(String value1, String value2) {
            addCriterion("back1 between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotBetween(String value1, String value2) {
            addCriterion("back1 not between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack2IsNull() {
            addCriterion("back2 is null");
            return (Criteria) this;
        }

        public Criteria andBack2IsNotNull() {
            addCriterion("back2 is not null");
            return (Criteria) this;
        }

        public Criteria andBack2EqualTo(String value) {
            addCriterion("back2 =", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotEqualTo(String value) {
            addCriterion("back2 <>", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThan(String value) {
            addCriterion("back2 >", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThanOrEqualTo(String value) {
            addCriterion("back2 >=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThan(String value) {
            addCriterion("back2 <", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThanOrEqualTo(String value) {
            addCriterion("back2 <=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Like(String value) {
            addCriterion("back2 like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotLike(String value) {
            addCriterion("back2 not like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2In(List<String> values) {
            addCriterion("back2 in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotIn(List<String> values) {
            addCriterion("back2 not in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Between(String value1, String value2) {
            addCriterion("back2 between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotBetween(String value1, String value2) {
            addCriterion("back2 not between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack3IsNull() {
            addCriterion("back3 is null");
            return (Criteria) this;
        }

        public Criteria andBack3IsNotNull() {
            addCriterion("back3 is not null");
            return (Criteria) this;
        }

        public Criteria andBack3EqualTo(String value) {
            addCriterion("back3 =", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotEqualTo(String value) {
            addCriterion("back3 <>", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThan(String value) {
            addCriterion("back3 >", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThanOrEqualTo(String value) {
            addCriterion("back3 >=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThan(String value) {
            addCriterion("back3 <", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThanOrEqualTo(String value) {
            addCriterion("back3 <=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Like(String value) {
            addCriterion("back3 like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotLike(String value) {
            addCriterion("back3 not like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3In(List<String> values) {
            addCriterion("back3 in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotIn(List<String> values) {
            addCriterion("back3 not in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Between(String value1, String value2) {
            addCriterion("back3 between", value1, value2, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotBetween(String value1, String value2) {
            addCriterion("back3 not between", value1, value2, "back3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}