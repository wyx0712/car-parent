/**
 * 
 */
package pojo.VO.lll;

import java.util.Date;

/**
 * @author 亮亮
 * 2018年10月28日
 * 
 */
public class SurveyInfoVO {

	    private Integer surveyId;

	    private Integer tableId;

	    private Integer reportNo;

	    private String reportName;

	    private String driverName;

	    private String carType;

	    private String driverTel;

	    private String surveyAddress;

	    private String dangerReason;

	    private String dangerAddress;

	    private String dangerArea;

	    private String roadInfo;

	    private String dangerThrough;

	    private String surveyOpinion;

	    private String imgOne;

	    private String imgTwo;

	    private String imgThree;

	    private String surveyName;

	    private String companyName;

	    private Date surveyTime;

	    private Date createTime;

	    private String returnIdea;

	    private Integer loadId;

		public SurveyInfoVO() {
			super();
			// TODO Auto-generated constructor stub
		}

		public Integer getSurveyId() {
			return surveyId;
		}

		public void setSurveyId(Integer surveyId) {
			this.surveyId = surveyId;
		}

		public Integer getTableId() {
			return tableId;
		}

		public void setTableId(Integer tableId) {
			this.tableId = tableId;
		}

		public Integer getReportNo() {
			return reportNo;
		}

		public void setReportNo(Integer reportNo) {
			this.reportNo = reportNo;
		}

		public String getReportName() {
			return reportName;
		}

		public void setReportName(String reportName) {
			this.reportName = reportName;
		}

		public String getDriverName() {
			return driverName;
		}

		public void setDriverName(String driverName) {
			this.driverName = driverName;
		}

		public String getCarType() {
			return carType;
		}

		public void setCarType(String carType) {
			this.carType = carType;
		}

		public String getDriverTel() {
			return driverTel;
		}

		public void setDriverTel(String driverTel) {
			this.driverTel = driverTel;
		}

		public String getSurveyAddress() {
			return surveyAddress;
		}

		public void setSurveyAddress(String surveyAddress) {
			this.surveyAddress = surveyAddress;
		}

		public String getDangerReason() {
			return dangerReason;
		}

		public void setDangerReason(String dangerReason) {
			this.dangerReason = dangerReason;
		}

		public String getDangerAddress() {
			return dangerAddress;
		}

		public void setDangerAddress(String dangerAddress) {
			this.dangerAddress = dangerAddress;
		}

		public String getDangerArea() {
			return dangerArea;
		}

		public void setDangerArea(String dangerArea) {
			this.dangerArea = dangerArea;
		}

		public String getRoadInfo() {
			return roadInfo;
		}

		public void setRoadInfo(String roadInfo) {
			this.roadInfo = roadInfo;
		}

		public String getDangerThrough() {
			return dangerThrough;
		}

		public void setDangerThrough(String dangerThrough) {
			this.dangerThrough = dangerThrough;
		}

		public String getSurveyOpinion() {
			return surveyOpinion;
		}

		public void setSurveyOpinion(String surveyOpinion) {
			this.surveyOpinion = surveyOpinion;
		}

		public String getImgOne() {
			return imgOne;
		}

		public void setImgOne(String imgOne) {
			this.imgOne = imgOne;
		}

		public String getImgTwo() {
			return imgTwo;
		}

		public void setImgTwo(String imgTwo) {
			this.imgTwo = imgTwo;
		}

		public String getImgThree() {
			return imgThree;
		}

		public void setImgThree(String imgThree) {
			this.imgThree = imgThree;
		}

		public String getSurveyName() {
			return surveyName;
		}

		public void setSurveyName(String surveyName) {
			this.surveyName = surveyName;
		}

		public String getCompanyName() {
			return companyName;
		}

		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}

		public Date getSurveyTime() {
			return surveyTime;
		}

		public void setSurveyTime(Date surveyTime) {
			this.surveyTime = surveyTime;
		}

		public Date getCreateTime() {
			return createTime;
		}

		public void setCreateTime(Date createTime) {
			this.createTime = createTime;
		}

		public String getReturnIdea() {
			return returnIdea;
		}

		public void setReturnIdea(String returnIdea) {
			this.returnIdea = returnIdea;
		}

		public Integer getLoadId() {
			return loadId;
		}

		public void setLoadId(Integer loadId) {
			this.loadId = loadId;
		}
	
	
}
