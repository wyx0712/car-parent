/**
 * 
 */
package pojo.VO.lll;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import mapper.CompanyMapper;
import pojo.Carnotoparts;
import pojo.DataChoose;
import pojo.DataMust;
import pojo.Lossinfo;
import pojo.ReportInfo;
import pojo.SurveyInfo;

/**
 * @author 亮亮 2018年10月22日
 * 
 */



public class DataChooseVO {
	

	private List<ReportInfo> reportInfoList;
	 
	public List<ReportInfo> getReportInfoList() {
		return reportInfoList;
	}

	public void setReportInfoList(List<ReportInfo> reportInfoList) {
		this.reportInfoList = reportInfoList;
	}


	private List<DataChoose> daList;
	
	private List<DataMust> doList;
	
	
	private String travelLicenseA;
	    public String getTravelLicenseA() {
		return travelLicenseA;
	}

	public void setTravelLicenseA(String travelLicenseA) {
		this.travelLicenseA = travelLicenseA;
	}

		public List<DataMust> getDoList() {
		return doList;
	}

	public void setDoList(List<DataMust> doList) {
		this.doList = doList;
	}

		public List<DataChoose> getDaList() {
		return daList;
	}

	public void setDaList(List<DataChoose> daList) {
		this.daList = daList;
	}

		public String getDriverLicense() {
		return driverLicense;
	}

	public void setDriverLicense(String driverLicense) {
		this.driverLicense = driverLicense;
	}

	public String getInsurerCertificate() {
		return insurerCertificate;
	}

	public void setInsurerCertificate(String insurerCertificate) {
		this.insurerCertificate = insurerCertificate;
	}

	public String getAutobytelInvoice() {
		return autobytelInvoice;
	}

	public void setAutobytelInvoice(String autobytelInvoice) {
		this.autobytelInvoice = autobytelInvoice;
	}

	public String getCarCertificate() {
		return carCertificate;
	}

	public void setCarCertificate(String carCertificate) {
		this.carCertificate = carCertificate;
	}

	public String getAdditionalPrices() {
		return additionalPrices;
	}

	public void setAdditionalPrices(String additionalPrices) {
		this.additionalPrices = additionalPrices;
	}

	public String getTravelLicenseB() {
		return travelLicenseB;
	}

	public void setTravelLicenseB(String travelLicenseB) {
		this.travelLicenseB = travelLicenseB;
	}

	public String getClaim() {
		return claim;
	}

	public void setClaim(String claim) {
		this.claim = claim;
	}

	public String getSalvagePrices() {
		return salvagePrices;
	}

	public void setSalvagePrices(String salvagePrices) {
		this.salvagePrices = salvagePrices;
	}

	public String getAccidentCertificate() {
		return accidentCertificate;
	}

	public void setAccidentCertificate(String accidentCertificate) {
		this.accidentCertificate = accidentCertificate;
	}

	public String getReparationMediation() {
		return reparationMediation;
	}

	public void setReparationMediation(String reparationMediation) {
		this.reparationMediation = reparationMediation;
	}

	public String getRepairInvoice() {
		return repairInvoice;
	}

	public void setRepairInvoice(String repairInvoice) {
		this.repairInvoice = repairInvoice;
	}

	public String getDriverVip() {
		return driverVip;
	}

	public void setDriverVip(String driverVip) {
		this.driverVip = driverVip;
	}

	public String getMedicalPrices() {
		return medicalPrices;
	}

	public void setMedicalPrices(String medicalPrices) {
		this.medicalPrices = medicalPrices;
	}

	public String getDieCertificate() {
		return dieCertificate;
	}

	public void setDieCertificate(String dieCertificate) {
		this.dieCertificate = dieCertificate;
	}

	public String getDisabilityCertificate() {
		return disabilityCertificate;
	}

	public void setDisabilityCertificate(String disabilityCertificate) {
		this.disabilityCertificate = disabilityCertificate;
	}

	public String getYanglufei() {
		return yanglufei;
	}

	public void setYanglufei(String yanglufei) {
		this.yanglufei = yanglufei;
	}

	public String getTouqiekanpoCertificate() {
		return touqiekanpoCertificate;
	}

	public void setTouqiekanpoCertificate(String touqiekanpoCertificate) {
		this.touqiekanpoCertificate = touqiekanpoCertificate;
	}

	public String getCarKey() {
		return carKey;
	}

	public void setCarKey(String carKey) {
		this.carKey = carKey;
	}

	public String getLossVehiclesCertificate() {
		return lossVehiclesCertificate;
	}

	public void setLossVehiclesCertificate(String lossVehiclesCertificate) {
		this.lossVehiclesCertificate = lossVehiclesCertificate;
	}

	public String getTouqiefdCertificate() {
		return touqiefdCertificate;
	}

	public void setTouqiefdCertificate(String touqiefdCertificate) {
		this.touqiefdCertificate = touqiefdCertificate;
	}

	public String getWugongCertificate() {
		return wugongCertificate;
	}

	public void setWugongCertificate(String wugongCertificate) {
		this.wugongCertificate = wugongCertificate;
	}

	public String getAssignment() {
		return assignment;
	}

	public void setAssignment(String assignment) {
		this.assignment = assignment;
	}

	public String getFireCertificate() {
		return fireCertificate;
	}

	public void setFireCertificate(String fireCertificate) {
		this.fireCertificate = fireCertificate;
	}

	public String getRainstormCertificate() {
		return rainstormCertificate;
	}

	public void setRainstormCertificate(String rainstormCertificate) {
		this.rainstormCertificate = rainstormCertificate;
	}

		private String driverLicense;

	    private String insurerCertificate;

	    private String autobytelInvoice;

	    private String carCertificate;

	    private String additionalPrices;

	    private String travelLicenseB;

	    private String claim;

	    private String salvagePrices;

	  

	    private String accidentCertificate;
	    private String reparationMediation;

	    private String repairInvoice;

	    private String driverVip;

	    private String medicalPrices;

	    private String dieCertificate;

	    private String disabilityCertificate;

	    private String yanglufei;

	    private String touqiekanpoCertificate;

	    private String carKey;

	    private String lossVehiclesCertificate;

	    private String touqiefdCertificate;

	    private String wugongCertificate;

	    private String assignment;

	    private String fireCertificate;

	    private String rainstormCertificate;

	 private Integer carnotopartId;

	 private String back1;

	    private String back2;

	    private String back3;

	    public String getBack1() {
			return back1;
		}

		public void setBack1(String back1) {
			this.back1 = back1;
		}

		public String getBack2() {
			return back2;
		}

		public void setBack2(String back2) {
			this.back2 = back2;
		}

		public String getBack3() {
			return back3;
		}

		public void setBack3(String back3) {
			this.back3 = back3;
		}

		private String partsName;

	    private Integer partsCount;

	    private Double lossFee;

	    private Double lossMoney;

	    private Double lossPrices;

	   
	private  List<Lossinfo> list;
	

	    public List<Lossinfo> getList() {
		return list;
	}

	public void setList(List<Lossinfo> list) {
		this.list = list;
	}

		public Integer getCarnotopartId() {
		return carnotopartId;
	}

	public void setCarnotopartId(Integer carnotopartId) {
		this.carnotopartId = carnotopartId;
	}

	public String getPartsName() {
		return partsName;
	}

	public void setPartsName(String partsName) {
		this.partsName = partsName;
	}

	public Integer getPartsCount() {
		return partsCount;
	}

	public void setPartsCount(Integer partsCount) {
		this.partsCount = partsCount;
	}

	public Double getLossFee() {
		return lossFee;
	}

	public void setLossFee(Double lossFee) {
		this.lossFee = lossFee;
	}

	public Double getLossMoney() {
		return lossMoney;
	}

	public void setLossMoney(Double lossMoney) {
		this.lossMoney = lossMoney;
	}

	public Double getLossPrices() {
		return lossPrices;
	}

	public void setLossPrices(Double lossPrices) {
		this.lossPrices = lossPrices;
	}

		public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getSurveyAddress() {
		return surveyAddress;
	}

	public void setSurveyAddress(String surveyAddress) {
		this.surveyAddress = surveyAddress;
	}

	public String getRoadInfo() {
		return roadInfo;
	}

	public void setRoadInfo(String roadInfo) {
		this.roadInfo = roadInfo;
	}

	public String getDangerThrough() {
		return dangerThrough;
	}

	public void setDangerThrough(String dangerThrough) {
		this.dangerThrough = dangerThrough;
	}

	public String getSurveyOpinion() {
		return surveyOpinion;
	}

	public void setSurveyOpinion(String surveyOpinion) {
		this.surveyOpinion = surveyOpinion;
	}

	public String getImgOne() {
		return imgOne;
	}

	public void setImgOne(String imgOne) {
		this.imgOne = imgOne;
	}

	public String getImgTwo() {
		return imgTwo;
	}

	public void setImgTwo(String imgTwo) {
		this.imgTwo = imgTwo;
	}

	public String getImgThree() {
		return imgThree;
	}

	public void setImgThree(String imgThree) {
		this.imgThree = imgThree;
	}

	public Date getSurveyTime() {
		return surveyTime;
	}

	public void setSurveyTime(Date surveyTime) {
		this.surveyTime = surveyTime;
	}

		private String carType;

	 
	    private String surveyAddress;

	 
	    private String roadInfo;

	    private String dangerThrough;

	    private String surveyOpinion;

	    private String imgOne;

	    private String imgTwo;

	    private String imgThree;

	

	    private Date surveyTime;


	
	 private String dangerArea;
	 private String surveyName;
	public String getDangerArea() {
		return dangerArea;
	}

	public void setDangerArea(String dangerArea) {
		this.dangerArea = dangerArea;
	}

	public String getSurveyName() {
		return surveyName;
	}

	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}

	private Integer companyId;

	private String companyName;

	private Integer reportNo;

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	private Integer tableId;

	private String policyId;

	private String reportName;

	private String reporTel;
    @DateTimeFormat(pattern="yyyy-MM-ddHH:mm:ss") 
	private Date reportTime;

	private String driverName;

	private String driverTel;

	private Date dangerTime;

	private String dangerAddress;

	private String dangerReason;

	private Integer lossType;

	private Integer serviceId;

	private Integer reportState;

	private Integer surveyId;

	private Integer auditId;

	private String carNo;

	private String applicant;

	private String aIdcard;

	private String aPhone;

	private String aAddress;

	private String beneficiary;

	private String bIdcard;

	private String bPhone;

	private String bAdress;

	private Double insuredAmount;

	private Double compensateAmount;

	private Date startDate;

	private Date endTime;

	private Integer insuranceId;

	private Date createTime;

	private Date updateTime;

	private Integer loadId;
	private Integer lossId;

	private String repairShop;

	private String repairType;

	private String carOwner;

	private Double carMoney;

	private String userName;
	@DateTimeFormat(pattern="yyyy-MM-ddHH:mm:ss")  
	private Date lossTime;

	private String lossOpinion;

	private Integer partsId;

	public Integer getReportNo() {
		return reportNo;
	}

	public void setReportNo(Integer reportNo) {
		this.reportNo = reportNo;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public String getPolicyId() {
		return policyId;
	}

	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReporTel() {
		return reporTel;
	}

	public void setReporTel(String reporTel) {
		this.reporTel = reporTel;
	}

	public Date getReportTime() {
		return reportTime;
	}

	public void setReportTime(Date reportTime) {
		this.reportTime = reportTime;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverTel() {
		return driverTel;
	}

	public void setDriverTel(String driverTel) {
		this.driverTel = driverTel;
	}

	public Date getDangerTime() {
		return dangerTime;
	}

	public void setDangerTime(Date dangerTime) {
		this.dangerTime = dangerTime;
	}

	public String getDangerAddress() {
		return dangerAddress;
	}

	public void setDangerAddress(String dangerAddress) {
		this.dangerAddress = dangerAddress;
	}

	public String getDangerReason() {
		return dangerReason;
	}

	public void setDangerReason(String dangerReason) {
		this.dangerReason = dangerReason;
	}

	public Integer getLossType() {
		return lossType;
	}

	public void setLossType(Integer lossType) {
		this.lossType = lossType;
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getReportState() {
		return reportState;
	}

	public void setReportState(Integer reportState) {
		this.reportState = reportState;
	}

	public Integer getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}

	public Integer getAuditId() {
		return auditId;
	}

	public void setAuditId(Integer auditId) {
		this.auditId = auditId;
	}

	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getaIdcard() {
		return aIdcard;
	}

	public void setaIdcard(String aIdcard) {
		this.aIdcard = aIdcard;
	}

	public String getaPhone() {
		return aPhone;
	}

	public void setaPhone(String aPhone) {
		this.aPhone = aPhone;
	}

	public String getaAddress() {
		return aAddress;
	}

	public void setaAddress(String aAddress) {
		this.aAddress = aAddress;
	}

	public String getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(String beneficiary) {
		this.beneficiary = beneficiary;
	}

	public String getbIdcard() {
		return bIdcard;
	}

	public void setbIdcard(String bIdcard) {
		this.bIdcard = bIdcard;
	}

	public String getbPhone() {
		return bPhone;
	}

	public void setbPhone(String bPhone) {
		this.bPhone = bPhone;
	}

	public String getbAdress() {
		return bAdress;
	}

	public void setbAdress(String bAdress) {
		this.bAdress = bAdress;
	}

	public Double getInsuredAmount() {
		return insuredAmount;
	}

	public void setInsuredAmount(Double insuredAmount) {
		this.insuredAmount = insuredAmount;
	}

	public Double getCompensateAmount() {
		return compensateAmount;
	}

	public void setCompensateAmount(Double compensateAmount) {
		this.compensateAmount = compensateAmount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getInsuranceId() {
		return insuranceId;
	}

	public void setInsuranceId(Integer insuranceId) {
		this.insuranceId = insuranceId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getLoadId() {
		return loadId;
	}

	public void setLoadId(Integer loadId) {
		this.loadId = loadId;
	}

	public Integer getLossId() {
		return lossId;
	}

	public void setLossId(Integer lossId) {
		this.lossId = lossId;
	}

	public String getRepairShop() {
		return repairShop;
	}

	public void setRepairShop(String repairShop) {
		this.repairShop = repairShop;
	}

	public String getRepairType() {
		return repairType;
	}

	public void setRepairType(String repairType) {
		this.repairType = repairType;
	}

	public String getCarOwner() {
		return carOwner;
	}

	public void setCarOwner(String carOwner) {
		this.carOwner = carOwner;
	}

	public Double getCarMoney() {
		return carMoney;
	}

	public void setCarMoney(Double carMoney) {
		this.carMoney = carMoney;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getLossTime() {
		return lossTime;
	}

	public void setLossTime(Date lossTime) {
		this.lossTime = lossTime;
	}

	public String getLossOpinion() {
		return lossOpinion;
	}

	public void setLossOpinion(String lossOpinion) {
		this.lossOpinion = lossOpinion;
	}

	public Integer getPartsId() {
		return partsId;
	}

	public void setPartsId(Integer partsId) {
		this.partsId = partsId;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getReturnIdea() {
		return returnIdea;
	}

	public void setReturnIdea(String returnIdea) {
		this.returnIdea = returnIdea;
	}

	private String sign;

	private String returnIdea;

	public DataChooseVO() {
		super();
	}

}
