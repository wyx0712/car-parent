package pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ReportInfo {
	private Integer reportNo;// 报案号

	private Integer tableId = 2001;// 表编号

	private String policyId;// 保单号

	private String reportName;// 报案人

	private String reporTel;// 报案人电话
	@DateTimeFormat(pattern = "yyyy-MM-ddHH:mm:ss")
	private Date reportTime;// 报案时间

	private String driverName;// 驾驶人

	private String driverTel;// 驾驶人电话

	private Date dangerTime;// 出险时间

	private String dangerAddress;// 出险地址

	private String dangerReason;// 出险原因

	private Integer lossType = 1;// 损失类型(本车、三方、人身)

	private Integer serviceId;// 接案人id

	private Integer reportState;// 接案状态

	private Integer surveyId;// 查勘人id

	private Integer auditId;// 核赔人id

	private String back1;// 备用字段1

	private String back2;// 备用字段2

	private String back3;// 备用字段3

	public Integer getReportNo() {
		return reportNo;
	}

	public void setReportNo(Integer reportNo) {
		this.reportNo = reportNo;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public String getPolicyId() {
		return policyId;
	}

	public void setPolicyId(String policyId) {
		this.policyId = policyId == null ? null : policyId.trim();
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName == null ? null : reportName.trim();
	}

	public String getReporTel() {
		return reporTel;
	}

	public void setReporTel(String reporTel) {
		this.reporTel = reporTel == null ? null : reporTel.trim();
	}

	public Date getReportTime() {
		return reportTime;
	}

	public void setReportTime(Date reportTime) {
		this.reportTime = reportTime;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName == null ? null : driverName.trim();
	}

	public String getDriverTel() {
		return driverTel;
	}

	public void setDriverTel(String driverTel) {
		this.driverTel = driverTel == null ? null : driverTel.trim();
	}

	public Date getDangerTime() {
		return dangerTime;
	}

	public void setDangerTime(Date dangerTime) {
		this.dangerTime = dangerTime;
	}

	public String getDangerAddress() {
		return dangerAddress;
	}

	public void setDangerAddress(String dangerAddress) {
		this.dangerAddress = dangerAddress == null ? null : dangerAddress.trim();
	}

	public String getDangerReason() {
		return dangerReason;
	}

	public void setDangerReason(String dangerReason) {
		this.dangerReason = dangerReason == null ? null : dangerReason.trim();
	}

	public Integer getLossType() {
		return lossType;
	}

	public void setLossType(Integer lossType) {
		this.lossType = lossType;
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getReportState() {
		return reportState;
	}

	public void setReportState(Integer reportState) {
		this.reportState = reportState;
	}

	public Integer getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}

	public Integer getAuditId() {
		return auditId;
	}

	public void setAuditId(Integer auditId) {
		this.auditId = auditId;
	}

	public String getBack1() {
		return back1;
	}

	public void setBack1(String back1) {
		this.back1 = back1 == null ? null : back1.trim();
	}

	public String getBack2() {
		return back2;
	}

	public void setBack2(String back2) {
		this.back2 = back2 == null ? null : back2.trim();
	}

	public String getBack3() {
		return back3;
	}

	public void setBack3(String back3) {
		this.back3 = back3 == null ? null : back3.trim();
	}
}