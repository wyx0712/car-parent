package pojo;

public class DataChoose {
    private Integer reportNo;

    private Integer tableId;

    private String reparationMediation;

    private String repairInvoice;

    private String driverVip;

    private String medicalPrices;

    private String dieCertificate;

    private String disabilityCertificate;

    private String yanglufei;

    private String touqiekanpoCertificate;

    private String carKey;

    private String lossVehiclesCertificate;

    private String touqiefdCertificate;

    private String wugongCertificate;

    private String assignment;

    private String fireCertificate;

    private String rainstormCertificate;

    private String back1;

    private String back2;

    private String back3;

    public Integer getReportNo() {
        return reportNo;
    }

    public void setReportNo(Integer reportNo) {
        this.reportNo = reportNo;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getReparationMediation() {
        return reparationMediation;
    }

    public void setReparationMediation(String reparationMediation) {
        this.reparationMediation = reparationMediation == null ? null : reparationMediation.trim();
    }

    public String getRepairInvoice() {
        return repairInvoice;
    }

    public void setRepairInvoice(String repairInvoice) {
        this.repairInvoice = repairInvoice == null ? null : repairInvoice.trim();
    }

    public String getDriverVip() {
        return driverVip;
    }

    public void setDriverVip(String driverVip) {
        this.driverVip = driverVip == null ? null : driverVip.trim();
    }

    public String getMedicalPrices() {
        return medicalPrices;
    }

    public void setMedicalPrices(String medicalPrices) {
        this.medicalPrices = medicalPrices == null ? null : medicalPrices.trim();
    }

    public String getDieCertificate() {
        return dieCertificate;
    }

    public void setDieCertificate(String dieCertificate) {
        this.dieCertificate = dieCertificate == null ? null : dieCertificate.trim();
    }

    public String getDisabilityCertificate() {
        return disabilityCertificate;
    }

    public void setDisabilityCertificate(String disabilityCertificate) {
        this.disabilityCertificate = disabilityCertificate == null ? null : disabilityCertificate.trim();
    }

    public String getYanglufei() {
        return yanglufei;
    }

    public void setYanglufei(String yanglufei) {
        this.yanglufei = yanglufei == null ? null : yanglufei.trim();
    }

    public String getTouqiekanpoCertificate() {
        return touqiekanpoCertificate;
    }

    public void setTouqiekanpoCertificate(String touqiekanpoCertificate) {
        this.touqiekanpoCertificate = touqiekanpoCertificate == null ? null : touqiekanpoCertificate.trim();
    }

    public String getCarKey() {
        return carKey;
    }

    public void setCarKey(String carKey) {
        this.carKey = carKey == null ? null : carKey.trim();
    }

    public String getLossVehiclesCertificate() {
        return lossVehiclesCertificate;
    }

    public void setLossVehiclesCertificate(String lossVehiclesCertificate) {
        this.lossVehiclesCertificate = lossVehiclesCertificate == null ? null : lossVehiclesCertificate.trim();
    }

    public String getTouqiefdCertificate() {
        return touqiefdCertificate;
    }

    public void setTouqiefdCertificate(String touqiefdCertificate) {
        this.touqiefdCertificate = touqiefdCertificate == null ? null : touqiefdCertificate.trim();
    }

    public String getWugongCertificate() {
        return wugongCertificate;
    }

    public void setWugongCertificate(String wugongCertificate) {
        this.wugongCertificate = wugongCertificate == null ? null : wugongCertificate.trim();
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment == null ? null : assignment.trim();
    }

    public String getFireCertificate() {
        return fireCertificate;
    }

    public void setFireCertificate(String fireCertificate) {
        this.fireCertificate = fireCertificate == null ? null : fireCertificate.trim();
    }

    public String getRainstormCertificate() {
        return rainstormCertificate;
    }

    public void setRainstormCertificate(String rainstormCertificate) {
        this.rainstormCertificate = rainstormCertificate == null ? null : rainstormCertificate.trim();
    }

    public String getBack1() {
        return back1;
    }

    public void setBack1(String back1) {
        this.back1 = back1 == null ? null : back1.trim();
    }

    public String getBack2() {
        return back2;
    }

    public void setBack2(String back2) {
        this.back2 = back2 == null ? null : back2.trim();
    }

    public String getBack3() {
        return back3;
    }

    public void setBack3(String back3) {
        this.back3 = back3 == null ? null : back3.trim();
    }
}