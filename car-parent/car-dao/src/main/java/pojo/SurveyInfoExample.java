package pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SurveyInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SurveyInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSurveyIdIsNull() {
            addCriterion("survey_id is null");
            return (Criteria) this;
        }

        public Criteria andSurveyIdIsNotNull() {
            addCriterion("survey_id is not null");
            return (Criteria) this;
        }

        public Criteria andSurveyIdEqualTo(Integer value) {
            addCriterion("survey_id =", value, "surveyId");
            return (Criteria) this;
        }

        public Criteria andSurveyIdNotEqualTo(Integer value) {
            addCriterion("survey_id <>", value, "surveyId");
            return (Criteria) this;
        }

        public Criteria andSurveyIdGreaterThan(Integer value) {
            addCriterion("survey_id >", value, "surveyId");
            return (Criteria) this;
        }

        public Criteria andSurveyIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("survey_id >=", value, "surveyId");
            return (Criteria) this;
        }

        public Criteria andSurveyIdLessThan(Integer value) {
            addCriterion("survey_id <", value, "surveyId");
            return (Criteria) this;
        }

        public Criteria andSurveyIdLessThanOrEqualTo(Integer value) {
            addCriterion("survey_id <=", value, "surveyId");
            return (Criteria) this;
        }

        public Criteria andSurveyIdIn(List<Integer> values) {
            addCriterion("survey_id in", values, "surveyId");
            return (Criteria) this;
        }

        public Criteria andSurveyIdNotIn(List<Integer> values) {
            addCriterion("survey_id not in", values, "surveyId");
            return (Criteria) this;
        }

        public Criteria andSurveyIdBetween(Integer value1, Integer value2) {
            addCriterion("survey_id between", value1, value2, "surveyId");
            return (Criteria) this;
        }

        public Criteria andSurveyIdNotBetween(Integer value1, Integer value2) {
            addCriterion("survey_id not between", value1, value2, "surveyId");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNull() {
            addCriterion("table_id is null");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNotNull() {
            addCriterion("table_id is not null");
            return (Criteria) this;
        }

        public Criteria andTableIdEqualTo(Integer value) {
            addCriterion("table_id =", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotEqualTo(Integer value) {
            addCriterion("table_id <>", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThan(Integer value) {
            addCriterion("table_id >", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("table_id >=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThan(Integer value) {
            addCriterion("table_id <", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThanOrEqualTo(Integer value) {
            addCriterion("table_id <=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdIn(List<Integer> values) {
            addCriterion("table_id in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotIn(List<Integer> values) {
            addCriterion("table_id not in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdBetween(Integer value1, Integer value2) {
            addCriterion("table_id between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotBetween(Integer value1, Integer value2) {
            addCriterion("table_id not between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNull() {
            addCriterion("report_no is null");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNotNull() {
            addCriterion("report_no is not null");
            return (Criteria) this;
        }

        public Criteria andReportNoEqualTo(Integer value) {
            addCriterion("report_no =", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotEqualTo(Integer value) {
            addCriterion("report_no <>", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThan(Integer value) {
            addCriterion("report_no >", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("report_no >=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThan(Integer value) {
            addCriterion("report_no <", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThanOrEqualTo(Integer value) {
            addCriterion("report_no <=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoIn(List<Integer> values) {
            addCriterion("report_no in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotIn(List<Integer> values) {
            addCriterion("report_no not in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoBetween(Integer value1, Integer value2) {
            addCriterion("report_no between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotBetween(Integer value1, Integer value2) {
            addCriterion("report_no not between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNameIsNull() {
            addCriterion("report_name is null");
            return (Criteria) this;
        }

        public Criteria andReportNameIsNotNull() {
            addCriterion("report_name is not null");
            return (Criteria) this;
        }

        public Criteria andReportNameEqualTo(String value) {
            addCriterion("report_name =", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameNotEqualTo(String value) {
            addCriterion("report_name <>", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameGreaterThan(String value) {
            addCriterion("report_name >", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameGreaterThanOrEqualTo(String value) {
            addCriterion("report_name >=", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameLessThan(String value) {
            addCriterion("report_name <", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameLessThanOrEqualTo(String value) {
            addCriterion("report_name <=", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameLike(String value) {
            addCriterion("report_name like", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameNotLike(String value) {
            addCriterion("report_name not like", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameIn(List<String> values) {
            addCriterion("report_name in", values, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameNotIn(List<String> values) {
            addCriterion("report_name not in", values, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameBetween(String value1, String value2) {
            addCriterion("report_name between", value1, value2, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameNotBetween(String value1, String value2) {
            addCriterion("report_name not between", value1, value2, "reportName");
            return (Criteria) this;
        }

        public Criteria andDriverNameIsNull() {
            addCriterion("driver_name is null");
            return (Criteria) this;
        }

        public Criteria andDriverNameIsNotNull() {
            addCriterion("driver_name is not null");
            return (Criteria) this;
        }

        public Criteria andDriverNameEqualTo(String value) {
            addCriterion("driver_name =", value, "driverName");
            return (Criteria) this;
        }

        public Criteria andDriverNameNotEqualTo(String value) {
            addCriterion("driver_name <>", value, "driverName");
            return (Criteria) this;
        }

        public Criteria andDriverNameGreaterThan(String value) {
            addCriterion("driver_name >", value, "driverName");
            return (Criteria) this;
        }

        public Criteria andDriverNameGreaterThanOrEqualTo(String value) {
            addCriterion("driver_name >=", value, "driverName");
            return (Criteria) this;
        }

        public Criteria andDriverNameLessThan(String value) {
            addCriterion("driver_name <", value, "driverName");
            return (Criteria) this;
        }

        public Criteria andDriverNameLessThanOrEqualTo(String value) {
            addCriterion("driver_name <=", value, "driverName");
            return (Criteria) this;
        }

        public Criteria andDriverNameLike(String value) {
            addCriterion("driver_name like", value, "driverName");
            return (Criteria) this;
        }

        public Criteria andDriverNameNotLike(String value) {
            addCriterion("driver_name not like", value, "driverName");
            return (Criteria) this;
        }

        public Criteria andDriverNameIn(List<String> values) {
            addCriterion("driver_name in", values, "driverName");
            return (Criteria) this;
        }

        public Criteria andDriverNameNotIn(List<String> values) {
            addCriterion("driver_name not in", values, "driverName");
            return (Criteria) this;
        }

        public Criteria andDriverNameBetween(String value1, String value2) {
            addCriterion("driver_name between", value1, value2, "driverName");
            return (Criteria) this;
        }

        public Criteria andDriverNameNotBetween(String value1, String value2) {
            addCriterion("driver_name not between", value1, value2, "driverName");
            return (Criteria) this;
        }

        public Criteria andCarTypeIsNull() {
            addCriterion("car_type is null");
            return (Criteria) this;
        }

        public Criteria andCarTypeIsNotNull() {
            addCriterion("car_type is not null");
            return (Criteria) this;
        }

        public Criteria andCarTypeEqualTo(String value) {
            addCriterion("car_type =", value, "carType");
            return (Criteria) this;
        }

        public Criteria andCarTypeNotEqualTo(String value) {
            addCriterion("car_type <>", value, "carType");
            return (Criteria) this;
        }

        public Criteria andCarTypeGreaterThan(String value) {
            addCriterion("car_type >", value, "carType");
            return (Criteria) this;
        }

        public Criteria andCarTypeGreaterThanOrEqualTo(String value) {
            addCriterion("car_type >=", value, "carType");
            return (Criteria) this;
        }

        public Criteria andCarTypeLessThan(String value) {
            addCriterion("car_type <", value, "carType");
            return (Criteria) this;
        }

        public Criteria andCarTypeLessThanOrEqualTo(String value) {
            addCriterion("car_type <=", value, "carType");
            return (Criteria) this;
        }

        public Criteria andCarTypeLike(String value) {
            addCriterion("car_type like", value, "carType");
            return (Criteria) this;
        }

        public Criteria andCarTypeNotLike(String value) {
            addCriterion("car_type not like", value, "carType");
            return (Criteria) this;
        }

        public Criteria andCarTypeIn(List<String> values) {
            addCriterion("car_type in", values, "carType");
            return (Criteria) this;
        }

        public Criteria andCarTypeNotIn(List<String> values) {
            addCriterion("car_type not in", values, "carType");
            return (Criteria) this;
        }

        public Criteria andCarTypeBetween(String value1, String value2) {
            addCriterion("car_type between", value1, value2, "carType");
            return (Criteria) this;
        }

        public Criteria andCarTypeNotBetween(String value1, String value2) {
            addCriterion("car_type not between", value1, value2, "carType");
            return (Criteria) this;
        }

        public Criteria andDriverTelIsNull() {
            addCriterion("driver_tel is null");
            return (Criteria) this;
        }

        public Criteria andDriverTelIsNotNull() {
            addCriterion("driver_tel is not null");
            return (Criteria) this;
        }

        public Criteria andDriverTelEqualTo(String value) {
            addCriterion("driver_tel =", value, "driverTel");
            return (Criteria) this;
        }

        public Criteria andDriverTelNotEqualTo(String value) {
            addCriterion("driver_tel <>", value, "driverTel");
            return (Criteria) this;
        }

        public Criteria andDriverTelGreaterThan(String value) {
            addCriterion("driver_tel >", value, "driverTel");
            return (Criteria) this;
        }

        public Criteria andDriverTelGreaterThanOrEqualTo(String value) {
            addCriterion("driver_tel >=", value, "driverTel");
            return (Criteria) this;
        }

        public Criteria andDriverTelLessThan(String value) {
            addCriterion("driver_tel <", value, "driverTel");
            return (Criteria) this;
        }

        public Criteria andDriverTelLessThanOrEqualTo(String value) {
            addCriterion("driver_tel <=", value, "driverTel");
            return (Criteria) this;
        }

        public Criteria andDriverTelLike(String value) {
            addCriterion("driver_tel like", value, "driverTel");
            return (Criteria) this;
        }

        public Criteria andDriverTelNotLike(String value) {
            addCriterion("driver_tel not like", value, "driverTel");
            return (Criteria) this;
        }

        public Criteria andDriverTelIn(List<String> values) {
            addCriterion("driver_tel in", values, "driverTel");
            return (Criteria) this;
        }

        public Criteria andDriverTelNotIn(List<String> values) {
            addCriterion("driver_tel not in", values, "driverTel");
            return (Criteria) this;
        }

        public Criteria andDriverTelBetween(String value1, String value2) {
            addCriterion("driver_tel between", value1, value2, "driverTel");
            return (Criteria) this;
        }

        public Criteria andDriverTelNotBetween(String value1, String value2) {
            addCriterion("driver_tel not between", value1, value2, "driverTel");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressIsNull() {
            addCriterion("survey_address is null");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressIsNotNull() {
            addCriterion("survey_address is not null");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressEqualTo(String value) {
            addCriterion("survey_address =", value, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressNotEqualTo(String value) {
            addCriterion("survey_address <>", value, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressGreaterThan(String value) {
            addCriterion("survey_address >", value, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressGreaterThanOrEqualTo(String value) {
            addCriterion("survey_address >=", value, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressLessThan(String value) {
            addCriterion("survey_address <", value, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressLessThanOrEqualTo(String value) {
            addCriterion("survey_address <=", value, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressLike(String value) {
            addCriterion("survey_address like", value, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressNotLike(String value) {
            addCriterion("survey_address not like", value, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressIn(List<String> values) {
            addCriterion("survey_address in", values, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressNotIn(List<String> values) {
            addCriterion("survey_address not in", values, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressBetween(String value1, String value2) {
            addCriterion("survey_address between", value1, value2, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andSurveyAddressNotBetween(String value1, String value2) {
            addCriterion("survey_address not between", value1, value2, "surveyAddress");
            return (Criteria) this;
        }

        public Criteria andDangerReasonIsNull() {
            addCriterion("danger_reason is null");
            return (Criteria) this;
        }

        public Criteria andDangerReasonIsNotNull() {
            addCriterion("danger_reason is not null");
            return (Criteria) this;
        }

        public Criteria andDangerReasonEqualTo(String value) {
            addCriterion("danger_reason =", value, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerReasonNotEqualTo(String value) {
            addCriterion("danger_reason <>", value, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerReasonGreaterThan(String value) {
            addCriterion("danger_reason >", value, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerReasonGreaterThanOrEqualTo(String value) {
            addCriterion("danger_reason >=", value, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerReasonLessThan(String value) {
            addCriterion("danger_reason <", value, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerReasonLessThanOrEqualTo(String value) {
            addCriterion("danger_reason <=", value, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerReasonLike(String value) {
            addCriterion("danger_reason like", value, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerReasonNotLike(String value) {
            addCriterion("danger_reason not like", value, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerReasonIn(List<String> values) {
            addCriterion("danger_reason in", values, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerReasonNotIn(List<String> values) {
            addCriterion("danger_reason not in", values, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerReasonBetween(String value1, String value2) {
            addCriterion("danger_reason between", value1, value2, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerReasonNotBetween(String value1, String value2) {
            addCriterion("danger_reason not between", value1, value2, "dangerReason");
            return (Criteria) this;
        }

        public Criteria andDangerAddressIsNull() {
            addCriterion("danger_address is null");
            return (Criteria) this;
        }

        public Criteria andDangerAddressIsNotNull() {
            addCriterion("danger_address is not null");
            return (Criteria) this;
        }

        public Criteria andDangerAddressEqualTo(String value) {
            addCriterion("danger_address =", value, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAddressNotEqualTo(String value) {
            addCriterion("danger_address <>", value, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAddressGreaterThan(String value) {
            addCriterion("danger_address >", value, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAddressGreaterThanOrEqualTo(String value) {
            addCriterion("danger_address >=", value, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAddressLessThan(String value) {
            addCriterion("danger_address <", value, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAddressLessThanOrEqualTo(String value) {
            addCriterion("danger_address <=", value, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAddressLike(String value) {
            addCriterion("danger_address like", value, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAddressNotLike(String value) {
            addCriterion("danger_address not like", value, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAddressIn(List<String> values) {
            addCriterion("danger_address in", values, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAddressNotIn(List<String> values) {
            addCriterion("danger_address not in", values, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAddressBetween(String value1, String value2) {
            addCriterion("danger_address between", value1, value2, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAddressNotBetween(String value1, String value2) {
            addCriterion("danger_address not between", value1, value2, "dangerAddress");
            return (Criteria) this;
        }

        public Criteria andDangerAreaIsNull() {
            addCriterion("danger_area is null");
            return (Criteria) this;
        }

        public Criteria andDangerAreaIsNotNull() {
            addCriterion("danger_area is not null");
            return (Criteria) this;
        }

        public Criteria andDangerAreaEqualTo(String value) {
            addCriterion("danger_area =", value, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andDangerAreaNotEqualTo(String value) {
            addCriterion("danger_area <>", value, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andDangerAreaGreaterThan(String value) {
            addCriterion("danger_area >", value, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andDangerAreaGreaterThanOrEqualTo(String value) {
            addCriterion("danger_area >=", value, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andDangerAreaLessThan(String value) {
            addCriterion("danger_area <", value, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andDangerAreaLessThanOrEqualTo(String value) {
            addCriterion("danger_area <=", value, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andDangerAreaLike(String value) {
            addCriterion("danger_area like", value, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andDangerAreaNotLike(String value) {
            addCriterion("danger_area not like", value, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andDangerAreaIn(List<String> values) {
            addCriterion("danger_area in", values, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andDangerAreaNotIn(List<String> values) {
            addCriterion("danger_area not in", values, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andDangerAreaBetween(String value1, String value2) {
            addCriterion("danger_area between", value1, value2, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andDangerAreaNotBetween(String value1, String value2) {
            addCriterion("danger_area not between", value1, value2, "dangerArea");
            return (Criteria) this;
        }

        public Criteria andRoadInfoIsNull() {
            addCriterion("road_info is null");
            return (Criteria) this;
        }

        public Criteria andRoadInfoIsNotNull() {
            addCriterion("road_info is not null");
            return (Criteria) this;
        }

        public Criteria andRoadInfoEqualTo(String value) {
            addCriterion("road_info =", value, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andRoadInfoNotEqualTo(String value) {
            addCriterion("road_info <>", value, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andRoadInfoGreaterThan(String value) {
            addCriterion("road_info >", value, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andRoadInfoGreaterThanOrEqualTo(String value) {
            addCriterion("road_info >=", value, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andRoadInfoLessThan(String value) {
            addCriterion("road_info <", value, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andRoadInfoLessThanOrEqualTo(String value) {
            addCriterion("road_info <=", value, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andRoadInfoLike(String value) {
            addCriterion("road_info like", value, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andRoadInfoNotLike(String value) {
            addCriterion("road_info not like", value, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andRoadInfoIn(List<String> values) {
            addCriterion("road_info in", values, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andRoadInfoNotIn(List<String> values) {
            addCriterion("road_info not in", values, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andRoadInfoBetween(String value1, String value2) {
            addCriterion("road_info between", value1, value2, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andRoadInfoNotBetween(String value1, String value2) {
            addCriterion("road_info not between", value1, value2, "roadInfo");
            return (Criteria) this;
        }

        public Criteria andDangerThroughIsNull() {
            addCriterion("danger_through is null");
            return (Criteria) this;
        }

        public Criteria andDangerThroughIsNotNull() {
            addCriterion("danger_through is not null");
            return (Criteria) this;
        }

        public Criteria andDangerThroughEqualTo(String value) {
            addCriterion("danger_through =", value, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andDangerThroughNotEqualTo(String value) {
            addCriterion("danger_through <>", value, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andDangerThroughGreaterThan(String value) {
            addCriterion("danger_through >", value, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andDangerThroughGreaterThanOrEqualTo(String value) {
            addCriterion("danger_through >=", value, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andDangerThroughLessThan(String value) {
            addCriterion("danger_through <", value, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andDangerThroughLessThanOrEqualTo(String value) {
            addCriterion("danger_through <=", value, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andDangerThroughLike(String value) {
            addCriterion("danger_through like", value, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andDangerThroughNotLike(String value) {
            addCriterion("danger_through not like", value, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andDangerThroughIn(List<String> values) {
            addCriterion("danger_through in", values, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andDangerThroughNotIn(List<String> values) {
            addCriterion("danger_through not in", values, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andDangerThroughBetween(String value1, String value2) {
            addCriterion("danger_through between", value1, value2, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andDangerThroughNotBetween(String value1, String value2) {
            addCriterion("danger_through not between", value1, value2, "dangerThrough");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionIsNull() {
            addCriterion("survey_opinion is null");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionIsNotNull() {
            addCriterion("survey_opinion is not null");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionEqualTo(String value) {
            addCriterion("survey_opinion =", value, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionNotEqualTo(String value) {
            addCriterion("survey_opinion <>", value, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionGreaterThan(String value) {
            addCriterion("survey_opinion >", value, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionGreaterThanOrEqualTo(String value) {
            addCriterion("survey_opinion >=", value, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionLessThan(String value) {
            addCriterion("survey_opinion <", value, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionLessThanOrEqualTo(String value) {
            addCriterion("survey_opinion <=", value, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionLike(String value) {
            addCriterion("survey_opinion like", value, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionNotLike(String value) {
            addCriterion("survey_opinion not like", value, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionIn(List<String> values) {
            addCriterion("survey_opinion in", values, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionNotIn(List<String> values) {
            addCriterion("survey_opinion not in", values, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionBetween(String value1, String value2) {
            addCriterion("survey_opinion between", value1, value2, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andSurveyOpinionNotBetween(String value1, String value2) {
            addCriterion("survey_opinion not between", value1, value2, "surveyOpinion");
            return (Criteria) this;
        }

        public Criteria andImgOneIsNull() {
            addCriterion("img_one is null");
            return (Criteria) this;
        }

        public Criteria andImgOneIsNotNull() {
            addCriterion("img_one is not null");
            return (Criteria) this;
        }

        public Criteria andImgOneEqualTo(String value) {
            addCriterion("img_one =", value, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgOneNotEqualTo(String value) {
            addCriterion("img_one <>", value, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgOneGreaterThan(String value) {
            addCriterion("img_one >", value, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgOneGreaterThanOrEqualTo(String value) {
            addCriterion("img_one >=", value, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgOneLessThan(String value) {
            addCriterion("img_one <", value, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgOneLessThanOrEqualTo(String value) {
            addCriterion("img_one <=", value, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgOneLike(String value) {
            addCriterion("img_one like", value, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgOneNotLike(String value) {
            addCriterion("img_one not like", value, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgOneIn(List<String> values) {
            addCriterion("img_one in", values, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgOneNotIn(List<String> values) {
            addCriterion("img_one not in", values, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgOneBetween(String value1, String value2) {
            addCriterion("img_one between", value1, value2, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgOneNotBetween(String value1, String value2) {
            addCriterion("img_one not between", value1, value2, "imgOne");
            return (Criteria) this;
        }

        public Criteria andImgTwoIsNull() {
            addCriterion("img_two is null");
            return (Criteria) this;
        }

        public Criteria andImgTwoIsNotNull() {
            addCriterion("img_two is not null");
            return (Criteria) this;
        }

        public Criteria andImgTwoEqualTo(String value) {
            addCriterion("img_two =", value, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgTwoNotEqualTo(String value) {
            addCriterion("img_two <>", value, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgTwoGreaterThan(String value) {
            addCriterion("img_two >", value, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgTwoGreaterThanOrEqualTo(String value) {
            addCriterion("img_two >=", value, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgTwoLessThan(String value) {
            addCriterion("img_two <", value, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgTwoLessThanOrEqualTo(String value) {
            addCriterion("img_two <=", value, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgTwoLike(String value) {
            addCriterion("img_two like", value, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgTwoNotLike(String value) {
            addCriterion("img_two not like", value, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgTwoIn(List<String> values) {
            addCriterion("img_two in", values, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgTwoNotIn(List<String> values) {
            addCriterion("img_two not in", values, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgTwoBetween(String value1, String value2) {
            addCriterion("img_two between", value1, value2, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgTwoNotBetween(String value1, String value2) {
            addCriterion("img_two not between", value1, value2, "imgTwo");
            return (Criteria) this;
        }

        public Criteria andImgThreeIsNull() {
            addCriterion("img_three is null");
            return (Criteria) this;
        }

        public Criteria andImgThreeIsNotNull() {
            addCriterion("img_three is not null");
            return (Criteria) this;
        }

        public Criteria andImgThreeEqualTo(String value) {
            addCriterion("img_three =", value, "imgThree");
            return (Criteria) this;
        }

        public Criteria andImgThreeNotEqualTo(String value) {
            addCriterion("img_three <>", value, "imgThree");
            return (Criteria) this;
        }

        public Criteria andImgThreeGreaterThan(String value) {
            addCriterion("img_three >", value, "imgThree");
            return (Criteria) this;
        }

        public Criteria andImgThreeGreaterThanOrEqualTo(String value) {
            addCriterion("img_three >=", value, "imgThree");
            return (Criteria) this;
        }

        public Criteria andImgThreeLessThan(String value) {
            addCriterion("img_three <", value, "imgThree");
            return (Criteria) this;
        }

        public Criteria andImgThreeLessThanOrEqualTo(String value) {
            addCriterion("img_three <=", value, "imgThree");
            return (Criteria) this;
        }

        public Criteria andImgThreeLike(String value) {
            addCriterion("img_three like", value, "imgThree");
            return (Criteria) this;
        }

        public Criteria andImgThreeNotLike(String value) {
            addCriterion("img_three not like", value, "imgThree");
            return (Criteria) this;
        }

        public Criteria andImgThreeIn(List<String> values) {
            addCriterion("img_three in", values, "imgThree");
            return (Criteria) this;
        }

        public Criteria andImgThreeNotIn(List<String> values) {
            addCriterion("img_three not in", values, "imgThree");
            return (Criteria) this;
        }

        public Criteria andImgThreeBetween(String value1, String value2) {
            addCriterion("img_three between", value1, value2, "imgThree");
            return (Criteria) this;
        }

        public Criteria andImgThreeNotBetween(String value1, String value2) {
            addCriterion("img_three not between", value1, value2, "imgThree");
            return (Criteria) this;
        }

        public Criteria andSurveyNameIsNull() {
            addCriterion("survey_name is null");
            return (Criteria) this;
        }

        public Criteria andSurveyNameIsNotNull() {
            addCriterion("survey_name is not null");
            return (Criteria) this;
        }

        public Criteria andSurveyNameEqualTo(String value) {
            addCriterion("survey_name =", value, "surveyName");
            return (Criteria) this;
        }

        public Criteria andSurveyNameNotEqualTo(String value) {
            addCriterion("survey_name <>", value, "surveyName");
            return (Criteria) this;
        }

        public Criteria andSurveyNameGreaterThan(String value) {
            addCriterion("survey_name >", value, "surveyName");
            return (Criteria) this;
        }

        public Criteria andSurveyNameGreaterThanOrEqualTo(String value) {
            addCriterion("survey_name >=", value, "surveyName");
            return (Criteria) this;
        }

        public Criteria andSurveyNameLessThan(String value) {
            addCriterion("survey_name <", value, "surveyName");
            return (Criteria) this;
        }

        public Criteria andSurveyNameLessThanOrEqualTo(String value) {
            addCriterion("survey_name <=", value, "surveyName");
            return (Criteria) this;
        }

        public Criteria andSurveyNameLike(String value) {
            addCriterion("survey_name like", value, "surveyName");
            return (Criteria) this;
        }

        public Criteria andSurveyNameNotLike(String value) {
            addCriterion("survey_name not like", value, "surveyName");
            return (Criteria) this;
        }

        public Criteria andSurveyNameIn(List<String> values) {
            addCriterion("survey_name in", values, "surveyName");
            return (Criteria) this;
        }

        public Criteria andSurveyNameNotIn(List<String> values) {
            addCriterion("survey_name not in", values, "surveyName");
            return (Criteria) this;
        }

        public Criteria andSurveyNameBetween(String value1, String value2) {
            addCriterion("survey_name between", value1, value2, "surveyName");
            return (Criteria) this;
        }

        public Criteria andSurveyNameNotBetween(String value1, String value2) {
            addCriterion("survey_name not between", value1, value2, "surveyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameIsNull() {
            addCriterion("company_name is null");
            return (Criteria) this;
        }

        public Criteria andCompanyNameIsNotNull() {
            addCriterion("company_name is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyNameEqualTo(String value) {
            addCriterion("company_name =", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotEqualTo(String value) {
            addCriterion("company_name <>", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameGreaterThan(String value) {
            addCriterion("company_name >", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameGreaterThanOrEqualTo(String value) {
            addCriterion("company_name >=", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameLessThan(String value) {
            addCriterion("company_name <", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameLessThanOrEqualTo(String value) {
            addCriterion("company_name <=", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameLike(String value) {
            addCriterion("company_name like", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotLike(String value) {
            addCriterion("company_name not like", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameIn(List<String> values) {
            addCriterion("company_name in", values, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotIn(List<String> values) {
            addCriterion("company_name not in", values, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameBetween(String value1, String value2) {
            addCriterion("company_name between", value1, value2, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotBetween(String value1, String value2) {
            addCriterion("company_name not between", value1, value2, "companyName");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeIsNull() {
            addCriterion("survey_time is null");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeIsNotNull() {
            addCriterion("survey_time is not null");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeEqualTo(Date value) {
            addCriterion("survey_time =", value, "surveyTime");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeNotEqualTo(Date value) {
            addCriterion("survey_time <>", value, "surveyTime");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeGreaterThan(Date value) {
            addCriterion("survey_time >", value, "surveyTime");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("survey_time >=", value, "surveyTime");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeLessThan(Date value) {
            addCriterion("survey_time <", value, "surveyTime");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeLessThanOrEqualTo(Date value) {
            addCriterion("survey_time <=", value, "surveyTime");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeIn(List<Date> values) {
            addCriterion("survey_time in", values, "surveyTime");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeNotIn(List<Date> values) {
            addCriterion("survey_time not in", values, "surveyTime");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeBetween(Date value1, Date value2) {
            addCriterion("survey_time between", value1, value2, "surveyTime");
            return (Criteria) this;
        }

        public Criteria andSurveyTimeNotBetween(Date value1, Date value2) {
            addCriterion("survey_time not between", value1, value2, "surveyTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaIsNull() {
            addCriterion("return_idea is null");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaIsNotNull() {
            addCriterion("return_idea is not null");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaEqualTo(String value) {
            addCriterion("return_idea =", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaNotEqualTo(String value) {
            addCriterion("return_idea <>", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaGreaterThan(String value) {
            addCriterion("return_idea >", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaGreaterThanOrEqualTo(String value) {
            addCriterion("return_idea >=", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaLessThan(String value) {
            addCriterion("return_idea <", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaLessThanOrEqualTo(String value) {
            addCriterion("return_idea <=", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaLike(String value) {
            addCriterion("return_idea like", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaNotLike(String value) {
            addCriterion("return_idea not like", value, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaIn(List<String> values) {
            addCriterion("return_idea in", values, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaNotIn(List<String> values) {
            addCriterion("return_idea not in", values, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaBetween(String value1, String value2) {
            addCriterion("return_idea between", value1, value2, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andReturnIdeaNotBetween(String value1, String value2) {
            addCriterion("return_idea not between", value1, value2, "returnIdea");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNull() {
            addCriterion("load_id is null");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNotNull() {
            addCriterion("load_id is not null");
            return (Criteria) this;
        }

        public Criteria andLoadIdEqualTo(Integer value) {
            addCriterion("load_id =", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotEqualTo(Integer value) {
            addCriterion("load_id <>", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThan(Integer value) {
            addCriterion("load_id >", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("load_id >=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThan(Integer value) {
            addCriterion("load_id <", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThanOrEqualTo(Integer value) {
            addCriterion("load_id <=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdIn(List<Integer> values) {
            addCriterion("load_id in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotIn(List<Integer> values) {
            addCriterion("load_id not in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdBetween(Integer value1, Integer value2) {
            addCriterion("load_id between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotBetween(Integer value1, Integer value2) {
            addCriterion("load_id not between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andBack1IsNull() {
            addCriterion("back1 is null");
            return (Criteria) this;
        }

        public Criteria andBack1IsNotNull() {
            addCriterion("back1 is not null");
            return (Criteria) this;
        }

        public Criteria andBack1EqualTo(String value) {
            addCriterion("back1 =", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotEqualTo(String value) {
            addCriterion("back1 <>", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThan(String value) {
            addCriterion("back1 >", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThanOrEqualTo(String value) {
            addCriterion("back1 >=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThan(String value) {
            addCriterion("back1 <", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThanOrEqualTo(String value) {
            addCriterion("back1 <=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Like(String value) {
            addCriterion("back1 like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotLike(String value) {
            addCriterion("back1 not like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1In(List<String> values) {
            addCriterion("back1 in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotIn(List<String> values) {
            addCriterion("back1 not in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Between(String value1, String value2) {
            addCriterion("back1 between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotBetween(String value1, String value2) {
            addCriterion("back1 not between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack2IsNull() {
            addCriterion("back2 is null");
            return (Criteria) this;
        }

        public Criteria andBack2IsNotNull() {
            addCriterion("back2 is not null");
            return (Criteria) this;
        }

        public Criteria andBack2EqualTo(String value) {
            addCriterion("back2 =", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotEqualTo(String value) {
            addCriterion("back2 <>", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThan(String value) {
            addCriterion("back2 >", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThanOrEqualTo(String value) {
            addCriterion("back2 >=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThan(String value) {
            addCriterion("back2 <", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThanOrEqualTo(String value) {
            addCriterion("back2 <=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Like(String value) {
            addCriterion("back2 like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotLike(String value) {
            addCriterion("back2 not like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2In(List<String> values) {
            addCriterion("back2 in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotIn(List<String> values) {
            addCriterion("back2 not in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Between(String value1, String value2) {
            addCriterion("back2 between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotBetween(String value1, String value2) {
            addCriterion("back2 not between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack3IsNull() {
            addCriterion("back3 is null");
            return (Criteria) this;
        }

        public Criteria andBack3IsNotNull() {
            addCriterion("back3 is not null");
            return (Criteria) this;
        }

        public Criteria andBack3EqualTo(String value) {
            addCriterion("back3 =", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotEqualTo(String value) {
            addCriterion("back3 <>", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThan(String value) {
            addCriterion("back3 >", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThanOrEqualTo(String value) {
            addCriterion("back3 >=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThan(String value) {
            addCriterion("back3 <", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThanOrEqualTo(String value) {
            addCriterion("back3 <=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Like(String value) {
            addCriterion("back3 like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotLike(String value) {
            addCriterion("back3 not like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3In(List<String> values) {
            addCriterion("back3 in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotIn(List<String> values) {
            addCriterion("back3 not in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Between(String value1, String value2) {
            addCriterion("back3 between", value1, value2, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotBetween(String value1, String value2) {
            addCriterion("back3 not between", value1, value2, "back3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}