package pojo;

import java.util.Date;

public class SurveyInfo {
    private Integer surveyId;

    private Integer tableId;

    private Integer reportNo;

    private String reportName;

    private String driverName;

    private String carType;

    private String driverTel;

    private String surveyAddress;

    private String dangerReason;

    private String dangerAddress;

    private String dangerArea;

    private String roadInfo;

    private String dangerThrough;

    private String surveyOpinion;

    private String imgOne;

    private String imgTwo;

    private String imgThree;

    private String surveyName;

    private String companyName;

    private Date surveyTime;

    private Date createTime;

    private String returnIdea;

    private Integer loadId;

    private String back1;

    private String back2;

    private String back3;

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public Integer getReportNo() {
        return reportNo;
    }

    public void setReportNo(Integer reportNo) {
        this.reportNo = reportNo;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName == null ? null : reportName.trim();
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName == null ? null : driverName.trim();
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType == null ? null : carType.trim();
    }

    public String getDriverTel() {
        return driverTel;
    }

    public void setDriverTel(String driverTel) {
        this.driverTel = driverTel == null ? null : driverTel.trim();
    }

    public String getSurveyAddress() {
        return surveyAddress;
    }

    public void setSurveyAddress(String surveyAddress) {
        this.surveyAddress = surveyAddress == null ? null : surveyAddress.trim();
    }

    public String getDangerReason() {
        return dangerReason;
    }

    public void setDangerReason(String dangerReason) {
        this.dangerReason = dangerReason == null ? null : dangerReason.trim();
    }

    public String getDangerAddress() {
        return dangerAddress;
    }

    public void setDangerAddress(String dangerAddress) {
        this.dangerAddress = dangerAddress == null ? null : dangerAddress.trim();
    }

    public String getDangerArea() {
        return dangerArea;
    }

    public void setDangerArea(String dangerArea) {
        this.dangerArea = dangerArea == null ? null : dangerArea.trim();
    }

    public String getRoadInfo() {
        return roadInfo;
    }

    public void setRoadInfo(String roadInfo) {
        this.roadInfo = roadInfo == null ? null : roadInfo.trim();
    }

    public String getDangerThrough() {
        return dangerThrough;
    }

    public void setDangerThrough(String dangerThrough) {
        this.dangerThrough = dangerThrough == null ? null : dangerThrough.trim();
    }

    public String getSurveyOpinion() {
        return surveyOpinion;
    }

    public void setSurveyOpinion(String surveyOpinion) {
        this.surveyOpinion = surveyOpinion == null ? null : surveyOpinion.trim();
    }

    public String getImgOne() {
        return imgOne;
    }

    public void setImgOne(String imgOne) {
        this.imgOne = imgOne == null ? null : imgOne.trim();
    }

    public String getImgTwo() {
        return imgTwo;
    }

    public void setImgTwo(String imgTwo) {
        this.imgTwo = imgTwo == null ? null : imgTwo.trim();
    }

    public String getImgThree() {
        return imgThree;
    }

    public void setImgThree(String imgThree) {
        this.imgThree = imgThree == null ? null : imgThree.trim();
    }

    public String getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(String surveyName) {
        this.surveyName = surveyName == null ? null : surveyName.trim();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public Date getSurveyTime() {
        return surveyTime;
    }

    public void setSurveyTime(Date surveyTime) {
        this.surveyTime = surveyTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getReturnIdea() {
        return returnIdea;
    }

    public void setReturnIdea(String returnIdea) {
        this.returnIdea = returnIdea == null ? null : returnIdea.trim();
    }

    public Integer getLoadId() {
        return loadId;
    }

    public void setLoadId(Integer loadId) {
        this.loadId = loadId;
    }

    public String getBack1() {
        return back1;
    }

    public void setBack1(String back1) {
        this.back1 = back1 == null ? null : back1.trim();
    }

    public String getBack2() {
        return back2;
    }

    public void setBack2(String back2) {
        this.back2 = back2 == null ? null : back2.trim();
    }

    public String getBack3() {
        return back3;
    }

    public void setBack3(String back3) {
        this.back3 = back3 == null ? null : back3.trim();
    }
}