package pojo;

import java.util.Date;

public class DatabaseBackup {
	private Integer backId;

	private Integer tableId = 1006;

	private String fileName;

	private Date createTime;

	private String loadName;

	private String back1;

	private String back2;

	private String back3;

	public Integer getBackId() {
		return backId;
	}

	public void setBackId(Integer backId) {
		this.backId = backId;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName == null ? null : fileName.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getLoadName() {
		return loadName;
	}

	public void setLoadName(String loadName) {
		this.loadName = loadName == null ? null : loadName.trim();
	}

	public String getBack1() {
		return back1;
	}

	public void setBack1(String back1) {
		this.back1 = back1 == null ? null : back1.trim();
	}

	public String getBack2() {
		return back2;
	}

	public void setBack2(String back2) {
		this.back2 = back2 == null ? null : back2.trim();
	}

	public String getBack3() {
		return back3;
	}

	public void setBack3(String back3) {
		this.back3 = back3 == null ? null : back3.trim();
	}
}