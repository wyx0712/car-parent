package pojo;

import java.util.ArrayList;
import java.util.List;

public class DataMustExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DataMustExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andReportNoIsNull() {
            addCriterion("report_no is null");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNotNull() {
            addCriterion("report_no is not null");
            return (Criteria) this;
        }

        public Criteria andReportNoEqualTo(Integer value) {
            addCriterion("report_no =", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotEqualTo(Integer value) {
            addCriterion("report_no <>", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThan(Integer value) {
            addCriterion("report_no >", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("report_no >=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThan(Integer value) {
            addCriterion("report_no <", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThanOrEqualTo(Integer value) {
            addCriterion("report_no <=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoIn(List<Integer> values) {
            addCriterion("report_no in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotIn(List<Integer> values) {
            addCriterion("report_no not in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoBetween(Integer value1, Integer value2) {
            addCriterion("report_no between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotBetween(Integer value1, Integer value2) {
            addCriterion("report_no not between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNull() {
            addCriterion("table_id is null");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNotNull() {
            addCriterion("table_id is not null");
            return (Criteria) this;
        }

        public Criteria andTableIdEqualTo(Integer value) {
            addCriterion("table_id =", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotEqualTo(Integer value) {
            addCriterion("table_id <>", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThan(Integer value) {
            addCriterion("table_id >", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("table_id >=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThan(Integer value) {
            addCriterion("table_id <", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThanOrEqualTo(Integer value) {
            addCriterion("table_id <=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdIn(List<Integer> values) {
            addCriterion("table_id in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotIn(List<Integer> values) {
            addCriterion("table_id not in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdBetween(Integer value1, Integer value2) {
            addCriterion("table_id between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotBetween(Integer value1, Integer value2) {
            addCriterion("table_id not between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdIsNull() {
            addCriterion("policy_id is null");
            return (Criteria) this;
        }

        public Criteria andPolicyIdIsNotNull() {
            addCriterion("policy_id is not null");
            return (Criteria) this;
        }

        public Criteria andPolicyIdEqualTo(String value) {
            addCriterion("policy_id =", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotEqualTo(String value) {
            addCriterion("policy_id <>", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdGreaterThan(String value) {
            addCriterion("policy_id >", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdGreaterThanOrEqualTo(String value) {
            addCriterion("policy_id >=", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLessThan(String value) {
            addCriterion("policy_id <", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLessThanOrEqualTo(String value) {
            addCriterion("policy_id <=", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLike(String value) {
            addCriterion("policy_id like", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotLike(String value) {
            addCriterion("policy_id not like", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdIn(List<String> values) {
            addCriterion("policy_id in", values, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotIn(List<String> values) {
            addCriterion("policy_id not in", values, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdBetween(String value1, String value2) {
            addCriterion("policy_id between", value1, value2, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotBetween(String value1, String value2) {
            addCriterion("policy_id not between", value1, value2, "policyId");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseIsNull() {
            addCriterion("driver_license is null");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseIsNotNull() {
            addCriterion("driver_license is not null");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseEqualTo(String value) {
            addCriterion("driver_license =", value, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseNotEqualTo(String value) {
            addCriterion("driver_license <>", value, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseGreaterThan(String value) {
            addCriterion("driver_license >", value, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseGreaterThanOrEqualTo(String value) {
            addCriterion("driver_license >=", value, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseLessThan(String value) {
            addCriterion("driver_license <", value, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseLessThanOrEqualTo(String value) {
            addCriterion("driver_license <=", value, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseLike(String value) {
            addCriterion("driver_license like", value, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseNotLike(String value) {
            addCriterion("driver_license not like", value, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseIn(List<String> values) {
            addCriterion("driver_license in", values, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseNotIn(List<String> values) {
            addCriterion("driver_license not in", values, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseBetween(String value1, String value2) {
            addCriterion("driver_license between", value1, value2, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andDriverLicenseNotBetween(String value1, String value2) {
            addCriterion("driver_license not between", value1, value2, "driverLicense");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateIsNull() {
            addCriterion("insurer_certificate is null");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateIsNotNull() {
            addCriterion("insurer_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateEqualTo(String value) {
            addCriterion("insurer_certificate =", value, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateNotEqualTo(String value) {
            addCriterion("insurer_certificate <>", value, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateGreaterThan(String value) {
            addCriterion("insurer_certificate >", value, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("insurer_certificate >=", value, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateLessThan(String value) {
            addCriterion("insurer_certificate <", value, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateLessThanOrEqualTo(String value) {
            addCriterion("insurer_certificate <=", value, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateLike(String value) {
            addCriterion("insurer_certificate like", value, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateNotLike(String value) {
            addCriterion("insurer_certificate not like", value, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateIn(List<String> values) {
            addCriterion("insurer_certificate in", values, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateNotIn(List<String> values) {
            addCriterion("insurer_certificate not in", values, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateBetween(String value1, String value2) {
            addCriterion("insurer_certificate between", value1, value2, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andInsurerCertificateNotBetween(String value1, String value2) {
            addCriterion("insurer_certificate not between", value1, value2, "insurerCertificate");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceIsNull() {
            addCriterion("autobytel_invoice is null");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceIsNotNull() {
            addCriterion("autobytel_invoice is not null");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceEqualTo(String value) {
            addCriterion("autobytel_invoice =", value, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceNotEqualTo(String value) {
            addCriterion("autobytel_invoice <>", value, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceGreaterThan(String value) {
            addCriterion("autobytel_invoice >", value, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceGreaterThanOrEqualTo(String value) {
            addCriterion("autobytel_invoice >=", value, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceLessThan(String value) {
            addCriterion("autobytel_invoice <", value, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceLessThanOrEqualTo(String value) {
            addCriterion("autobytel_invoice <=", value, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceLike(String value) {
            addCriterion("autobytel_invoice like", value, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceNotLike(String value) {
            addCriterion("autobytel_invoice not like", value, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceIn(List<String> values) {
            addCriterion("autobytel_invoice in", values, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceNotIn(List<String> values) {
            addCriterion("autobytel_invoice not in", values, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceBetween(String value1, String value2) {
            addCriterion("autobytel_invoice between", value1, value2, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andAutobytelInvoiceNotBetween(String value1, String value2) {
            addCriterion("autobytel_invoice not between", value1, value2, "autobytelInvoice");
            return (Criteria) this;
        }

        public Criteria andCarCertificateIsNull() {
            addCriterion("car_certificate is null");
            return (Criteria) this;
        }

        public Criteria andCarCertificateIsNotNull() {
            addCriterion("car_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andCarCertificateEqualTo(String value) {
            addCriterion("car_certificate =", value, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andCarCertificateNotEqualTo(String value) {
            addCriterion("car_certificate <>", value, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andCarCertificateGreaterThan(String value) {
            addCriterion("car_certificate >", value, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andCarCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("car_certificate >=", value, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andCarCertificateLessThan(String value) {
            addCriterion("car_certificate <", value, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andCarCertificateLessThanOrEqualTo(String value) {
            addCriterion("car_certificate <=", value, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andCarCertificateLike(String value) {
            addCriterion("car_certificate like", value, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andCarCertificateNotLike(String value) {
            addCriterion("car_certificate not like", value, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andCarCertificateIn(List<String> values) {
            addCriterion("car_certificate in", values, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andCarCertificateNotIn(List<String> values) {
            addCriterion("car_certificate not in", values, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andCarCertificateBetween(String value1, String value2) {
            addCriterion("car_certificate between", value1, value2, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andCarCertificateNotBetween(String value1, String value2) {
            addCriterion("car_certificate not between", value1, value2, "carCertificate");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesIsNull() {
            addCriterion("additional_prices is null");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesIsNotNull() {
            addCriterion("additional_prices is not null");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesEqualTo(String value) {
            addCriterion("additional_prices =", value, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesNotEqualTo(String value) {
            addCriterion("additional_prices <>", value, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesGreaterThan(String value) {
            addCriterion("additional_prices >", value, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesGreaterThanOrEqualTo(String value) {
            addCriterion("additional_prices >=", value, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesLessThan(String value) {
            addCriterion("additional_prices <", value, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesLessThanOrEqualTo(String value) {
            addCriterion("additional_prices <=", value, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesLike(String value) {
            addCriterion("additional_prices like", value, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesNotLike(String value) {
            addCriterion("additional_prices not like", value, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesIn(List<String> values) {
            addCriterion("additional_prices in", values, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesNotIn(List<String> values) {
            addCriterion("additional_prices not in", values, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesBetween(String value1, String value2) {
            addCriterion("additional_prices between", value1, value2, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andAdditionalPricesNotBetween(String value1, String value2) {
            addCriterion("additional_prices not between", value1, value2, "additionalPrices");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBIsNull() {
            addCriterion("travel_license_b is null");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBIsNotNull() {
            addCriterion("travel_license_b is not null");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBEqualTo(String value) {
            addCriterion("travel_license_b =", value, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBNotEqualTo(String value) {
            addCriterion("travel_license_b <>", value, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBGreaterThan(String value) {
            addCriterion("travel_license_b >", value, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBGreaterThanOrEqualTo(String value) {
            addCriterion("travel_license_b >=", value, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBLessThan(String value) {
            addCriterion("travel_license_b <", value, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBLessThanOrEqualTo(String value) {
            addCriterion("travel_license_b <=", value, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBLike(String value) {
            addCriterion("travel_license_b like", value, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBNotLike(String value) {
            addCriterion("travel_license_b not like", value, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBIn(List<String> values) {
            addCriterion("travel_license_b in", values, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBNotIn(List<String> values) {
            addCriterion("travel_license_b not in", values, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBBetween(String value1, String value2) {
            addCriterion("travel_license_b between", value1, value2, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andTravelLicenseBNotBetween(String value1, String value2) {
            addCriterion("travel_license_b not between", value1, value2, "travelLicenseB");
            return (Criteria) this;
        }

        public Criteria andClaimIsNull() {
            addCriterion("claim is null");
            return (Criteria) this;
        }

        public Criteria andClaimIsNotNull() {
            addCriterion("claim is not null");
            return (Criteria) this;
        }

        public Criteria andClaimEqualTo(String value) {
            addCriterion("claim =", value, "claim");
            return (Criteria) this;
        }

        public Criteria andClaimNotEqualTo(String value) {
            addCriterion("claim <>", value, "claim");
            return (Criteria) this;
        }

        public Criteria andClaimGreaterThan(String value) {
            addCriterion("claim >", value, "claim");
            return (Criteria) this;
        }

        public Criteria andClaimGreaterThanOrEqualTo(String value) {
            addCriterion("claim >=", value, "claim");
            return (Criteria) this;
        }

        public Criteria andClaimLessThan(String value) {
            addCriterion("claim <", value, "claim");
            return (Criteria) this;
        }

        public Criteria andClaimLessThanOrEqualTo(String value) {
            addCriterion("claim <=", value, "claim");
            return (Criteria) this;
        }

        public Criteria andClaimLike(String value) {
            addCriterion("claim like", value, "claim");
            return (Criteria) this;
        }

        public Criteria andClaimNotLike(String value) {
            addCriterion("claim not like", value, "claim");
            return (Criteria) this;
        }

        public Criteria andClaimIn(List<String> values) {
            addCriterion("claim in", values, "claim");
            return (Criteria) this;
        }

        public Criteria andClaimNotIn(List<String> values) {
            addCriterion("claim not in", values, "claim");
            return (Criteria) this;
        }

        public Criteria andClaimBetween(String value1, String value2) {
            addCriterion("claim between", value1, value2, "claim");
            return (Criteria) this;
        }

        public Criteria andClaimNotBetween(String value1, String value2) {
            addCriterion("claim not between", value1, value2, "claim");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesIsNull() {
            addCriterion("salvage_prices is null");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesIsNotNull() {
            addCriterion("salvage_prices is not null");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesEqualTo(String value) {
            addCriterion("salvage_prices =", value, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesNotEqualTo(String value) {
            addCriterion("salvage_prices <>", value, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesGreaterThan(String value) {
            addCriterion("salvage_prices >", value, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesGreaterThanOrEqualTo(String value) {
            addCriterion("salvage_prices >=", value, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesLessThan(String value) {
            addCriterion("salvage_prices <", value, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesLessThanOrEqualTo(String value) {
            addCriterion("salvage_prices <=", value, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesLike(String value) {
            addCriterion("salvage_prices like", value, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesNotLike(String value) {
            addCriterion("salvage_prices not like", value, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesIn(List<String> values) {
            addCriterion("salvage_prices in", values, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesNotIn(List<String> values) {
            addCriterion("salvage_prices not in", values, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesBetween(String value1, String value2) {
            addCriterion("salvage_prices between", value1, value2, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andSalvagePricesNotBetween(String value1, String value2) {
            addCriterion("salvage_prices not between", value1, value2, "salvagePrices");
            return (Criteria) this;
        }

        public Criteria andReparationMediationIsNull() {
            addCriterion("reparation_mediation is null");
            return (Criteria) this;
        }

        public Criteria andReparationMediationIsNotNull() {
            addCriterion("reparation_mediation is not null");
            return (Criteria) this;
        }

        public Criteria andReparationMediationEqualTo(String value) {
            addCriterion("reparation_mediation =", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationNotEqualTo(String value) {
            addCriterion("reparation_mediation <>", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationGreaterThan(String value) {
            addCriterion("reparation_mediation >", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationGreaterThanOrEqualTo(String value) {
            addCriterion("reparation_mediation >=", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationLessThan(String value) {
            addCriterion("reparation_mediation <", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationLessThanOrEqualTo(String value) {
            addCriterion("reparation_mediation <=", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationLike(String value) {
            addCriterion("reparation_mediation like", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationNotLike(String value) {
            addCriterion("reparation_mediation not like", value, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationIn(List<String> values) {
            addCriterion("reparation_mediation in", values, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationNotIn(List<String> values) {
            addCriterion("reparation_mediation not in", values, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationBetween(String value1, String value2) {
            addCriterion("reparation_mediation between", value1, value2, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andReparationMediationNotBetween(String value1, String value2) {
            addCriterion("reparation_mediation not between", value1, value2, "reparationMediation");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateIsNull() {
            addCriterion("accident_certificate is null");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateIsNotNull() {
            addCriterion("accident_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateEqualTo(String value) {
            addCriterion("accident_certificate =", value, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateNotEqualTo(String value) {
            addCriterion("accident_certificate <>", value, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateGreaterThan(String value) {
            addCriterion("accident_certificate >", value, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("accident_certificate >=", value, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateLessThan(String value) {
            addCriterion("accident_certificate <", value, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateLessThanOrEqualTo(String value) {
            addCriterion("accident_certificate <=", value, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateLike(String value) {
            addCriterion("accident_certificate like", value, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateNotLike(String value) {
            addCriterion("accident_certificate not like", value, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateIn(List<String> values) {
            addCriterion("accident_certificate in", values, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateNotIn(List<String> values) {
            addCriterion("accident_certificate not in", values, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateBetween(String value1, String value2) {
            addCriterion("accident_certificate between", value1, value2, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andAccidentCertificateNotBetween(String value1, String value2) {
            addCriterion("accident_certificate not between", value1, value2, "accidentCertificate");
            return (Criteria) this;
        }

        public Criteria andBack1IsNull() {
            addCriterion("back1 is null");
            return (Criteria) this;
        }

        public Criteria andBack1IsNotNull() {
            addCriterion("back1 is not null");
            return (Criteria) this;
        }

        public Criteria andBack1EqualTo(String value) {
            addCriterion("back1 =", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotEqualTo(String value) {
            addCriterion("back1 <>", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThan(String value) {
            addCriterion("back1 >", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThanOrEqualTo(String value) {
            addCriterion("back1 >=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThan(String value) {
            addCriterion("back1 <", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThanOrEqualTo(String value) {
            addCriterion("back1 <=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Like(String value) {
            addCriterion("back1 like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotLike(String value) {
            addCriterion("back1 not like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1In(List<String> values) {
            addCriterion("back1 in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotIn(List<String> values) {
            addCriterion("back1 not in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Between(String value1, String value2) {
            addCriterion("back1 between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotBetween(String value1, String value2) {
            addCriterion("back1 not between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack2IsNull() {
            addCriterion("back2 is null");
            return (Criteria) this;
        }

        public Criteria andBack2IsNotNull() {
            addCriterion("back2 is not null");
            return (Criteria) this;
        }

        public Criteria andBack2EqualTo(String value) {
            addCriterion("back2 =", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotEqualTo(String value) {
            addCriterion("back2 <>", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThan(String value) {
            addCriterion("back2 >", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThanOrEqualTo(String value) {
            addCriterion("back2 >=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThan(String value) {
            addCriterion("back2 <", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThanOrEqualTo(String value) {
            addCriterion("back2 <=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Like(String value) {
            addCriterion("back2 like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotLike(String value) {
            addCriterion("back2 not like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2In(List<String> values) {
            addCriterion("back2 in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotIn(List<String> values) {
            addCriterion("back2 not in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Between(String value1, String value2) {
            addCriterion("back2 between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotBetween(String value1, String value2) {
            addCriterion("back2 not between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack3IsNull() {
            addCriterion("back3 is null");
            return (Criteria) this;
        }

        public Criteria andBack3IsNotNull() {
            addCriterion("back3 is not null");
            return (Criteria) this;
        }

        public Criteria andBack3EqualTo(String value) {
            addCriterion("back3 =", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotEqualTo(String value) {
            addCriterion("back3 <>", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThan(String value) {
            addCriterion("back3 >", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThanOrEqualTo(String value) {
            addCriterion("back3 >=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThan(String value) {
            addCriterion("back3 <", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThanOrEqualTo(String value) {
            addCriterion("back3 <=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Like(String value) {
            addCriterion("back3 like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotLike(String value) {
            addCriterion("back3 not like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3In(List<String> values) {
            addCriterion("back3 in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotIn(List<String> values) {
            addCriterion("back3 not in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Between(String value1, String value2) {
            addCriterion("back3 between", value1, value2, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotBetween(String value1, String value2) {
            addCriterion("back3 not between", value1, value2, "back3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}