package pojo;

public class DataMust {
    private Integer reportNo;

    private Integer tableId;

    private String policyId;

    private String driverLicense;

    private String insurerCertificate;

    private String autobytelInvoice;

    private String carCertificate;

    private String additionalPrices;

    private String travelLicenseB;

    private String claim;

    private String salvagePrices;

    private String reparationMediation;

    private String accidentCertificate;

    private String back1;

    private String back2;

    private String back3;

    public Integer getReportNo() {
        return reportNo;
    }

    public void setReportNo(Integer reportNo) {
        this.reportNo = reportNo;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId == null ? null : policyId.trim();
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense == null ? null : driverLicense.trim();
    }

    public String getInsurerCertificate() {
        return insurerCertificate;
    }

    public void setInsurerCertificate(String insurerCertificate) {
        this.insurerCertificate = insurerCertificate == null ? null : insurerCertificate.trim();
    }

    public String getAutobytelInvoice() {
        return autobytelInvoice;
    }

    public void setAutobytelInvoice(String autobytelInvoice) {
        this.autobytelInvoice = autobytelInvoice == null ? null : autobytelInvoice.trim();
    }

    public String getCarCertificate() {
        return carCertificate;
    }

    public void setCarCertificate(String carCertificate) {
        this.carCertificate = carCertificate == null ? null : carCertificate.trim();
    }

    public String getAdditionalPrices() {
        return additionalPrices;
    }

    public void setAdditionalPrices(String additionalPrices) {
        this.additionalPrices = additionalPrices == null ? null : additionalPrices.trim();
    }

    public String getTravelLicenseB() {
        return travelLicenseB;
    }

    public void setTravelLicenseB(String travelLicenseB) {
        this.travelLicenseB = travelLicenseB == null ? null : travelLicenseB.trim();
    }

    public String getClaim() {
        return claim;
    }

    public void setClaim(String claim) {
        this.claim = claim == null ? null : claim.trim();
    }

    public String getSalvagePrices() {
        return salvagePrices;
    }

    public void setSalvagePrices(String salvagePrices) {
        this.salvagePrices = salvagePrices == null ? null : salvagePrices.trim();
    }

    public String getReparationMediation() {
        return reparationMediation;
    }

    public void setReparationMediation(String reparationMediation) {
        this.reparationMediation = reparationMediation == null ? null : reparationMediation.trim();
    }

    public String getAccidentCertificate() {
        return accidentCertificate;
    }

    public void setAccidentCertificate(String accidentCertificate) {
        this.accidentCertificate = accidentCertificate == null ? null : accidentCertificate.trim();
    }

    public String getBack1() {
        return back1;
    }

    public void setBack1(String back1) {
        this.back1 = back1 == null ? null : back1.trim();
    }

    public String getBack2() {
        return back2;
    }

    public void setBack2(String back2) {
        this.back2 = back2 == null ? null : back2.trim();
    }

    public String getBack3() {
        return back3;
    }

    public void setBack3(String back3) {
        this.back3 = back3 == null ? null : back3.trim();
    }
}