package pojo;

public class CarMessage {
    private String carNo;

    private Integer reportNo;

    private Integer tableId;

    private String policyId;

    private String reportName;

    private Integer surveyLook;

    private Integer surveyCarone;

    private Integer surveyCartwo;

    private Integer surveyPeople;

    private Integer surveyNow;

    private String back1;

    private String back2;

    private String back3;

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo == null ? null : carNo.trim();
    }

    public Integer getReportNo() {
        return reportNo;
    }

    public void setReportNo(Integer reportNo) {
        this.reportNo = reportNo;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId == null ? null : policyId.trim();
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName == null ? null : reportName.trim();
    }

    public Integer getSurveyLook() {
        return surveyLook;
    }

    public void setSurveyLook(Integer surveyLook) {
        this.surveyLook = surveyLook;
    }

    public Integer getSurveyCarone() {
        return surveyCarone;
    }

    public void setSurveyCarone(Integer surveyCarone) {
        this.surveyCarone = surveyCarone;
    }

    public Integer getSurveyCartwo() {
        return surveyCartwo;
    }

    public void setSurveyCartwo(Integer surveyCartwo) {
        this.surveyCartwo = surveyCartwo;
    }

    public Integer getSurveyPeople() {
        return surveyPeople;
    }

    public void setSurveyPeople(Integer surveyPeople) {
        this.surveyPeople = surveyPeople;
    }

    public Integer getSurveyNow() {
        return surveyNow;
    }

    public void setSurveyNow(Integer surveyNow) {
        this.surveyNow = surveyNow;
    }

    public String getBack1() {
        return back1;
    }

    public void setBack1(String back1) {
        this.back1 = back1 == null ? null : back1.trim();
    }

    public String getBack2() {
        return back2;
    }

    public void setBack2(String back2) {
        this.back2 = back2 == null ? null : back2.trim();
    }

    public String getBack3() {
        return back3;
    }

    public void setBack3(String back3) {
        this.back3 = back3 == null ? null : back3.trim();
    }
}