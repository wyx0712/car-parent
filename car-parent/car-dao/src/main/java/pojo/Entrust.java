package pojo;

public class Entrust {
    private Integer entrustId;

    private Integer tableId;

    private String policyId;

    private Integer reportNo;

    private String applicant;

    private String entrustingContent;

    private String dataProcessing;

    private String authorizedAgency;

    private String committalCharge;

    private String back1;

    private String back2;

    private String back3;

    public Integer getEntrustId() {
        return entrustId;
    }

    public void setEntrustId(Integer entrustId) {
        this.entrustId = entrustId;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId == null ? null : policyId.trim();
    }

    public Integer getReportNo() {
        return reportNo;
    }

    public void setReportNo(Integer reportNo) {
        this.reportNo = reportNo;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant == null ? null : applicant.trim();
    }

    public String getEntrustingContent() {
        return entrustingContent;
    }

    public void setEntrustingContent(String entrustingContent) {
        this.entrustingContent = entrustingContent == null ? null : entrustingContent.trim();
    }

    public String getDataProcessing() {
        return dataProcessing;
    }

    public void setDataProcessing(String dataProcessing) {
        this.dataProcessing = dataProcessing == null ? null : dataProcessing.trim();
    }

    public String getAuthorizedAgency() {
        return authorizedAgency;
    }

    public void setAuthorizedAgency(String authorizedAgency) {
        this.authorizedAgency = authorizedAgency == null ? null : authorizedAgency.trim();
    }

    public String getCommittalCharge() {
        return committalCharge;
    }

    public void setCommittalCharge(String committalCharge) {
        this.committalCharge = committalCharge == null ? null : committalCharge.trim();
    }

    public String getBack1() {
        return back1;
    }

    public void setBack1(String back1) {
        this.back1 = back1 == null ? null : back1.trim();
    }

    public String getBack2() {
        return back2;
    }

    public void setBack2(String back2) {
        this.back2 = back2 == null ? null : back2.trim();
    }

    public String getBack3() {
        return back3;
    }

    public void setBack3(String back3) {
        this.back3 = back3 == null ? null : back3.trim();
    }
}