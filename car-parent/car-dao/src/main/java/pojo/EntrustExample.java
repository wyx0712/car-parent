package pojo;

import java.util.ArrayList;
import java.util.List;

public class EntrustExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public EntrustExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEntrustIdIsNull() {
            addCriterion("entrust_id is null");
            return (Criteria) this;
        }

        public Criteria andEntrustIdIsNotNull() {
            addCriterion("entrust_id is not null");
            return (Criteria) this;
        }

        public Criteria andEntrustIdEqualTo(Integer value) {
            addCriterion("entrust_id =", value, "entrustId");
            return (Criteria) this;
        }

        public Criteria andEntrustIdNotEqualTo(Integer value) {
            addCriterion("entrust_id <>", value, "entrustId");
            return (Criteria) this;
        }

        public Criteria andEntrustIdGreaterThan(Integer value) {
            addCriterion("entrust_id >", value, "entrustId");
            return (Criteria) this;
        }

        public Criteria andEntrustIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("entrust_id >=", value, "entrustId");
            return (Criteria) this;
        }

        public Criteria andEntrustIdLessThan(Integer value) {
            addCriterion("entrust_id <", value, "entrustId");
            return (Criteria) this;
        }

        public Criteria andEntrustIdLessThanOrEqualTo(Integer value) {
            addCriterion("entrust_id <=", value, "entrustId");
            return (Criteria) this;
        }

        public Criteria andEntrustIdIn(List<Integer> values) {
            addCriterion("entrust_id in", values, "entrustId");
            return (Criteria) this;
        }

        public Criteria andEntrustIdNotIn(List<Integer> values) {
            addCriterion("entrust_id not in", values, "entrustId");
            return (Criteria) this;
        }

        public Criteria andEntrustIdBetween(Integer value1, Integer value2) {
            addCriterion("entrust_id between", value1, value2, "entrustId");
            return (Criteria) this;
        }

        public Criteria andEntrustIdNotBetween(Integer value1, Integer value2) {
            addCriterion("entrust_id not between", value1, value2, "entrustId");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNull() {
            addCriterion("table_id is null");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNotNull() {
            addCriterion("table_id is not null");
            return (Criteria) this;
        }

        public Criteria andTableIdEqualTo(Integer value) {
            addCriterion("table_id =", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotEqualTo(Integer value) {
            addCriterion("table_id <>", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThan(Integer value) {
            addCriterion("table_id >", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("table_id >=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThan(Integer value) {
            addCriterion("table_id <", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThanOrEqualTo(Integer value) {
            addCriterion("table_id <=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdIn(List<Integer> values) {
            addCriterion("table_id in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotIn(List<Integer> values) {
            addCriterion("table_id not in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdBetween(Integer value1, Integer value2) {
            addCriterion("table_id between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotBetween(Integer value1, Integer value2) {
            addCriterion("table_id not between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdIsNull() {
            addCriterion("policy_id is null");
            return (Criteria) this;
        }

        public Criteria andPolicyIdIsNotNull() {
            addCriterion("policy_id is not null");
            return (Criteria) this;
        }

        public Criteria andPolicyIdEqualTo(String value) {
            addCriterion("policy_id =", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotEqualTo(String value) {
            addCriterion("policy_id <>", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdGreaterThan(String value) {
            addCriterion("policy_id >", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdGreaterThanOrEqualTo(String value) {
            addCriterion("policy_id >=", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLessThan(String value) {
            addCriterion("policy_id <", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLessThanOrEqualTo(String value) {
            addCriterion("policy_id <=", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLike(String value) {
            addCriterion("policy_id like", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotLike(String value) {
            addCriterion("policy_id not like", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdIn(List<String> values) {
            addCriterion("policy_id in", values, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotIn(List<String> values) {
            addCriterion("policy_id not in", values, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdBetween(String value1, String value2) {
            addCriterion("policy_id between", value1, value2, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotBetween(String value1, String value2) {
            addCriterion("policy_id not between", value1, value2, "policyId");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNull() {
            addCriterion("report_no is null");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNotNull() {
            addCriterion("report_no is not null");
            return (Criteria) this;
        }

        public Criteria andReportNoEqualTo(Integer value) {
            addCriterion("report_no =", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotEqualTo(Integer value) {
            addCriterion("report_no <>", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThan(Integer value) {
            addCriterion("report_no >", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("report_no >=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThan(Integer value) {
            addCriterion("report_no <", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThanOrEqualTo(Integer value) {
            addCriterion("report_no <=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoIn(List<Integer> values) {
            addCriterion("report_no in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotIn(List<Integer> values) {
            addCriterion("report_no not in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoBetween(Integer value1, Integer value2) {
            addCriterion("report_no between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotBetween(Integer value1, Integer value2) {
            addCriterion("report_no not between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andApplicantIsNull() {
            addCriterion("applicant is null");
            return (Criteria) this;
        }

        public Criteria andApplicantIsNotNull() {
            addCriterion("applicant is not null");
            return (Criteria) this;
        }

        public Criteria andApplicantEqualTo(String value) {
            addCriterion("applicant =", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantNotEqualTo(String value) {
            addCriterion("applicant <>", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantGreaterThan(String value) {
            addCriterion("applicant >", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantGreaterThanOrEqualTo(String value) {
            addCriterion("applicant >=", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantLessThan(String value) {
            addCriterion("applicant <", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantLessThanOrEqualTo(String value) {
            addCriterion("applicant <=", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantLike(String value) {
            addCriterion("applicant like", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantNotLike(String value) {
            addCriterion("applicant not like", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantIn(List<String> values) {
            addCriterion("applicant in", values, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantNotIn(List<String> values) {
            addCriterion("applicant not in", values, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantBetween(String value1, String value2) {
            addCriterion("applicant between", value1, value2, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantNotBetween(String value1, String value2) {
            addCriterion("applicant not between", value1, value2, "applicant");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentIsNull() {
            addCriterion("entrusting_content is null");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentIsNotNull() {
            addCriterion("entrusting_content is not null");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentEqualTo(String value) {
            addCriterion("entrusting_content =", value, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentNotEqualTo(String value) {
            addCriterion("entrusting_content <>", value, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentGreaterThan(String value) {
            addCriterion("entrusting_content >", value, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentGreaterThanOrEqualTo(String value) {
            addCriterion("entrusting_content >=", value, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentLessThan(String value) {
            addCriterion("entrusting_content <", value, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentLessThanOrEqualTo(String value) {
            addCriterion("entrusting_content <=", value, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentLike(String value) {
            addCriterion("entrusting_content like", value, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentNotLike(String value) {
            addCriterion("entrusting_content not like", value, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentIn(List<String> values) {
            addCriterion("entrusting_content in", values, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentNotIn(List<String> values) {
            addCriterion("entrusting_content not in", values, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentBetween(String value1, String value2) {
            addCriterion("entrusting_content between", value1, value2, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andEntrustingContentNotBetween(String value1, String value2) {
            addCriterion("entrusting_content not between", value1, value2, "entrustingContent");
            return (Criteria) this;
        }

        public Criteria andDataProcessingIsNull() {
            addCriterion("data_processing is null");
            return (Criteria) this;
        }

        public Criteria andDataProcessingIsNotNull() {
            addCriterion("data_processing is not null");
            return (Criteria) this;
        }

        public Criteria andDataProcessingEqualTo(String value) {
            addCriterion("data_processing =", value, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andDataProcessingNotEqualTo(String value) {
            addCriterion("data_processing <>", value, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andDataProcessingGreaterThan(String value) {
            addCriterion("data_processing >", value, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andDataProcessingGreaterThanOrEqualTo(String value) {
            addCriterion("data_processing >=", value, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andDataProcessingLessThan(String value) {
            addCriterion("data_processing <", value, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andDataProcessingLessThanOrEqualTo(String value) {
            addCriterion("data_processing <=", value, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andDataProcessingLike(String value) {
            addCriterion("data_processing like", value, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andDataProcessingNotLike(String value) {
            addCriterion("data_processing not like", value, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andDataProcessingIn(List<String> values) {
            addCriterion("data_processing in", values, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andDataProcessingNotIn(List<String> values) {
            addCriterion("data_processing not in", values, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andDataProcessingBetween(String value1, String value2) {
            addCriterion("data_processing between", value1, value2, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andDataProcessingNotBetween(String value1, String value2) {
            addCriterion("data_processing not between", value1, value2, "dataProcessing");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyIsNull() {
            addCriterion("authorized_agency is null");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyIsNotNull() {
            addCriterion("authorized_agency is not null");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyEqualTo(String value) {
            addCriterion("authorized_agency =", value, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyNotEqualTo(String value) {
            addCriterion("authorized_agency <>", value, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyGreaterThan(String value) {
            addCriterion("authorized_agency >", value, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyGreaterThanOrEqualTo(String value) {
            addCriterion("authorized_agency >=", value, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyLessThan(String value) {
            addCriterion("authorized_agency <", value, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyLessThanOrEqualTo(String value) {
            addCriterion("authorized_agency <=", value, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyLike(String value) {
            addCriterion("authorized_agency like", value, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyNotLike(String value) {
            addCriterion("authorized_agency not like", value, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyIn(List<String> values) {
            addCriterion("authorized_agency in", values, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyNotIn(List<String> values) {
            addCriterion("authorized_agency not in", values, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyBetween(String value1, String value2) {
            addCriterion("authorized_agency between", value1, value2, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andAuthorizedAgencyNotBetween(String value1, String value2) {
            addCriterion("authorized_agency not between", value1, value2, "authorizedAgency");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeIsNull() {
            addCriterion("committal_charge is null");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeIsNotNull() {
            addCriterion("committal_charge is not null");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeEqualTo(String value) {
            addCriterion("committal_charge =", value, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeNotEqualTo(String value) {
            addCriterion("committal_charge <>", value, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeGreaterThan(String value) {
            addCriterion("committal_charge >", value, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeGreaterThanOrEqualTo(String value) {
            addCriterion("committal_charge >=", value, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeLessThan(String value) {
            addCriterion("committal_charge <", value, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeLessThanOrEqualTo(String value) {
            addCriterion("committal_charge <=", value, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeLike(String value) {
            addCriterion("committal_charge like", value, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeNotLike(String value) {
            addCriterion("committal_charge not like", value, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeIn(List<String> values) {
            addCriterion("committal_charge in", values, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeNotIn(List<String> values) {
            addCriterion("committal_charge not in", values, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeBetween(String value1, String value2) {
            addCriterion("committal_charge between", value1, value2, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andCommittalChargeNotBetween(String value1, String value2) {
            addCriterion("committal_charge not between", value1, value2, "committalCharge");
            return (Criteria) this;
        }

        public Criteria andBack1IsNull() {
            addCriterion("back1 is null");
            return (Criteria) this;
        }

        public Criteria andBack1IsNotNull() {
            addCriterion("back1 is not null");
            return (Criteria) this;
        }

        public Criteria andBack1EqualTo(String value) {
            addCriterion("back1 =", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotEqualTo(String value) {
            addCriterion("back1 <>", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThan(String value) {
            addCriterion("back1 >", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThanOrEqualTo(String value) {
            addCriterion("back1 >=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThan(String value) {
            addCriterion("back1 <", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThanOrEqualTo(String value) {
            addCriterion("back1 <=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Like(String value) {
            addCriterion("back1 like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotLike(String value) {
            addCriterion("back1 not like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1In(List<String> values) {
            addCriterion("back1 in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotIn(List<String> values) {
            addCriterion("back1 not in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Between(String value1, String value2) {
            addCriterion("back1 between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotBetween(String value1, String value2) {
            addCriterion("back1 not between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack2IsNull() {
            addCriterion("back2 is null");
            return (Criteria) this;
        }

        public Criteria andBack2IsNotNull() {
            addCriterion("back2 is not null");
            return (Criteria) this;
        }

        public Criteria andBack2EqualTo(String value) {
            addCriterion("back2 =", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotEqualTo(String value) {
            addCriterion("back2 <>", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThan(String value) {
            addCriterion("back2 >", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThanOrEqualTo(String value) {
            addCriterion("back2 >=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThan(String value) {
            addCriterion("back2 <", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThanOrEqualTo(String value) {
            addCriterion("back2 <=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Like(String value) {
            addCriterion("back2 like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotLike(String value) {
            addCriterion("back2 not like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2In(List<String> values) {
            addCriterion("back2 in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotIn(List<String> values) {
            addCriterion("back2 not in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Between(String value1, String value2) {
            addCriterion("back2 between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotBetween(String value1, String value2) {
            addCriterion("back2 not between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack3IsNull() {
            addCriterion("back3 is null");
            return (Criteria) this;
        }

        public Criteria andBack3IsNotNull() {
            addCriterion("back3 is not null");
            return (Criteria) this;
        }

        public Criteria andBack3EqualTo(String value) {
            addCriterion("back3 =", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotEqualTo(String value) {
            addCriterion("back3 <>", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThan(String value) {
            addCriterion("back3 >", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThanOrEqualTo(String value) {
            addCriterion("back3 >=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThan(String value) {
            addCriterion("back3 <", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThanOrEqualTo(String value) {
            addCriterion("back3 <=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Like(String value) {
            addCriterion("back3 like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotLike(String value) {
            addCriterion("back3 not like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3In(List<String> values) {
            addCriterion("back3 in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotIn(List<String> values) {
            addCriterion("back3 not in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Between(String value1, String value2) {
            addCriterion("back3 between", value1, value2, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotBetween(String value1, String value2) {
            addCriterion("back3 not between", value1, value2, "back3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}