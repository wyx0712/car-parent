package pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Lossinfo {
    private Integer lossId;//定损表主键
    
    private Integer tableId;//表名（默认值3002不要改）

    private Integer reportNo;//报案号

    private String repairShop;//修理厂名

    private String repairType;//修理厂类型

    private String carOwner;//车主

    private String carNo;//车牌号

    private Double carMoney;//定损总价

    private String userName;//定损员
    @DateTimeFormat(pattern="yyyy-MM-ddHH:mm:ss")  
    private Date lossTime;//定损时间

    private String lossOpinion;//定损意见

    private Integer partsId;//配件id 与配件表关联

    private Date createTime;//该数据创建时间

    private Date updateTime;//数据修改时间

    private Integer loadId;//操作人员id

    private String sign;//本车or三车1为本车2为三车

    private String returnIdea;//退档意见

    private String back1;//备用1

    private String back2;//备用2

    private String back3;//备用3

    public Integer getLossId() {
        return lossId;
    }

    public void setLossId(Integer lossId) {
        this.lossId = lossId;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public Integer getReportNo() {
        return reportNo;
    }

    public void setReportNo(Integer reportNo) {
        this.reportNo = reportNo;
    }

    public String getRepairShop() {
        return repairShop;
    }

    public void setRepairShop(String repairShop) {
        this.repairShop = repairShop == null ? null : repairShop.trim();
    }

    public String getRepairType() {
        return repairType;
    }

    public void setRepairType(String repairType) {
        this.repairType = repairType == null ? null : repairType.trim();
    }

    public String getCarOwner() {
        return carOwner;
    }

    public void setCarOwner(String carOwner) {
        this.carOwner = carOwner == null ? null : carOwner.trim();
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo == null ? null : carNo.trim();
    }

    public Double getCarMoney() {
        return carMoney;
    }

    public void setCarMoney(Double carMoney) {
        this.carMoney = carMoney;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Date getLossTime() {
        return lossTime;
    }

    public void setLossTime(Date lossTime) {
        this.lossTime = lossTime;
    }

    public String getLossOpinion() {
        return lossOpinion;
    }

    public void setLossOpinion(String lossOpinion) {
        this.lossOpinion = lossOpinion == null ? null : lossOpinion.trim();
    }

    public Integer getPartsId() {
        return partsId;
    }

    public void setPartsId(Integer partsId) {
        this.partsId = partsId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getLoadId() {
        return loadId;
    }

    public void setLoadId(Integer loadId) {
        this.loadId = loadId;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign == null ? null : sign.trim();
    }

    public String getReturnIdea() {
        return returnIdea;
    }

    public void setReturnIdea(String returnIdea) {
        this.returnIdea = returnIdea == null ? null : returnIdea.trim();
    }

    public String getBack1() {
        return back1;
    }

    public void setBack1(String back1) {
        this.back1 = back1 == null ? null : back1.trim();
    }

    public String getBack2() {
        return back2;
    }

    public void setBack2(String back2) {
        this.back2 = back2 == null ? null : back2.trim();
    }

    public String getBack3() {
        return back3;
    }

    public void setBack3(String back3) {
        this.back3 = back3 == null ? null : back3.trim();
    }
}