package pojo;

import java.util.ArrayList;
import java.util.List;

public class CarMessageExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CarMessageExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCarNoIsNull() {
            addCriterion("car_no is null");
            return (Criteria) this;
        }

        public Criteria andCarNoIsNotNull() {
            addCriterion("car_no is not null");
            return (Criteria) this;
        }

        public Criteria andCarNoEqualTo(String value) {
            addCriterion("car_no =", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotEqualTo(String value) {
            addCriterion("car_no <>", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoGreaterThan(String value) {
            addCriterion("car_no >", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoGreaterThanOrEqualTo(String value) {
            addCriterion("car_no >=", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLessThan(String value) {
            addCriterion("car_no <", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLessThanOrEqualTo(String value) {
            addCriterion("car_no <=", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLike(String value) {
            addCriterion("car_no like", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotLike(String value) {
            addCriterion("car_no not like", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoIn(List<String> values) {
            addCriterion("car_no in", values, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotIn(List<String> values) {
            addCriterion("car_no not in", values, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoBetween(String value1, String value2) {
            addCriterion("car_no between", value1, value2, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotBetween(String value1, String value2) {
            addCriterion("car_no not between", value1, value2, "carNo");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNull() {
            addCriterion("report_no is null");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNotNull() {
            addCriterion("report_no is not null");
            return (Criteria) this;
        }

        public Criteria andReportNoEqualTo(Integer value) {
            addCriterion("report_no =", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotEqualTo(Integer value) {
            addCriterion("report_no <>", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThan(Integer value) {
            addCriterion("report_no >", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("report_no >=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThan(Integer value) {
            addCriterion("report_no <", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThanOrEqualTo(Integer value) {
            addCriterion("report_no <=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoIn(List<Integer> values) {
            addCriterion("report_no in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotIn(List<Integer> values) {
            addCriterion("report_no not in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoBetween(Integer value1, Integer value2) {
            addCriterion("report_no between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotBetween(Integer value1, Integer value2) {
            addCriterion("report_no not between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNull() {
            addCriterion("table_id is null");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNotNull() {
            addCriterion("table_id is not null");
            return (Criteria) this;
        }

        public Criteria andTableIdEqualTo(Integer value) {
            addCriterion("table_id =", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotEqualTo(Integer value) {
            addCriterion("table_id <>", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThan(Integer value) {
            addCriterion("table_id >", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("table_id >=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThan(Integer value) {
            addCriterion("table_id <", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThanOrEqualTo(Integer value) {
            addCriterion("table_id <=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdIn(List<Integer> values) {
            addCriterion("table_id in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotIn(List<Integer> values) {
            addCriterion("table_id not in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdBetween(Integer value1, Integer value2) {
            addCriterion("table_id between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotBetween(Integer value1, Integer value2) {
            addCriterion("table_id not between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdIsNull() {
            addCriterion("policy_id is null");
            return (Criteria) this;
        }

        public Criteria andPolicyIdIsNotNull() {
            addCriterion("policy_id is not null");
            return (Criteria) this;
        }

        public Criteria andPolicyIdEqualTo(String value) {
            addCriterion("policy_id =", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotEqualTo(String value) {
            addCriterion("policy_id <>", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdGreaterThan(String value) {
            addCriterion("policy_id >", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdGreaterThanOrEqualTo(String value) {
            addCriterion("policy_id >=", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLessThan(String value) {
            addCriterion("policy_id <", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLessThanOrEqualTo(String value) {
            addCriterion("policy_id <=", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLike(String value) {
            addCriterion("policy_id like", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotLike(String value) {
            addCriterion("policy_id not like", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdIn(List<String> values) {
            addCriterion("policy_id in", values, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotIn(List<String> values) {
            addCriterion("policy_id not in", values, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdBetween(String value1, String value2) {
            addCriterion("policy_id between", value1, value2, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotBetween(String value1, String value2) {
            addCriterion("policy_id not between", value1, value2, "policyId");
            return (Criteria) this;
        }

        public Criteria andReportNameIsNull() {
            addCriterion("report_name is null");
            return (Criteria) this;
        }

        public Criteria andReportNameIsNotNull() {
            addCriterion("report_name is not null");
            return (Criteria) this;
        }

        public Criteria andReportNameEqualTo(String value) {
            addCriterion("report_name =", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameNotEqualTo(String value) {
            addCriterion("report_name <>", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameGreaterThan(String value) {
            addCriterion("report_name >", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameGreaterThanOrEqualTo(String value) {
            addCriterion("report_name >=", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameLessThan(String value) {
            addCriterion("report_name <", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameLessThanOrEqualTo(String value) {
            addCriterion("report_name <=", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameLike(String value) {
            addCriterion("report_name like", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameNotLike(String value) {
            addCriterion("report_name not like", value, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameIn(List<String> values) {
            addCriterion("report_name in", values, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameNotIn(List<String> values) {
            addCriterion("report_name not in", values, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameBetween(String value1, String value2) {
            addCriterion("report_name between", value1, value2, "reportName");
            return (Criteria) this;
        }

        public Criteria andReportNameNotBetween(String value1, String value2) {
            addCriterion("report_name not between", value1, value2, "reportName");
            return (Criteria) this;
        }

        public Criteria andSurveyLookIsNull() {
            addCriterion("survey_look is null");
            return (Criteria) this;
        }

        public Criteria andSurveyLookIsNotNull() {
            addCriterion("survey_look is not null");
            return (Criteria) this;
        }

        public Criteria andSurveyLookEqualTo(Integer value) {
            addCriterion("survey_look =", value, "surveyLook");
            return (Criteria) this;
        }

        public Criteria andSurveyLookNotEqualTo(Integer value) {
            addCriterion("survey_look <>", value, "surveyLook");
            return (Criteria) this;
        }

        public Criteria andSurveyLookGreaterThan(Integer value) {
            addCriterion("survey_look >", value, "surveyLook");
            return (Criteria) this;
        }

        public Criteria andSurveyLookGreaterThanOrEqualTo(Integer value) {
            addCriterion("survey_look >=", value, "surveyLook");
            return (Criteria) this;
        }

        public Criteria andSurveyLookLessThan(Integer value) {
            addCriterion("survey_look <", value, "surveyLook");
            return (Criteria) this;
        }

        public Criteria andSurveyLookLessThanOrEqualTo(Integer value) {
            addCriterion("survey_look <=", value, "surveyLook");
            return (Criteria) this;
        }

        public Criteria andSurveyLookIn(List<Integer> values) {
            addCriterion("survey_look in", values, "surveyLook");
            return (Criteria) this;
        }

        public Criteria andSurveyLookNotIn(List<Integer> values) {
            addCriterion("survey_look not in", values, "surveyLook");
            return (Criteria) this;
        }

        public Criteria andSurveyLookBetween(Integer value1, Integer value2) {
            addCriterion("survey_look between", value1, value2, "surveyLook");
            return (Criteria) this;
        }

        public Criteria andSurveyLookNotBetween(Integer value1, Integer value2) {
            addCriterion("survey_look not between", value1, value2, "surveyLook");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneIsNull() {
            addCriterion("survey_carone is null");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneIsNotNull() {
            addCriterion("survey_carone is not null");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneEqualTo(Integer value) {
            addCriterion("survey_carone =", value, "surveyCarone");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneNotEqualTo(Integer value) {
            addCriterion("survey_carone <>", value, "surveyCarone");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneGreaterThan(Integer value) {
            addCriterion("survey_carone >", value, "surveyCarone");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneGreaterThanOrEqualTo(Integer value) {
            addCriterion("survey_carone >=", value, "surveyCarone");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneLessThan(Integer value) {
            addCriterion("survey_carone <", value, "surveyCarone");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneLessThanOrEqualTo(Integer value) {
            addCriterion("survey_carone <=", value, "surveyCarone");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneIn(List<Integer> values) {
            addCriterion("survey_carone in", values, "surveyCarone");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneNotIn(List<Integer> values) {
            addCriterion("survey_carone not in", values, "surveyCarone");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneBetween(Integer value1, Integer value2) {
            addCriterion("survey_carone between", value1, value2, "surveyCarone");
            return (Criteria) this;
        }

        public Criteria andSurveyCaroneNotBetween(Integer value1, Integer value2) {
            addCriterion("survey_carone not between", value1, value2, "surveyCarone");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoIsNull() {
            addCriterion("survey_cartwo is null");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoIsNotNull() {
            addCriterion("survey_cartwo is not null");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoEqualTo(Integer value) {
            addCriterion("survey_cartwo =", value, "surveyCartwo");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoNotEqualTo(Integer value) {
            addCriterion("survey_cartwo <>", value, "surveyCartwo");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoGreaterThan(Integer value) {
            addCriterion("survey_cartwo >", value, "surveyCartwo");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoGreaterThanOrEqualTo(Integer value) {
            addCriterion("survey_cartwo >=", value, "surveyCartwo");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoLessThan(Integer value) {
            addCriterion("survey_cartwo <", value, "surveyCartwo");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoLessThanOrEqualTo(Integer value) {
            addCriterion("survey_cartwo <=", value, "surveyCartwo");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoIn(List<Integer> values) {
            addCriterion("survey_cartwo in", values, "surveyCartwo");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoNotIn(List<Integer> values) {
            addCriterion("survey_cartwo not in", values, "surveyCartwo");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoBetween(Integer value1, Integer value2) {
            addCriterion("survey_cartwo between", value1, value2, "surveyCartwo");
            return (Criteria) this;
        }

        public Criteria andSurveyCartwoNotBetween(Integer value1, Integer value2) {
            addCriterion("survey_cartwo not between", value1, value2, "surveyCartwo");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleIsNull() {
            addCriterion("survey_people is null");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleIsNotNull() {
            addCriterion("survey_people is not null");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleEqualTo(Integer value) {
            addCriterion("survey_people =", value, "surveyPeople");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleNotEqualTo(Integer value) {
            addCriterion("survey_people <>", value, "surveyPeople");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleGreaterThan(Integer value) {
            addCriterion("survey_people >", value, "surveyPeople");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleGreaterThanOrEqualTo(Integer value) {
            addCriterion("survey_people >=", value, "surveyPeople");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleLessThan(Integer value) {
            addCriterion("survey_people <", value, "surveyPeople");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleLessThanOrEqualTo(Integer value) {
            addCriterion("survey_people <=", value, "surveyPeople");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleIn(List<Integer> values) {
            addCriterion("survey_people in", values, "surveyPeople");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleNotIn(List<Integer> values) {
            addCriterion("survey_people not in", values, "surveyPeople");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleBetween(Integer value1, Integer value2) {
            addCriterion("survey_people between", value1, value2, "surveyPeople");
            return (Criteria) this;
        }

        public Criteria andSurveyPeopleNotBetween(Integer value1, Integer value2) {
            addCriterion("survey_people not between", value1, value2, "surveyPeople");
            return (Criteria) this;
        }

        public Criteria andSurveyNowIsNull() {
            addCriterion("survey_now is null");
            return (Criteria) this;
        }

        public Criteria andSurveyNowIsNotNull() {
            addCriterion("survey_now is not null");
            return (Criteria) this;
        }

        public Criteria andSurveyNowEqualTo(Integer value) {
            addCriterion("survey_now =", value, "surveyNow");
            return (Criteria) this;
        }

        public Criteria andSurveyNowNotEqualTo(Integer value) {
            addCriterion("survey_now <>", value, "surveyNow");
            return (Criteria) this;
        }

        public Criteria andSurveyNowGreaterThan(Integer value) {
            addCriterion("survey_now >", value, "surveyNow");
            return (Criteria) this;
        }

        public Criteria andSurveyNowGreaterThanOrEqualTo(Integer value) {
            addCriterion("survey_now >=", value, "surveyNow");
            return (Criteria) this;
        }

        public Criteria andSurveyNowLessThan(Integer value) {
            addCriterion("survey_now <", value, "surveyNow");
            return (Criteria) this;
        }

        public Criteria andSurveyNowLessThanOrEqualTo(Integer value) {
            addCriterion("survey_now <=", value, "surveyNow");
            return (Criteria) this;
        }

        public Criteria andSurveyNowIn(List<Integer> values) {
            addCriterion("survey_now in", values, "surveyNow");
            return (Criteria) this;
        }

        public Criteria andSurveyNowNotIn(List<Integer> values) {
            addCriterion("survey_now not in", values, "surveyNow");
            return (Criteria) this;
        }

        public Criteria andSurveyNowBetween(Integer value1, Integer value2) {
            addCriterion("survey_now between", value1, value2, "surveyNow");
            return (Criteria) this;
        }

        public Criteria andSurveyNowNotBetween(Integer value1, Integer value2) {
            addCriterion("survey_now not between", value1, value2, "surveyNow");
            return (Criteria) this;
        }

        public Criteria andBack1IsNull() {
            addCriterion("back1 is null");
            return (Criteria) this;
        }

        public Criteria andBack1IsNotNull() {
            addCriterion("back1 is not null");
            return (Criteria) this;
        }

        public Criteria andBack1EqualTo(String value) {
            addCriterion("back1 =", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotEqualTo(String value) {
            addCriterion("back1 <>", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThan(String value) {
            addCriterion("back1 >", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThanOrEqualTo(String value) {
            addCriterion("back1 >=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThan(String value) {
            addCriterion("back1 <", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThanOrEqualTo(String value) {
            addCriterion("back1 <=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Like(String value) {
            addCriterion("back1 like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotLike(String value) {
            addCriterion("back1 not like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1In(List<String> values) {
            addCriterion("back1 in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotIn(List<String> values) {
            addCriterion("back1 not in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Between(String value1, String value2) {
            addCriterion("back1 between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotBetween(String value1, String value2) {
            addCriterion("back1 not between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack2IsNull() {
            addCriterion("back2 is null");
            return (Criteria) this;
        }

        public Criteria andBack2IsNotNull() {
            addCriterion("back2 is not null");
            return (Criteria) this;
        }

        public Criteria andBack2EqualTo(String value) {
            addCriterion("back2 =", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotEqualTo(String value) {
            addCriterion("back2 <>", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThan(String value) {
            addCriterion("back2 >", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThanOrEqualTo(String value) {
            addCriterion("back2 >=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThan(String value) {
            addCriterion("back2 <", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThanOrEqualTo(String value) {
            addCriterion("back2 <=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Like(String value) {
            addCriterion("back2 like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotLike(String value) {
            addCriterion("back2 not like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2In(List<String> values) {
            addCriterion("back2 in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotIn(List<String> values) {
            addCriterion("back2 not in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Between(String value1, String value2) {
            addCriterion("back2 between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotBetween(String value1, String value2) {
            addCriterion("back2 not between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack3IsNull() {
            addCriterion("back3 is null");
            return (Criteria) this;
        }

        public Criteria andBack3IsNotNull() {
            addCriterion("back3 is not null");
            return (Criteria) this;
        }

        public Criteria andBack3EqualTo(String value) {
            addCriterion("back3 =", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotEqualTo(String value) {
            addCriterion("back3 <>", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThan(String value) {
            addCriterion("back3 >", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThanOrEqualTo(String value) {
            addCriterion("back3 >=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThan(String value) {
            addCriterion("back3 <", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThanOrEqualTo(String value) {
            addCriterion("back3 <=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Like(String value) {
            addCriterion("back3 like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotLike(String value) {
            addCriterion("back3 not like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3In(List<String> values) {
            addCriterion("back3 in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotIn(List<String> values) {
            addCriterion("back3 not in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Between(String value1, String value2) {
            addCriterion("back3 between", value1, value2, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotBetween(String value1, String value2) {
            addCriterion("back3 not between", value1, value2, "back3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}