package pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PolicyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PolicyExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPolicyIdIsNull() {
            addCriterion("policy_id is null");
            return (Criteria) this;
        }

        public Criteria andPolicyIdIsNotNull() {
            addCriterion("policy_id is not null");
            return (Criteria) this;
        }

        public Criteria andPolicyIdEqualTo(String value) {
            addCriterion("policy_id =", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotEqualTo(String value) {
            addCriterion("policy_id <>", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdGreaterThan(String value) {
            addCriterion("policy_id >", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdGreaterThanOrEqualTo(String value) {
            addCriterion("policy_id >=", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLessThan(String value) {
            addCriterion("policy_id <", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLessThanOrEqualTo(String value) {
            addCriterion("policy_id <=", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdLike(String value) {
            addCriterion("policy_id like", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotLike(String value) {
            addCriterion("policy_id not like", value, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdIn(List<String> values) {
            addCriterion("policy_id in", values, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotIn(List<String> values) {
            addCriterion("policy_id not in", values, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdBetween(String value1, String value2) {
            addCriterion("policy_id between", value1, value2, "policyId");
            return (Criteria) this;
        }

        public Criteria andPolicyIdNotBetween(String value1, String value2) {
            addCriterion("policy_id not between", value1, value2, "policyId");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNull() {
            addCriterion("table_id is null");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNotNull() {
            addCriterion("table_id is not null");
            return (Criteria) this;
        }

        public Criteria andTableIdEqualTo(Integer value) {
            addCriterion("table_id =", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotEqualTo(Integer value) {
            addCriterion("table_id <>", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThan(Integer value) {
            addCriterion("table_id >", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("table_id >=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThan(Integer value) {
            addCriterion("table_id <", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThanOrEqualTo(Integer value) {
            addCriterion("table_id <=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdIn(List<Integer> values) {
            addCriterion("table_id in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotIn(List<Integer> values) {
            addCriterion("table_id not in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdBetween(Integer value1, Integer value2) {
            addCriterion("table_id between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotBetween(Integer value1, Integer value2) {
            addCriterion("table_id not between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andCarNoIsNull() {
            addCriterion("car_no is null");
            return (Criteria) this;
        }

        public Criteria andCarNoIsNotNull() {
            addCriterion("car_no is not null");
            return (Criteria) this;
        }

        public Criteria andCarNoEqualTo(String value) {
            addCriterion("car_no =", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotEqualTo(String value) {
            addCriterion("car_no <>", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoGreaterThan(String value) {
            addCriterion("car_no >", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoGreaterThanOrEqualTo(String value) {
            addCriterion("car_no >=", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLessThan(String value) {
            addCriterion("car_no <", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLessThanOrEqualTo(String value) {
            addCriterion("car_no <=", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLike(String value) {
            addCriterion("car_no like", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotLike(String value) {
            addCriterion("car_no not like", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoIn(List<String> values) {
            addCriterion("car_no in", values, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotIn(List<String> values) {
            addCriterion("car_no not in", values, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoBetween(String value1, String value2) {
            addCriterion("car_no between", value1, value2, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotBetween(String value1, String value2) {
            addCriterion("car_no not between", value1, value2, "carNo");
            return (Criteria) this;
        }

        public Criteria andApplicantIsNull() {
            addCriterion("applicant is null");
            return (Criteria) this;
        }

        public Criteria andApplicantIsNotNull() {
            addCriterion("applicant is not null");
            return (Criteria) this;
        }

        public Criteria andApplicantEqualTo(String value) {
            addCriterion("applicant =", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantNotEqualTo(String value) {
            addCriterion("applicant <>", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantGreaterThan(String value) {
            addCriterion("applicant >", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantGreaterThanOrEqualTo(String value) {
            addCriterion("applicant >=", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantLessThan(String value) {
            addCriterion("applicant <", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantLessThanOrEqualTo(String value) {
            addCriterion("applicant <=", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantLike(String value) {
            addCriterion("applicant like", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantNotLike(String value) {
            addCriterion("applicant not like", value, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantIn(List<String> values) {
            addCriterion("applicant in", values, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantNotIn(List<String> values) {
            addCriterion("applicant not in", values, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantBetween(String value1, String value2) {
            addCriterion("applicant between", value1, value2, "applicant");
            return (Criteria) this;
        }

        public Criteria andApplicantNotBetween(String value1, String value2) {
            addCriterion("applicant not between", value1, value2, "applicant");
            return (Criteria) this;
        }

        public Criteria andAIdcardIsNull() {
            addCriterion("a_idcard is null");
            return (Criteria) this;
        }

        public Criteria andAIdcardIsNotNull() {
            addCriterion("a_idcard is not null");
            return (Criteria) this;
        }

        public Criteria andAIdcardEqualTo(String value) {
            addCriterion("a_idcard =", value, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAIdcardNotEqualTo(String value) {
            addCriterion("a_idcard <>", value, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAIdcardGreaterThan(String value) {
            addCriterion("a_idcard >", value, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAIdcardGreaterThanOrEqualTo(String value) {
            addCriterion("a_idcard >=", value, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAIdcardLessThan(String value) {
            addCriterion("a_idcard <", value, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAIdcardLessThanOrEqualTo(String value) {
            addCriterion("a_idcard <=", value, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAIdcardLike(String value) {
            addCriterion("a_idcard like", value, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAIdcardNotLike(String value) {
            addCriterion("a_idcard not like", value, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAIdcardIn(List<String> values) {
            addCriterion("a_idcard in", values, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAIdcardNotIn(List<String> values) {
            addCriterion("a_idcard not in", values, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAIdcardBetween(String value1, String value2) {
            addCriterion("a_idcard between", value1, value2, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAIdcardNotBetween(String value1, String value2) {
            addCriterion("a_idcard not between", value1, value2, "aIdcard");
            return (Criteria) this;
        }

        public Criteria andAPhoneIsNull() {
            addCriterion("a_phone is null");
            return (Criteria) this;
        }

        public Criteria andAPhoneIsNotNull() {
            addCriterion("a_phone is not null");
            return (Criteria) this;
        }

        public Criteria andAPhoneEqualTo(String value) {
            addCriterion("a_phone =", value, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAPhoneNotEqualTo(String value) {
            addCriterion("a_phone <>", value, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAPhoneGreaterThan(String value) {
            addCriterion("a_phone >", value, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("a_phone >=", value, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAPhoneLessThan(String value) {
            addCriterion("a_phone <", value, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAPhoneLessThanOrEqualTo(String value) {
            addCriterion("a_phone <=", value, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAPhoneLike(String value) {
            addCriterion("a_phone like", value, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAPhoneNotLike(String value) {
            addCriterion("a_phone not like", value, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAPhoneIn(List<String> values) {
            addCriterion("a_phone in", values, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAPhoneNotIn(List<String> values) {
            addCriterion("a_phone not in", values, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAPhoneBetween(String value1, String value2) {
            addCriterion("a_phone between", value1, value2, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAPhoneNotBetween(String value1, String value2) {
            addCriterion("a_phone not between", value1, value2, "aPhone");
            return (Criteria) this;
        }

        public Criteria andAAddressIsNull() {
            addCriterion("a_address is null");
            return (Criteria) this;
        }

        public Criteria andAAddressIsNotNull() {
            addCriterion("a_address is not null");
            return (Criteria) this;
        }

        public Criteria andAAddressEqualTo(String value) {
            addCriterion("a_address =", value, "aAddress");
            return (Criteria) this;
        }

        public Criteria andAAddressNotEqualTo(String value) {
            addCriterion("a_address <>", value, "aAddress");
            return (Criteria) this;
        }

        public Criteria andAAddressGreaterThan(String value) {
            addCriterion("a_address >", value, "aAddress");
            return (Criteria) this;
        }

        public Criteria andAAddressGreaterThanOrEqualTo(String value) {
            addCriterion("a_address >=", value, "aAddress");
            return (Criteria) this;
        }

        public Criteria andAAddressLessThan(String value) {
            addCriterion("a_address <", value, "aAddress");
            return (Criteria) this;
        }

        public Criteria andAAddressLessThanOrEqualTo(String value) {
            addCriterion("a_address <=", value, "aAddress");
            return (Criteria) this;
        }

        public Criteria andAAddressLike(String value) {
            addCriterion("a_address like", value, "aAddress");
            return (Criteria) this;
        }

        public Criteria andAAddressNotLike(String value) {
            addCriterion("a_address not like", value, "aAddress");
            return (Criteria) this;
        }

        public Criteria andAAddressIn(List<String> values) {
            addCriterion("a_address in", values, "aAddress");
            return (Criteria) this;
        }

        public Criteria andAAddressNotIn(List<String> values) {
            addCriterion("a_address not in", values, "aAddress");
            return (Criteria) this;
        }

        public Criteria andAAddressBetween(String value1, String value2) {
            addCriterion("a_address between", value1, value2, "aAddress");
            return (Criteria) this;
        }

        public Criteria andAAddressNotBetween(String value1, String value2) {
            addCriterion("a_address not between", value1, value2, "aAddress");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryIsNull() {
            addCriterion("beneficiary is null");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryIsNotNull() {
            addCriterion("beneficiary is not null");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryEqualTo(String value) {
            addCriterion("beneficiary =", value, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryNotEqualTo(String value) {
            addCriterion("beneficiary <>", value, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryGreaterThan(String value) {
            addCriterion("beneficiary >", value, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryGreaterThanOrEqualTo(String value) {
            addCriterion("beneficiary >=", value, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryLessThan(String value) {
            addCriterion("beneficiary <", value, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryLessThanOrEqualTo(String value) {
            addCriterion("beneficiary <=", value, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryLike(String value) {
            addCriterion("beneficiary like", value, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryNotLike(String value) {
            addCriterion("beneficiary not like", value, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryIn(List<String> values) {
            addCriterion("beneficiary in", values, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryNotIn(List<String> values) {
            addCriterion("beneficiary not in", values, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryBetween(String value1, String value2) {
            addCriterion("beneficiary between", value1, value2, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBeneficiaryNotBetween(String value1, String value2) {
            addCriterion("beneficiary not between", value1, value2, "beneficiary");
            return (Criteria) this;
        }

        public Criteria andBIdcardIsNull() {
            addCriterion("b_idcard is null");
            return (Criteria) this;
        }

        public Criteria andBIdcardIsNotNull() {
            addCriterion("b_idcard is not null");
            return (Criteria) this;
        }

        public Criteria andBIdcardEqualTo(String value) {
            addCriterion("b_idcard =", value, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBIdcardNotEqualTo(String value) {
            addCriterion("b_idcard <>", value, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBIdcardGreaterThan(String value) {
            addCriterion("b_idcard >", value, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBIdcardGreaterThanOrEqualTo(String value) {
            addCriterion("b_idcard >=", value, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBIdcardLessThan(String value) {
            addCriterion("b_idcard <", value, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBIdcardLessThanOrEqualTo(String value) {
            addCriterion("b_idcard <=", value, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBIdcardLike(String value) {
            addCriterion("b_idcard like", value, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBIdcardNotLike(String value) {
            addCriterion("b_idcard not like", value, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBIdcardIn(List<String> values) {
            addCriterion("b_idcard in", values, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBIdcardNotIn(List<String> values) {
            addCriterion("b_idcard not in", values, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBIdcardBetween(String value1, String value2) {
            addCriterion("b_idcard between", value1, value2, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBIdcardNotBetween(String value1, String value2) {
            addCriterion("b_idcard not between", value1, value2, "bIdcard");
            return (Criteria) this;
        }

        public Criteria andBPhoneIsNull() {
            addCriterion("b_phone is null");
            return (Criteria) this;
        }

        public Criteria andBPhoneIsNotNull() {
            addCriterion("b_phone is not null");
            return (Criteria) this;
        }

        public Criteria andBPhoneEqualTo(String value) {
            addCriterion("b_phone =", value, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBPhoneNotEqualTo(String value) {
            addCriterion("b_phone <>", value, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBPhoneGreaterThan(String value) {
            addCriterion("b_phone >", value, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("b_phone >=", value, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBPhoneLessThan(String value) {
            addCriterion("b_phone <", value, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBPhoneLessThanOrEqualTo(String value) {
            addCriterion("b_phone <=", value, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBPhoneLike(String value) {
            addCriterion("b_phone like", value, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBPhoneNotLike(String value) {
            addCriterion("b_phone not like", value, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBPhoneIn(List<String> values) {
            addCriterion("b_phone in", values, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBPhoneNotIn(List<String> values) {
            addCriterion("b_phone not in", values, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBPhoneBetween(String value1, String value2) {
            addCriterion("b_phone between", value1, value2, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBPhoneNotBetween(String value1, String value2) {
            addCriterion("b_phone not between", value1, value2, "bPhone");
            return (Criteria) this;
        }

        public Criteria andBAdressIsNull() {
            addCriterion("b_adress is null");
            return (Criteria) this;
        }

        public Criteria andBAdressIsNotNull() {
            addCriterion("b_adress is not null");
            return (Criteria) this;
        }

        public Criteria andBAdressEqualTo(String value) {
            addCriterion("b_adress =", value, "bAdress");
            return (Criteria) this;
        }

        public Criteria andBAdressNotEqualTo(String value) {
            addCriterion("b_adress <>", value, "bAdress");
            return (Criteria) this;
        }

        public Criteria andBAdressGreaterThan(String value) {
            addCriterion("b_adress >", value, "bAdress");
            return (Criteria) this;
        }

        public Criteria andBAdressGreaterThanOrEqualTo(String value) {
            addCriterion("b_adress >=", value, "bAdress");
            return (Criteria) this;
        }

        public Criteria andBAdressLessThan(String value) {
            addCriterion("b_adress <", value, "bAdress");
            return (Criteria) this;
        }

        public Criteria andBAdressLessThanOrEqualTo(String value) {
            addCriterion("b_adress <=", value, "bAdress");
            return (Criteria) this;
        }

        public Criteria andBAdressLike(String value) {
            addCriterion("b_adress like", value, "bAdress");
            return (Criteria) this;
        }

        public Criteria andBAdressNotLike(String value) {
            addCriterion("b_adress not like", value, "bAdress");
            return (Criteria) this;
        }

        public Criteria andBAdressIn(List<String> values) {
            addCriterion("b_adress in", values, "bAdress");
            return (Criteria) this;
        }

        public Criteria andBAdressNotIn(List<String> values) {
            addCriterion("b_adress not in", values, "bAdress");
            return (Criteria) this;
        }

        public Criteria andBAdressBetween(String value1, String value2) {
            addCriterion("b_adress between", value1, value2, "bAdress");
            return (Criteria) this;
        }

        public Criteria andBAdressNotBetween(String value1, String value2) {
            addCriterion("b_adress not between", value1, value2, "bAdress");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountIsNull() {
            addCriterion("insured_amount is null");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountIsNotNull() {
            addCriterion("insured_amount is not null");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountEqualTo(Double value) {
            addCriterion("insured_amount =", value, "insuredAmount");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountNotEqualTo(Double value) {
            addCriterion("insured_amount <>", value, "insuredAmount");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountGreaterThan(Double value) {
            addCriterion("insured_amount >", value, "insuredAmount");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountGreaterThanOrEqualTo(Double value) {
            addCriterion("insured_amount >=", value, "insuredAmount");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountLessThan(Double value) {
            addCriterion("insured_amount <", value, "insuredAmount");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountLessThanOrEqualTo(Double value) {
            addCriterion("insured_amount <=", value, "insuredAmount");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountIn(List<Double> values) {
            addCriterion("insured_amount in", values, "insuredAmount");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountNotIn(List<Double> values) {
            addCriterion("insured_amount not in", values, "insuredAmount");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountBetween(Double value1, Double value2) {
            addCriterion("insured_amount between", value1, value2, "insuredAmount");
            return (Criteria) this;
        }

        public Criteria andInsuredAmountNotBetween(Double value1, Double value2) {
            addCriterion("insured_amount not between", value1, value2, "insuredAmount");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountIsNull() {
            addCriterion("compensate_amount is null");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountIsNotNull() {
            addCriterion("compensate_amount is not null");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountEqualTo(Double value) {
            addCriterion("compensate_amount =", value, "compensateAmount");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountNotEqualTo(Double value) {
            addCriterion("compensate_amount <>", value, "compensateAmount");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountGreaterThan(Double value) {
            addCriterion("compensate_amount >", value, "compensateAmount");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountGreaterThanOrEqualTo(Double value) {
            addCriterion("compensate_amount >=", value, "compensateAmount");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountLessThan(Double value) {
            addCriterion("compensate_amount <", value, "compensateAmount");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountLessThanOrEqualTo(Double value) {
            addCriterion("compensate_amount <=", value, "compensateAmount");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountIn(List<Double> values) {
            addCriterion("compensate_amount in", values, "compensateAmount");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountNotIn(List<Double> values) {
            addCriterion("compensate_amount not in", values, "compensateAmount");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountBetween(Double value1, Double value2) {
            addCriterion("compensate_amount between", value1, value2, "compensateAmount");
            return (Criteria) this;
        }

        public Criteria andCompensateAmountNotBetween(Double value1, Double value2) {
            addCriterion("compensate_amount not between", value1, value2, "compensateAmount");
            return (Criteria) this;
        }

        public Criteria andStartDateIsNull() {
            addCriterion("start_date is null");
            return (Criteria) this;
        }

        public Criteria andStartDateIsNotNull() {
            addCriterion("start_date is not null");
            return (Criteria) this;
        }

        public Criteria andStartDateEqualTo(Date value) {
            addCriterion("start_date =", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotEqualTo(Date value) {
            addCriterion("start_date <>", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateGreaterThan(Date value) {
            addCriterion("start_date >", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("start_date >=", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateLessThan(Date value) {
            addCriterion("start_date <", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateLessThanOrEqualTo(Date value) {
            addCriterion("start_date <=", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateIn(List<Date> values) {
            addCriterion("start_date in", values, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotIn(List<Date> values) {
            addCriterion("start_date not in", values, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateBetween(Date value1, Date value2) {
            addCriterion("start_date between", value1, value2, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotBetween(Date value1, Date value2) {
            addCriterion("start_date not between", value1, value2, "startDate");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("end_time is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("end_time is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Date value) {
            addCriterion("end_time =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Date value) {
            addCriterion("end_time <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Date value) {
            addCriterion("end_time >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("end_time >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Date value) {
            addCriterion("end_time <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("end_time <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Date> values) {
            addCriterion("end_time in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Date> values) {
            addCriterion("end_time not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Date value1, Date value2) {
            addCriterion("end_time between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("end_time not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdIsNull() {
            addCriterion("insurance_id is null");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdIsNotNull() {
            addCriterion("insurance_id is not null");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdEqualTo(Integer value) {
            addCriterion("insurance_id =", value, "insuranceId");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdNotEqualTo(Integer value) {
            addCriterion("insurance_id <>", value, "insuranceId");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdGreaterThan(Integer value) {
            addCriterion("insurance_id >", value, "insuranceId");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("insurance_id >=", value, "insuranceId");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdLessThan(Integer value) {
            addCriterion("insurance_id <", value, "insuranceId");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdLessThanOrEqualTo(Integer value) {
            addCriterion("insurance_id <=", value, "insuranceId");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdIn(List<Integer> values) {
            addCriterion("insurance_id in", values, "insuranceId");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdNotIn(List<Integer> values) {
            addCriterion("insurance_id not in", values, "insuranceId");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdBetween(Integer value1, Integer value2) {
            addCriterion("insurance_id between", value1, value2, "insuranceId");
            return (Criteria) this;
        }

        public Criteria andInsuranceIdNotBetween(Integer value1, Integer value2) {
            addCriterion("insurance_id not between", value1, value2, "insuranceId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNull() {
            addCriterion("load_id is null");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNotNull() {
            addCriterion("load_id is not null");
            return (Criteria) this;
        }

        public Criteria andLoadIdEqualTo(Integer value) {
            addCriterion("load_id =", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotEqualTo(Integer value) {
            addCriterion("load_id <>", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThan(Integer value) {
            addCriterion("load_id >", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("load_id >=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThan(Integer value) {
            addCriterion("load_id <", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThanOrEqualTo(Integer value) {
            addCriterion("load_id <=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdIn(List<Integer> values) {
            addCriterion("load_id in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotIn(List<Integer> values) {
            addCriterion("load_id not in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdBetween(Integer value1, Integer value2) {
            addCriterion("load_id between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotBetween(Integer value1, Integer value2) {
            addCriterion("load_id not between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andBack1IsNull() {
            addCriterion("back1 is null");
            return (Criteria) this;
        }

        public Criteria andBack1IsNotNull() {
            addCriterion("back1 is not null");
            return (Criteria) this;
        }

        public Criteria andBack1EqualTo(String value) {
            addCriterion("back1 =", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotEqualTo(String value) {
            addCriterion("back1 <>", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThan(String value) {
            addCriterion("back1 >", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThanOrEqualTo(String value) {
            addCriterion("back1 >=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThan(String value) {
            addCriterion("back1 <", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThanOrEqualTo(String value) {
            addCriterion("back1 <=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Like(String value) {
            addCriterion("back1 like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotLike(String value) {
            addCriterion("back1 not like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1In(List<String> values) {
            addCriterion("back1 in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotIn(List<String> values) {
            addCriterion("back1 not in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Between(String value1, String value2) {
            addCriterion("back1 between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotBetween(String value1, String value2) {
            addCriterion("back1 not between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack2IsNull() {
            addCriterion("back2 is null");
            return (Criteria) this;
        }

        public Criteria andBack2IsNotNull() {
            addCriterion("back2 is not null");
            return (Criteria) this;
        }

        public Criteria andBack2EqualTo(String value) {
            addCriterion("back2 =", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotEqualTo(String value) {
            addCriterion("back2 <>", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThan(String value) {
            addCriterion("back2 >", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThanOrEqualTo(String value) {
            addCriterion("back2 >=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThan(String value) {
            addCriterion("back2 <", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThanOrEqualTo(String value) {
            addCriterion("back2 <=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Like(String value) {
            addCriterion("back2 like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotLike(String value) {
            addCriterion("back2 not like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2In(List<String> values) {
            addCriterion("back2 in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotIn(List<String> values) {
            addCriterion("back2 not in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Between(String value1, String value2) {
            addCriterion("back2 between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotBetween(String value1, String value2) {
            addCriterion("back2 not between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack3IsNull() {
            addCriterion("back3 is null");
            return (Criteria) this;
        }

        public Criteria andBack3IsNotNull() {
            addCriterion("back3 is not null");
            return (Criteria) this;
        }

        public Criteria andBack3EqualTo(String value) {
            addCriterion("back3 =", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotEqualTo(String value) {
            addCriterion("back3 <>", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThan(String value) {
            addCriterion("back3 >", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThanOrEqualTo(String value) {
            addCriterion("back3 >=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThan(String value) {
            addCriterion("back3 <", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThanOrEqualTo(String value) {
            addCriterion("back3 <=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Like(String value) {
            addCriterion("back3 like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotLike(String value) {
            addCriterion("back3 not like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3In(List<String> values) {
            addCriterion("back3 in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotIn(List<String> values) {
            addCriterion("back3 not in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Between(String value1, String value2) {
            addCriterion("back3 between", value1, value2, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotBetween(String value1, String value2) {
            addCriterion("back3 not between", value1, value2, "back3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}