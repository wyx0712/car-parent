package pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HurtInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HurtInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andHurtIdIsNull() {
            addCriterion("hurt_id is null");
            return (Criteria) this;
        }

        public Criteria andHurtIdIsNotNull() {
            addCriterion("hurt_id is not null");
            return (Criteria) this;
        }

        public Criteria andHurtIdEqualTo(Integer value) {
            addCriterion("hurt_id =", value, "hurtId");
            return (Criteria) this;
        }

        public Criteria andHurtIdNotEqualTo(Integer value) {
            addCriterion("hurt_id <>", value, "hurtId");
            return (Criteria) this;
        }

        public Criteria andHurtIdGreaterThan(Integer value) {
            addCriterion("hurt_id >", value, "hurtId");
            return (Criteria) this;
        }

        public Criteria andHurtIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("hurt_id >=", value, "hurtId");
            return (Criteria) this;
        }

        public Criteria andHurtIdLessThan(Integer value) {
            addCriterion("hurt_id <", value, "hurtId");
            return (Criteria) this;
        }

        public Criteria andHurtIdLessThanOrEqualTo(Integer value) {
            addCriterion("hurt_id <=", value, "hurtId");
            return (Criteria) this;
        }

        public Criteria andHurtIdIn(List<Integer> values) {
            addCriterion("hurt_id in", values, "hurtId");
            return (Criteria) this;
        }

        public Criteria andHurtIdNotIn(List<Integer> values) {
            addCriterion("hurt_id not in", values, "hurtId");
            return (Criteria) this;
        }

        public Criteria andHurtIdBetween(Integer value1, Integer value2) {
            addCriterion("hurt_id between", value1, value2, "hurtId");
            return (Criteria) this;
        }

        public Criteria andHurtIdNotBetween(Integer value1, Integer value2) {
            addCriterion("hurt_id not between", value1, value2, "hurtId");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNull() {
            addCriterion("table_id is null");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNotNull() {
            addCriterion("table_id is not null");
            return (Criteria) this;
        }

        public Criteria andTableIdEqualTo(Integer value) {
            addCriterion("table_id =", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotEqualTo(Integer value) {
            addCriterion("table_id <>", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThan(Integer value) {
            addCriterion("table_id >", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("table_id >=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThan(Integer value) {
            addCriterion("table_id <", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThanOrEqualTo(Integer value) {
            addCriterion("table_id <=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdIn(List<Integer> values) {
            addCriterion("table_id in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotIn(List<Integer> values) {
            addCriterion("table_id not in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdBetween(Integer value1, Integer value2) {
            addCriterion("table_id between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotBetween(Integer value1, Integer value2) {
            addCriterion("table_id not between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNull() {
            addCriterion("report_no is null");
            return (Criteria) this;
        }

        public Criteria andReportNoIsNotNull() {
            addCriterion("report_no is not null");
            return (Criteria) this;
        }

        public Criteria andReportNoEqualTo(Integer value) {
            addCriterion("report_no =", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotEqualTo(Integer value) {
            addCriterion("report_no <>", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThan(Integer value) {
            addCriterion("report_no >", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("report_no >=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThan(Integer value) {
            addCriterion("report_no <", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoLessThanOrEqualTo(Integer value) {
            addCriterion("report_no <=", value, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoIn(List<Integer> values) {
            addCriterion("report_no in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotIn(List<Integer> values) {
            addCriterion("report_no not in", values, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoBetween(Integer value1, Integer value2) {
            addCriterion("report_no between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andReportNoNotBetween(Integer value1, Integer value2) {
            addCriterion("report_no not between", value1, value2, "reportNo");
            return (Criteria) this;
        }

        public Criteria andInjuredNameIsNull() {
            addCriterion("injured_name is null");
            return (Criteria) this;
        }

        public Criteria andInjuredNameIsNotNull() {
            addCriterion("injured_name is not null");
            return (Criteria) this;
        }

        public Criteria andInjuredNameEqualTo(String value) {
            addCriterion("injured_name =", value, "injuredName");
            return (Criteria) this;
        }

        public Criteria andInjuredNameNotEqualTo(String value) {
            addCriterion("injured_name <>", value, "injuredName");
            return (Criteria) this;
        }

        public Criteria andInjuredNameGreaterThan(String value) {
            addCriterion("injured_name >", value, "injuredName");
            return (Criteria) this;
        }

        public Criteria andInjuredNameGreaterThanOrEqualTo(String value) {
            addCriterion("injured_name >=", value, "injuredName");
            return (Criteria) this;
        }

        public Criteria andInjuredNameLessThan(String value) {
            addCriterion("injured_name <", value, "injuredName");
            return (Criteria) this;
        }

        public Criteria andInjuredNameLessThanOrEqualTo(String value) {
            addCriterion("injured_name <=", value, "injuredName");
            return (Criteria) this;
        }

        public Criteria andInjuredNameLike(String value) {
            addCriterion("injured_name like", value, "injuredName");
            return (Criteria) this;
        }

        public Criteria andInjuredNameNotLike(String value) {
            addCriterion("injured_name not like", value, "injuredName");
            return (Criteria) this;
        }

        public Criteria andInjuredNameIn(List<String> values) {
            addCriterion("injured_name in", values, "injuredName");
            return (Criteria) this;
        }

        public Criteria andInjuredNameNotIn(List<String> values) {
            addCriterion("injured_name not in", values, "injuredName");
            return (Criteria) this;
        }

        public Criteria andInjuredNameBetween(String value1, String value2) {
            addCriterion("injured_name between", value1, value2, "injuredName");
            return (Criteria) this;
        }

        public Criteria andInjuredNameNotBetween(String value1, String value2) {
            addCriterion("injured_name not between", value1, value2, "injuredName");
            return (Criteria) this;
        }

        public Criteria andGenderIsNull() {
            addCriterion("gender is null");
            return (Criteria) this;
        }

        public Criteria andGenderIsNotNull() {
            addCriterion("gender is not null");
            return (Criteria) this;
        }

        public Criteria andGenderEqualTo(String value) {
            addCriterion("gender =", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotEqualTo(String value) {
            addCriterion("gender <>", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThan(String value) {
            addCriterion("gender >", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThanOrEqualTo(String value) {
            addCriterion("gender >=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThan(String value) {
            addCriterion("gender <", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThanOrEqualTo(String value) {
            addCriterion("gender <=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLike(String value) {
            addCriterion("gender like", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotLike(String value) {
            addCriterion("gender not like", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderIn(List<String> values) {
            addCriterion("gender in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotIn(List<String> values) {
            addCriterion("gender not in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderBetween(String value1, String value2) {
            addCriterion("gender between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotBetween(String value1, String value2) {
            addCriterion("gender not between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andAgeIsNull() {
            addCriterion("age is null");
            return (Criteria) this;
        }

        public Criteria andAgeIsNotNull() {
            addCriterion("age is not null");
            return (Criteria) this;
        }

        public Criteria andAgeEqualTo(String value) {
            addCriterion("age =", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotEqualTo(String value) {
            addCriterion("age <>", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeGreaterThan(String value) {
            addCriterion("age >", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeGreaterThanOrEqualTo(String value) {
            addCriterion("age >=", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLessThan(String value) {
            addCriterion("age <", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLessThanOrEqualTo(String value) {
            addCriterion("age <=", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLike(String value) {
            addCriterion("age like", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotLike(String value) {
            addCriterion("age not like", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeIn(List<String> values) {
            addCriterion("age in", values, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotIn(List<String> values) {
            addCriterion("age not in", values, "age");
            return (Criteria) this;
        }

        public Criteria andAgeBetween(String value1, String value2) {
            addCriterion("age between", value1, value2, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotBetween(String value1, String value2) {
            addCriterion("age not between", value1, value2, "age");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterIsNull() {
            addCriterion("census_register is null");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterIsNotNull() {
            addCriterion("census_register is not null");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterEqualTo(String value) {
            addCriterion("census_register =", value, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterNotEqualTo(String value) {
            addCriterion("census_register <>", value, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterGreaterThan(String value) {
            addCriterion("census_register >", value, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterGreaterThanOrEqualTo(String value) {
            addCriterion("census_register >=", value, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterLessThan(String value) {
            addCriterion("census_register <", value, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterLessThanOrEqualTo(String value) {
            addCriterion("census_register <=", value, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterLike(String value) {
            addCriterion("census_register like", value, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterNotLike(String value) {
            addCriterion("census_register not like", value, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterIn(List<String> values) {
            addCriterion("census_register in", values, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterNotIn(List<String> values) {
            addCriterion("census_register not in", values, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterBetween(String value1, String value2) {
            addCriterion("census_register between", value1, value2, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andCensusRegisterNotBetween(String value1, String value2) {
            addCriterion("census_register not between", value1, value2, "censusRegister");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andHospitalIsNull() {
            addCriterion("hospital is null");
            return (Criteria) this;
        }

        public Criteria andHospitalIsNotNull() {
            addCriterion("hospital is not null");
            return (Criteria) this;
        }

        public Criteria andHospitalEqualTo(String value) {
            addCriterion("hospital =", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalNotEqualTo(String value) {
            addCriterion("hospital <>", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalGreaterThan(String value) {
            addCriterion("hospital >", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalGreaterThanOrEqualTo(String value) {
            addCriterion("hospital >=", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalLessThan(String value) {
            addCriterion("hospital <", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalLessThanOrEqualTo(String value) {
            addCriterion("hospital <=", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalLike(String value) {
            addCriterion("hospital like", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalNotLike(String value) {
            addCriterion("hospital not like", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalIn(List<String> values) {
            addCriterion("hospital in", values, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalNotIn(List<String> values) {
            addCriterion("hospital not in", values, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalBetween(String value1, String value2) {
            addCriterion("hospital between", value1, value2, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalNotBetween(String value1, String value2) {
            addCriterion("hospital not between", value1, value2, "hospital");
            return (Criteria) this;
        }

        public Criteria andDoctorIsNull() {
            addCriterion("doctor is null");
            return (Criteria) this;
        }

        public Criteria andDoctorIsNotNull() {
            addCriterion("doctor is not null");
            return (Criteria) this;
        }

        public Criteria andDoctorEqualTo(String value) {
            addCriterion("doctor =", value, "doctor");
            return (Criteria) this;
        }

        public Criteria andDoctorNotEqualTo(String value) {
            addCriterion("doctor <>", value, "doctor");
            return (Criteria) this;
        }

        public Criteria andDoctorGreaterThan(String value) {
            addCriterion("doctor >", value, "doctor");
            return (Criteria) this;
        }

        public Criteria andDoctorGreaterThanOrEqualTo(String value) {
            addCriterion("doctor >=", value, "doctor");
            return (Criteria) this;
        }

        public Criteria andDoctorLessThan(String value) {
            addCriterion("doctor <", value, "doctor");
            return (Criteria) this;
        }

        public Criteria andDoctorLessThanOrEqualTo(String value) {
            addCriterion("doctor <=", value, "doctor");
            return (Criteria) this;
        }

        public Criteria andDoctorLike(String value) {
            addCriterion("doctor like", value, "doctor");
            return (Criteria) this;
        }

        public Criteria andDoctorNotLike(String value) {
            addCriterion("doctor not like", value, "doctor");
            return (Criteria) this;
        }

        public Criteria andDoctorIn(List<String> values) {
            addCriterion("doctor in", values, "doctor");
            return (Criteria) this;
        }

        public Criteria andDoctorNotIn(List<String> values) {
            addCriterion("doctor not in", values, "doctor");
            return (Criteria) this;
        }

        public Criteria andDoctorBetween(String value1, String value2) {
            addCriterion("doctor between", value1, value2, "doctor");
            return (Criteria) this;
        }

        public Criteria andDoctorNotBetween(String value1, String value2) {
            addCriterion("doctor not between", value1, value2, "doctor");
            return (Criteria) this;
        }

        public Criteria andOfficeIsNull() {
            addCriterion("office is null");
            return (Criteria) this;
        }

        public Criteria andOfficeIsNotNull() {
            addCriterion("office is not null");
            return (Criteria) this;
        }

        public Criteria andOfficeEqualTo(String value) {
            addCriterion("office =", value, "office");
            return (Criteria) this;
        }

        public Criteria andOfficeNotEqualTo(String value) {
            addCriterion("office <>", value, "office");
            return (Criteria) this;
        }

        public Criteria andOfficeGreaterThan(String value) {
            addCriterion("office >", value, "office");
            return (Criteria) this;
        }

        public Criteria andOfficeGreaterThanOrEqualTo(String value) {
            addCriterion("office >=", value, "office");
            return (Criteria) this;
        }

        public Criteria andOfficeLessThan(String value) {
            addCriterion("office <", value, "office");
            return (Criteria) this;
        }

        public Criteria andOfficeLessThanOrEqualTo(String value) {
            addCriterion("office <=", value, "office");
            return (Criteria) this;
        }

        public Criteria andOfficeLike(String value) {
            addCriterion("office like", value, "office");
            return (Criteria) this;
        }

        public Criteria andOfficeNotLike(String value) {
            addCriterion("office not like", value, "office");
            return (Criteria) this;
        }

        public Criteria andOfficeIn(List<String> values) {
            addCriterion("office in", values, "office");
            return (Criteria) this;
        }

        public Criteria andOfficeNotIn(List<String> values) {
            addCriterion("office not in", values, "office");
            return (Criteria) this;
        }

        public Criteria andOfficeBetween(String value1, String value2) {
            addCriterion("office between", value1, value2, "office");
            return (Criteria) this;
        }

        public Criteria andOfficeNotBetween(String value1, String value2) {
            addCriterion("office not between", value1, value2, "office");
            return (Criteria) this;
        }

        public Criteria andCarNoIsNull() {
            addCriterion("car_no is null");
            return (Criteria) this;
        }

        public Criteria andCarNoIsNotNull() {
            addCriterion("car_no is not null");
            return (Criteria) this;
        }

        public Criteria andCarNoEqualTo(String value) {
            addCriterion("car_no =", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotEqualTo(String value) {
            addCriterion("car_no <>", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoGreaterThan(String value) {
            addCriterion("car_no >", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoGreaterThanOrEqualTo(String value) {
            addCriterion("car_no >=", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLessThan(String value) {
            addCriterion("car_no <", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLessThanOrEqualTo(String value) {
            addCriterion("car_no <=", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoLike(String value) {
            addCriterion("car_no like", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotLike(String value) {
            addCriterion("car_no not like", value, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoIn(List<String> values) {
            addCriterion("car_no in", values, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotIn(List<String> values) {
            addCriterion("car_no not in", values, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoBetween(String value1, String value2) {
            addCriterion("car_no between", value1, value2, "carNo");
            return (Criteria) this;
        }

        public Criteria andCarNoNotBetween(String value1, String value2) {
            addCriterion("car_no not between", value1, value2, "carNo");
            return (Criteria) this;
        }

        public Criteria andCureTimeIsNull() {
            addCriterion("cure_time is null");
            return (Criteria) this;
        }

        public Criteria andCureTimeIsNotNull() {
            addCriterion("cure_time is not null");
            return (Criteria) this;
        }

        public Criteria andCureTimeEqualTo(Date value) {
            addCriterion("cure_time =", value, "cureTime");
            return (Criteria) this;
        }

        public Criteria andCureTimeNotEqualTo(Date value) {
            addCriterion("cure_time <>", value, "cureTime");
            return (Criteria) this;
        }

        public Criteria andCureTimeGreaterThan(Date value) {
            addCriterion("cure_time >", value, "cureTime");
            return (Criteria) this;
        }

        public Criteria andCureTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("cure_time >=", value, "cureTime");
            return (Criteria) this;
        }

        public Criteria andCureTimeLessThan(Date value) {
            addCriterion("cure_time <", value, "cureTime");
            return (Criteria) this;
        }

        public Criteria andCureTimeLessThanOrEqualTo(Date value) {
            addCriterion("cure_time <=", value, "cureTime");
            return (Criteria) this;
        }

        public Criteria andCureTimeIn(List<Date> values) {
            addCriterion("cure_time in", values, "cureTime");
            return (Criteria) this;
        }

        public Criteria andCureTimeNotIn(List<Date> values) {
            addCriterion("cure_time not in", values, "cureTime");
            return (Criteria) this;
        }

        public Criteria andCureTimeBetween(Date value1, Date value2) {
            addCriterion("cure_time between", value1, value2, "cureTime");
            return (Criteria) this;
        }

        public Criteria andCureTimeNotBetween(Date value1, Date value2) {
            addCriterion("cure_time not between", value1, value2, "cureTime");
            return (Criteria) this;
        }

        public Criteria andLossFeeIsNull() {
            addCriterion("loss_fee is null");
            return (Criteria) this;
        }

        public Criteria andLossFeeIsNotNull() {
            addCriterion("loss_fee is not null");
            return (Criteria) this;
        }

        public Criteria andLossFeeEqualTo(Double value) {
            addCriterion("loss_fee =", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeNotEqualTo(Double value) {
            addCriterion("loss_fee <>", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeGreaterThan(Double value) {
            addCriterion("loss_fee >", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeGreaterThanOrEqualTo(Double value) {
            addCriterion("loss_fee >=", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeLessThan(Double value) {
            addCriterion("loss_fee <", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeLessThanOrEqualTo(Double value) {
            addCriterion("loss_fee <=", value, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeIn(List<Double> values) {
            addCriterion("loss_fee in", values, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeNotIn(List<Double> values) {
            addCriterion("loss_fee not in", values, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeBetween(Double value1, Double value2) {
            addCriterion("loss_fee between", value1, value2, "lossFee");
            return (Criteria) this;
        }

        public Criteria andLossFeeNotBetween(Double value1, Double value2) {
            addCriterion("loss_fee not between", value1, value2, "lossFee");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("user_name is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("user_name is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("user_name =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("user_name <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("user_name >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_name >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("user_name <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("user_name <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("user_name like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("user_name not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("user_name in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("user_name not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("user_name between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("user_name not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andLossTimeIsNull() {
            addCriterion("loss_time is null");
            return (Criteria) this;
        }

        public Criteria andLossTimeIsNotNull() {
            addCriterion("loss_time is not null");
            return (Criteria) this;
        }

        public Criteria andLossTimeEqualTo(Date value) {
            addCriterion("loss_time =", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeNotEqualTo(Date value) {
            addCriterion("loss_time <>", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeGreaterThan(Date value) {
            addCriterion("loss_time >", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("loss_time >=", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeLessThan(Date value) {
            addCriterion("loss_time <", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeLessThanOrEqualTo(Date value) {
            addCriterion("loss_time <=", value, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeIn(List<Date> values) {
            addCriterion("loss_time in", values, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeNotIn(List<Date> values) {
            addCriterion("loss_time not in", values, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeBetween(Date value1, Date value2) {
            addCriterion("loss_time between", value1, value2, "lossTime");
            return (Criteria) this;
        }

        public Criteria andLossTimeNotBetween(Date value1, Date value2) {
            addCriterion("loss_time not between", value1, value2, "lossTime");
            return (Criteria) this;
        }

        public Criteria andIllnessStateIsNull() {
            addCriterion("illness_state is null");
            return (Criteria) this;
        }

        public Criteria andIllnessStateIsNotNull() {
            addCriterion("illness_state is not null");
            return (Criteria) this;
        }

        public Criteria andIllnessStateEqualTo(String value) {
            addCriterion("illness_state =", value, "illnessState");
            return (Criteria) this;
        }

        public Criteria andIllnessStateNotEqualTo(String value) {
            addCriterion("illness_state <>", value, "illnessState");
            return (Criteria) this;
        }

        public Criteria andIllnessStateGreaterThan(String value) {
            addCriterion("illness_state >", value, "illnessState");
            return (Criteria) this;
        }

        public Criteria andIllnessStateGreaterThanOrEqualTo(String value) {
            addCriterion("illness_state >=", value, "illnessState");
            return (Criteria) this;
        }

        public Criteria andIllnessStateLessThan(String value) {
            addCriterion("illness_state <", value, "illnessState");
            return (Criteria) this;
        }

        public Criteria andIllnessStateLessThanOrEqualTo(String value) {
            addCriterion("illness_state <=", value, "illnessState");
            return (Criteria) this;
        }

        public Criteria andIllnessStateLike(String value) {
            addCriterion("illness_state like", value, "illnessState");
            return (Criteria) this;
        }

        public Criteria andIllnessStateNotLike(String value) {
            addCriterion("illness_state not like", value, "illnessState");
            return (Criteria) this;
        }

        public Criteria andIllnessStateIn(List<String> values) {
            addCriterion("illness_state in", values, "illnessState");
            return (Criteria) this;
        }

        public Criteria andIllnessStateNotIn(List<String> values) {
            addCriterion("illness_state not in", values, "illnessState");
            return (Criteria) this;
        }

        public Criteria andIllnessStateBetween(String value1, String value2) {
            addCriterion("illness_state between", value1, value2, "illnessState");
            return (Criteria) this;
        }

        public Criteria andIllnessStateNotBetween(String value1, String value2) {
            addCriterion("illness_state not between", value1, value2, "illnessState");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNull() {
            addCriterion("load_id is null");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNotNull() {
            addCriterion("load_id is not null");
            return (Criteria) this;
        }

        public Criteria andLoadIdEqualTo(Integer value) {
            addCriterion("load_id =", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotEqualTo(Integer value) {
            addCriterion("load_id <>", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThan(Integer value) {
            addCriterion("load_id >", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("load_id >=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThan(Integer value) {
            addCriterion("load_id <", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThanOrEqualTo(Integer value) {
            addCriterion("load_id <=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdIn(List<Integer> values) {
            addCriterion("load_id in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotIn(List<Integer> values) {
            addCriterion("load_id not in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdBetween(Integer value1, Integer value2) {
            addCriterion("load_id between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotBetween(Integer value1, Integer value2) {
            addCriterion("load_id not between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andBack1IsNull() {
            addCriterion("back1 is null");
            return (Criteria) this;
        }

        public Criteria andBack1IsNotNull() {
            addCriterion("back1 is not null");
            return (Criteria) this;
        }

        public Criteria andBack1EqualTo(String value) {
            addCriterion("back1 =", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotEqualTo(String value) {
            addCriterion("back1 <>", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThan(String value) {
            addCriterion("back1 >", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1GreaterThanOrEqualTo(String value) {
            addCriterion("back1 >=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThan(String value) {
            addCriterion("back1 <", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1LessThanOrEqualTo(String value) {
            addCriterion("back1 <=", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Like(String value) {
            addCriterion("back1 like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotLike(String value) {
            addCriterion("back1 not like", value, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1In(List<String> values) {
            addCriterion("back1 in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotIn(List<String> values) {
            addCriterion("back1 not in", values, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1Between(String value1, String value2) {
            addCriterion("back1 between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack1NotBetween(String value1, String value2) {
            addCriterion("back1 not between", value1, value2, "back1");
            return (Criteria) this;
        }

        public Criteria andBack2IsNull() {
            addCriterion("back2 is null");
            return (Criteria) this;
        }

        public Criteria andBack2IsNotNull() {
            addCriterion("back2 is not null");
            return (Criteria) this;
        }

        public Criteria andBack2EqualTo(String value) {
            addCriterion("back2 =", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotEqualTo(String value) {
            addCriterion("back2 <>", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThan(String value) {
            addCriterion("back2 >", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2GreaterThanOrEqualTo(String value) {
            addCriterion("back2 >=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThan(String value) {
            addCriterion("back2 <", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2LessThanOrEqualTo(String value) {
            addCriterion("back2 <=", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Like(String value) {
            addCriterion("back2 like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotLike(String value) {
            addCriterion("back2 not like", value, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2In(List<String> values) {
            addCriterion("back2 in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotIn(List<String> values) {
            addCriterion("back2 not in", values, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2Between(String value1, String value2) {
            addCriterion("back2 between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack2NotBetween(String value1, String value2) {
            addCriterion("back2 not between", value1, value2, "back2");
            return (Criteria) this;
        }

        public Criteria andBack3IsNull() {
            addCriterion("back3 is null");
            return (Criteria) this;
        }

        public Criteria andBack3IsNotNull() {
            addCriterion("back3 is not null");
            return (Criteria) this;
        }

        public Criteria andBack3EqualTo(String value) {
            addCriterion("back3 =", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotEqualTo(String value) {
            addCriterion("back3 <>", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThan(String value) {
            addCriterion("back3 >", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3GreaterThanOrEqualTo(String value) {
            addCriterion("back3 >=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThan(String value) {
            addCriterion("back3 <", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3LessThanOrEqualTo(String value) {
            addCriterion("back3 <=", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Like(String value) {
            addCriterion("back3 like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotLike(String value) {
            addCriterion("back3 not like", value, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3In(List<String> values) {
            addCriterion("back3 in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotIn(List<String> values) {
            addCriterion("back3 not in", values, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3Between(String value1, String value2) {
            addCriterion("back3 between", value1, value2, "back3");
            return (Criteria) this;
        }

        public Criteria andBack3NotBetween(String value1, String value2) {
            addCriterion("back3 not between", value1, value2, "back3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}