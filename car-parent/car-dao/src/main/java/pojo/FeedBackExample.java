package pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FeedBackExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FeedBackExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andFeedBackIdIsNull() {
            addCriterion("feed_back_id is null");
            return (Criteria) this;
        }

        public Criteria andFeedBackIdIsNotNull() {
            addCriterion("feed_back_id is not null");
            return (Criteria) this;
        }

        public Criteria andFeedBackIdEqualTo(Integer value) {
            addCriterion("feed_back_id =", value, "feedBackId");
            return (Criteria) this;
        }

        public Criteria andFeedBackIdNotEqualTo(Integer value) {
            addCriterion("feed_back_id <>", value, "feedBackId");
            return (Criteria) this;
        }

        public Criteria andFeedBackIdGreaterThan(Integer value) {
            addCriterion("feed_back_id >", value, "feedBackId");
            return (Criteria) this;
        }

        public Criteria andFeedBackIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("feed_back_id >=", value, "feedBackId");
            return (Criteria) this;
        }

        public Criteria andFeedBackIdLessThan(Integer value) {
            addCriterion("feed_back_id <", value, "feedBackId");
            return (Criteria) this;
        }

        public Criteria andFeedBackIdLessThanOrEqualTo(Integer value) {
            addCriterion("feed_back_id <=", value, "feedBackId");
            return (Criteria) this;
        }

        public Criteria andFeedBackIdIn(List<Integer> values) {
            addCriterion("feed_back_id in", values, "feedBackId");
            return (Criteria) this;
        }

        public Criteria andFeedBackIdNotIn(List<Integer> values) {
            addCriterion("feed_back_id not in", values, "feedBackId");
            return (Criteria) this;
        }

        public Criteria andFeedBackIdBetween(Integer value1, Integer value2) {
            addCriterion("feed_back_id between", value1, value2, "feedBackId");
            return (Criteria) this;
        }

        public Criteria andFeedBackIdNotBetween(Integer value1, Integer value2) {
            addCriterion("feed_back_id not between", value1, value2, "feedBackId");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNull() {
            addCriterion("table_id is null");
            return (Criteria) this;
        }

        public Criteria andTableIdIsNotNull() {
            addCriterion("table_id is not null");
            return (Criteria) this;
        }

        public Criteria andTableIdEqualTo(Integer value) {
            addCriterion("table_id =", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotEqualTo(Integer value) {
            addCriterion("table_id <>", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThan(Integer value) {
            addCriterion("table_id >", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("table_id >=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThan(Integer value) {
            addCriterion("table_id <", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdLessThanOrEqualTo(Integer value) {
            addCriterion("table_id <=", value, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdIn(List<Integer> values) {
            addCriterion("table_id in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotIn(List<Integer> values) {
            addCriterion("table_id not in", values, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdBetween(Integer value1, Integer value2) {
            addCriterion("table_id between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andTableIdNotBetween(Integer value1, Integer value2) {
            addCriterion("table_id not between", value1, value2, "tableId");
            return (Criteria) this;
        }

        public Criteria andCustomerNameIsNull() {
            addCriterion("customer_name is null");
            return (Criteria) this;
        }

        public Criteria andCustomerNameIsNotNull() {
            addCriterion("customer_name is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerNameEqualTo(String value) {
            addCriterion("customer_name =", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotEqualTo(String value) {
            addCriterion("customer_name <>", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameGreaterThan(String value) {
            addCriterion("customer_name >", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameGreaterThanOrEqualTo(String value) {
            addCriterion("customer_name >=", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameLessThan(String value) {
            addCriterion("customer_name <", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameLessThanOrEqualTo(String value) {
            addCriterion("customer_name <=", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameLike(String value) {
            addCriterion("customer_name like", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotLike(String value) {
            addCriterion("customer_name not like", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameIn(List<String> values) {
            addCriterion("customer_name in", values, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotIn(List<String> values) {
            addCriterion("customer_name not in", values, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameBetween(String value1, String value2) {
            addCriterion("customer_name between", value1, value2, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotBetween(String value1, String value2) {
            addCriterion("customer_name not between", value1, value2, "customerName");
            return (Criteria) this;
        }

        public Criteria andReportTimeIsNull() {
            addCriterion("report_time is null");
            return (Criteria) this;
        }

        public Criteria andReportTimeIsNotNull() {
            addCriterion("report_time is not null");
            return (Criteria) this;
        }

        public Criteria andReportTimeEqualTo(Date value) {
            addCriterion("report_time =", value, "reportTime");
            return (Criteria) this;
        }

        public Criteria andReportTimeNotEqualTo(Date value) {
            addCriterion("report_time <>", value, "reportTime");
            return (Criteria) this;
        }

        public Criteria andReportTimeGreaterThan(Date value) {
            addCriterion("report_time >", value, "reportTime");
            return (Criteria) this;
        }

        public Criteria andReportTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("report_time >=", value, "reportTime");
            return (Criteria) this;
        }

        public Criteria andReportTimeLessThan(Date value) {
            addCriterion("report_time <", value, "reportTime");
            return (Criteria) this;
        }

        public Criteria andReportTimeLessThanOrEqualTo(Date value) {
            addCriterion("report_time <=", value, "reportTime");
            return (Criteria) this;
        }

        public Criteria andReportTimeIn(List<Date> values) {
            addCriterion("report_time in", values, "reportTime");
            return (Criteria) this;
        }

        public Criteria andReportTimeNotIn(List<Date> values) {
            addCriterion("report_time not in", values, "reportTime");
            return (Criteria) this;
        }

        public Criteria andReportTimeBetween(Date value1, Date value2) {
            addCriterion("report_time between", value1, value2, "reportTime");
            return (Criteria) this;
        }

        public Criteria andReportTimeNotBetween(Date value1, Date value2) {
            addCriterion("report_time not between", value1, value2, "reportTime");
            return (Criteria) this;
        }

        public Criteria andCustomerTelIsNull() {
            addCriterion("customer_tel is null");
            return (Criteria) this;
        }

        public Criteria andCustomerTelIsNotNull() {
            addCriterion("customer_tel is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerTelEqualTo(Integer value) {
            addCriterion("customer_tel =", value, "customerTel");
            return (Criteria) this;
        }

        public Criteria andCustomerTelNotEqualTo(Integer value) {
            addCriterion("customer_tel <>", value, "customerTel");
            return (Criteria) this;
        }

        public Criteria andCustomerTelGreaterThan(Integer value) {
            addCriterion("customer_tel >", value, "customerTel");
            return (Criteria) this;
        }

        public Criteria andCustomerTelGreaterThanOrEqualTo(Integer value) {
            addCriterion("customer_tel >=", value, "customerTel");
            return (Criteria) this;
        }

        public Criteria andCustomerTelLessThan(Integer value) {
            addCriterion("customer_tel <", value, "customerTel");
            return (Criteria) this;
        }

        public Criteria andCustomerTelLessThanOrEqualTo(Integer value) {
            addCriterion("customer_tel <=", value, "customerTel");
            return (Criteria) this;
        }

        public Criteria andCustomerTelIn(List<Integer> values) {
            addCriterion("customer_tel in", values, "customerTel");
            return (Criteria) this;
        }

        public Criteria andCustomerTelNotIn(List<Integer> values) {
            addCriterion("customer_tel not in", values, "customerTel");
            return (Criteria) this;
        }

        public Criteria andCustomerTelBetween(Integer value1, Integer value2) {
            addCriterion("customer_tel between", value1, value2, "customerTel");
            return (Criteria) this;
        }

        public Criteria andCustomerTelNotBetween(Integer value1, Integer value2) {
            addCriterion("customer_tel not between", value1, value2, "customerTel");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameIsNull() {
            addCriterion("insurance_name is null");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameIsNotNull() {
            addCriterion("insurance_name is not null");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameEqualTo(String value) {
            addCriterion("insurance_name =", value, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameNotEqualTo(String value) {
            addCriterion("insurance_name <>", value, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameGreaterThan(String value) {
            addCriterion("insurance_name >", value, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameGreaterThanOrEqualTo(String value) {
            addCriterion("insurance_name >=", value, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameLessThan(String value) {
            addCriterion("insurance_name <", value, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameLessThanOrEqualTo(String value) {
            addCriterion("insurance_name <=", value, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameLike(String value) {
            addCriterion("insurance_name like", value, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameNotLike(String value) {
            addCriterion("insurance_name not like", value, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameIn(List<String> values) {
            addCriterion("insurance_name in", values, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameNotIn(List<String> values) {
            addCriterion("insurance_name not in", values, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameBetween(String value1, String value2) {
            addCriterion("insurance_name between", value1, value2, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andInsuranceNameNotBetween(String value1, String value2) {
            addCriterion("insurance_name not between", value1, value2, "insuranceName");
            return (Criteria) this;
        }

        public Criteria andContentrsIsNull() {
            addCriterion("contentrs is null");
            return (Criteria) this;
        }

        public Criteria andContentrsIsNotNull() {
            addCriterion("contentrs is not null");
            return (Criteria) this;
        }

        public Criteria andContentrsEqualTo(String value) {
            addCriterion("contentrs =", value, "contentrs");
            return (Criteria) this;
        }

        public Criteria andContentrsNotEqualTo(String value) {
            addCriterion("contentrs <>", value, "contentrs");
            return (Criteria) this;
        }

        public Criteria andContentrsGreaterThan(String value) {
            addCriterion("contentrs >", value, "contentrs");
            return (Criteria) this;
        }

        public Criteria andContentrsGreaterThanOrEqualTo(String value) {
            addCriterion("contentrs >=", value, "contentrs");
            return (Criteria) this;
        }

        public Criteria andContentrsLessThan(String value) {
            addCriterion("contentrs <", value, "contentrs");
            return (Criteria) this;
        }

        public Criteria andContentrsLessThanOrEqualTo(String value) {
            addCriterion("contentrs <=", value, "contentrs");
            return (Criteria) this;
        }

        public Criteria andContentrsLike(String value) {
            addCriterion("contentrs like", value, "contentrs");
            return (Criteria) this;
        }

        public Criteria andContentrsNotLike(String value) {
            addCriterion("contentrs not like", value, "contentrs");
            return (Criteria) this;
        }

        public Criteria andContentrsIn(List<String> values) {
            addCriterion("contentrs in", values, "contentrs");
            return (Criteria) this;
        }

        public Criteria andContentrsNotIn(List<String> values) {
            addCriterion("contentrs not in", values, "contentrs");
            return (Criteria) this;
        }

        public Criteria andContentrsBetween(String value1, String value2) {
            addCriterion("contentrs between", value1, value2, "contentrs");
            return (Criteria) this;
        }

        public Criteria andContentrsNotBetween(String value1, String value2) {
            addCriterion("contentrs not between", value1, value2, "contentrs");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestIsNull() {
            addCriterion("customer_request is null");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestIsNotNull() {
            addCriterion("customer_request is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestEqualTo(String value) {
            addCriterion("customer_request =", value, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestNotEqualTo(String value) {
            addCriterion("customer_request <>", value, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestGreaterThan(String value) {
            addCriterion("customer_request >", value, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestGreaterThanOrEqualTo(String value) {
            addCriterion("customer_request >=", value, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestLessThan(String value) {
            addCriterion("customer_request <", value, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestLessThanOrEqualTo(String value) {
            addCriterion("customer_request <=", value, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestLike(String value) {
            addCriterion("customer_request like", value, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestNotLike(String value) {
            addCriterion("customer_request not like", value, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestIn(List<String> values) {
            addCriterion("customer_request in", values, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestNotIn(List<String> values) {
            addCriterion("customer_request not in", values, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestBetween(String value1, String value2) {
            addCriterion("customer_request between", value1, value2, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andCustomerRequestNotBetween(String value1, String value2) {
            addCriterion("customer_request not between", value1, value2, "customerRequest");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenIsNull() {
            addCriterion("contentrs_humen is null");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenIsNotNull() {
            addCriterion("contentrs_humen is not null");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenEqualTo(String value) {
            addCriterion("contentrs_humen =", value, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenNotEqualTo(String value) {
            addCriterion("contentrs_humen <>", value, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenGreaterThan(String value) {
            addCriterion("contentrs_humen >", value, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenGreaterThanOrEqualTo(String value) {
            addCriterion("contentrs_humen >=", value, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenLessThan(String value) {
            addCriterion("contentrs_humen <", value, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenLessThanOrEqualTo(String value) {
            addCriterion("contentrs_humen <=", value, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenLike(String value) {
            addCriterion("contentrs_humen like", value, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenNotLike(String value) {
            addCriterion("contentrs_humen not like", value, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenIn(List<String> values) {
            addCriterion("contentrs_humen in", values, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenNotIn(List<String> values) {
            addCriterion("contentrs_humen not in", values, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenBetween(String value1, String value2) {
            addCriterion("contentrs_humen between", value1, value2, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsHumenNotBetween(String value1, String value2) {
            addCriterion("contentrs_humen not between", value1, value2, "contentrsHumen");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleIsNull() {
            addCriterion("contentrs_pepole is null");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleIsNotNull() {
            addCriterion("contentrs_pepole is not null");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleEqualTo(String value) {
            addCriterion("contentrs_pepole =", value, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleNotEqualTo(String value) {
            addCriterion("contentrs_pepole <>", value, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleGreaterThan(String value) {
            addCriterion("contentrs_pepole >", value, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleGreaterThanOrEqualTo(String value) {
            addCriterion("contentrs_pepole >=", value, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleLessThan(String value) {
            addCriterion("contentrs_pepole <", value, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleLessThanOrEqualTo(String value) {
            addCriterion("contentrs_pepole <=", value, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleLike(String value) {
            addCriterion("contentrs_pepole like", value, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleNotLike(String value) {
            addCriterion("contentrs_pepole not like", value, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleIn(List<String> values) {
            addCriterion("contentrs_pepole in", values, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleNotIn(List<String> values) {
            addCriterion("contentrs_pepole not in", values, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleBetween(String value1, String value2) {
            addCriterion("contentrs_pepole between", value1, value2, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andContentrsPepoleNotBetween(String value1, String value2) {
            addCriterion("contentrs_pepole not between", value1, value2, "contentrsPepole");
            return (Criteria) this;
        }

        public Criteria andReceiverIsNull() {
            addCriterion("receiver is null");
            return (Criteria) this;
        }

        public Criteria andReceiverIsNotNull() {
            addCriterion("receiver is not null");
            return (Criteria) this;
        }

        public Criteria andReceiverEqualTo(String value) {
            addCriterion("receiver =", value, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverNotEqualTo(String value) {
            addCriterion("receiver <>", value, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverGreaterThan(String value) {
            addCriterion("receiver >", value, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverGreaterThanOrEqualTo(String value) {
            addCriterion("receiver >=", value, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverLessThan(String value) {
            addCriterion("receiver <", value, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverLessThanOrEqualTo(String value) {
            addCriterion("receiver <=", value, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverLike(String value) {
            addCriterion("receiver like", value, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverNotLike(String value) {
            addCriterion("receiver not like", value, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverIn(List<String> values) {
            addCriterion("receiver in", values, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverNotIn(List<String> values) {
            addCriterion("receiver not in", values, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverBetween(String value1, String value2) {
            addCriterion("receiver between", value1, value2, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverNotBetween(String value1, String value2) {
            addCriterion("receiver not between", value1, value2, "receiver");
            return (Criteria) this;
        }

        public Criteria andReceiverDateIsNull() {
            addCriterion("receiver_date is null");
            return (Criteria) this;
        }

        public Criteria andReceiverDateIsNotNull() {
            addCriterion("receiver_date is not null");
            return (Criteria) this;
        }

        public Criteria andReceiverDateEqualTo(Date value) {
            addCriterion("receiver_date =", value, "receiverDate");
            return (Criteria) this;
        }

        public Criteria andReceiverDateNotEqualTo(Date value) {
            addCriterion("receiver_date <>", value, "receiverDate");
            return (Criteria) this;
        }

        public Criteria andReceiverDateGreaterThan(Date value) {
            addCriterion("receiver_date >", value, "receiverDate");
            return (Criteria) this;
        }

        public Criteria andReceiverDateGreaterThanOrEqualTo(Date value) {
            addCriterion("receiver_date >=", value, "receiverDate");
            return (Criteria) this;
        }

        public Criteria andReceiverDateLessThan(Date value) {
            addCriterion("receiver_date <", value, "receiverDate");
            return (Criteria) this;
        }

        public Criteria andReceiverDateLessThanOrEqualTo(Date value) {
            addCriterion("receiver_date <=", value, "receiverDate");
            return (Criteria) this;
        }

        public Criteria andReceiverDateIn(List<Date> values) {
            addCriterion("receiver_date in", values, "receiverDate");
            return (Criteria) this;
        }

        public Criteria andReceiverDateNotIn(List<Date> values) {
            addCriterion("receiver_date not in", values, "receiverDate");
            return (Criteria) this;
        }

        public Criteria andReceiverDateBetween(Date value1, Date value2) {
            addCriterion("receiver_date between", value1, value2, "receiverDate");
            return (Criteria) this;
        }

        public Criteria andReceiverDateNotBetween(Date value1, Date value2) {
            addCriterion("receiver_date not between", value1, value2, "receiverDate");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andBeiyong2IsNull() {
            addCriterion("beiyong2 is null");
            return (Criteria) this;
        }

        public Criteria andBeiyong2IsNotNull() {
            addCriterion("beiyong2 is not null");
            return (Criteria) this;
        }

        public Criteria andBeiyong2EqualTo(String value) {
            addCriterion("beiyong2 =", value, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong2NotEqualTo(String value) {
            addCriterion("beiyong2 <>", value, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong2GreaterThan(String value) {
            addCriterion("beiyong2 >", value, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong2GreaterThanOrEqualTo(String value) {
            addCriterion("beiyong2 >=", value, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong2LessThan(String value) {
            addCriterion("beiyong2 <", value, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong2LessThanOrEqualTo(String value) {
            addCriterion("beiyong2 <=", value, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong2Like(String value) {
            addCriterion("beiyong2 like", value, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong2NotLike(String value) {
            addCriterion("beiyong2 not like", value, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong2In(List<String> values) {
            addCriterion("beiyong2 in", values, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong2NotIn(List<String> values) {
            addCriterion("beiyong2 not in", values, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong2Between(String value1, String value2) {
            addCriterion("beiyong2 between", value1, value2, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong2NotBetween(String value1, String value2) {
            addCriterion("beiyong2 not between", value1, value2, "beiyong2");
            return (Criteria) this;
        }

        public Criteria andBeiyong3IsNull() {
            addCriterion("beiyong3 is null");
            return (Criteria) this;
        }

        public Criteria andBeiyong3IsNotNull() {
            addCriterion("beiyong3 is not null");
            return (Criteria) this;
        }

        public Criteria andBeiyong3EqualTo(String value) {
            addCriterion("beiyong3 =", value, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andBeiyong3NotEqualTo(String value) {
            addCriterion("beiyong3 <>", value, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andBeiyong3GreaterThan(String value) {
            addCriterion("beiyong3 >", value, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andBeiyong3GreaterThanOrEqualTo(String value) {
            addCriterion("beiyong3 >=", value, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andBeiyong3LessThan(String value) {
            addCriterion("beiyong3 <", value, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andBeiyong3LessThanOrEqualTo(String value) {
            addCriterion("beiyong3 <=", value, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andBeiyong3Like(String value) {
            addCriterion("beiyong3 like", value, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andBeiyong3NotLike(String value) {
            addCriterion("beiyong3 not like", value, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andBeiyong3In(List<String> values) {
            addCriterion("beiyong3 in", values, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andBeiyong3NotIn(List<String> values) {
            addCriterion("beiyong3 not in", values, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andBeiyong3Between(String value1, String value2) {
            addCriterion("beiyong3 between", value1, value2, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andBeiyong3NotBetween(String value1, String value2) {
            addCriterion("beiyong3 not between", value1, value2, "beiyong3");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(String value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(String value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(String value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(String value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(String value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(String value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLike(String value) {
            addCriterion("state like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotLike(String value) {
            addCriterion("state not like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<String> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<String> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(String value1, String value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(String value1, String value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNull() {
            addCriterion("load_id is null");
            return (Criteria) this;
        }

        public Criteria andLoadIdIsNotNull() {
            addCriterion("load_id is not null");
            return (Criteria) this;
        }

        public Criteria andLoadIdEqualTo(Integer value) {
            addCriterion("load_id =", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotEqualTo(Integer value) {
            addCriterion("load_id <>", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThan(Integer value) {
            addCriterion("load_id >", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("load_id >=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThan(Integer value) {
            addCriterion("load_id <", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdLessThanOrEqualTo(Integer value) {
            addCriterion("load_id <=", value, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdIn(List<Integer> values) {
            addCriterion("load_id in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotIn(List<Integer> values) {
            addCriterion("load_id not in", values, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdBetween(Integer value1, Integer value2) {
            addCriterion("load_id between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andLoadIdNotBetween(Integer value1, Integer value2) {
            addCriterion("load_id not between", value1, value2, "loadId");
            return (Criteria) this;
        }

        public Criteria andBeiyong1IsNull() {
            addCriterion("beiyong1 is null");
            return (Criteria) this;
        }

        public Criteria andBeiyong1IsNotNull() {
            addCriterion("beiyong1 is not null");
            return (Criteria) this;
        }

        public Criteria andBeiyong1EqualTo(String value) {
            addCriterion("beiyong1 =", value, "beiyong1");
            return (Criteria) this;
        }

        public Criteria andBeiyong1NotEqualTo(String value) {
            addCriterion("beiyong1 <>", value, "beiyong1");
            return (Criteria) this;
        }

        public Criteria andBeiyong1GreaterThan(String value) {
            addCriterion("beiyong1 >", value, "beiyong1");
            return (Criteria) this;
        }

        public Criteria andBeiyong1GreaterThanOrEqualTo(String value) {
            addCriterion("beiyong1 >=", value, "beiyong1");
            return (Criteria) this;
        }

        public Criteria andBeiyong1LessThan(String value) {
            addCriterion("beiyong1 <", value, "beiyong1");
            return (Criteria) this;
        }

        public Criteria andBeiyong1LessThanOrEqualTo(String value) {
            addCriterion("beiyong1 <=", value, "beiyong1");
            return (Criteria) this;
        }

        public Criteria andBeiyong1Like(String value) {
            addCriterion("beiyong1 like", value, "beiyong1");
            return (Criteria) this;
        }

        public Criteria andBeiyong1NotLike(String value) {
            addCriterion("beiyong1 not like", value, "beiyong1");
            return (Criteria) this;
        }

        public Criteria andBeiyong1In(List<String> values) {
            addCriterion("beiyong1 in", values, "beiyong1");
            return (Criteria) this;
        }

        public Criteria andBeiyong1NotIn(List<String> values) {
            addCriterion("beiyong1 not in", values, "beiyong1");
            return (Criteria) this;
        }

        public Criteria andBeiyong1Between(String value1, String value2) {
            addCriterion("beiyong1 between", value1, value2, "beiyong1");
            return (Criteria) this;
        }

        public Criteria andBeiyong1NotBetween(String value1, String value2) {
            addCriterion("beiyong1 not between", value1, value2, "beiyong1");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}