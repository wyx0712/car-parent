/**
 * 
 */
package controller.dataChooseController;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import pojo.Carnotoparts;
import pojo.DataChoose;
import pojo.DataMust;
import pojo.Lossinfo;
import pojo.ReportInfo;
import pojo.SurveyInfo;
import pojo.VO.lll.DataChooseVO;
import service.auditService.AuditService;

/**
 * @author 亮亮 2018年10月25日
 * 
 */
// 获取以确审的案件信息
@Controller
public class dataChooseController {
	// 自动注入 serviece层方法
	@Autowired
	private AuditService auditService;

	@RequestMapping("/frists")
	public String test1(Model model) {
		// 查询所有值
		List<ReportInfo> list = auditService.getReportInfoList();

		List<DataChooseVO> listVO = new ArrayList<DataChooseVO>();

		for (ReportInfo reportInfo11 : list) {
			Integer reportNo = reportInfo11.getReportNo();
			DataChooseVO vo = new DataChooseVO();
			vo.setReportNo(reportNo);

			listVO.add(vo);
		}
		// 将VO对象传递到前端页面
		model.addAttribute("dataChooseVO", listVO);
		return "backstage";
	}

	@RequestMapping(value = "/detail", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String getDetai(Integer reportNo) {
		DataChooseVO vo = new DataChooseVO();

		// 查询所有值
		List<ReportInfo> list = new ArrayList<ReportInfo>();

		ReportInfo reportInfo = auditService.getReportInfo(reportNo);
		list.add(reportInfo);

		// 根据报案号 查找查勘表
		SurveyInfo surveyInfo1 = auditService.getSurveyInfo1(reportNo);
		vo.setReportNo(reportNo);
		vo.setReportInfoList(list);
		vo.setDangerArea(surveyInfo1.getDangerArea());
		vo.setSurveyName(surveyInfo1.getSurveyName());
		String js = JSON.toJSONString(vo);
		return js;

	}

	@RequestMapping(value = "/duomos", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String getduomo(Integer reportNo) {
		SurveyInfo surveyInfo1 = auditService.getSurveyInfo1(reportNo);

		DataChooseVO vo = new DataChooseVO();
		vo.setReportName(surveyInfo1.getReportName());
		vo.setDriverName(surveyInfo1.getDriverName());
		vo.setCarType(surveyInfo1.getCarType());
		vo.setDriverTel(surveyInfo1.getDriverTel());
		vo.setRoadInfo(surveyInfo1.getRoadInfo());
		vo.setDangerThrough(surveyInfo1.getDangerThrough());
		vo.setSurveyOpinion(surveyInfo1.getSurveyOpinion());
		vo.setImgOne(surveyInfo1.getImgOne());
		vo.setImgTwo(surveyInfo1.getImgTwo());
		vo.setImgThree(surveyInfo1.getImgThree());
		vo.setSurveyName(surveyInfo1.getSurveyName());
		vo.setCompanyName(surveyInfo1.getCompanyName());
		vo.setSurveyTime(surveyInfo1.getSurveyTime());
		String js = JSON.toJSONString(vo);

		return js;
	}

	// 通过报案编号 查找本车定损信息
	@RequestMapping(value = "/dingsuns", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String getDingsun(Integer reportNo) {

		// 根据前端传递的reportno查询reportinfo
		ReportInfo reportInfo = auditService.getReportInfo(reportNo);

		Lossinfo lossinfo = auditService.getLossinfo(reportNo);
		Integer partsId = lossinfo.getPartsId();
		Carnotoparts carnotoparts = auditService.getCarnotoparts(partsId);

		DataChooseVO vo = new DataChooseVO();

		vo.setRepairShop(lossinfo.getRepairShop());
		vo.setRepairType(lossinfo.getRepairType());
		vo.setCarOwner(lossinfo.getCarOwner());
		vo.setCarNo(lossinfo.getCarNo());
		vo.setCarMoney(lossinfo.getCarMoney());
		vo.setUserName(lossinfo.getUserName());
		vo.setLossTime(lossinfo.getLossTime());
		vo.setLossOpinion(lossinfo.getLossOpinion());
		vo.setPartsName(carnotoparts.getPartsName());
		vo.setLossPrices(carnotoparts.getLossPrices());
		vo.setLossFee(carnotoparts.getLossFee());
		vo.setLossMoney(carnotoparts.getLossMoney());
		vo.setCarMoney(lossinfo.getCarMoney());
		vo.setLossOpinion(lossinfo.getLossOpinion());
		String js = JSON.toJSONString(vo);

		return js;
	}

	// 通过报案编号 查找资料信息
	@RequestMapping(value = "/ziliaoxx", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String getZiliaoxx(Integer reportNo) {
		// 通过报案号查找资料信息A,B
		// DataChoose dataChoose = auditService.getDataChoose(reportNo);
		// DataMust dataMust = auditService.getDataMust(reportNo);
		// 假数据
		DataChoose dataChoose = auditService.getDataChoose(1);
		DataMust dataMust = auditService.getDataMust(1);
		// 创建vo类
		DataChooseVO vo = new DataChooseVO();

		// 创建集合存储查询出来的资料信息A,B
		List<DataChoose> list1 = new ArrayList<DataChoose>();
		List<DataMust> list2 = new ArrayList<DataMust>();

		// 添加资料信息A,B
		list1.add(dataChoose);
		list2.add(dataMust);

		// 将集合添加入VO类
		vo.setDaList(list1);
		vo.setDoList(list2);
		String js = JSON.toJSONString(vo);

		return js;
	}

}
