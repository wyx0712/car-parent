package controller.surveyController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import aop.ControllerLog;
import pojo.CarMessage;
import pojo.CarMessageExample;
import pojo.CarMessageExample.Criteria;
import pojo.Carnotoparts;
import pojo.CarnotopartsExample;
import pojo.HurtInfo;
import pojo.HurtInfoExample;
import pojo.Hurtrelation;
import pojo.HurtrelationExample;
import pojo.Lossinfo;
import pojo.LossinfoExample;
import service.surveyService.impl.SurveyServiceImpl;

@Controller
public class SurveyLossController {
	@Autowired
	private SurveyServiceImpl surveyServiceImpl;

	// 本车定损前台显示页面数据
	@RequestMapping("/surveylossone_thq")
	public String chakan(Model model) {
		LossinfoExample example = new LossinfoExample();
		pojo.LossinfoExample.Criteria criteria = example.createCriteria();
		criteria.andSignEqualTo("1");
		List<Lossinfo> lossinfo = surveyServiceImpl.seeLossinfo(example);
		model.addAttribute("lossinfo", lossinfo);

		return "surveylossone_thq";
	}

	// 三车定损前台显示页面数据
	@RequestMapping("/surveylosstwo_thq")
	public String surveylosstwo_thq(Model model) {
		LossinfoExample example = new LossinfoExample();
		pojo.LossinfoExample.Criteria criteria = example.createCriteria();
		criteria.andSignEqualTo("2");
		List<Lossinfo> lossinfo = surveyServiceImpl.seeLossinfo(example);
		model.addAttribute("lossinfo", lossinfo);

		return "surveylosstwo_thq";
	}

	// 人伤调查前台显示页面数据
	@RequestMapping("/surveypeople_thq")
	public String surveypeople_thq(Model model) {

		List<HurtInfo> list = surveyServiceImpl.selectHurtInfobyExample(null);

		model.addAttribute("list", list);

		return "surveypeopleshow_thq";
	}

	// 本车定损
	@RequestMapping("/addownloss")
	public String addOwnLoss(Integer surveyId, Model model) {

		if (surveyId != null) {
			// 传值到修改页面
			LossinfoExample example = new LossinfoExample();
			pojo.LossinfoExample.Criteria criteria = example.createCriteria();
			criteria.andReportNoEqualTo(surveyId);
			Lossinfo lossinfo = surveyServiceImpl.selectlossByExample(example);

			CarnotopartsExample example2 = new CarnotopartsExample();
			pojo.CarnotopartsExample.Criteria createCriteria = example2.createCriteria();
			createCriteria.andPartsIdEqualTo(surveyId);
			List<Carnotoparts> carnotoparts = surveyServiceImpl.selectCarnotoparts(example2);
			model.addAttribute("lossinfo", lossinfo);
			model.addAttribute("carnotoparts", carnotoparts);
			model.addAttribute("operation", "update");
		} else {
			// 传值到添加页面
			CarMessageExample example = new CarMessageExample();
			Criteria criteria = example.createCriteria();
			criteria.andSurveyCaroneEqualTo(1);

			List<CarMessage> list = surveyServiceImpl.selectCarMessage(example);

			model.addAttribute("carMessage", list);
			model.addAttribute("operation", "add");
		}

		return "surveyownaddLoss_thq";
	}

	// 本车定损添加/修改
	@RequestMapping("/doloss")
	@ControllerLog(description = "查勘人员进行了本车定损添加")
	public String doLoss(Lossinfo lossinfo, String operation, Integer reportNo, String[] partsName,
			Integer[] partsCount, double[] lossFee, double[] lossMoney, double[] lossPrices) {

		if (operation.equals("add")) {
			CarMessage carMessage = new CarMessage();
			CarMessageExample example = new CarMessageExample();
			Criteria criteria = example.createCriteria();
			criteria.andReportNoEqualTo(reportNo);
			carMessage.setSurveyCarone(2);
			surveyServiceImpl.updateBycarmessKeyselect(carMessage, example);
			if (partsName.length > 0) {
				for (int i = 0; i < partsName.length; i++) {
					Carnotoparts carnotoparts = new Carnotoparts();
					carnotoparts.setPartsName(partsName[i]);
					carnotoparts.setPartsCount(partsCount[i]);
					carnotoparts.setLossFee(lossFee[i]);
					carnotoparts.setLossMoney(lossMoney[i]);
					carnotoparts.setLossPrices(lossPrices[i]);
					carnotoparts.setPartsId(reportNo);
					carnotoparts.setCarNo(lossinfo.getCarNo());
					carnotoparts.setCreateTime(new Date());
					carnotoparts.setTableId(3005);
					surveyServiceImpl.addcarnotoparts(carnotoparts);
				}
			}

			lossinfo.setLoadId(3);
			lossinfo.setPartsId(reportNo);
			lossinfo.setCreateTime(new Date());
			lossinfo.setLossTime(new Date());
			lossinfo.setSign("1");
			lossinfo.setTableId(3002);
			surveyServiceImpl.lossinfoinselt(lossinfo);
			return "redirect:/chakan.do";

		} else {
			LossinfoExample example = new LossinfoExample();
			pojo.LossinfoExample.Criteria createCriteria = example.createCriteria();
			createCriteria.andReportNoEqualTo(reportNo);
			createCriteria.andSignEqualTo("1");
			lossinfo.setUpdateTime(new Date());
			surveyServiceImpl.updatelossKeySelective(lossinfo, example);

			CarnotopartsExample example2 = new CarnotopartsExample();
			pojo.CarnotopartsExample.Criteria criteria = example2.createCriteria();
			criteria.andPartsIdEqualTo(reportNo);
			surveyServiceImpl.deleteByExample(example2);
			if (partsName.length > 0) {
				for (int i = 0; i < partsName.length; i++) {
					Carnotoparts carnotoparts = new Carnotoparts();
					carnotoparts.setPartsName(partsName[i]);
					carnotoparts.setPartsCount(partsCount[i]);
					carnotoparts.setLossFee(lossFee[i]);
					carnotoparts.setLossMoney(lossMoney[i]);
					carnotoparts.setLossPrices(lossPrices[i]);
					carnotoparts.setPartsId(reportNo);
					carnotoparts.setCarNo(lossinfo.getCarNo());
					carnotoparts.setCreateTime(new Date());
					carnotoparts.setTableId(3005);
					surveyServiceImpl.addcarnotoparts(carnotoparts);
				}
			}

		}

		return "redirect:/chakan.do";
	}

	// 三车定损
	@RequestMapping("/addtwoloss")
	public String addtwoloss(Integer surveyId, Model model) {

		if (surveyId != null) {
			// 传值到修改页面
			Lossinfo lossinfo = surveyServiceImpl.selectlossinfoByreportno(surveyId);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			model.addAttribute("time", sdf.format(lossinfo.getUpdateTime()));
			model.addAttribute("lossinfo", lossinfo);
			model.addAttribute("operation", "update");
		} else {
			// 传值到添加页面
			CarMessageExample example = new CarMessageExample();
			Criteria criteria = example.createCriteria();
			criteria.andSurveyCartwoEqualTo(1);

			List<CarMessage> list = surveyServiceImpl.selectCarMessage(example);

			model.addAttribute("carMessage", list);
			model.addAttribute("operation", "add");
		}

		return "surveytwoaddLoss_thq";
	}

	// 三车定损添加/修改
	@RequestMapping("/dolosstwo")
	@ControllerLog(description = "查勘人员进行了三车定损添加")
	public String dolosstwo(Lossinfo lossinfo, String operation, Integer reportNo, String[] partsName,
			Integer[] partsCount, double[] lossFee, double[] lossMoney, double[] lossPrices) {

		if (operation.equals("add")) {
			CarMessage carMessage = new CarMessage();
			CarMessageExample example = new CarMessageExample();
			Criteria criteria = example.createCriteria();
			criteria.andReportNoEqualTo(reportNo);
			carMessage.setSurveyCartwo(2);
			surveyServiceImpl.updateBycarmessKeyselect(carMessage, example);
			if (partsName != null && partsName.length > 0) {
				for (int i = 0; i < partsName.length; i++) {
					Carnotoparts carnotoparts = new Carnotoparts();
					carnotoparts.setPartsName(partsName[i]);
					carnotoparts.setPartsCount(partsCount[i]);
					carnotoparts.setLossFee(lossFee[i]);
					carnotoparts.setLossMoney(lossMoney[i]);
					carnotoparts.setLossPrices(lossPrices[i]);
					carnotoparts.setPartsId(reportNo + 1000);// 本车+1000和三车区别开
					carnotoparts.setCarNo(lossinfo.getCarNo());
					carnotoparts.setCreateTime(new Date());
					carnotoparts.setTableId(3005);
					surveyServiceImpl.addcarnotoparts(carnotoparts);
				}
			}

			lossinfo.setLoadId(3);
			lossinfo.setPartsId(reportNo);
			lossinfo.setCreateTime(new Date());
			lossinfo.setLossTime(new Date());
			lossinfo.setSign("2");
			lossinfo.setTableId(3002);
			surveyServiceImpl.lossinfoinselt(lossinfo);
			return "redirect:/chakan.do";

		} else {

		}

		return "redirect:/chakan.do";
	}

	// 人伤调查
	@RequestMapping("/addpeopleinfo")
	public String addpeopleinfo(Integer surveyId, Model model) {

		if (surveyId != null) {
			HurtInfoExample example = new HurtInfoExample();
			pojo.HurtInfoExample.Criteria criteria = example.createCriteria();
			criteria.andReportNoEqualTo(surveyId);
			List<HurtInfo> list = surveyServiceImpl.selectHurtInfobyExample(example);
			HurtrelationExample example2 = new HurtrelationExample();
			pojo.HurtrelationExample.Criteria criteria2 = example2.createCriteria();
			criteria2.andReportNoEqualTo(surveyId);
			List<Hurtrelation> list2 = surveyServiceImpl.selectHurtrelationexampl(example2);
			model.addAttribute("hurtInfo", list.get(0));
			model.addAttribute("hurtrelation", list2);
			model.addAttribute("operation", "update");
		} else {
			// 传值到添加页面
			CarMessageExample example = new CarMessageExample();
			Criteria criteria = example.createCriteria();
			criteria.andSurveyPeopleEqualTo(1);

			List<CarMessage> list = surveyServiceImpl.selectCarMessage(example);

			model.addAttribute("carMessage", list);
			model.addAttribute("operation", "add");
		}

		return "surveyinjuredadd_thq";
	}

	// 人伤调查添加/修改
	@RequestMapping("/addpeoplemess")
	@ControllerLog(description = "查勘人员进行了人伤调查添加")
	public String addpeoplemess(HurtInfo hurtInfo, String operation, String[] name, String[] genders, String[] ages,
			String[] phones, String[] relation) {

		if (operation.equals("add")) {
			CarMessage carMessage = new CarMessage();
			CarMessageExample example = new CarMessageExample();
			Criteria criteria = example.createCriteria();
			criteria.andReportNoEqualTo(hurtInfo.getReportNo());
			carMessage.setSurveyPeople(2);
			surveyServiceImpl.updateBycarmessKeyselect(carMessage, example);
			if (name != null && name.length > 0) {
				for (int i = 0; i < name.length; i++) {
					Hurtrelation hurtrelation = new Hurtrelation();
					hurtrelation.setName(name[i]);
					hurtrelation.setGender(genders[i]);
					hurtrelation.setAge(ages[i]);
					hurtrelation.setPhone(phones[i]);
					hurtrelation.setRelation(relation[i]);
					hurtrelation.setTableId(3004);
					hurtrelation.setLoadId(3);
					hurtrelation.setReportNo(hurtInfo.getReportNo());
					surveyServiceImpl.insertHurttion(hurtrelation);
				}
			}
			hurtInfo.setTableId(3003);
			hurtInfo.setLoadId(3);
			hurtInfo.setLossTime(new Date());
			hurtInfo.setCreateTime(new Date());
			surveyServiceImpl.insertHurt(hurtInfo);

		} else {

			HurtInfoExample example2 = new HurtInfoExample();
			pojo.HurtInfoExample.Criteria criteria = example2.createCriteria();
			criteria.andReportNoEqualTo(hurtInfo.getReportNo());
			hurtInfo.setUpdateTime(new Date());
			surveyServiceImpl.updatehurtKeySelective(hurtInfo, example2);

			HurtrelationExample example = new HurtrelationExample();
			pojo.HurtrelationExample.Criteria createCriteria = example.createCriteria();
			createCriteria.andReportNoEqualTo(hurtInfo.getReportNo());
			surveyServiceImpl.deleteHurtrelationByExample(example);
			if (name != null && name.length > 0) {
				for (int i = 0; i < name.length; i++) {
					Hurtrelation hurtrelation = new Hurtrelation();
					hurtrelation.setName(name[i]);
					hurtrelation.setGender(genders[i]);
					hurtrelation.setAge(ages[i]);
					hurtrelation.setPhone(phones[i]);
					hurtrelation.setRelation(relation[i]);
					hurtrelation.setTableId(3004);
					hurtrelation.setLoadId(3);
					hurtrelation.setReportNo(hurtInfo.getReportNo());
					surveyServiceImpl.insertHurttion(hurtrelation);
				}
			}

		}

		return "redirect:/chakan.do";
	}

}
