package controller.surveyController;



import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;

import aop.ControllerLog;
import pojo.CarMessage;
import pojo.CarMessageExample;
import pojo.User;
import pojo.CarMessageExample.Criteria;
import pojo.ReportInfo;
import pojo.ReportInfoExample;
import service.surveyService.impl.SurveyServiceImpl;



@Controller
public class SsubmitController {
	
	@Autowired
	private SurveyServiceImpl surveyServiceImpl;
	
	
	@RequestMapping("/suvreysubmit_thq")
	 @ControllerLog(description = "查勘人员进行了案件的提交")
	public String surveylookshow_thq(Integer reportNos,HttpSession session) {
		CarMessage carMessage = new CarMessage();
		CarMessageExample example = new CarMessageExample();
		Criteria criteria = example.createCriteria();
		criteria.andReportNoEqualTo(reportNos);
		carMessage.setSurveyNow(1);
		  Date currentTime = new Date();
		  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		  String dateString = formatter.format(currentTime);
		  User loginuser = (User) session.getAttribute("user");
		  
		  carMessage.setBack2(loginuser.getRealName());
		carMessage.setBack1(dateString);
		surveyServiceImpl.updateBycarmessKeyselect(carMessage, example);
		
		ReportInfo reportInfo = new ReportInfo();
		reportInfo.setDangerTime(new Date());
		ReportInfoExample example2 = new ReportInfoExample();
		pojo.ReportInfoExample.Criteria criteria2 = example2.createCriteria();
		criteria2.andReportNoEqualTo(reportNos);
		surveyServiceImpl.updatereportinfo(reportInfo, example2);
		
		
		
		return "redirect:/surveyshow_thq.do";
	}
	@RequestMapping("/suvreysubmittwo_thq")
	 @ControllerLog(description = "查勘人员进行了退回案件的再次提交")
	public String suvreysubmittwo_thq(Integer reportNos,HttpSession session) {
		CarMessage carMessage = new CarMessage();
		CarMessageExample example = new CarMessageExample();
		Criteria criteria = example.createCriteria();
		criteria.andReportNoEqualTo(reportNos);
		carMessage.setSurveyNow(4);
		  Date currentTime = new Date();
		  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		  String dateString = formatter.format(currentTime);
		  User loginuser = (User) session.getAttribute("user");
		  
		  carMessage.setBack2(loginuser.getRealName());
		carMessage.setBack1(dateString);
		surveyServiceImpl.updateBycarmessKeyselect(carMessage, example);
		
		return "redirect:/surveyshowfu_thq.do";
	}
	 
	
	
	
	
	
}
