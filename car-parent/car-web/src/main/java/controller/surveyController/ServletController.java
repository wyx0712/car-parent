package controller.surveyController;



import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;



import net.sf.json.JSONObject;
import pojo.CarMessage;
import pojo.CarMessageExample;
import pojo.CarMessageExample.Criteria;
import pojo.Lossinfo;
import pojo.ReportInfo;
import pojo.ReportInfoExample;
import pojo.SurveyInfo;
import pojo.SurveyInfoExample;
import service.surveyService.impl.SurveyServiceImpl;




@Controller
public class ServletController {
	@Autowired
	private SurveyServiceImpl surveyServiceImpl;
	
	@RequestMapping("/shownumber")
	@ResponseBody
	public JSONObject shownumber() {
		JSONObject jsonObject = new JSONObject();
		//1案件调查数量
		CarMessageExample example1 = new CarMessageExample();
		Criteria criteria1 = example1.createCriteria();
		criteria1.andSurveyNowEqualTo(0);
		int i1 = surveyServiceImpl.countByExample(example1);
		jsonObject.put("num1", i1);
		//2待查勘数量
		CarMessageExample example2 = new CarMessageExample();
		Criteria criteria2 = example2.createCriteria();
		criteria2.andSurveyLookEqualTo(1);
		int i2 = surveyServiceImpl.countByExample(example2);
		jsonObject.put("num2", i2);
		//3待本车定损数量
		CarMessageExample example3 = new CarMessageExample();
		Criteria criteria3 = example3.createCriteria();
		criteria3.andSurveyCaroneEqualTo(1);
		int i3 = surveyServiceImpl.countByExample(example3);
		jsonObject.put("num3", i3);
		//4待三车定损数量
		CarMessageExample example4 = new CarMessageExample();
		Criteria criteria4 = example4.createCriteria();
		criteria4.andSurveyCartwoEqualTo(1);
		int i4 = surveyServiceImpl.countByExample(example4);
		jsonObject.put("num4", i4);
		//5人伤调查数量
		CarMessageExample example5 = new CarMessageExample();
		Criteria criteria5 = example5.createCriteria();
		criteria5.andSurveyPeopleEqualTo(1);
		int i5 = surveyServiceImpl.countByExample(example5);
		jsonObject.put("num5", i5);
		//6以提交待审核案件
		CarMessageExample example6 = new CarMessageExample();
		Criteria criteria6 = example6.createCriteria();
		criteria6.andSurveyNowEqualTo(1);
		int i6 = surveyServiceImpl.countByExample(example6);
		jsonObject.put("num6", i6);
		//被退回案件
		CarMessageExample example7 = new CarMessageExample();
		Criteria criteria7 = example7.createCriteria();
		criteria7.andSurveyNowEqualTo(3);
		int i7 = surveyServiceImpl.countByExample(example7);
		jsonObject.put("num7", i7);
		
		
		
		return jsonObject;
	}
	
	@RequestMapping("/surveyshow_thq")
	public String surveyshow_thq(Model model) {
		
		CarMessageExample example = new CarMessageExample();
		Criteria criteria = example.createCriteria();
			
		criteria.andSurveyNowEqualTo(0);		
		List<CarMessage> list = surveyServiceImpl.selectCarMessage(example);		
		model.addAttribute("list", list);		
		return "surveyshow_thq";
	}
	
	@RequestMapping("/surveyshows_thq")
	public String surveyshows_thq(Model model,String surveyType,String surveyName) {
		
	
		CarMessageExample example = new CarMessageExample();
		Criteria criteria = example.createCriteria();
		if(surveyType.equals("1") ){
			criteria.andReportNoEqualTo(Integer.parseInt(surveyName));			
		}else if(surveyType.equals("2")){
			criteria.andPolicyIdLike("%"+surveyName+"%");
		}else if(surveyType.equals("3")){
			criteria.andReportNameLike("%"+surveyName+"%");
		}
		
		
		
		criteria.andSurveyNowEqualTo(0);
		
		List<CarMessage> list = surveyServiceImpl.selectCarMessage(example);
		
		model.addAttribute("list", list);
		
		return "surveyshow_thq";
	}
	
	
	@RequestMapping("/surveyshowti_thq")
	public String surveyshowti_thq(Model model) {
		
		CarMessageExample example = new CarMessageExample();
		Criteria criteria = example.createCriteria();
		criteria.andSurveyNowEqualTo(1);
		
		List<CarMessage> list = surveyServiceImpl.selectCarMessage(example);
		
		model.addAttribute("list", list);
		
		return "surveyshowti_thq";
	}

	
		@RequestMapping("/surveyshowfu_thq")
		public String surveyshowfu_thq(Model model) {
			
			CarMessageExample example = new CarMessageExample();
			Criteria criteria = example.createCriteria();
			criteria.andSurveyNowEqualTo(3);
			
			List<CarMessage> list = surveyServiceImpl.selectCarMessage(example);
			
			model.addAttribute("list", list);
			
			return "surveyshowfu_thq";
		}
	
	
	@RequestMapping("/surveylookshow_thq")
	public String surveylookshow_thq(Model model) {
		
		List<SurveyInfo> list = surveyServiceImpl.selectSurveyInfoByExample(null);
		
		model.addAttribute("surveyInfo", list);
		return "surveylookshow_thq";
	}
	
	@RequestMapping("/surveylookshows_thq")
	public String surveylookshows_thq(Model model,String surveyType,String surveyName) {
		SurveyInfoExample example = new SurveyInfoExample();
		pojo.SurveyInfoExample.Criteria criteria = example.createCriteria();
		if(surveyType.equals("1") ){
				
		}else if(surveyType.equals("2")){
			criteria.andReportNoEqualTo(Integer.parseInt(surveyName));	
		}else if(surveyType.equals("3")){
			criteria.andReportNameLike("%"+surveyName+"%");
		}
		
		
		List<SurveyInfo> list = surveyServiceImpl.selectSurveyInfoByExample(example);
		
		model.addAttribute("surveyInfo", list);
		return "surveylookshow_thq";
	}
	
	
	
	
	
	@RequestMapping("/demoy")
    public String test01(Integer surveyId, Model model){
        if(surveyId != null){
          //暂时不能修改
            model.addAttribute("operation", "update");
        }else{
            //传值到添加页面
        	CarMessageExample example = new CarMessageExample();
    		Criteria criteria = example.createCriteria();
    		criteria.andSurveyLookEqualTo(1);
    		
    		List<CarMessage> list = surveyServiceImpl.selectCarMessage(example);
           
           
    		model.addAttribute("carMessage", list);
            model.addAttribute("operation", "add");
        }
        
        return "surveylookadd_thq";
    }
	
	
	
	
	
	 @RequestMapping("/showreportNo")
	 @ResponseBody
		public JSONObject showreportNo(String reportNo) {
		
		 JSONObject jsonObject = new JSONObject();
		 
			if(reportNo.equals("1")){
				jsonObject.put("error", 1);
				System.out.println(2);
				return jsonObject;
			}
			ReportInfoExample example = new ReportInfoExample();
			pojo.ReportInfoExample.Criteria criteria = example.createCriteria();
			criteria.andReportNoEqualTo(Integer.parseInt(reportNo));
			ReportInfo info = surveyServiceImpl.selectreportByreno(example);
			jsonObject.put("policyId", info.getPolicyId());
			jsonObject.put("driverName", info.getDriverName());
			jsonObject.put("reportName", info.getReportName());
			jsonObject.put("reporTel", info.getReporTel());
			jsonObject.put("driverTel", info.getDriverTel());
			
			return jsonObject;
		}
		
	 
	
	 @RequestMapping("/doSurvey")
	    public String doSurvey(@RequestParam(value = "img", required = false) MultipartFile[] img, SurveyInfo si,
	            String operation,Integer num1,Integer num2,HttpServletRequest request) throws IllegalStateException, IOException {
		 
	        for (int i = 0; i < img.length; i++) {
	            MultipartFile imga = img[i];
	            if (!imga.isEmpty()) {
	                //获取文件名称
	                String filename = imga.getOriginalFilename();
	                //获得文件名的后缀
	                String suffix = FilenameUtils.getExtension(filename);
	                //判断文件是否是图片格式
	                String[] imgSuffix = {"jpg","png","bmp","gif","jpeg"};
	                for (int j = 0; j < imgSuffix.length; j++) {
	                    
	                    if(!suffix.equalsIgnoreCase(imgSuffix[i])){
	                        //不是图片格式的处理
	                        
	                    }
	                }
	                
	                //获取服务器绝对路径
	                String realPath = request.getSession().getServletContext().getRealPath("/imgs");
	               
	                //获取时间戳
	                long currentTimeMillis = System.currentTimeMillis();
	                
	                //创建新的图片名
	                String newFileName = currentTimeMillis + "." + suffix;
	               
	                //构建图片路径
	                File file = new File(realPath + "/" + newFileName);
	                
	                
	                if (!file.exists()) {
	                    file.mkdirs();
	                }
	                //保存图片
	                imga.transferTo(file);
	                
	                if(i == 0){
	                    si.setImgOne("imgs/"+newFileName);
	                }else if(i == 1){
	                    si.setImgTwo("imgs/"+newFileName);
	                }else{
	                    si.setImgThree("imgs/"+newFileName);
	                }

	            }

	        }
	        if("add".equals(operation)){
	            //添加
	            si.setCreateTime(new Date());
	            si.setSurveyTime(new Date());
	            si.setTableId(3001);
	            si.setLoadId(3);
	            CarMessage message = new CarMessage();
	            CarMessageExample example = new CarMessageExample();
	            Criteria criteria = example.createCriteria();
	            criteria.andReportNoEqualTo(si.getReportNo());
	            message.setSurveyLook(2);
	            message.setSurveyCarone(1);
	           
	            if(num1==1){
	            	message.setSurveyCartwo(1);
	        		
	            }else{
	            	message.setSurveyCartwo(0);
	            }
	            if(num2==1){
	            	message.setSurveyPeople(1);
	            }else{
	            	message.setSurveyPeople(0);
	            }
	            surveyServiceImpl.updateBycarmessKeyselect(message, example);
	            
	            
	            surveyServiceImpl.insert(si);
	            
	            
	        }else{
	            //修改 不能修改
	           
	        }
	        
	        return "redirect:/chakan.do";
	    }
	    
	 @RequestMapping("/chakan")
		public String chakan(Model model) {
			
			
			
			return "surveyclose_thq";
		}
	
	
	
	
}
