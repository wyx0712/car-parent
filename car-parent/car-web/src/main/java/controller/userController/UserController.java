/**
 * 
 */
package controller.userController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import aop.ControllerLog;
import pojo.DatabaseBackup;
import pojo.Role;
import pojo.User;
import service.userService.UserService;
import utils.DatabaseBackupUtil;
import utils.ExcelExportWrapper;
import utils.ExportExcel;

/**
 * @author 王耀星
 * @date 2018年10月22日
 */
@Controller
public class UserController {
	@Autowired
	private UserService userService;

	// 前往登陆页面
	@RequestMapping("/login")
	public String loginIn() {

		return "login_wyx";
	}

	// 处理登陆页面提交的信息
	@ControllerLog(description = "登录")
	@RequestMapping("/doLogin_wyx")
	public String doLogin(User user, HttpSession session, Model model) {
		Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(user.getUserName(), user.getPassword());
		try {
			subject.login(token); // 调用realm的进行token验证
			User user2 = userService.selectUser(user);
			// 验证通过后，在session中存入username，
			session.setAttribute("user", user2);
			// 根据user的roleId，分别进入不同的主页面
			Integer roleid = user2.getRoleId();
			switch (roleid) {
			case 1: // 进入超级管理员页面
				return "admin_main_wyx";
			case 2: // 进入报案委托页面
				return "lzw";
			case 3: // 进入查勘定损页面
				return "survey_thq";
			case 4: // 核赔页面
				return "redirect:/frists.do";
			case 5: // 综合管理页面
				return "kuangjia_lzj";
			default:
				return "redirect:/login.do";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "登录信息有误！！！");
			return "login_wyx";
		}

		/*
		 * User user2 = userService.selectUser(user); if (user2 == null) {
		 * model.addAttribute("error", "登录信息有误！！！"); return "login_wyx"; } //
		 * 验证通过后，在session中存入username， session.setAttribute("user", user2); //
		 * 根据user的roleId，分别进入不同的主页面 Integer roleid = user2.getRoleId(); switch
		 * (roleid) { case 1: // 进入超级管理员页面 return "admin_main_wyx"; case 2: //
		 * 进入报案委托页面 return "lzw"; case 3: // 进入查勘定损页面 return "survey_thq"; case
		 * 4: // 核赔页面 return "redirect:/frists.do"; case 5: // 综合管理页面 return
		 * "kuangjia_lzj"; default: return "redirect:/login.do"; }
		 */

	}

	// 注销（切换账号或者注销）
	@ControllerLog(description = "退出系统")
	@RequestMapping("/loginOut")
	public String loginOut(HttpSession session) {
		// 注销时，销毁session中的user
		session.removeAttribute("user");
		return "redirect:/login.do";
	}

	// 进入管理员页面后的主桌面
	@RequestMapping("/welcome_admin_wyx")
	public String welcomeAdmin() {

		return "admin_welcome_wyx";
	}

	// 进入用户列表的页面
	@RequestMapping("/admin_adminlist_wyx")
	public String adminList(Model model) {
		return "admin_adminlist_wyx";
	}

	// ajax显示用户列表(状态为1的)
	@RequestMapping(value = "/admin_adminlist_wyx01", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String adminListPage(Integer pageNo, String userName, Model model) {

		if (pageNo == null || pageNo == 0) {
			pageNo = 1;
		}
		PageHelper.startPage(pageNo, 3);

		List<User> listA = null;
		if (userName == null || "".equals(userName)) {
			listA = userService.getUserListA(1); // 不带名字查询
		} else {
			listA = userService.getUserListA1(1, userName); // 带名字查询
		}

		for (User user : listA) {
			Role role = userService.getRoleById(user.getRoleId());
			user.setRole(role);
		}
		PageInfo<User> page = new PageInfo<User>(listA);

		String js = JSON.toJSONString(page);
		return js;
	}

	// 进入回收站用户列表的页面
	@RequestMapping("/admin_adminlist_wyx_b")
	public String adminTrashList() {
		return "admin_adminlist_wyx_b";
	}

	// ajax显示用户列表(状态为0的)，显示已回收的用户列表
	@RequestMapping(value = "/admin_adminlist_wyx02", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String adminListPageB(Integer pageNo, String userName, Model model) {

		if (pageNo == null || pageNo == 0) {
			pageNo = 1;
		}
		PageHelper.startPage(pageNo, 3);

		List<User> listA = null;
		if (userName == null || "".equals(userName)) {
			listA = userService.getUserListA(0); // 不带名字查询
		} else {
			listA = userService.getUserListA1(0, userName); // 带名字查询
		}

		for (User user : listA) {
			Role role = userService.getRoleById(user.getRoleId());
			user.setRole(role);
		}
		PageInfo<User> page = new PageInfo<User>(listA);

		String js = JSON.toJSONString(page);
		return js;
	}

	// 回收用户(修改状态码)的方法
	@ControllerLog(description = "管理员回收用户")
	@RequestMapping(value = "/admin_deladmin_wyxA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String adminDelete(Integer userId, HttpSession session) {
		// 比较当前登录的用户的userId与要删除的用户userIdb,避免出现自己删除自己的情况
		User loginuser = (User) session.getAttribute("user");
		if (loginuser.getUserId() == userId) {
			Integer i = 0;
			String js = JSON.toJSONString(i);
			return js;
		}
		Integer i = userService.recycleUserById(userId);
		String js = JSON.toJSONString(i);
		return js;
	}

	// 恢复用户的方法
	@ControllerLog(description = "管理员恢复用户信息")
	@RequestMapping(value = "/admin_restoreadmin_wyx", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String userInfoRestore(Integer userId, HttpSession session) {

		Integer i = userService.restoreUserById(userId);
		String js = JSON.toJSONString(i);
		return js;
	}

	// 进入编辑用户信息页面
	@RequestMapping("/admin_adminedit_wyx")
	public String adminEdit(Integer userId, Model model) {
		User user = userService.getUserById(userId);
		model.addAttribute("user", user);
		return "admin_adminedit_wyx";
	}

	// 编辑用户的ajax
	@ControllerLog(description = "管理员编辑用户信息")
	@RequestMapping(value = "/admin_adminedit_wyxA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String adminEdit(Integer userId, String userName, String realName, String phone, String password,
			Integer roleId, HttpSession session) {
		// 根据主键查询要编辑的对象原数据
		User user = userService.getUserById(userId);
		user.setRealName(realName);
		user.setPassword(password);
		user.setRoleId(roleId);
		user.setPhone(phone);
		user.setUpdateTime(new Date());
		// 从session中获取当前操作人的userid,
		User op_user = (User) session.getAttribute("user");
		user.setOpId(op_user.getRoleId());
		// 更新数据
		Integer i = userService.editUser(user);
		String js = JSON.toJSONString(i);
		return js;
	}

	// 进入添加用户信息页面
	@RequestMapping("/admin_addadmin_wyx")
	public String adminAdd() {

		return "admin_addadmin_wyx";
	}

	// 新增用户的ajax
	@ControllerLog(description = "管理员添加用户")
	@RequestMapping(value = "/admin_addadmin_wyxA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String adminAdd(String userName, String realName, String phone, String password, Integer roleId,
			HttpSession session) {
		// 调用比较用户名是否重复
		boolean flag = userService.selectUserName(userName);
		if (!flag) {
			String js = JSON.toJSONString(0);
			return js;
		}

		User user = new User();
		user.setUserName(userName);
		user.setRealName(realName);
		user.setPassword(password);
		user.setRoleId(roleId);
		user.setPhone(phone);
		user.setCreateTime(new Date());
		user.setUpdateTime(new Date());
		// 从session中获取当前操作人的userid,
		User op_user = (User) session.getAttribute("user");
		user.setOpId(op_user.getUserId());
		// 调用service的添加用户的方法
		Integer i = userService.addUser(user);
		String js = JSON.toJSONString(i);
		return js;
	}

	// 打印用户信息表格并作为excel格式下载
	@ControllerLog(description = "打印用户信息表")
	@RequestMapping("/getUserInfoExcel")
	public void getUserInfoExcel(HttpServletRequest request, HttpServletResponse response) {
		// 从数据库中获取user信息
		List<User> list = userService.getUserList();
		// 确定输出的excel的列名
		String[] columnNames = { "用户编号", "表名", "登录名", "真实姓名", "登录密码", "手机号", "角色编号", "在职状态", "创建时间", "最后更新时间",
				"最后操作人编号", "备注1", "备注2", "备注3" };
		// 确定输出excel文件名
		String fileName = "用户信息";

		// 调用工具类的方法
		ExcelExportWrapper<User> eew = new ExcelExportWrapper<User>();
		eew.exportExcel(fileName, fileName, columnNames, list, response, ExportExcel.EXCEL_FILE_2003);
	}

	// 进入角色列表的页面
	@RequestMapping("/admin_rolelist_wyx")
	public String roleList() {

		return "admin_rolelist_wyx";
	}

	// 进入回收站角色列表的页面
	@RequestMapping("/admin_rolelist_wyx_b")
	public String roleTrashList() {

		return "admin_rolelist_wyx_b";
	}

	// ajax获取角色列表,状态back1为1的（使用中的）
	@RequestMapping(value = "/admin_rolelist_wyxA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String roleListPage(Integer pageNo) {
		if (pageNo == null || pageNo == 0) {
			pageNo = 1;
		}
		// 设置pagesize为3
		PageHelper.startPage(pageNo, 3);
		List<Role> list = userService.getRoleList("1");

		PageInfo<Role> page = new PageInfo<>(list);
		String js = JSON.toJSONString(page);
		return js;
	}

	// ajax获取回收站的角色列表,状态back1为0的（未使用的）
	@RequestMapping(value = "/admin_rolelist_b_wyxA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String roleTrashListPage(Integer pageNo) {
		if (pageNo == null || pageNo == 0) {
			pageNo = 1;
		}
		// 设置pagesize为3
		PageHelper.startPage(pageNo, 3);
		List<Role> list = userService.getRoleList("0");

		PageInfo<Role> page = new PageInfo<>(list);
		String js = JSON.toJSONString(page);
		return js;
	}

	// 进入添加角色信息页面
	@RequestMapping("/admin_roleadd_wyx")
	public String roleAdd() {

		return "admin_roleadd_wyx";
	}

	// ajax添加角色的方法
	@ControllerLog(description = "添加角色")
	@RequestMapping(value = "/admin_roleadd_wyxA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String addRole(String roleName, String describe) {
		// 查询角色名是否重复
		Role role = userService.distinctRoleName(roleName);
		if (role != null) {
			Integer i = 0;
			String js = JSON.toJSONString(i);
			return js;
		}

		// 调用service的方法添加role
		Integer i = userService.addRole(roleName, describe);
		String js = JSON.toJSONString(i);
		return js;
	}

	// 删除角色（修改状态码back由1改为0）
	@ControllerLog(description = "管理员回收角色")
	@RequestMapping(value = "/admin_delrole_wyxA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String roleDelete(Integer roleId, HttpSession session) {
		// 比较当前登录的用户的roleId与要删除的用户roleIdb,避免出现自己删除自己的情况
		User loginuser = (User) session.getAttribute("user");
		if (loginuser.getRoleId() == roleId) {
			Integer i = 0;
			String js = JSON.toJSONString(i);
			return js;
		}
		Integer i = userService.recycleRoleById(roleId);
		String js = JSON.toJSONString(i);
		return js;
	}

	// 恢复角色（修改状态码back由0改为1）
	@ControllerLog(description = "管理员恢复角色信息")
	@RequestMapping(value = "/admin_resrole_b_wyxA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String roleRestore(Integer roleId, HttpSession session) {

		Integer i = userService.restoreRoleById(roleId);
		String js = JSON.toJSONString(i);
		return js;
	}

	// 进入数据备份页面
	@RequestMapping("/admin_backup_wyx")
	public String dataBack() {

		return "admin_backup_wyx";
	}

	// 获取数据库备份信息的列表
	@RequestMapping(value = "/admin_backup_wyxA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String getDatabaseBackFiles(Integer pageNo) {
		List<DatabaseBackup> list = userService.getDatabaseBackupList();
		// 设置pagesize为3
		PageHelper.startPage(pageNo, 3);
		PageInfo<DatabaseBackup> page = new PageInfo<DatabaseBackup>(list);
		String js = JSON.toJSONString(page);
		return js;

	}

	// 添加备份
	@ControllerLog(description = "备份数据库")
	@RequestMapping(value = "/admin_addbackup_wyx", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String addDatabaseBackup(String loadName) {
		// 生成新的备份文件名，使用时间+car组成
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String str = sdf.format(new Date());
		System.out.println(str);
		String fileName = "car" + sdf.format(new Date()) + ".sql";
		System.out.println(fileName);
		Integer i = userService.backupDatabase(fileName, loadName);
		String js = JSON.toJSONString(i);
		return js;
	}

	// 进入还原数据库的页面

	@RequestMapping("/admin_restoredatabase_wyx")
	public String inRestore(String fileName, Model model) {
		model.addAttribute("fileName", fileName);
		return "admin_restoredatabase_wyx";
	}

	// 还原数据库
	@ControllerLog(description = "还原数据库")
	@RequestMapping(value = "/admin_restoredatabase_wyxs", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String restoreDatabase(String fileName) {
		/*
		 * // 根据备份信息的id出获取备份文件的名称 DatabaseBackup db =
		 * userService.getDatabaseBackupById(backId); // 获取备份文件名 String fileName
		 * = db.getFileName();
		 */
		System.out.println(fileName);
		// 调用工具类的方法恢复数据库
		DatabaseBackupUtil.restore(fileName);

		int i = 1;
		String js = JSON.toJSONString(i);
		return js;
	}

	// 进入回收站页面
	@RequestMapping("/admin_trash_wyx")
	public String inTrash() {

		return "admin_trash_wyx";
	}

}
