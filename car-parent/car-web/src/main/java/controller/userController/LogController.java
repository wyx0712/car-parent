/**
 * 
 */
package controller.userController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import aop.ControllerLog;
import pojo.OperateLog;
import service.userService.OperateLogService;
import service.userService.UserService;
import utils.ExcelExportWrapper;
import utils.ExportExcel;

/**
 * @author 王耀星
 * @date 2018年10月28日
 */
@Controller
public class LogController {
	@Autowired
	private UserService userService;
	@Autowired
	private OperateLogService logService;

	// 进入日志列表的页面
	@RequestMapping("/admin_loglist_wyx")
	public String logList(Model model) {
		return "admin_loglist_wyx";
	}

	// 进入回收站日志列表的页面
	@RequestMapping("/admin_loglist_wyx_b")
	public String logTrashList() {
		return "admin_loglist_wyx_b";
	}

	// ajax显示日志列表
	@RequestMapping(value = "/admin_loglist_wyx01", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String logListPage(Integer pageNo, String userName, Model model) {
		if (pageNo == null || pageNo == 0) {
			pageNo = 1;
		}
		PageHelper.startPage(pageNo, 5);
		List<OperateLog> listA = null;
		if (userName == null || "".equals(userName)) {
			listA = logService.getAllOperateLogA("1"); // 不带名字查询
		} else {
			listA = logService.getAllOperateLogB("1", userName);// 带名字查询
		}

		PageInfo<OperateLog> page = new PageInfo<OperateLog>(listA);

		String js = JSON.toJSONString(page);
		return js;
	}

	// ajax显示回收站日志列表
	@RequestMapping(value = "/admin_loglist_b_wyx01", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String logTrashListPage(Integer pageNo, String userName, Model model) {
		if (pageNo == null || pageNo == 0) {
			pageNo = 1;
		}
		PageHelper.startPage(pageNo, 3);
		List<OperateLog> listA = null;
		if (userName == null || "".equals(userName)) {
			listA = logService.getAllOperateLogA("0"); // 不带名字查询
		} else {
			listA = logService.getAllOperateLogB("0", userName);// 带名字查询
		}

		PageInfo<OperateLog> page = new PageInfo<OperateLog>(listA);

		String js = JSON.toJSONString(page);
		return js;
	}

	// 显示日志详情的页面
	@RequestMapping("/admin_logdetail_wyx")
	public String logDetail(Integer logId, Model model) {
		// 调用service层方法获取operateLog
		OperateLog operateLog = logService.getOperateLogById(logId);
		model.addAttribute("operateLog", operateLog);
		return "admin_logdetail_wyx";
	}

	// 回收日志的方法（更改状态1改0）
	@ControllerLog(description = "管理员回收日志")
	@RequestMapping(value = "/admin_dellog_wyxA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String logDelete(Integer logId, HttpSession session) {

		Integer i = logService.recycleOperateLog(logId);
		String js = JSON.toJSONString(i);
		return js;
	}

	// 恢复日志的方法（更改状态0改1）
	@RequestMapping(value = "/admin_reslog_b_wyxA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String logRestore(Integer logId, HttpSession session) {

		Integer i = logService.restoreOperateLog(logId);
		String js = JSON.toJSONString(i);
		return js;
	}

	// 打印日志信息表格并作为excel格式下载
	@ControllerLog(description = "管理员打印日志信息表")
	@RequestMapping("/getLogInfoExcel")
	public void getLogInfoExcel(HttpServletRequest request, HttpServletResponse response) {
		// 从数据库中获取user信息
		List<OperateLog> list = logService.getAllOperateLog();
		// 确定输出的excel的列名
		String[] columnNames = { "日志编号", "表名", "操作人", "操作", "操作时间", "具体内容", "备注1", "备注2", "备注3" };
		// 确定输出excel文件名
		String fileName = "日志信息";

		// 调用工具类的方法
		ExcelExportWrapper<OperateLog> eew = new ExcelExportWrapper<OperateLog>();
		eew.exportExcel(fileName, fileName, columnNames, list, response, ExportExcel.EXCEL_FILE_2003);
	}

	// 测试某个时间段的日志
	@RequestMapping("/logDate")
	public String test01() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date start = sdf.parse("2018-10-27 00:00:00");
		Date end = sdf.parse("2018-10-28 00:00:00");
		List<OperateLog> list = logService.getLogListTime(start, end);
		for (OperateLog operateLog : list) {
			System.out.println(operateLog.getLogId() + "\t" + operateLog.getCreateDate());
		}
		return "admin_loglist_wyx";
	}

}
