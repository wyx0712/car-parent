/**
 * 
 */
package controller.awaitingController;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mysql.fabric.xmlrpc.base.Array;

import pojo.CarMessage;
import pojo.ReportInfo;
import service.SurveyInfo_lll.AwaitingService;
import service.auditService.AuditService;

/**
 * @author 亮亮 2018年10月30日
 * 
 */
// 用来返回待审案件/复审案件的控制层
@Controller
public class AwaitingController {

	@Autowired
	private AwaitingService awaitingService;
	@Autowired
	private AuditService auditService;

	// 待审案件
	@RequestMapping(value = "/shidayl", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String getShidayls(Integer reportNo, Integer pageNo, Model model) {
		if (pageNo == null || pageNo == 0) {
			pageNo = 1;  
		}
		PageHelper.startPage(pageNo, 3);

		// 首先获得所有carMessage
		List<CarMessage> carMessage1 = awaitingService.getCarMessage1(1);

		List<ReportInfo> reportInfoList = new ArrayList<ReportInfo>();
		for (CarMessage carMessage : carMessage1) {
			Integer reportNo2 = carMessage.getReportNo();
			ReportInfo reportInfo = auditService.getReportInfo(reportNo2);
			reportInfoList.add(reportInfo);
		}

		if (reportInfoList == null) {
			return "404";
		}

		PageInfo<ReportInfo> pageInfo = new PageInfo<ReportInfo>();
		pageInfo.setList(reportInfoList);

		String js = JSON.toJSONString(pageInfo);

		return js;

	}

	// 确审案件
	@RequestMapping(value = "/queshenA", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String getQueshenAs(Integer reportNo, Integer pageNo, Model model) {
		if (pageNo == null || pageNo == 0) {
			pageNo = 1;
		}
		PageHelper.startPage(pageNo, 3);

		// 首先获得所有carMessage
		List<CarMessage> carMessage1 = awaitingService.getCarMessage1(2);

		List<ReportInfo> reportInfoList = new ArrayList<ReportInfo>();
		for (CarMessage carMessage : carMessage1) {
			Integer reportNo2 = carMessage.getReportNo();
			ReportInfo reportInfo = auditService.getReportInfo(reportNo2);
			reportInfoList.add(reportInfo);
		}

		if (reportInfoList == null) {
			return "404";
		}

		PageInfo<ReportInfo> pageInfo = new PageInfo<ReportInfo>();
		pageInfo.setList(reportInfoList);

		String js = JSON.toJSONString(pageInfo);

		return js;

	}

	// 复审案件
	@RequestMapping(value = "/ailisi", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String getAilisis(Integer reportNo, Integer pageNo, Model model) {
		if (pageNo == null || pageNo == 0) {
			pageNo = 1;
		}
		PageHelper.startPage(pageNo, 3);

		// 首先获得所有carMessage
		List<CarMessage> carMessage1 = awaitingService.getCarMessage1(4);

		List<ReportInfo> reportInfoList = new ArrayList<ReportInfo>();
		for (CarMessage carMessage : carMessage1) {
			Integer reportNo2 = carMessage.getReportNo();
			ReportInfo reportInfo = auditService.getReportInfo(reportNo2);
			reportInfoList.add(reportInfo);
		}

		if (reportInfoList == null) {
			return "404";
		}

		PageInfo<ReportInfo> pageInfo = new PageInfo<ReportInfo>();
		pageInfo.setList(reportInfoList);

		String js = JSON.toJSONString(pageInfo);

		return js;

	}

	// 根据提交的报案号id 修改对应的报案号状态码 确审·
	@RequestMapping(value = "/submitda")
	public String getSubmitda(Integer reportNo) {
		
	   
		// 通过报案号查找状态表
		CarMessage carMessage = auditService.getCarMessage(reportNo);
		carMessage.setSurveyNow(2);
		int updateByPrimaryKey = auditService.updateByPrimaryKey(carMessage);

		return "backstage";

	}

	// 根据提交的报案号id 修改对应的报案号状态码 撤回·
	@RequestMapping(value = "/submitdb")
	public String getSubmitdb(Integer reportNo) {
		// 通过报案号查找状态表
		CarMessage carMessage = auditService.getCarMessage(reportNo);
		carMessage.setSurveyNow(4);
		int updateByPrimaryKey = auditService.updateByPrimaryKey(carMessage);

		return "backstage";

	}

	
}
