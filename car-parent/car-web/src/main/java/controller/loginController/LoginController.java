/**
 * 
 */
package controller.loginController;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;

import aop.ControllerLog;
import pojo.CarMessage;
import pojo.Company;
import pojo.Dispatch;
import pojo.Entrust;
import pojo.Policy;
import pojo.ReportInfo;
import pojo.User;
import service.loginService.CompanyService;
import service.loginService.LoginService;
import service.loginService.ReportInfoService;
import service.loginService.UserService_lzw;
import service.loginService.impl.CompanyServiceImpl;
import service.loginService.impl.DispatchServiceImpl;
import service.loginService.impl.EntrustServiceImpl;
import service.loginService.impl.LoginServiceImpl;
import service.loginService.impl.PolicyServiceimpl;
import service.loginService.impl.ReportInfoServiceImpl;
import service.loginService.impl.UserServiceImpl_lzw;
import service.surveyService.impl.SurveyServiceImpl;

/**
 * @author 李智维
 *
 *         2018年10月23日-下午2:15:25
 */
@Controller
public class LoginController {

	@Autowired
	private LoginService loginService;
	@Autowired
	private ReportInfoServiceImpl reportInfoServiceImpl;
	@Autowired
	private ReportInfoService reportInfoService;
	@Autowired
	private LoginServiceImpl loginServiceImpl;
	@Autowired
	private EntrustServiceImpl entrustServiceImpl;
	@Autowired
	private DispatchServiceImpl dispatchServiceImpl;
	@Autowired
	private SurveyServiceImpl surveyServiceImpl;
	@Autowired
	private PolicyServiceimpl policyServiceimpl;
	@Autowired
	private CompanyServiceImpl companyServiceImpl;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private UserServiceImpl_lzw userServiceImpl;
	@Autowired
	private UserService_lzw userService;

	// 客服中心
	@RequestMapping("/lzw")
	public String lzw() {

		return "lzw";
	}

	// 案件委托
	@RequestMapping("/delega_lzw")
	public String delega(Model model) {
		List<Company> list = companyService.selectByExample(null);
		model.addAttribute("list", list);
		for (Company company : list) {
			System.out.println(company);
		}
		return "delegate_lzw";
	}

	// 案件委托
	@RequestMapping("/delegateasdg_lzw")
	@ControllerLog(description = "存储案件委托信息")
	public String delegate(String insuranceId, int reportNo, String policyId, String context, String num2,
			String surveyorsId, String mission, HttpSession session) {
		Entrust entrust = new Entrust();
		entrust.setPolicyId(insuranceId);
		entrust.setReportNo(reportNo);
		entrust.setTableId(2002);
		entrust.setApplicant(policyId);
		entrust.setEntrustingContent(context);
		entrust.setDataProcessing(num2);
		entrust.setAuthorizedAgency(surveyorsId);
		entrust.setCommittalCharge(mission);

		entrustServiceImpl.insert(entrust);

		return "delegate_lzw";
	}

	// 我的桌面
	@RequestMapping("/desktop_lzw")
	public String desktop() {

		return "desktop_lzw";
	}

	// 调度派工
	@RequestMapping("/dispatch_lzw")
	public String dispatch(Model model) {
		List<User> list = userService.selectByExample(null);
		model.addAttribute("list", list);
		return "dispatch_lzw";
	}

	// 调度派工
	@RequestMapping("/dispatchdsga_lzw")
	@ControllerLog(description = "存储派工信息")
	public String dispatchdsga(String surveyorsId, String insuranceId, int reportNo, String policyId, String address,
			String num3, HttpSession session) {
		Dispatch dispatch = new Dispatch();
		CarMessage carMessage = new CarMessage();
		dispatch.setSurveyname(surveyorsId);
		dispatch.setTableId(2003);
		dispatch.setPolicyId(insuranceId);
		dispatch.setReportNo(reportNo);
		dispatch.setApplicant(policyId);
		dispatch.setSite(address);
		dispatch.setCompany(num3);
		carMessage.setCarNo(policyServiceimpl.getcarNo(insuranceId).getCarNo());
		carMessage.setPolicyId(insuranceId);
		carMessage.setReportNo(reportNo);
		carMessage.setReportName(reportInfoService.getreportNo(insuranceId).getReportName());
		surveyServiceImpl.addcarmessage(carMessage);
		dispatchServiceImpl.insert(dispatch);

		return "dispatch_lzw";
	}

	// 客服中心
	@RequestMapping("/indexcf_lzw")
	public String indexcf() {

		return "indexcf_lzw";
	}

	@RequestMapping("/repo_lzw")
	public String report_lzw() {

		return "report_lzw";
	}

	// 报案平台
	@RequestMapping("/reporghjt_lzw")
	@ControllerLog(description = "存储添加报案信息")
	public String report(String carNo, String policyId, String reportName, String reporTel, String dangerReason,
			String province, String city, String town, String driverName, String driverTel, HttpSession session) {
		ReportInfo reportInfo = new ReportInfo();
		int random = (int) (10000 + Math.random() * 990000);
		String num = "10" + random;
		reportInfo.setReportNo(null);
		reportInfo.setPolicyId(policyId);
		reportInfo.setReportName(reportName);
		reportInfo.setReporTel(reporTel);
		reportInfo.setReportTime(new Date());
		reportInfo.setReportState(1);
		reportInfo.setSurveyId(3);
		String address = province + city + town;
		reportInfo.setDangerAddress(address);
		reportInfo.setDriverName(driverName);
		reportInfo.setDriverTel(driverTel);
		reportInfo.setDangerReason(dangerReason);
		reportInfoServiceImpl.insert(reportInfo);

		return "report_lzw";
	}

	// 判断保单是否存在
	@RequestMapping(value = "/getPolicy", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String selectPolicy(String carNo, HttpSession session) {
		Policy p = loginService.getInfo(carNo);
		Policy policy = new Policy();
		if (p == null) {
			policy.setBack1("保单不存在!");
		} else {
			Date date = p.getEndTime();
			Date newDate = new Date();
			int i = date.compareTo(newDate);
			if (i > 0) {
				policy.setBack1("保单有效!");
				System.out.println(p.getPolicyId());
				policy.setBack2(p.getPolicyId().toString());
				policy.setBack3(p.getApplicant().toString());
				session.setAttribute("p", p);
			} else {
				policy.setBack1("保单已过期,请通知顾客及时续保!");
			}
		}

		return JSONArray.toJSONString(policy);
	}

	// 通过保单号查询报案号投保人
	@RequestMapping(value = "/getapplicant", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String selecPolicy(String insuranceId, HttpSession session) {

		Policy policy = new Policy();

		ReportInfo getreportNo = reportInfoService.getreportNo(insuranceId);
		Policy getapplicant = loginService.getapplicant(insuranceId);
		if (getreportNo == null) {
			policy.setBack1("报案号不存在");
		} else {
			policy.setBack2(getreportNo.getReportNo().toString());
			policy.setBack3(getapplicant.getApplicant().toString());
			System.out.println(policy.getBack2());
			System.out.println(policy.getBack3());
			session.setAttribute("getreportNo", getreportNo);
		}
		return JSONArray.toJSONString(policy);

	}

	@RequestMapping(value = "/getappli", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public String selPolicy(String insuranceId, HttpSession session) {
		Policy policy = new Policy();
		ReportInfo getreportNo = reportInfoService.getreportNo(insuranceId);
		Policy getapplicant = loginService.getapplicant(insuranceId);
		if (getreportNo == null) {
			policy.setBack1("报案号不存在");
		} else {
			policy.setBack2(getreportNo.getReportNo().toString());
			policy.setBack3(getapplicant.getApplicant().toString());
			policy.setBack1(getreportNo.getDangerAddress());
			System.out.println(policy.getBack2());
			System.out.println(policy.getBack3());
			session.setAttribute("getreportNo", getreportNo);
		}
		return JSONArray.toJSONString(policy);

	}

}
