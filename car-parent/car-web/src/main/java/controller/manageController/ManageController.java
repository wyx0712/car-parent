package controller.manageController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import mapper.LossinfoMapper;
import mapper.PolicyMapper;
import mapper.ReportInfoMapper;
import mapper.UserMapper;
import pojo.Lossinfo;
import pojo.Policy;
import pojo.ReportInfo;
import pojo.User;
import service.loginService.PolicyService;
import service.service_lzj.LossinfoService_lzj;
import service.service_lzj.PolicyService_lzj;
import service.service_lzj.ReportInfoService_lzj;
import service.service_lzj.UserService_lzj;




@Controller
public class ManageController {
	
	
	
	@Autowired
	private PolicyService_lzj policyService;
	@Autowired
	private ReportInfoService_lzj reportInfoService_lzj;
	@Autowired
	private UserService_lzj userService_lzj;
	@Autowired
	private LossinfoService_lzj lossinfoService_lzj;
	@Autowired
	
	//主页面
@RequestMapping("/test01")
	public String test001(){
		
		return "zhuyemian_lzj";
		 
	}
	//外部框架
	@RequestMapping("/kuangjia_lzj")
	public String kuangjia(){
		
		return "kuangjia_lzj";
		 
	}

	//会员管理 --分页（内勤中心）（带条件）
		@RequestMapping("/huiyuanguanli222")
		public String huiyuanguanlifenyeA(Integer pageNo,String policyId,Model model){
			if(pageNo==null || pageNo<1){pageNo=1;}
			 PageHelper.startPage(pageNo, 2);
			
			 List<Policy> list=new ArrayList<Policy>();
			 //如果没有查询条件
			 if(policyId==null || "".equals(policyId)){
				 list=policyService.selectByExample(null);
			 }else{
				 Policy policy=policyService.selectPolicyById(policyId);
				 //如果查询保单存在
				 if(policy!=null){
					 list.add(policy);
				 }
			 }
			 
			 PageInfo<Policy> pageInfo=new PageInfo<Policy>(list);
			 model.addAttribute("policy", pageInfo);
			 model.addAttribute("policyId",policyId);
			 
			 return "member_list_lzj";
		}

	
	
	
	//到期续保（内勤中心）
	@RequestMapping("/daoqixubao")
	public String daoqixubao(Model model){
		
		List<Policy> list = policyService.selectByExample(null);
		model.addAttribute("policy", list);
		for (Policy policy : list) {
			System.out.println(policy);
		}
		
		return "insurance_policy_lzj";
	
		
		
		
		
	}
	//投诉反馈（内勤中心）
	@RequestMapping("/userxiugai")
	public String userxiugai(Model model,String id){
		Policy selectPolicyById = policyService.selectPolicyById(id);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		
		String startDate=sdf.format(selectPolicyById.getStartDate());
		String endDate=sdf.format(selectPolicyById.getEndTime());	
		model.addAttribute("policy", selectPolicyById);
		model.addAttribute("startDate1", startDate);
		model.addAttribute("endDate1", endDate);
		return "complainJlg_lzj";
		 
	}

	
	//投诉反馈（内勤中心）
		@RequestMapping("/userxiugaibaocun")
		public String userxiugaibaocun(Model model,Policy policy){
			String policyId=policy.getPolicyId();
			String aPhone=policy.getaPhone();
			System.out.println(aPhone);
			
			String applicant = policy.getApplicant();
			String aIdcard = policy.getaIdcard();
			
			String bPhone = policy.getbPhone();
			String beneficiary = policy.getBeneficiary();
			String bIdcard = policy.getbIdcard();
			
			
			Policy oldPolicy= policyService.selectPolicyById(policyId);
			System.out.println(oldPolicy);
			
			oldPolicy.setaPhone(aPhone);
			oldPolicy.setaIdcard(aIdcard);
			oldPolicy.setApplicant(applicant);
			
			oldPolicy.setbPhone(bPhone);
			oldPolicy.setbIdcard(bIdcard);
			oldPolicy.setBeneficiary(beneficiary);
			
		Integer updatePolicy = policyService.updatePolicy(oldPolicy);
			
			return "redirect:/huiyuanguanli222.do";
			 
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//续费通知单（内勤中心）
	@RequestMapping("/xufeitongzhidan")
	public String xufeitongzhidan(){
		
		return "renewalNotice_lzj";
		 
	}
	//报案信息（综合管理）
	@RequestMapping("/baoan")
	public String baoan( Integer pageNo,Integer reportNo,Model model){
		if (pageNo == null || pageNo<1) {
			pageNo=1;
		}
		PageHelper.startPage(pageNo,5);
		List<ReportInfo> list = new ArrayList<ReportInfo>();
		
		//如果没有查询条件
	if (reportNo ==null ||"".equals(reportNo)) {
	list = reportInfoService_lzj.selectByExample(null);
	}else {
		ReportInfo reportInfo = reportInfoService_lzj.selectByPrimaryKey(reportNo);
		//如果查询报案号存在
	if (reportInfo!=null) {
		list.add(reportInfo);
	}
	}
	  PageInfo<ReportInfo> pageInfo = new PageInfo<ReportInfo>(list);
       
		model.addAttribute("reportInfo", pageInfo);
		model.addAttribute("reportNo",reportNo);
		return "baoan_lzj";
		 
	}
	//人员信息（综合管理）
		@RequestMapping("/renyuanxinxi")
		public String renyuanxinxi(Integer pageNo,String realName,Model model){
			if (pageNo == null || pageNo<1) {
				pageNo=1;
			}
			PageHelper.startPage(pageNo,2);
		List<User> list = new ArrayList<User>();
			//如果没有查询条件
		if (realName==null||"".equals(realName)) {
			
			list = userService_lzj.getUserList1(1);
		}else {
			List<User> selectByPrimaryKey1 = userService_lzj.selectByPrimaryKey1(realName,1);
			if (selectByPrimaryKey1!=null) {
				list.addAll(selectByPrimaryKey1);
			}
		}
			PageInfo<User> pageInfo = new PageInfo<User>(list);
	model.addAttribute("user", pageInfo);
		model.addAttribute("realName", realName);
		return "manage_admin_lzj";
		}
		
		
		
		//车辆定损信息（综合管理）
		@RequestMapping("/cheliangdingsunxinxi")
		public String cheliangdingsunxinxi(Integer pageNo,String carNo,Model model){
			if (pageNo == null || pageNo<1) {
				pageNo=1;
			}
			PageHelper.startPage(pageNo,2);
	        List<Lossinfo> list = new ArrayList<Lossinfo>();
	      //如果没有查询条件
	        if (carNo==null||"".equals(carNo)) {
	        	list = lossinfoService_lzj.selectByExample(null);
			}else {
				List<Lossinfo> selectByPrimaryKey1 = lossinfoService_lzj.selectByPrimaryKey1(carNo);
			
			if (selectByPrimaryKey1!=null) {
				list.addAll(selectByPrimaryKey1);
			}
			}
	        PageInfo<Lossinfo> pageInfo = new PageInfo<Lossinfo>(list);
			model.addAttribute("lossinfo", pageInfo);
			model.addAttribute("carNo", carNo);
			return "carloss_lzj";
			 
		}
		
		//回收站（综合管理）
				@RequestMapping("/huishouzhan")
				public String huishouzhan(Integer pageNo,String realName,Model model){
					if (pageNo == null || pageNo<1) {
						pageNo=1;
					}
					PageHelper.startPage(pageNo,2);
				List<User> list = new ArrayList<User>();
					//如果没有查询条件
				if (realName==null||"".equals(realName)) {
					list= userService_lzj.getUserList1(0);
				}else {
					List<User> selectByPrimaryKey1 = userService_lzj.selectByPrimaryKey1(realName,0);
					if (selectByPrimaryKey1!=null) {
						list.addAll(selectByPrimaryKey1);
					}
				}
					PageInfo<User> pageInfo = new PageInfo<User>(list);
			model.addAttribute("user", pageInfo);
				model.addAttribute("realName", realName);
					return "trashxh_lzj";
				}
				
			//会员管理详情
				@RequestMapping("/HYGLxiangqing")
				public String HYGLxiangqing(Model model,String id){
					Policy selectPolicyById = policyService.selectPolicyById(id);
					model.addAttribute("policy", selectPolicyById);
					return "insurance_policy2JLG_lzj";
				}
				
				
				
				//报案详情
				@RequestMapping("/BAxianging")
				public String BAxianging(Model model,Integer id){
					ReportInfo selectByPrimaryKey = reportInfoService_lzj.selectByPrimaryKey(id);
					model.addAttribute("reportNo", selectByPrimaryKey);
					return "insurance_BAXX_lzj";
				}
				
				//人员信息详情
				@RequestMapping("/USERxiangqing")
				public String USERxiangqing(Model model,Integer id){
					User selectByPrimaryKey = userService_lzj.selectByPrimaryKey(id);
					model.addAttribute("user", selectByPrimaryKey);
					return "insurance_USER_lzj";
					 
				}
				
				
				//车辆定损信息详情
						@RequestMapping("/CarDSxiangqing")
						public String CarDSxiangqing(Model model,Integer id){
							Lossinfo selectByPrimaryKey = lossinfoService_lzj.selectByPrimaryKey(id);
							model.addAttribute("lossinfo", selectByPrimaryKey);
							return "insurance_CarDS_lzj";
							 
						}
						
			//还原用户信息的方法
			@RequestMapping(value="/userTrashRestore_lzj",produces={"application/json;charset=UTF-8"})
			@ResponseBody  //表示对json格式的数据的支持
			public String userTrashRestore(Integer userId){
				Integer i=userService_lzj.updateByPrimaryKey(userId);
				
				String js=JSON.toJSONString(i);
				return js;
			}
			
			//删除用户信息的方法 状态1改0
			@RequestMapping(value="/userTrashRestore_lzj1",produces={"application/json;charset=UTF-8"})
			@ResponseBody  //表示对json格式的数据的支持
			public String userTrashRestore1(Integer userId){
				Integer i=userService_lzj.xiugaizhuangtai(userId);
				String js=JSON.toJSONString(i);
				return js;
			}	
				
}
