package controller.manageController;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import aop.ControllerLog;
import pojo.Lossinfo;
import pojo.Policy;
import pojo.ReportInfo;
import pojo.User;
import service.service_lzj.LossinfoService_lzj;
import service.service_lzj.PolicyService_lzj;
import service.service_lzj.ReportInfoService_lzj;
import service.service_lzj.UserService_lzj;
import utils.ExcelExportWrapper;
import utils.ExportExcel;
import utils.ExportWordWrapper;

@Controller
public class ManageController2 {
	@Autowired
	private PolicyService_lzj policyService;
	@Autowired
	private ReportInfoService_lzj reportInfoService_lzj;
	@Autowired
	private UserService_lzj userService_lzj;
	@Autowired
	private LossinfoService_lzj lossinfoService_lzj;
	
	
	// 打印用户信息表格并作为excel格式下载
		@RequestMapping("/dayinrenyuanxinxi.do")
		public void getMemberInfoExcel(HttpServletRequest request, HttpServletResponse response) {
			// 从数据库中获取user信息
			List<User> list = userService_lzj.getUserList();
			// 确定输出的excel的列名
			String[] columnNames = { "用户编号", "表名", "登录名", "真实姓名", "登录密码", "手机号", "角色编号", "在职状态", "创建时间","最后更新时间",
					"最后操作人编号", "备注1", "备注2", "备注3"};
			// 确定输出excel文件名
			String fileName = "用户信息";
			System.out.println("qwqe");
			// 调用工具类的方法
			ExcelExportWrapper<User> eew = new ExcelExportWrapper<User>();
			eew.exportExcel(fileName, fileName, columnNames, list, response, ExportExcel.EXCEL_FILE_2003);
		}
	
	
		
		
		
		
		
		
		
		
		
		
		
		// 打印用户信息表格并作为excel格式下载
				@RequestMapping("/dayinyonghuxinxi.do")
				public void getMemberInfoExce2(HttpServletRequest request, HttpServletResponse response) {
					// 从数据库中获取user信息
					List<Policy> list = policyService.getUserList();
					// 确定输出的excel的列名
					String[] columnNames = { "保单号", "表编号", "车牌号", "投保人", "投保人身份证号", "投保人手机号", "投保人住址", "受益人", "受益人身份证号", "受益人手机号",
							"受益人住址", "保费", "保额", "生效时间","结束时间","险种","创建时间","更新时间","操作人id" };
					// 确定输出excel文件名
					String fileName = "会员信息";
					System.out.println("qwqe");
					// 调用工具类的方法
					ExcelExportWrapper<Policy> eew = new ExcelExportWrapper<Policy>();
					eew.exportExcel(fileName, fileName, columnNames, list, response, ExportExcel.EXCEL_FILE_2003);
				}
	
			
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				// 打印报案信息表格并作为excel格式下载
				@RequestMapping("/dayinbaoanxinxi.do")
				public void getMemberInfoExce3(HttpServletRequest request, HttpServletResponse response) {
					// 从数据库中获取user信息
					List<ReportInfo> list = reportInfoService_lzj.getUserList();
					// 确定输出的excel的列名
					String[] columnNames = { "报案号", "表编号", "保单号", "报案人", "报案人电话", "报案时间", "驾驶人", "驾驶人电话", "出险时间", "出险地址",
							"出险原因", "损失类型", "接案人id", "接案状态","查勘人id","核赔人id"};
					// 确定输出excel文件名
					String fileName = "报案信息";
					System.out.println("qwqe");
					// 调用工具类的方法
					ExcelExportWrapper<ReportInfo> eew = new ExcelExportWrapper<ReportInfo>();
					eew.exportExcel(fileName, fileName, columnNames, list, response, ExportExcel.EXCEL_FILE_2003);
				}
				
				
				// 打印报案信息表格并作为excel格式下载
				@RequestMapping("/dayinchesunxinxi.do")
				public void getMemberInfoExce4(HttpServletRequest request, HttpServletResponse response) {
					// 从数据库中获取user信息
					List< Lossinfo> list = lossinfoService_lzj.getUserList();
					// 确定输出的excel的列名
					String[] columnNames = { "表名", "报案号", "修理厂名", "修理厂类型", "车主", "车牌号", "定损总价", "定损员", "定损时间", "定损意见",
							"配件id", "该数据创建时间", "数据修改时间", "操作人员id","退档意见"};
					// 确定输出excel文件名
					String fileName = "车辆定损信息";
					System.out.println("qwqe");
					// 调用工具类的方法
					ExcelExportWrapper< Lossinfo> eew = new ExcelExportWrapper< Lossinfo>();
					eew.exportExcel(fileName, fileName, columnNames, list, response, ExportExcel.EXCEL_FILE_2003);
				}
				
				
				// 下载续费通知单到word格式打印
				@RequestMapping("/dayinxufeitongzhidan")
				public void xuqi(HttpServletRequest request, HttpServletResponse response) {
					ExportWordWrapper eww = new ExportWordWrapper();
					eww.exportWord(response);
				}
	
}
