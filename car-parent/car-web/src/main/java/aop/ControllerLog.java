/**
 * 
 */
package aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 王耀星
 * @date 2018年10月27日
 * 
 *       自定义注解，用于切入点获取操作名称
 */

@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ControllerLog {

	// 对操作的描述
	String description() default "";

}
