/**
 * 
 */
package aop;

import java.lang.reflect.Method;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NamedThreadLocal;
import org.springframework.stereotype.Component;

import pojo.OperateLog;
import pojo.User;
import service.userService.OperateLogService;
import service.userService.UserService;

/**
 * @author 王耀星
 * @date 2018年10月27日
 */
@Aspect
@Component
public class LogAspect {
	private static final ThreadLocal<User> currentUser = new NamedThreadLocal<>("ThreadLocal user");

	@Autowired(required = false)
	private HttpServletRequest request;
	@Autowired
	private OperateLogService logService;
	@Autowired
	private UserService userService;

	// Controller层切点 注解拦截
	@Pointcut("@annotation(aop.ControllerLog)")
	public void controllerAspect() {
	}

	// 前置通知,获取session中的用户
	@Before("controllerAspect()")
	public void doBefore(JoinPoint joinPoint) throws InterruptedException {

		// 读取session中的用户
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		currentUser.set(user);

	}

	// 后置通知 用于拦截Controller层记录用户的操作
	@SuppressWarnings("unchecked")
	@After("controllerAspect()")
	public void doAfter(JoinPoint joinPoint) {
		User user = currentUser.get();
		// 登入login操作，所以session中不存在用户信息
		if (user == null) {
			HttpSession session = request.getSession();
			user = (User) session.getAttribute("user");
			if (user == null) {
				return;
			}
		}

		String operation = ""; // 操作类型
		String remoteAddr = request.getRemoteAddr();// 请求的IP
		String requestUri = request.getRequestURI();// 请求的Uri
		String methodName = joinPoint.getSignature().getName(); // 请求的方法名

		String content = optionContent(joinPoint.getArgs(), methodName); // 具体的参数
		// 获取操作内容

		try {
			operation = getControllerMethodDescription(joinPoint);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 新建日志的对象
		OperateLog log = new OperateLog();
		log.setOperation(operation);
		log.setContent(content);
		log.setOpName(user.getRealName());
		log.setCreateDate(new Date());
		// 执行保存操作
		logService.saveLog(log);
	}

	// 获取注解中的操作名称
	public static String getControllerMethodDescription(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		ControllerLog controllerLog = method.getAnnotation(ControllerLog.class);
		String discription = controllerLog.description();
		return discription;
	}

	// 根据具体被拦截的方法获取被拦截方法的参数值， 将参数值拼接为具体的操作内容
	public String optionContent(Object[] args, String mName) {
		if (args == null) {
			return null;
		}
		StringBuffer rs = new StringBuffer();
		rs.append("调用方法为：" + mName);
		String className = null;

		if ("doLogin".equals(mName)) { // 切入的为登录方法时，只需获取user的userName,user为第一个参数
			Object info = args[0];
			User user = (User) info;
			// 将值加入内容中
			rs.append("，用户名为" + user.getUserName() + "的用户登录");

			return rs.toString();
		} else if ("loginOut".equals(mName)) { // 切入的为注销方法时，无需参数
			rs.append("，无参数");
			return rs.toString();
		} else if ("adminDelete".equals(mName)) { // 切入的方法为管理员回收用户时，第一个参数Integer
			Object info = args[0];
			Integer userId = (Integer) info;
			rs.append("，传入参数为userId,值为:" + userId);
			return rs.toString();
		} else if ("userInfoRestore".equals(mName)) {
			// 切入的为管理员还原用户信息时，参数列表Integer userId,,
			Integer userId = (Integer) args[0];
			rs.append("，传入参数为userId,值为:" + userId);
			return rs.toString();

		} else if ("adminEdit".equals(mName)) {
			// 切入的为管理员编辑用户时，参数列表Integer userId, String userName, String
			// realName, String phone, String password,Integer roleId,
			Integer userId = (Integer) args[0];
			rs.append("，传入参数1为userId,值为:" + userId);
			String userName = (String) args[1];
			rs.append("，传入参数2为userName,值为:" + userName);
			String realName = (String) args[2];
			rs.append("，传入参数3为userName,值为:" + realName);
			String phone = (String) args[3];
			rs.append("，传入参数4为phone,值为:" + phone);
			String password = (String) args[4];
			rs.append("，传入参数5为password,值为:" + password);
			Integer roleId = (Integer) args[5];
			rs.append("，传入参数6为roleId,值为:" + roleId);

			return rs.toString();

		} else if ("adminAdd".equals(mName)) {
			// 切入的为管理员添加用户时，参数列表String userName, String realName, String phone,
			// String password, Integer roleId

			String userName = (String) args[0];
			rs.append("，传入参数1为userName,值为:" + userName);
			String realName = (String) args[1];
			rs.append("，传入参数2为userName,值为:" + realName);
			String phone = (String) args[2];
			rs.append("，传入参数3为phone,值为:" + phone);
			String password = (String) args[3];
			rs.append("，传入参数4为password,值为:" + password);
			Integer roleId = (Integer) args[4];
			rs.append("，传入参数5为roleId,值为:" + roleId);

			return rs.toString();

		} else if ("getUserInfoExcel".equals(mName)) {
			// 切入的为管理员打印用户信息时，无参数
			rs.append("，无参数");
			return rs.toString();

		} else if ("addRole".equals(mName)) {
			// 切入的为管理员添加角色时，参数列表String roleName, String describe
			String roleName = (String) args[0];
			rs.append("，传入参数1为roleName,值为:" + roleName);
			String describe = (String) args[1];
			rs.append("，传入参数2为describe,值为:" + describe);
			return rs.toString();

		} else if ("roleDelete".equals(mName)) {
			// 切入的为管理员回收角色时，参数列表Integer roleId,
			Integer roleId = (Integer) args[0];
			rs.append("，传入参数为roleId,值为:" + roleId);
			return rs.toString();

		} else if ("roleRestore".equals(mName)) {
			// 切入的为管理员恢复角色时，参数列表Integer roleId,
			Integer roleId = (Integer) args[0];
			rs.append("，传入参数为roleId,值为:" + roleId);
			return rs.toString();

		} else if ("addDatabaseBackup".equals(mName)) {
			// 切入的为管理员备份数据库时，参数列表String loadName,
			String loadName = (String) args[0];
			rs.append("，传入参数1为loadName,值为:" + loadName);
			return rs.toString();

		} else if ("restoreDatabase".equals(mName)) {
			// 切入的为管理员还原数据库时，参数列表String fileName,
			String fileName = (String) args[0];
			rs.append("，传入参数为fileName,值为:" + fileName);
			return rs.toString();

		} else if ("logDelete".equals(mName)) {
			// 切入的为管理员删除日志时，参数列表Integer logId,
			Integer logId = (Integer) args[0];
			rs.append("，传入参数为logId,值为:" + logId);
			return rs.toString();

		} else if ("getLogInfoExcel".equals(mName)) {
			// 切入的为管理员打印日志信息时，无参数
			rs.append("，无参数");
			return rs.toString();

		} else if ("delegate".equals(mName)) {
			// 切入的为报案员存储案件委托信息时，参数String insuranceId,int reportNo,
			// String policyId,String context,String num2,
			// String surveyorsId,String mission,
			String insuranceId = (String) args[0];
			rs.append("，传入参数1为insuranceId,值为:" + insuranceId);
			Integer reportNo = (Integer) args[1];
			rs.append("，传入参数2为reportNo,值为:" + reportNo);
			String policyId = (String) args[2];
			rs.append("，传入参数3为policyId,值为:" + policyId);
			String context = (String) args[3];
			rs.append("，传入参数4为context,值为:" + context);
			String num2 = (String) args[4];
			rs.append("，传入参数5为num2,值为:" + num2);
			String surveyorsId = (String) args[5];
			rs.append("，传入参数6为surveyorsId,值为:" + surveyorsId);
			String mission = (String) args[6];
			rs.append("，传入参数7为mission,值为:" + mission);

			return rs.toString();

		} else if ("dispatchdsga".equals(mName)) {
			// 切入的为报案员存储案件派工信息时，参数String surveyorsId,String insuranceId,
			// int reportNo,String policyId,String address,String num3,
			String surveyorsId = (String) args[0];
			rs.append("，传入参数1为surveyorsId,值为:" + surveyorsId);
			String insuranceId = (String) args[1];
			rs.append("，传入参数2为insuranceId,值为:" + insuranceId);
			Integer reportNo = (Integer) args[2];
			rs.append("，传入参数3为reportNo,值为:" + reportNo);
			String policyId = (String) args[3];
			rs.append("，传入参数4为policyId,值为:" + policyId);
			String address = (String) args[4];
			rs.append("，传入参数4为address,值为:" + address);
			String num3 = (String) args[5];
			rs.append("，传入参数5为num3,值为:" + num3);

			return rs.toString();

		} else if ("report".equals(mName)) {
			// 切入的为报案员存储报案信息时，参数String carNo,String policyId,String reportName,
			// String reporTel,String dangerReason,String province,String
			// city,String town,
			// String driverName,String driverTel,
			String carNo = (String) args[0];
			rs.append("，传入参数1为carNo,值为:" + carNo);
			String policyId = (String) args[1];
			rs.append("，传入参数1为policyId,值为:" + policyId);
			String reportName = (String) args[2];
			rs.append("，传入参数3为reportName,值为:" + reportName);
			String reporTel = (String) args[3];
			rs.append("，传入参数4为reporTel,值为:" + reporTel);
			String dangerReason = (String) args[4];
			rs.append("，传入参数5为dangerReason,值为:" + dangerReason);
			String province = (String) args[5];
			rs.append("，传入参数6为province,值为:" + province);
			String city = (String) args[6];
			rs.append("，传入参数7为city,值为:" + city);
			String town = (String) args[7];
			rs.append("，传入参数8为city,值为:" + town);
			String driverName = (String) args[8];
			rs.append("，传入参数9为driverName,值为:" + driverName);
			String driverTel = (String) args[9];
			rs.append("，传入参数10为driverTel,值为:" + driverTel);

			return rs.toString();

		} else if ("surveylookshow_thq".equals(mName)) {
			// 切入的为查勘人员进行了案件的提交时，参数列表Integer reportNos,
			Integer reportNos = (Integer) args[0];
			rs.append("，传入参数为reportNos,值为:" + reportNos);
			return rs.toString();

		} else if ("dolosstwo".equals(mName)) {
			// 切入的为查勘人员进行了三车定损添加时，参数列表Lossinfo lossinfo, String operation,
			// Integer reportNo, String[] partsName,
			// Integer[] partsCount, double[] lossFee, double[] lossMoney,
			// double[] lossPrices
			String operation = (String) args[1];
			Integer reportNos = (Integer) args[2];
			rs.append("，对案件号为:" + reportNos + "的案件进行了第三方车定损信息" + operation + "操作。");
			return rs.toString();

		} else if ("doLoss".equals(mName)) {
			// 切入的为查勘人员进行了本车定损添加时，参数列表ossinfo lossinfo, String operation,Integer
			// reportNo,String[] partsName,Integer[] partsCount
			// double[] lossFee,double[] lossMoney,double[] lossPrices
			String operation = (String) args[1];
			Integer reportNos = (Integer) args[2];
			rs.append("，对案件号为:" + reportNos + "的案件进行了本车定损信息" + operation + "操作。");
			return rs.toString();

		} else if ("addpeoplemess".equals(mName)) {
			// 切入的为查勘人员进行了人伤调查添加时，参数列表HurtInfo hurtInfo, String operation,
			// String[] name, String[] genders, String[] ages,
			// String[] phones, String[] relation

			return rs.toString();

		} else {
			return rs.toString();
		}

	}

}
