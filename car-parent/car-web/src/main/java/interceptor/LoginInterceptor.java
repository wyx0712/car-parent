/**
 * 
 */
package interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author 王耀星
 * @date 2018年10月22日
 */
public class LoginInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// 获取url
		String url = request.getRequestURI();
		// 判断是否是指向登录页面或者处理登录
		if (url.contains("login") || url.contains("doLogin")) {
			return true;
		}
		// 从session中取出userName;
		Object username = request.getSession().getAttribute("userName");
		// 如果session中有username，则说明·已经登陆，继续访问
		if (username != null) {
			return true;
		}
		// 其他情况，转发至登录页面
		request.getRequestDispatcher("/login").forward(request, response);

		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

}
