<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%> 
    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.0</title>
      <style type="text/css">
    	.chexianjiaofei{
    	    height: 38px;
    line-height: 38px;
    padding: 0 18px;
    background-color: #009688;
    color: #fff;
    white-space: nowrap;
    text-align: center;
    font-size: 14px;
    border: none;
    border-radius: 2px;
    cursor: pointer;
    	}
    </style>
    
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <link rel="stylesheet" type="text/css" href="../lib/layui/css/layui.css"/>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    	<script type="text/javascript" src="jq/jquery-1.12.4.min.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
   
    <div class="x-body">
      <table class="layui-table" lay-even lay-skin="row" lay-size="lg">
      	
      	<tr style="background-image: url(images/597125587009628352.jpg);">
            <td style="text-align: center; height: 60px;" colspan="6"><h1>平安车险保险单</h1></td>
        </tr>
         <c:if test="${not empty policy}">
         	<c:forEach  var="policy" items="${policy}">
        <tr>
        	<td style="width: 100px;" align="center">保单编号</td>
        	<td style="width: 100px;">${policy.policyId}</td>
        	<td style="width: 100px;"align="center">投保人姓名</td>
        	<td style="width: 100px;">${policy.applicant}</td>
        	<td style="width: 100px;"align="center">受益人姓名</td>
        	<td style="width: 100px;">${policy.beneficiary}</td>
        </tr>
        <tr>
        	<td style="width: 100px;"align="center">投保人身份证号</td>
        	<td style="width: 400px;">${policy.aIdcard}</td>
        	<td style="width: 100px;"align="center">投保人电话</td>
        	<td style="width: 400px;">${policy.aPhone}</td>
        	<td style="width: 100px;"align="center">投保人地址</td>
        	<td style="width: 400px;">${policy.aAddress}</td>
        	
        	
        </tr>
       
        <tr>
        	<td style="width: 100px;"align="center">受益人身份证号</td>
        	<td style="width: 400px;">${policy.bIdcard}</td>
        	<td style="width: 100px;"align="center">受益人电话</td>
        	<td style="width: 400px;">${policy.bPhone}</td>
        	<td style="width: 100px;"align="center">受益人地址</td>
        	<td style="width: 400px;">${policy.bAdress}</td>
        	
        	
        </tr>
        <tr>
        	<td style="width: 100px;"align="center">车牌号</td>
        	<td style="width: 400px;">${policy.carNo}</td>
        	<%-- <td style="width: 100px;"align="center">发动机号</td>  
        	<td style="width: 400px;">${policy.carNo}</td>
        	<td style="width: 100px;"align="center">车架号</td>
        	<td style="width: 400px;">${policy.carNo}</td> --%>
        	
        </tr>
        
        <tr>
        	<td align="center">险种</td>
        	<td colspan="5">
        	
        		<input type="checkbox" name="insurance" value="1" checked="checked" disabled="false" />交强险&emsp;&emsp;&emsp;&nbsp;
				<input type="checkbox" name="insurance" value="2" disabled="false" />第三方责任险&emsp;&emsp;&emsp;&nbsp;
				<input type="checkbox" name="insurance" value="3" disabled="false" />车辆损失险&emsp;&emsp;&emsp;&nbsp;
        		<input type="checkbox" name="insurance" value="4" disabled="false" />全车盗抢险&emsp;&emsp;&emsp;&nbsp;
				<input type="checkbox" name="insurance" value="5" disabled="false" />车上责任险&emsp;&emsp;&emsp;&nbsp;
        		<input type="checkbox" name="insurance" value="6" disabled="false" />无过失责任险&emsp;&emsp;&emsp;&nbsp;
				<input type="checkbox" name="insurance" value="7" disabled="false" />划痕险&emsp;&emsp;&emsp;&nbsp;
				<input type="checkbox" name="insurance" value="8" disabled="false" />车辆停驶损失险&emsp;&emsp;&emsp;&nbsp;
				<input type="checkbox" name="insurance" value="9" disabled="false" />自燃损失险&emsp;&emsp;&emsp;&nbsp;
        		<input type="checkbox" name="insurance" value="10" disabled="false" />设备损失险
        	</td>
        </tr>
        
        <tr>
        	<td style="width: 100px;"align="center">起始日期</td>
        	<td style="width: 100px;"><fmt:formatDate value="${policy.startDate}" pattern="yyyy-MM-dd" /></td>
        	<td style="width: 100px;"align="center">结束日期</td>
        	<td style="width: 400px;"><input type="date" value="2018-03-21"/></td>
        	<td style="width: 100px;"align="center">经办人</td>
        	<td style="width: 100px;">${policy.loadId}</td>
        </tr>
        
        <tr>
       		 <td style="width: 100px;"align="center">投保金额</td>
        	<td style="width: 400px;">${policy.insuredAmount}</td>
        	<td style="width: 100px;"align="center">赔偿金</td>
        	<td style="width: 400px;">${policy.compensateAmount}</td>
        	
        	<td><a href="insurance_policy2JLG.html">提交
        	</a>
        	</td>
        	
        	<td style="width: 400px;"></td>
        </tr>
        
      	  </c:forEach>
        
        
        </c:if>
      	
		</table>
    </div>
    <script>
      layui.use('laydate', function(){
        var laydate = layui.laydate;
        
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });





     
    </script>
    <script>var _hmt = _hmt || []; (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
      })();</script>
  </body>

</html>