<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
  	
  	<script type="text/javascript" src="jq/jquery-1.12.4.min.js"></script>
  	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
  	
	


</head>
<body class="layui-anim layui-anim-up">



    <div class="x-nav">
   
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="renyuanxinxi.do" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    
    <div class="x-body">
      <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" method="post">
           <input type="text" name="realName"  id="xiayao1" placeholder="请输入姓名" value="${realName}" autocomplete="off" class="layui-input">
          <button type="submit" class="layui-btn"  lay-submit="" id="xiayao2" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
          <span class="x-right" style="line-height:40px" id ="count" >共有数据：${user.total} 条</span>
        </form>
        </form>
        <button class="layui-btn  layui-btn-radius layui-btn-warm"   onclick="javascript:beforeSubmit1()"><i class="layui-icon"> &#xe63c;</i>打印数据</button>
           <form id="form_login123" action="/car-web/dayinrenyuanxinxi.do" method="post">
	 </form>
      </div>
     
      
      <table class="layui-table"  >
        <thead>
          <tr>
            
            <th >ID</th>
            <th>用户登录名</th>
            <th>姓名</th>
            <th>手机</th>
         
            <th>创建时间</th>	
            <th>最后更新时间</th>
          <th>详情</th>
             <th>删除</th>
            
            </tr>
        </thead>
        <tbody>
          <c:if test="${not empty user.list}">
         	<c:forEach  var="user" items="${user.list}">
          <tr id="tt">
           
            <td>${user.userId}</td>
            <td>${user.userName}</td>
            <td>${user.realName}</td>
            <td>${user.phone}</td>
          
          <td><fmt:formatDate value="${user.createTime}" pattern="yyyy-MM-dd" /></td>
            <td><fmt:formatDate value="${user.updateTime}" pattern="yyyy-MM-dd" /></td>
            
            
            
             
            <td class="td-manage">
             </a>
             <a onclick="" title="详情" href="USERxiangqing.do?id=${user.userId}">
                <i class="layui-icon">&#xe631;</i>
              </a>
            </td>
             <td class="">
             </a>
             <a onclick="" title="删除" href="javascript:restore1(${user.userId})">
                <i class="layui-icon">&#xe631;</i>
              </a>
            </td>
            
          </tr>
         </c:forEach>
        </c:if>
        </tbody>
      </table>
      
      
      <div class="page">
        <div>
           <a class="no1" style="background-color: #189F92;color: white; border-radius: 3px" href="/car-web/renyuanxinxi.do?pageNo=${user.pages-user.pages+1}">首页</a>
          <a class="first" style="background-color: #189F92;color: white; border-radius: 3px" href="/car-web/renyuanxinxi.do?pageNo=${user.prePage}">上一页</a>

          <a class="layout" >${user.pageNum}/${user.pages}</a>
          <a class="next" style="background-color: #189F92;color: white; border-radius: 3px" href="/car-web/renyuanxinxi.do?pageNo=${user.nextPage}">下一页</a>
          <a class="last" style="background-color: #189F92;color: white; border-radius: 3px" href="/car-web/renyuanxinxi.do?pageNo=${user.pages}">末页</a>
        </div>
      </div>



    </div>
    
    
    
  </body>
<script type="text/javascript">
function beforeSubmit1(){
    $("#form_login123").submit();
}
function restore1(userId){
	$.getJSON(
			"userTrashRestore_lzj1.do",
			{"userId":userId},
			function(data){
				if(data==1){
					alert("删除成功！！！");
					window.location.reload();
				}else{
					alert("删除失败！！！");
				}
				
			});
		}

</script>


</html>