<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%> 
    
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.0</title>
      <style type="text/css">
    	.chexianjiaofei{
    	    height: 38px;
    line-height: 38px;
    padding: 0 18px;
    background-color: #009688;
    color: #fff;
    white-space: nowrap;
    text-align: center;
    font-size: 14px;
    border: none;
    border-radius: 2px;
    cursor: pointer;
    	}
    </style>
    
    <meta name="renderer" content="webkit"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <link rel="stylesheet" type="text/css" href="./lib/layui/css/layui.css"/>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    	<script type="text/javascript" src="./js/jquery-1.12.4.min.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
  <div class="x-nav">
   
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="renyuanxinxi.do" title="刷新">
        <i class="layui-icon" style="line-height:30px">返回</i></a>
    </div> 
    <div class="x-body">
      <table class="layui-table" lay-even lay-skin="row" lay-size="lg">
      	
      	<tr>
            <td style="text-align: center; height: 60px;" colspan="6"><h1>用户信息</h1></td>
        </tr>
       <c:if test="${not empty user}">
        <tr>
        	<td style="width: 100px;" align="center">员工编号</td>
        	<td style="width: 100px;">${user.userId}</td>
        	<td style="width: 100px;"align="center">员工姓名</td>
        	<td style="width: 100px;">${user.realName}</td>
        	<td style="width: 100px;"align="center">手机号</td>
        	<td style="width: 100px;">${user.phone}</td>
        </tr>
        <tr>
        	
        	<td style="width: 100px;"align="center">权限ID</td>
        	<td style="width: 400px;">${user.roleId}</td>
        	<td style="width: 100px;"align="center">登录用户名</td>
        	<td style="width: 400px;">${user.userName}</td>
        	
        	<td style="width: 100px;"align="center">登录密码</td>
        	<td style="width: 400px;">${user.password}</td>
         <tr>	
        	<td style="width: 100px;"align="center">在职状态(1.在职/2.离职)</td>
        	<td style="width: 400px;">${user.jobState}</td>
        	
        	<td style="width: 100px;"align="center">创建时间</td>
        	<td style="width: 100px;"><fmt:formatDate value="${user.createTime}" pattern="yyyy-MM-dd" /></td>
        	
        	<td style="width: 100px;"align="center">操作人</td>
        	<td style="width: 400px;">${user.opId}</td>
       
   </tr>
    
    
   <%--  <tr>
  
    
       <td style="width: 100px;"align="center">接案状态</td>
        	<td style="width: 400px;">${user.reportState}</td>
       
       <td style="width: 100px;"align="center">查勘人id</td>
        	<td style="width: 400px;">${user.surveyId}</td>
       
       <td style="width: 100px;"align="center">核赔人id</td>
        	<td style="width: 400px;">${user.auditId}</td>
        </tr> --%>
       
      
        
        
        
      <%--    <tr>
        	 <td style="width: 100px;"align="center">报案时间</td>
        	<td style="width: 100px;"><fmt:formatDate value="${reportNo.reportTime}" pattern="yyyy-MM-dd" /></td>
        	<td style="width: 100px;"align="center">结束日期</td>
        	<td style="width: 400px;"> <fmt:formatDate value="${reportNo.endTime}" pattern="yyyy-MM-dd" /></td> 
   
        </tr>
        
        <tr>
       		 <td style="width: 100px;"align="center">投保金额</td>
        	<td style="width: 400px;">${policy.insuredAmount}</td>
        	<td style="width: 100px;"align="center">赔偿金</td>
        	<td style="width: 400px;">${policy.compensateAmount}</td>
        	
        	
        	<td style="width: 400px;"></td>
        </tr>  --%>
        
        
        </c:if> 
      	
		</table>
    </div>

  </body>

</html>