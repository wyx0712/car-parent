<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="UTF-8">
    <title>查勘信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <style>
    	img{
    		width: 60px;
    		height: 56px;
    		padding-right: 30px;
    		color: darkred;
    	}
    </style>
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript">
    function member_del(dd){
         layer.confirm('您确认要提交吗？', {
  		btn: ['确认','取消'] //按钮
			}, function(){
				
				
				layer.msg('提交成功！',{time:2000});
				setTimeout("qwerr("+dd+")",1000);
			  
			}, function(){
				layer.msg('已取消..');
			 return false;
			});
			return false;//重点二！！！！  阻止提交行为！！

    }
    
    function qwerr(dd) {
    
    	  $("#report").val(dd);
		  $("#form001").submit();
	}
    
      </script>
  
  </head>
  
  <body>
  <form action="suvreysubmit_thq.do" method="post" id="form001">
  	<input type="hidden" id="report" name="reportNos" />
  	
  	
  </form>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="survey02_thq.do">返回桌面</a>
        
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    <div class="x-body">
      <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so">
          <div class="layui-input-inline">
            <select name="surveyType" id="surveyType">
              <option value="0">全部</option>
              <option value="1">报案号</option>
              <option value="2">订单号</option>
              <option value="3">报案人</option>
            </select>
          </div>
          <input type="hidden" class="pageNo" name="pageNo" />
          <input type="text" name="surveyName" id="surveyName" placeholder="请输入查询内容" autocomplete="off" class="layui-input">
          <button type="button" class="layui-btn" id="selectSurvey"><i class="layui-icon">&#xe615;</i></button>
        </form>
      </div>
      <xblock>
       
        <span class="x-right" style="line-height:40px" id="rows"></span>
      </xblock>
      <table class="layui-table">
        <thead>
          <tr>
            <th style="width:10%;">报案号</th>
            <th style="width:10%;">订单号</th>
            <th style="width:10%;">报案人</th>
            <th style="width:10%;">查勘状态</th>
            <th style="width:10%;">本车定损</th>
			<th style="width:10%;">三车定损</th>
            <th style="width:10%;">人伤调查</th>
            <th style="width:10%;">提交状态</th>
            <th style="width:20%;">操作</th>
            </tr>
        </thead>
        <tbody>
        	 <c:if test="${not empty requestScope.list}">
         	<c:forEach  var="show" items="${requestScope.list}" varStatus="status">
         		<tr>
            		<td>${show.reportNo}</td>
            		<td>${show.policyId}</td>
            		<td>${show.reportName}</td>
            		<td <c:if test="${show.surveyLook eq '1'}">style="color: red;"</c:if><c:if test="${show.surveyLook eq '2'}">style="color: green;"</c:if>><c:if test="${show.surveyLook eq '1'}">X</c:if><c:if test="${show.surveyLook eq '2'}">√</c:if></td>
            		<td <c:if test="${show.surveyCarone eq '0'}">style="color: darkred;"</c:if><c:if test="${show.surveyCarone eq '1'}">style="color: red;"</c:if><c:if test="${show.surveyCarone eq '2'}">style="color: green;"</c:if><c:if test="${show.surveyCarone eq '3'}">style="color: black;"</c:if>>
            			<c:if test="${show.surveyCarone eq '0'}">∞</c:if><c:if test="${show.surveyCarone eq '1'}">X</c:if><c:if test="${show.surveyCarone eq '2'}">√</c:if><c:if test="${show.surveyCarone eq '3'}">？</c:if></td>
            		<td <c:if test="${show.surveyCartwo eq '0'}">style="color: darkred;"</c:if><c:if test="${show.surveyCartwo eq '1'}">style="color: red;"</c:if><c:if test="${show.surveyCartwo eq '2'}">style="color: green;"</c:if><c:if test="${show.surveyCartwo eq '3'}">style="color: black;"</c:if>>
            			<c:if test="${show.surveyCartwo eq '0'}">∞</c:if><c:if test="${show.surveyCartwo eq '1'}">X</c:if><c:if test="${show.surveyCartwo eq '2'}">√</c:if><c:if test="${show.surveyCartwo eq '3'}">？</c:if></td>
            		<td <c:if test="${show.surveyPeople eq '0'}">style="color: darkred;"</c:if><c:if test="${show.surveyPeople eq '1'}">style="color: red;"</c:if><c:if test="${show.surveyPeople eq '2'}">style="color: green;"</c:if><c:if test="${show.surveyPeople eq '3'}">style="color: black;"</c:if>>
            			<c:if test="${show.surveyPeople eq '0'}">∞</c:if><c:if test="${show.surveyPeople eq '1'}">X</c:if><c:if test="${show.surveyPeople eq '2'}">√</c:if><c:if test="${show.surveyPeople eq '3'}">？</c:if></td>
            <td class="td-status">
              <span class="layui-btn layui-btn-normal layui-btn-mini">退回案件</span>
            </td>
         <td>
				<c:if test="${show.surveyLook eq '2'}"><c:if test="${show.surveyCarone ne '1'}"><c:if test="${show.surveyCartwo ne '1'}"><c:if test="${show.surveyPeople ne '1'}"><button class="layui-btn-danger layui-btn layui-btn-xs"  onclick="member_del(${show.reportNo})" href="javascript:;" ><i class="layui-icon">&#xe70a;</i>重新提交</button></c:if></c:if></c:if></c:if>
				
			</td>
          </tr>
         	</c:forEach>
        	</c:if>
         
        </tbody>
      </table>
      <div class="page">
        <div>
          <a class="num1" href="javascript:location.replace(location.href);">首页</a>
          <a class="num2" href="javascript:location.replace(location.href);">上一页</a>
          &nbsp;<span>1/1</span>&nbsp;
          <a class="num3" href="javascript:location.replace(location.href);">下一页</a>
          <a class="num4" href="javascript:location.replace(location.href);">尾页</a>
        </div>
      </div>

    </div>
    <div id="showImg"></div>
    
  </body>

</html>