<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>现场查勘</title>
</head>
<body>
	<div id="">
		<h1 align="center">现场查勘</h1>
	<form action="doSurvey.do" method="post" enctype="multipart/form-data">
		
		<table style="margin: 0 auto;" border="1" bordercolor="#3AB2FF" cellspacing="3" cellpadding="5">
			<tr>
				<td align="right">报案号：</td>
				<td>
					<select id="reportNo" name="reportNo" style="width: 56%;">
					<c:if test="${not empty  carMessage}" >
							<option value="1" selected="selected">待查勘报案号</option>
							<c:forEach  var="carMessage" items="${requestScope.carMessage}" varStatus="status">
								<option value="${carMessage.reportNo}" >${carMessage.reportNo}</option>
							</c:forEach>
							
						</c:if>
					
							<c:forEach  var="surveyInfo" items="${requestScope.surveyInfo}" varStatus="status">
								<option value="${surveyInfo.reportNo}" >${surveyInfo.reportNo}</option>
							</c:forEach>
						
					</select>
				</td>
				<td align="right">出险驾驶员：</td>
				<td><input type="text" id="driverName" name="driverName" id="" value="${surveyInfo.driverName }	" /></td>
			</tr>
			<tr>
				<td align="right">报案人：</td>
				<td ><input type="text" id="reportName" name="reportName" id="" value="${surveyInfo.reportName }	" /></td>
				<td align="right">报案人电话：</td>
				<td ><input type="text" id="reporTel" name="reporTel" id="" value="${surveyInfo.reporTel }	" /></td>
				
			</tr>
			<tr>
				<td align="right">准驾车型：</td>
				<td>
					<select id=""name="carType" style="width: 56%;">						
							<option value="大客车" selected="selected">大客车</option>
												
							<option value="货车">货车</option>
			
							<option value="小汽车">小汽车</option>
						
							<option value="摩托车">摩托车</option>

							<option value="面包车">面包车</option>
						
					</select>
				</td>
				<td align="right">出险驾驶员电话：</td>
				<td><input type="text" id="driverTel" name="driverTel" id="" value="${surveyInfo.driverTel }"/></td>
			</tr>
			<tr>
				<td align="right">查勘地点：</td>
				<td>
					<select id=""name="surveyAddress" style="width: 50%;">
					
							<option value="第一现场" selected="selected">第一现场</option>
						
							<option value="第二现场">第二现场</option>
						
							<option value="第三现场">第三现场</option>
						
					</select>
				</td>
				<td align="right">出险原因：</td>
				<td>
					<select id=""name="dangerReason" style="width: 45%;">
						<option value="1">-请选择-</option>
						<c:if test="${surveyInfo.dangerReason == '人为' }">
							<option value="人为" selected="selected">人为</option>
						</c:if>
						<c:if test="${surveyInfo.dangerReason != '人为' || surveyInfo.dangerReason == null}">
							<option value="人为">人为</option>
						</c:if>
						<c:if test="${surveyInfo.dangerReason == '天灾' }">
							<option value="天灾" selected="selected">天灾</option>
						</c:if>
						<c:if test="${surveyInfo.dangerReason != '天灾' || surveyInfo.dangerReason == null}">
							<option value="天灾">天灾</option>
						</c:if>
						<c:if test="${surveyInfo.dangerReason == '意外' }">
							<option value="意外" selected="selected">意外</option>
						</c:if>
						<c:if test="${surveyInfo.dangerReason != '意外' || surveyInfo.dangerReason == null}">
							<option value="意外">意外</option>
						</c:if>
						
					</select>
				</td>
			</tr>
			<tr>
				<td align="right">出险地点：</td>
				<td>
      				<input type="text" name="dangerAddress" id="" value="${surveyInfo.dangerAddress }"/>
				</td>
				<td align="right">第三方车损：</td>
				<td>
					<c:if test="${surveyInfo.num1 == 1 }">
						<input type="radio" name="num1" value="1" checked="checked"/>有
					</c:if>
					<c:if test="${surveyInfo.num1 != 1 || surveyInfo.num1 == null}">
						<input type="radio" name="num1" value="1"/>有
					</c:if>
					<c:if test="${surveyInfo.num1 == 0 }">
						<input type="radio" name="num1" value="0" checked="checked"/>没有
					</c:if>
					<c:if test="${surveyInfo.num1 != 0 || surveyInfo.num1 == null}">
						<input type="radio" name="num1" value="0"/>没有
					</c:if>
				</td>
			</tr>
			<tr>
				<td align="right">出险区域：</td>
				<td>
					<c:if test="${surveyInfo.dangerArea == '市内' }">
						<input type="radio" name="dangerArea" value="市内" checked="checked"/>市内
					</c:if>
					<c:if test="${surveyInfo.dangerArea != '市内' || surveyInfo.dangerArea == null}">
						<input type="radio" name="dangerArea" value="市内"/>市内
					</c:if>
					<c:if test="${surveyInfo.dangerArea == '省内' }">
						<input type="radio" name="dangerArea" value="省内" checked="checked"/>省内
					</c:if>
					<c:if test="${surveyInfo.dangerArea != '省内' || surveyInfo.dangerArea == null}">
						<input type="radio" name="dangerArea" value="省内"/>省内
					</c:if>
					<c:if test="${surveyInfo.dangerArea == '省外' }">
						<input type="radio" name="dangerArea" value="省外" checked="checked"/>省外
					</c:if>
					<c:if test="${surveyInfo.dangerArea != '省外' || surveyInfo.dangerArea == null}">
						<input type="radio" name="dangerArea" value="省外"/>省外
					</c:if>
				</td>
				<td align="right">人员伤亡：</td>
				<td>
					<c:if test="${surveyInfo.num2 == 1 }">
						<input type="radio" name="num2" value="1" checked="checked"/>有
					</c:if>
					<c:if test="${surveyInfo.num2 != 1 || surveyInfo.num2 == null}">
						<input type="radio" name="num2" value="1"/>有
					</c:if>
					<c:if test="${surveyInfo.num2 == 0 }">
						<input type="radio" name="num2" value="0" checked="checked"/>没有
					</c:if>
					<c:if test="${surveyInfo.num2 != 0 || surveyInfo.num2 == null}">
						<input type="radio" name="num2" value="0"/>没有
					</c:if>
				</td>
			</tr>
			<tr>
				<td align="right">道路信息：</td>
				<td colspan="3">
					<c:if test="${surveyInfo.roadInfo == '高速公路' }">
						<input type="radio" name="roadInfo" value="高速公路" checked="checked"/>高速公路
					</c:if>
					<c:if test="${surveyInfo.roadInfo != '高速公路' || surveyInfo.roadInfo == null}">
						<input type="radio" name="roadInfo" value="高速公路"/>高速公路
					</c:if>
					<c:if test="${surveyInfo.roadInfo == '普通公路' }">
						<input type="radio" name="roadInfo" value="普通公路" checked="checked"/>普通公路
					</c:if>
					<c:if test="${surveyInfo.roadInfo != '普通公路' || surveyInfo.roadInfo == null}">
						<input type="radio" name="roadInfo" value="普通公路"/>普通公路
					</c:if>
					<c:if test="${surveyInfo.roadInfo == '市区道路' }">
						<input type="radio" name="roadInfo" value="市区道路" checked="checked"/>市区道路
					</c:if>
					<c:if test="${surveyInfo.roadInfo != '市区道路' || surveyInfo.roadInfo == null}">
						<input type="radio" name="roadInfo" value="市区道路"/>市区道路
					</c:if>
					<c:if test="${surveyInfo.roadInfo == '乡村公路' }">
						<input type="radio" name="roadInfo" value="乡村公路" checked="checked"/>乡村公路
					</c:if>
					<c:if test="${surveyInfo.roadInfo != '乡村公路' || surveyInfo.roadInfo == null}">
						<input type="radio" name="roadInfo" value="乡村公路"/>乡村公路
					</c:if>
					<c:if test="${surveyInfo.roadInfo == '庭院车场' }">
						<input type="radio" name="roadInfo" value="庭院车场" checked="checked"/>庭院车场
					</c:if>
					<c:if test="${surveyInfo.roadInfo != '庭院车场' || surveyInfo.roadInfo == null}">
						<input type="radio" name="roadInfo" value="庭院车场"/>庭院车场
					</c:if>
					<c:if test="${surveyInfo.roadInfo == '其它' }">
						<input type="radio" name="roadInfo" value="其它" checked="checked"/>其它
					</c:if>
					<c:if test="${surveyInfo.roadInfo != '其它' || surveyInfo.roadInfo == null}">
						<input type="radio" name="roadInfo" value="其它"/>其它
					</c:if>
				</td>
			</tr>
			<tr>
				<td align="right">出险经过：</td>
				<td colspan="3">
					<textarea id="" name="dangerThrough" style="width: 90%;height: 80px;font-size: 18px;">${surveyInfo.dangerThrough }</textarea>
				</td>
			</tr>
			<tr>
				<td align="right">查勘意见：</td>
				<td colspan="3">
					<textarea id="" name="surveyOpinion" style="width: 90%;height: 80px;font-size: 18px;">${surveyInfo.surveyOpinion }</textarea>
				</td>
			</tr>
			<!--多图上传-->
			<tr>
				<td align="right">添加照片：</td>
				<td colspan="3" style="position: relative;">
					<c:if test="${surveyInfo.imgOne == null }">
						<input type="file" name="img" id="one" value="" onchange="mm(1)"/>&nbsp;&nbsp;&nbsp;
						<img src="images/tupian1.jpg" id="img1"/>
					</c:if>
					<c:if test="${surveyInfo.imgOne != null }">
						<input type="file" name="img" id="one" value="${surveyInfo.imgOne}" onchange="mm(1)"/>&nbsp;&nbsp;&nbsp;
						<img src="${surveyInfo.imgOne }" id="img1"/>
					</c:if>
					<c:if test="${surveyInfo.imgTwo == null }">
						<input type="file" name="img" id="two" value="" onchange="mm(2)"/>&nbsp;&nbsp;&nbsp;
						<img src="images/tupian1.jpg" id="img2"/>
					</c:if>
					<c:if test="${surveyInfo.imgTwo != null }">
						<input type="file" name="img" id="two" value="${surveyInfo.imgTwo }" onchange="mm(2)"/>&nbsp;&nbsp;&nbsp;
						<img src="${surveyInfo.imgTwo }" id="img2"/>
					</c:if>
					<c:if test="${surveyInfo.imgThree == null }">
						<input type="file" name="img" id="three" value="" onchange="mm(3)"/>&nbsp;&nbsp;&nbsp;
						<img src="images/tupian1.jpg" id="img3"/>
					</c:if>
					<c:if test="${surveyInfo.imgThree != null }">
						<input type="file" name="img" id="three" value="${surveyInfo.imgThree }" onchange="mm(3)"/>&nbsp;&nbsp;&nbsp;
						<img src="${surveyInfo.imgThree }" id="img3"/>
					</c:if>
				</td>
			</tr>
			<tr>
				<td align="right">查勘员：</td>
				<td colspan="3"><input type="text" name="surveyName" value="${surveyInfo.surveyName }"/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;查勘机构：
				<input type="text" name="companyName" value="${surveyInfo.companyName }"/>
				
				
				</td>
			</tr>
			<tr><td align="center" colspan="4">
				<input type="hidden" name="operation" value="${operation }">
				
				<input type="submit" value="保 存"/>&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="reset" value="重 置" onclick="resets()"/>
			</td></tr>
		</table>
	</form>
 </div>
 
 
 	<style type="text/css">
 		input[type="submit"]{
 			width: 10%;
 			height: 35px;
 			font-size: 18px;
 		}
 		input[type="reset"]{
 			width: 10%;
 			height: 35px;
 			font-size: 18px;
 		}
 		input[type="file"]{
 			width: 15%;
 			height: 140px;
 			opacity: 0;
 		}
 		
 		img:nth-of-type(1){
 			width: 15%;
 			height: 140px;
 			position: absolute;
 			left: 0.4%;
 			bottom: 3px;
 			border: 1px solid;
 			z-index: -1;
 		}
 		img:nth-of-type(2){
 			width: 15%;
 			height: 140px;
 			position: absolute;
 			left: 17.6%;
 			bottom: 3px;
 			border: 1px solid;
 			z-index: -1;
 		}
 		img:nth-of-type(3){
 			width: 15%;
 			height: 140px;
 			position: absolute;
 			left: 34.7%;
 			bottom: 3px;
 			border: 1px solid;
 			z-index: -1;
 		}
 	</style>
 	
 	
 	<script type="text/javascript" src="js/jquery-1.12.4.js" ></script>
 	<script>
 		
 		
 			$("#reportNo").change(function(){
 			
 				$.ajax({
              url : "showreportNo.do",
              type : "POST",
              dataType:"json",
              data :{reportNo:$("#reportNo").val()},
              success:function(result) {
            	
              	if(result.error == 1){
              		$("#driverName").val("");
            	  $("#reportName").val("");
            	  $("#reporTel").val("");
            	  $("#driverTel").val("");
              	}else{
              		 $("#driverName").val(result.driverName);
            	  $("#reportName").val(result.reportName);
            	  $("#reporTel").val(result.reporTel);
            	  $("#driverTel").val(result.driverTel);		
              	} 
              },
              error:function(result){
                
            	 
              }
          });
 				
 				
 			});
 		
 		
 		
 		function resets() {
 			$("#img1").attr("src","images/tupian1.jpg");
 			$("#img2").attr("src","images/tupian1.jpg");
 			$("#img3").attr("src","images/tupian1.jpg");
 		}
 		//前端获取“选中图片”的路径
 		function mm (code) {
 			var a = document.getElementById("one");
 			var b = document.getElementById("two");
 			var c = document.getElementById("three");
 			if(code == 1){
 				ii(a,code);
 			}else if (code == 2) {
 				ii(b,code);
 			}else{
 				ii(c,code);
 			}
 		}

		function ii(obj,codes){
			//解决文件路径C:\fakepath问题
    		var oFReader = new FileReader();
    		var file =obj.files[0];
    		oFReader.readAsDataURL(file);
    		
    		oFReader.onloadend = function(oFRevent){
	        	var src = oFRevent.target.result;
//	        	alert(src);
	        	if(codes == 1){
	        		$('#img1').attr('src',src);
	        	}
	        	if (codes == 2) {
	        		$('#img2').attr('src',src);
	        	}
	        	if (codes == 3) {
	        		$('#img3').attr('src',src);
	        	}
    		}
    	}
 	</script>
 	
 	
</body>
</html>