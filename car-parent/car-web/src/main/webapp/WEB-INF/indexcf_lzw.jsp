<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>客服中心</title>

 <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
       
 <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="./css/font.css">
        <link rel="stylesheet" href="./css/xadmin.css">
        <link rel="stylesheet" href="./css/layui.css">
        <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
        <script type="text/javascript"  src="js/echarts.min.js"></script>
        <script type="text/javascript">
        function showTime(){
            nowtime=new Date();
            year=nowtime.getFullYear();
            month=nowtime.getMonth()+1;
            date=nowtime.getDate();
            document.getElementById("mytime").innerText=year+"年"+month+"月"+date+" "+nowtime.toLocaleTimeString();
        }

        setInterval("showTime()",1000);

        </script>
        
        <style type="text/css">
        .layui-col-xs4 {
	width: 19.999999%
}
        </style>

</head>
<body>

 <div class="x-body layui-anim layui-anim-up">
 <blockquote class="layui-elem-quote">
			欢迎<span class="x-red">${sessionScope.loginUser.userName}！</span>
			<span id="mytime"></span>
		</blockquote>
<fieldset class="layui-elem-field">
            <legend>待办工作</legend>
            <div class="layui-field-box">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body">
                            <div class="layui-carousel x-admin-carousel x-admin-backlog" lay-anim="" lay-indicator="inside" lay-arrow="none" style="width: 100%; height: 90px;">
                                <div carousel-item="">
                                    <ul class="layui-row layui-col-space10 layui-this">
                                        <li class="layui-col-xs4">
                                            <a href="report_lzw.do" class="x-admin-backlog-body">
                                                <h3>报案平台</h3>
                                                <p>
                                                    <cite>${num1}</cite></p>
                                            </a>
                                        </li>
                                        <li class="layui-col-xs4">
                                            <a href="delega_lzw.do" class="x-admin-backlog-body">
                                                <h3>案件委托</h3>
                                                <p>
                                                    <cite>${num2}</cite></p>
                                            </a>
                                        </li>
                                        <li class="layui-col-xs4">
                                            <a href="delegate_lzw.do" class="x-admin-backlog-body">
                                                <h3>调度派工</h3>
                                                <p>
                                                    <cite>${num3}</cite></p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        </div>

</body>
</html>