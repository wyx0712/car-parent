<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>欢迎页面-X-admin2.0</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="./css/font.css">
<link rel="stylesheet" href="./css/xadmin.css">
<script type="text/javascript"
	src="./js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="./lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript" src="./js/xadmin.js"></script>
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<div class="x-body">
		<form class="layui-form" action="#" method="post">
			<div class="layui-form-item">
				<label for="roleName" class="layui-form-label"> <span
					class="x-red">*</span>角色名
				</label>
				<div class="layui-input-inline">
					<input type="text" id="roleName" name="roleName"  
						required="" lay-verify="required" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label"><span class="x-red">*</span>角色描述</label>
				<div class="layui-input-inline">
					<input type="text"  id="describe"
						name="describe" required="" lay-verify="required" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			
			<div >
				<input id ="sub" class="layui-btn" lay-filter="add" type="button" value="添加"/>	
			</div>
		</form>
	</div>
	<script>
	$(function(){
		$("#sub").click(function(){
			var roleName = $("#roleName").val();
			var describe = $("#describe").val();
			$.getJSON(
		    		"admin_roleadd_wyxA.do",
		    		{"roleName":roleName,"describe":describe},
		    		function(i){
		    			if(i>0){
		       				 layer.msg('增加成功!',{icon:1,time:2000});
		       				window.parent.location.reload(); //刷新父页面
		       				var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
		       				parent.layer.close(index); //关闭当前窗口
		       				 
		       			}else if(i==0){
		       				layer.msg('角色名已存在!',{icon:5,time:1000});
		       			}else{
		       				layer.msg('操作有误!',{icon:5,time:1000});
		       			}	
		    		}
		  	 ); 		

			
		}
	)
})
	
    </script>
	
</body>

</html>