<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
    <div class="x-body">
        <form class="layui-form" action="addpeoplemess.do" method="post">
          
          <div class="layui-form-item layui-form-text">
              
              <div class="layui-input-block">
                  <table class="layui-table">
                    <tbody>
                    <tr>
									<td align="right">待定损报案号：</td>
									<td>
										<select id="reportNo" name="reportNo" style="width: 56%;">
										<c:if test="${not empty  carMessage}" >
											
												<c:forEach  var="carMessage" items="${requestScope.carMessage}" varStatus="status">
													<option value="${carMessage.reportNo}" >${carMessage.reportNo}</option>
												</c:forEach>
												
											</c:if>
											<c:if test="${not empty  hurtInfo}" >
												
													<option value="${hurtInfo.reportNo}" >${hurtInfo.reportNo}</option>
												
											</c:if>
										</select>
									</td>
									
							
									
								</tr>
                    
                      <tr>
                        <td>伤者姓名：</td>
                        <td>
                        	<input type="text" name="injuredName" value="${hurtInfo.injuredName }" id="" class="layui-input" />
                        </td>
                        <td>性别：</td>
                        <td>
                        	<select name="gender" id="">
                        	
                        					<c:if test="${hurtInfo.gender == '男' }">
													<option value="男" selected="selected">男</option>  
											</c:if>
											<c:if test="${hurtInfo.gender != '男' || hurtInfo.gender == null }">
												<option value="男">男</option>
											</c:if>
                        					<c:if test="${hurtInfo.gender == '女' }">
													<option value="女" selected="selected">女</option>  
											</c:if>
											<c:if test="${hurtInfo.gender != '女' || hurtInfo.gender == null }">
												<option value="女">女</option>
											</c:if>
                        	
                        	</select>
                        </td>
                        <td>年龄：</td>
                        <td><input type="text" name="age" id="" value="${hurtInfo.age }" class="layui-input" /></td>
                      </tr>
                      <tr>
                      	<td>伤亡情况：</td>
                      	<td>
                      		<select id="" name="censusRegister">
                      						<c:if test="${hurtInfo.censusRegister == '轻伤' }">
													<option value="轻伤" selected="selected">轻伤</option>  
											</c:if>
											<c:if test="${hurtInfo.censusRegister != '轻伤' || hurtInfo.censusRegister == null }">
												<option value="轻伤">轻伤</option>
											</c:if>
                      						<c:if test="${hurtInfo.censusRegister == '重伤' }">
													<option value="重伤" selected="selected">重伤</option>  
											</c:if>
											<c:if test="${hurtInfo.censusRegister != '重伤' || hurtInfo.censusRegister == null }">
												<option value="重伤">重伤</option>
											</c:if>
                      						<c:if test="${hurtInfo.censusRegister == '残疾' }">
													<option value="残疾" selected="selected">残疾</option>  
											</c:if>
											<c:if test="${hurtInfo.censusRegister != '残疾' || hurtInfo.censusRegister == null }">
												<option value="残疾">残疾</option>
											</c:if>
                      						<c:if test="${hurtInfo.censusRegister == '死亡' }">
													<option value="死亡" selected="selected">死亡</option>  
											</c:if>
											<c:if test="${hurtInfo.censusRegister != '死亡' || hurtInfo.censusRegister == null }">
												<option value="死亡">死亡</option>
											</c:if>
									
                      		</select>
                      	</td>
                      	<td>联系电话：</td>
                      	<td><input type="text" name="phone" value="${hurtInfo.phone }" id="" class="layui-input" /></td>
                      	<td>就诊医院：</td>
                      	<td><input type="text" name="hospital" value="${hurtInfo.hospital }" id="" class="layui-input" /></td>
                      </tr>
                      <tr>
                      	<td>主治医生：</td>
                      	<td><input type="text" name="doctor" value="${hurtInfo.doctor }" id="" class="layui-input" /></td>
                      	<td>科室床位：</td>
                      	<td><input type="text" name="office" value="${hurtInfo.office }" id="" class="layui-input" /></td>
                      	<td>事故车牌号：</td>
                      	<td><input type="text" name="carNo" value="${hurtInfo.carNo }" id="" class="layui-input" /></td>
                      </tr>
                      <tr>
                      	<td colspan="6" style="height: 20px;background-color: rgb(200,238,238);"></td>
                      </tr>
                      <tr>
                      	<td colspan="6">
                      		<table id="parts">
                      			<tr>
                      				<td>姓名</td>
                      				<td>性别</td>
                      				<td>年龄</td>
                      				<td>联系电话</td>
                      				<td>与伤者关系</td>
                      				<td>操作</td>
                      				<td rowspan="100"><input type="button" value="添 加" id="add" class="layui-btn"/></td>
                      			</tr>
                      			<c:if test="${empty hurtrelation}">
	                      			<tr>
	                      				<td><input type="text" name="name" id="" class="layui-input" /></td>
	                      				<td><input type="text" name="genders" id="" class="layui-input" /></td>
	                      				<td><input type="text" name="ages" id="" class="layui-input" /></td>
	                      				<td><input type="text" name="phones" id="" class="layui-input" /></td>
	                      				<td><input type="text" name="relation" id="" class="layui-input" /></td>
	                      				<td><input type='button' value='删 除' id='del0' onclick='del(this)' class='layui-btn' style='background-color: rgb(255 87 34);'/></td>
	                      			</tr>
                      			</c:if>
                      			<c:if test="${not empty hurtrelation}">
                      				<c:forEach var="hurtrelation" items="${hurtrelation}"  >
		                      			<tr>
		                      				<td><input type="text" name="name" value="${hurtrelation.name}" id="" class="layui-input" /></td>
		                      				<td><input type="text" name="genders" id="" value="${hurtrelation.gender}" class="layui-input" /></td>
		                      				<td><input type="text" name="ages" id="" value="${hurtrelation.age}" class="layui-input" /></td>
		                      				<td><input type="text" name="phones" id="" value="${hurtrelation.phone}" class="layui-input" /></td>
		                      				<td><input type="text" name="relation" id="" value="${hurtrelation.relation}" class="layui-input" /></td>
		                      				<td><input type='button' value='删 除' id='del0' onclick='del(this)' class='layui-btn' style='background-color: rgb(255 87 34);'/></td>
		                      			</tr>
                      				</c:forEach>
                      			</c:if>
                      		</table>
                      	</td>
                      </tr>
                      <tr>
                      	<td colspan="6" style="height: 20px;background-color: rgb(200,238,238);"></td>
                      </tr>
                      <tr>
                      
                      	<td></td>
                      	<td></td>
                      	<td></td>
                      	<td></td>
                      </tr>
                      <tr>
                      	<td>医疗报价：</td>
                      	<td><input type="text" name="lossFee" value="${hurtInfo.lossFee }" id="" class="layui-input" /></td>
                      	<td>报损人：</td>
                      	<td><input type="text" name="userName" value="${hurtInfo.userName }" id="" class="layui-input" /></td>
                      	
                      </tr>
                      <tr>
                      	<td>主要诊断：</td>
                      	<td colspan="5"><textarea name="illnessState" id="" style="width: 70%; height: 100px;">${hurtInfo.illnessState }</textarea></td>
                      </tr>
                    </tbody>
                  </table>
              </div>
          </div>
          
          <div class="layui-form-item">
              <label for="L_repass" class="layui-form-label">
              </label>
          
              <input type="hidden" name="operation" value="${operation }" />
              <input type="submit" class="layui-btn"  value="保 存" />
          </div>
      </form>
    </div>
    
    
    	<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
		<script>
			$(document).ready(function() {
				$("input[name='name']").blur(function() {
					var a = $(this).val();
					if(a == "") {
						$("input[name='gender']").attr("disabled", "disabled");
						$("input[name='age']").attr("disabled", "disabled");
						$("input[name='phone']").attr("disabled", "disabled");
						$("input[name='relation']").attr("disabled", "disabled");
					} else {
						$("input[name='gender']").removeAttr("disabled");
						$("input[name='age']").removeAttr("disabled");
						$("input[name='phone']").removeAttr("disabled");
						$("input[name='relation']").removeAttr("disabled");
					}

				});
//				看来只能用js了
//				$("input[value='删 除']").click(function(){
//					alert("aa");
//					$(this).parent().parent().remove();
//				});
				var i = 1;
				//真是痛苦啊
				$("#add").click(function(){
						var $tr = $("<tr><td><input type='text' name='name' id='' class='layui-input' /></td>"
						+"<td><input type='text' name='genders' id='' class='layui-input' /></td><td>"
						+"<input type='text' name='ages' id='' class='layui-input' /></td><td>"
						+"<input type='text' name='phones' id='' class='layui-input' /></td><td>"
						+"<input type='text' name='relation' id='' class='layui-input' /></td><td>"
						+"<input type='button' value='删 除' id='del"+i+"' onclick='del(this)' class='layui-btn' style='background-color: rgb(255 87 34);'/></td></tr>");
						
						$("#parts tr:last-child").after($tr);
						i++;
				});
				

			})
		</script>
		<script>
			//终于找到你了
			function del (e) {
				document.getElementById(e.id).parentElement.parentElement.remove();
			}
		</script>
    
    <script>
        layui.use(['form','layer'], function(){
            $ = layui.jquery;
          var form = layui.form
          ,layer = layui.layer;
        
          //自定义验证规则
          form.verify({
            nikename: function(value){
              if(value.length < 5){
                return '昵称至少得5个字符啊';
              }
            }
            ,pass: [/(.+){6,12}$/, '密码必须6到12位']
            ,repass: function(value){
                if($('#L_pass').val()!=$('#L_repass').val()){
                    return '两次密码不一致';
                }
            }
          });

          //监听提交
          form.on('submit(add)', function(data){
            console.log(data);
            //发异步，把数据提交给php
            layer.alert("增加成功", {icon: 6},function () {
                // 获得frame索引
                var index = parent.layer.getFrameIndex(window.name);
                //关闭当前frame
                parent.layer.close(index);
            });
            return false;
          });
          
          
        });
    </script>
    <script>var _hmt = _hmt || []; (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
      })();</script>
  </body>

</html>