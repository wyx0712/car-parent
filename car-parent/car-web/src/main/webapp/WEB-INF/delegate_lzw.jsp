<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>案件委托</title>

<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="./css/font.css">
<link rel="stylesheet" href="./css/xadmin.css">
<link rel="stylesheet" type="text/css" href="./css/zcity.css">
<script type="text/javascript"
	src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="./lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript" src="./js/xadmin.js"></script>
<script type="text/javascript" src="./js/area.js"></script>
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->

<script type="text/javascript">
	$(function() {

		$("#find").click(function() {
			var insuranceId = $("#insuranceId").val();
			if (insuranceId == "") {
				$("#remind").html("保单号不能为空");
				$("#remind").css("color", "red");
			} else {
				$("#remind").html("");
				$.getJSON("getapplicant.do", {
					"insuranceId" : insuranceId
				}, function(data) {
					if(data.back1 == "报案号不存在"){
						$("#remind").html("报案号不存在");
						$("#remind").css("color", "red");
						$("#reportNo").val("");$("#policyId").val("");
					}else{
						var a= data.back2;
						var b= data.back3
							$("#reportNo").val(a);$("#policyId").val(b);
					}
				});                                   
			}
		})
	})
</script>


</head>
<body>
	<div class="x-body">
		<table class="layui-table">
			<xblock>
			<button class="layui-btn layui-btn-danger">赔案委托信息</button>
			<a href="desktop_lzw.do" class="layui-btn layui-btn-danger">返回我的桌面</a>
			</xblock>
			<form class="layui-form" action="delegateasdg_lzw.do" method="post">
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>保单号</td>
						<td style="width: 400px;">
						<input type="text" id="insuranceId" name="insuranceId" required=""
						class="layui-input"></td>
					<td colspan="4"><input type="button" name="find" id="find"
						value="查询" class="layui-btn layui-btn-radius layui-btn-danger" />&nbsp;&nbsp;<span
						id="remind" style="font-size: large;"></span></td>
				</tr>
					
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>报案号</td>
						<td style="width: 400px;"><input type="text" id="reportNo"
							name="reportNo" required="" class="layui-input" value="" autocomplete="off"/></td>
					

					<td style="width: 110px;"><span class="x-red">*</span>投保人</td>
					<td style="width: 400px;"><input type="text" id="policyId"
						name="policyId" required="" autocomplete="off"
						class="layui-input" value=""></td>
				</tr>
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>委托内容</td>
					<td colspan="5"><input type="checkbox" name="context"
						lay-skin="primary" value="保险事故处理及现场拍照">保险事故处理及现场拍照&nbsp;&nbsp;&nbsp;&nbsp; <input
						type="checkbox" name="context" lay-skin="primary" value="保险事故查勘及出具查勘报告、定损单">保险事故查勘及出具查勘报告、定损单<br />
						<input type="checkbox" name="context" lay-skin="primary" value="确定保险事故损失">确定保险事故损失&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="context" lay-skin="primary" value="车辆盗抢事故协查">车辆盗抢事故协查<br />
						<input type="checkbox" name="context" lay-skin="primary" value="交通事故案情核查">交通事故案情核查&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="context" lay-skin="primary" value="其他证明证物核查">其他证明证物核查<br />
						<input type="checkbox" name="context" lay-skin="primary" value="人员损伤情况调查">人员损伤情况调查&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="context" lay-skin="primary" value="其他委托">其他委托
					</td>
				</tr>
				<td style="width: 110px;"><span class="x-red">*</span>资料处理</td>
				<td colspan="5"><input type="radio" name="num2" checked="checked"
					value="查勘材料由保户带回" />查勘材料由保户带回&emsp;&emsp;&emsp;&nbsp;&nbsp; <input
					type="radio" name="num2" value="查勘材料寄回承保公司" />查勘材料寄回承保公司&emsp;&emsp;&emsp;&nbsp;&nbsp;
					<input type="radio" name="num2" value="通过邮箱发送到承保公司" />通过邮箱发送到承保公司&emsp;&emsp;&emsp;&nbsp;&nbsp;
				</td>
				<tr>
				</tr>
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>委托机构</td>
					<td colspan="5"><select name="surveyorsId">
							<option value="-1">---请选择委托机构---</option>
							<c:forEach  var="carMessage" items="${requestScope.list}" varStatus="status">
								<option value="${carMessage.companyName}" >${carMessage.companyName}</option>
							</c:forEach>
							
					</select></td>
				</tr>
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>委托任务</td>
					<td colspan="5"><input type="checkbox" name="mission"
						lay-skin="primary" value="现场查勘">现场查勘&nbsp;&nbsp; <input
						type="checkbox" name="mission" lay-skin="primary" value="本车定损">本车定损&nbsp;&nbsp;
						<input type="checkbox" name="mission" lay-skin="primary" value="物损定损">物损定损&nbsp;&nbsp;
						<input type="checkbox" name="mission" lay-skin="primary" value="医疗查勘">医疗查勘&nbsp;&nbsp;
						<input type="checkbox" name="mission" lay-skin="primary" value="核价">核价&nbsp;&nbsp;
						<input type="checkbox" name="mission" lay-skin="primary" value="核损">核损&nbsp;&nbsp;
						<input type="checkbox" name="mission" lay-skin="primary" value="立案">立案&nbsp;&nbsp;
						<input type="checkbox" name="mission" lay-skin="primary" value="核赔">核赔&nbsp;&nbsp;
						<input type="checkbox" name="mission" lay-skin="primary" value="预赔">预赔&nbsp;&nbsp;
						<input type="checkbox" name="mission" lay-skin="primary" value="结案">结案&nbsp;&nbsp;
						<input type="checkbox" name="mission" lay-skin="primary" value="支付">支付</td>
				</tr>
				<tr>
					<td colspan="6"><div class="layui-form-item"
							style="margin-left: 200px;">
							<label for="L_repass" class="layui-form-label"> </label>  <input id="dosubmit"
								type="submit" class="layui-btn layui-btn-radius" style="float: right;" value="保存" />
						</div></td>

				</tr>

			</form>
		</table>
	</div>






</body>
</html>