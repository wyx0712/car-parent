<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:if test="${not empty requestScope.dataChooseVO}">
		<c:forEach var="dataChooseVO" items="${requestScope.dataChooseVO}">
			<tr>
				<td>${dataChooseVO.policyId}</td>
				<td>${dataChooseVO.carNo}</td>
				<td>${dataChooseVO.companyName}</td>
				<td>${dataChooseVO.userName}</td>
				<td><fmt:formatDate value="${dataChooseVO.reportTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				<td><fmt:formatDate value="${dataChooseVO.lossTime}"  pattern="yyyy-MM-dd HH:mm:ss"/></td>
			</tr>
		</c:forEach>
	</c:if>
	<script type="text/javascript" src="./js/jquery-1.12.4.js">
		</script>
	<script type="text/javascript">
	
			</script>
</body>
</html>