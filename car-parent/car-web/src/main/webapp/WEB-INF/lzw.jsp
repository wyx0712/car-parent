<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>客服中心</title>

	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <!-- <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" /> -->  
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
	<link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>


</head>
<body>

    <!-- 顶部开始 -->
    <div class="container">
        <div class="logo"><a href="#">平安车险</a></div>
        <div class="left_open">
            <i title="展开左侧栏" class="iconfont">&#xe699;</i>
        </div>
        <iframe width="420" scrolling="no" height="60" frameborder="0" allowtransparency="true" src="//i.tianqi.com/index.php?c=code&id=12&color=%23FFFFFF&icon=1&num=3&site=12"></iframe>
        <ul class="layui-nav right" lay-filter="">
          <li class="layui-nav-item">
            <a href="javascript:;">${sessionScope.user.realName}</a>
            <dl class="layui-nav-child"> <!-- 二级菜单 -->
              <dd><a onclick="x_admin_show('个人信息','http://www.baidu.com')">百度一下</a></dd>
              <dd><a href="loginOut.do">切换帐号</a></dd>
              <dd><a href="loginOut.do">注销</a></dd>
            </dl>
          </li>
          <li class="layui-nav-item to-index"><a href="/"></a></li>
        </ul>
        
    </div>
    <!-- 顶部结束 -->
    <!-- 中部开始 -->
     <!-- 左侧菜单开始 -->
    <div class="left-nav">
      <div id="side-nav">
        <ul id="nav">
        
       <li id="${privilege1.url}">
	                <a _href="javascript:;">
	                    <i class="iconfont">${privilege1.iconclass}</i>
	                    <cite>报案中心</cite>
	                    <i class="iconfont nav_right">&#xe697;</i>
	                </a>
	                <ul class="sub-menu">
	               		 <li>
	                        <a _href="repo_lzw.do">
	                            <i class="iconfont">&#xe6a7;</i>
	                            <cite>报案平台</cite>
	                        </a>
	                    </li >
	                	 <li>
	                        <a _href="delega_lzw.do">
	                            <i class="iconfont">&#xe6a7;</i>
	                            <cite>案件委托</cite>
	                        </a>
	                    </li >
	                	<li>
	                        <a _href="dispatch_lzw.do">
	                            <i class="iconfont">&#xe6a7;</i>
	                            <cite>调度派工</cite>
	                        </a>
	                    </li >
	                </ul>
	            </li>
        </ul>
        
      </div>
    </div>
    <!-- <div class="x-slide_left"></div> -->
    <!-- 左侧菜单结束 -->
    <!-- 右侧主体开始 -->
    <div class="page-content">
        <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
          <ul class="layui-tab-title">
            <li class="home"><i class="layui-icon">&#xe68e;</i>我的桌面</li>
          </ul>
          <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <iframe src='desktop_lzw.do' frameborder="0" scrolling="yes" class="x-iframe" id="desk"></iframe>
            </div>
          </div>
        </div>
    </div>
    <div class="page-content-bg"></div>
    <!-- 右侧主体结束 -->
    <!-- 中部结束 -->
    <!-- 底部开始 -->
    <div class="footer">
        <div class="copyright" align="center">Copyright ©2018 x-admin v2.3 All Rights Reserved</div>  
    </div>
    <!-- 底部结束 -->


</body>
</html>