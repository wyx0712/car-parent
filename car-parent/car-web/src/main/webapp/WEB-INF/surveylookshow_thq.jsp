<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="UTF-8">
    <title>查勘信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
    <div class="x-nav">
      <span class="layui-breadcrumb">
         <a href="survey02_thq.do">返回桌面</a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    <div class="x-body">
      <div class="layui-row">
        <form action="surveylookshows_thq.do" class="layui-form layui-col-md12 x-so">
          <div class="layui-input-inline">
            <select name="surveyType" id="surveyType">
              <option value="1">全部</option>
              <option value="2">报案号</option>

              <option value="3">报案人</option>
            </select>
          </div>
          <input type="hidden" class="pageNo" name="pageNo" />
          <input type="text" name="surveyName" id="surveyName" placeholder="请输入查询内容" autocomplete="off" class="layui-input">
          <button type="submit" class="layui-btn" id="selectSurvey"><i class="layui-icon">&#xe615;</i></button>
        </form>
      </div>
      <xblock>
        <button class="layui-btn" onclick="x_admin_show('添加查勘信息','./demoy.do')"><i class="layui-icon"></i>添加</button>
        <span class="x-right" style="line-height:40px" id="rows"></span>
      </xblock>
      <table class="layui-table">
        <thead>
          <tr>
            <th style="width:8%;">报案号</th>
            <th style="width:8%;">报案人</th>
            <th style="width:10%;">出险原因</th>
            <th style="width:20%;">查勘意见</th>
            <th style="width:29%;">现场图片</th>
			<th style="width:10%;">状态</th>
            
            </tr>
        </thead>
        <tbody>
         <c:if test="${not empty requestScope.surveyInfo}">
         	<c:forEach  var="show" items="${requestScope.surveyInfo}" varStatus="status">
         	<tr>
            		<td>${show.reportNo}</td>
            		<td>${show.reportName}</td>
            		<td>${show.dangerReason}</td>
            		<td>${show.surveyOpinion}</td>
            		<td> <c:if test="${not empty show.imgOne}"><img onclick="show('${show.imgOne}')"  src="${show.imgOne}" /></c:if><c:if test="${not empty show.imgTwo}"><img onclick="show('${show.imgTwo}')"  src="${show.imgTwo}" /></c:if><c:if test="${not empty show.imgThree}"><img onclick="show('${show.imgThree}')"  src="${show.imgThree}" /></c:if></td>
            <td class="td-status">
              <span class="layui-btn layui-btn-normal layui-btn-mini">已提交</span>
            </td>
         		
          </tr>
        </c:forEach>
        	</c:if>
        
         
        </tbody>
      </table>
      <div class="page">
        <div>
        <a class="num1" href="javascript:location.replace(location.href);">首页</a>
          <a class="num2" href="javascript:location.replace(location.href);">上一页</a>
          &nbsp;<span>1/1</span>&nbsp;
          <a class="num3" href="javascript:location.replace(location.href);">下一页</a>
          <a class="num4" href="javascript:location.replace(location.href);">尾页</a>
        </div>
      </div>

    </div>
    <div id="showImg"></div>
    <style>
    	img{
    		width: 60px;
    		height: 56px;
    		padding-right: 30px;
    	}
    </style>
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript">
    
    	$(document).ready(function() {
			init(1,0,null,null);
			
			$("#selectSurvey").click(function(){
				
				var surveyType = $("#surveyType").val(); 
				var surveyName = $("#surveyName").val();
				
				init(1,surveyType,surveyName,null);
			});
		});
    	
    	
    	
    	/*用户-提交*/
        function member_del(obj,id){
            layer.confirm('确认要提交吗？',function(index){
                //发异步提交数据
                var surveyType = $("#surveyType").val(); 
				var surveyName = $("#surveyName").val();
				var pageNO = $(".pageNo").val();
                init(pageNO,surveyType,surveyName,id);
                layer.msg('已提交!',{icon:1,time:1000});
                
            });
        }
    
    </script>
    
    
    <script>
	    function show (obj) {
	    	var $img = $("<img src='"+obj+"'/>");
	    	$img.css({"width":"60%","height":"80%","position":"absolute","left":"20%","top":"10%"});
	    	$("#showImg").append($img);
	    	$img.click(function(){
	    		$(this).remove();
	    	});
	    }
    </script>	
    
    <script>
      layui.use('laydate', function(){
        var laydate = layui.laydate;
        
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });
	
	
	 
    </script>
    <script>var _hmt = _hmt || []; (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
      })();</script>
  </body>

</html>