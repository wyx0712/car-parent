<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>欢迎页面-X-admin2.0</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="./css/font.css">
<link rel="stylesheet" href="./css/xadmin.css">
<script type="text/javascript"
	src="./js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="./lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript" src="./js/xadmin.js"></script>
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<div class="x-body">
	<h2 style="text-align: center">日志详情</h2>
		<form class="layui-form" >
			<div class="layui-form-item">
				<label for="username" class="layui-form-label"> <span
					class="x-red">*</span>日志编号
				</label>
				<div class="layui-input-inline">
					<input type="text" id="logId" name="logId" required="" readonly="readonly"
						 autocomplete="off" value="${operateLog.logId}"
						class="layui-input">
				</div>
				
			</div>
			<div class="layui-form-item">
				<label for="opName" class="layui-form-label"> <span
					class="x-red">*</span>操作人
				</label>
				<div class="layui-input-inline">
					<input type="text"  id="opName" name="opName" required="" readonly="readonly"  autocomplete="off"
						class="layui-input" value="${operateLog.opName}">
				</div>
				
			</div>
			<div class="layui-form-item">
				<label for="operation" class="layui-form-label"> <span
					class="x-red">*</span>操作
				</label>
				<div class="layui-input-inline">
					<input type="text"  id="operation" name="operation" readonly="readonly"
						required=""  autocomplete="off"
						class="layui-input" value="${operateLog.operation}">
				</div>
			</div>
			
			
			<div class="layui-form-item">
				<label for="createDate" class="layui-form-label"> <span
					class="x-red">*</span>操作时间
				</label>
				<div class="layui-input-inline">
					<input type="text" id="createDate" name="createDate" required="" readonly="readonly"
						 autocomplete="off" class="layui-input" value=" <fmt:formatDate type='both' dateStyle='medium' timeStyle='medium' value='${operateLog.createDate}' />">
				</div>
			</div>
			<div class="layui-form-item">
				<label for="content" class="layui-form-label"> <span
					class="x-red">*</span>具体内容
				</label>
				<div class="layui-input-inline">
					<textarea  readonly="readonly" style="width: 1000px; height: 150px">
					${operateLog.content}
					</textarea>
					
				</div>
			</div>
		</form>
	</div>
	
</body>

</html>