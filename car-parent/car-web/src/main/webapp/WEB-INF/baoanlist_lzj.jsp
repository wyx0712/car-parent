<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%> 
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  
  <head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <link rel="stylesheet" type="text/css" href="../lib/layui/css/layui.css"/>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
  </head>
  
  <body>
   
    <div class="baoanxinxi">
						<table  class="layui-table"  lay-even lay-skin="row" lay-size="lg">
							<tr ><td colspan="6"width="900px"><h1 align="center" >报案受理信息</h1></td></tr>
							
							<tr>
								<td width="100px" align="center"> 报案号：</td>
								<td  >${reportInfos.reportNo}</td>
							
								<td width="100px" align="center" > 报案人：</td>
								<td >${reportInfos.reportName}</td>
								<td width="100px" align="center"> 报案人电话：</td>
								<td >${reportInfos.reporTel}</td>
							</tr>
							<tr>
								<td width="100px" align="center"> 驾驶人姓名：</td>
								<td >${reportInfos.driverName}</td>
								<td width="100px" align="center" > 驾驶人电话：</td>
								<td >${reportInfos.driverTel}</td>
								<td  width="100px" align="center"> 出险时间：</td>
								<td ><fmt:formatDate value="${reportInfos.dangerTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							</tr>
						
							<tr>
								<td width="100px"align="center"> 出险地点：</td>
								<td colspan="5" width="750px">${reportInfos.dangerAddress}
								</td>
							</tr>

							<tr>
								<td width="100px" align="center"> 出险原因：</td>
								<td colspan="5" width="750px">${reportInfo.dangerReason}</td>
							</tr>
							<tr>
								<td  width="100px"  align="center"> 损失类型：</td>
								<td colspan="5" width="750px">
								
								${reportInfos.num1}
								
								</td>
							</tr>
							
							<tr>
								<td  width="100px"align="center" > 出险区域：</td>
								<td colspan="5" width="750px">
								${reportInfos.num2}
									</tr>
							<tr>
								<td  width="100px" align="center"> 接案人：</td>
								<td colspan="2" width="300px">${reportInfos.serviceName}</td>
								<td  width="100px" align="center"> 勘测人：</td>
								<td colspan="2" width="300px">${reportInfos.surveyorsName}</td>
							</tr>
							<tr>
								<td>报案状态：</td>
								<c:if test="${reportInfos.reportState==1}">
								<td colspan="2" width="300px">报案中</td>
								</c:if>
								<c:if test="${reportInfos.reportState==2}">
								<td colspan="2" width="300px">勘测中</td>
								</c:if>
								<c:if test="${reportInfos.reportState==5}">
								<td colspan="2" width="300px">核赔中</td>
								</c:if>
								<c:if test="${reportInfos.reportState==8}">
								<td colspan="2" width="300px">垫付申请</td>
								</c:if>
								<c:if test="${reportInfos.reportState==10}">
								<td colspan="2" width="300px">垫付通过</td>
								</c:if>
								<td width="100px" align="center">核赔人：</td>
								<td colspan="2" width="300px">${reportInfos.surveyorsName}</td>
							</tr>
							<tr>
								<td>赔偿金：</td>
								<td colspan="2" width="300px">${reportInfos.compensate}</td>
								<td width="100px" align="center">垫付人：</td>
								<td colspan="2" width="300px">${reportInfos.payName}</td>
							</tr>
						</table>


				</div>
			</div>
		</div>
		</div>
    	
    </div>
    
    
    
    
    	
    	
     
    <script type="text/javascript">
    	
    </script>
    <script>var _hmt = _hmt || []; (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
      })();</script>
  </body>

</html>