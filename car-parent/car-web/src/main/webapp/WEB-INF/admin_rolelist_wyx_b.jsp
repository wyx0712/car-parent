<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="UTF-8">
    <title>欢迎页面</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="./js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    <div class="x-body">
    
      <xblock>
        <button class="layui-btn" onclick="x_admin_show('添加角色','admin_roleadd_wyx.do')"><i class="layui-icon"></i>添加</button>
        <span class="x-right" style="line-height:40px" id ="count" ></span>
      </xblock>
      <table class="layui-table">
        <thead>
          <tr>
            <th>角色编号</th>
            <th>角色名</th>
            <th>角色职责</th>
            <th>操作</th>
        </thead>
        <tbody>
          
        </tbody>
      </table>
       <div class="pages" style="text-align: center">
        <div>
        	<a class="first"><button class="layui-btn layui-btn-normal">首页</button></a>
          	<a class="pre"><button class="layui-btn layui-btn-normal">上一页</button></a>
          	&nbsp;&nbsp;<a class="layout" ></a>&nbsp;&nbsp;
          	<a class="next" ><button class="layui-btn layui-btn-normal">下一页</button></a>
          	<a class="last"><button class="layui-btn layui-btn-normal">尾页</button></a>
        </div>
      </div>

    </div>
    <script type="text/javascript">
    $(function(){
		//初始化值
		init(1);
	}); 
    
    function init(pageNo){
    	$.getJSON(
    		"admin_rolelist_b_wyxA.do",
    		{"pageNo":pageNo},
    		function(page){
    			$("tbody>tr").remove();
    			$(page.list).each(function(index,obj){
    				var tr = "<tr>"
    	            +"<td>"+obj.roleId+"</td>"
    	            +"<td>"+obj.roleName+"</td>"
    	            +"<td>"+obj.back2+"</td>"
    	            +"<td class=\"td-manage\">"
    	            +"<a title=\"恢复\" onclick=\"member_del(this,"+obj.roleId+")\" href=\"javascript:;\"><i class=\"layui-icon layui-icon-release\">&#xe609;</i></a>"  
    	            +"</td>"  
    	            +"</tr>"  
    	              
    	         $("tbody").append(tr);
    			});
    			
    			//分页
    			$("#count").html("共有数据："+page.total+"条");
				$(".layout").html(page.pageNum+"/"+page.pages);
				$(".first").attr("href","javascript:init(1)");
				$(".last").attr("href","javascript:init("+page.pages+")");
				if(pageNo==1){
					$(".pre").removeAttr('href');
				}else{
					$(".pre").attr("href","javascript:init("+(pageNo-1)+")");
				}
				if(pageNo==page.pages){
					$(".next").removeAttr('href');
				}else{
					$(".next").attr("href","javascript:init("+(pageNo+1)+")");
				}
    		}
    	);	
    };
    
    </script>
    
    <script>
      layui.use('laydate', function(){
        var laydate = layui.laydate;
        
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });

      /*恢复角色*/
      function member_del(obj,id){
          layer.confirm('确认要恢复吗？',function(index){
              //发异步删除数据
              $.getJSON(
        		"admin_resrole_b_wyxA.do",
        		{"roleId":id},
        		function(i){
        			if(i>0){
        				 $(obj).parents("tr").remove();
        				 layer.msg('已恢复!',{icon:1,time:3000});
        				 
        			}else{
        				layer.msg('操作有误!',{icon:5,time:3000});
        				
        			}
        		}
           );//ajax方法结束
          });
      }



      function delAll (argument) {

        var data = tableCheck.getData();
  
        layer.confirm('确认要删除吗？'+data,function(index){
            //捕捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
      }
    </script>
  </body>

</html>