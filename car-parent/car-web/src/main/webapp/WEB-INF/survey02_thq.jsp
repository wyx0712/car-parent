<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      
 <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="./css/font.css">
        <link rel="stylesheet" href="./css/xadmin.css">
        <link rel="stylesheet" href="./css/layui.css">
        <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
        <script type="text/javascript"  src="js/echarts.min.js"></script>
        <script type="text/javascript">
      	
      	function showNumber(){
      	  $.ajax({
              url : "shownumber.do",
              type : "POST",
              dataType:"json",
            
              
              data :{},
              success:function(result) {
            	  $("#number01").html(result.num1);
            	  $("#number02").html(result.num2);
            	  $("#number03").html(result.num3);
            	  $("#number04").html(result.num4);
            	  $("#number05").html(result.num5);
            	  $("#number06").html(result.num6);
            	  $("#number07").html(result.num7);
              },
              error:function(result){
                
              }
          });

      
      	}
      	
     $(function(){
    	 showNumber();
     });
      
      
      
      
      
      
        function showTime(){
            nowtime=new Date();
            year=nowtime.getFullYear();
            month=nowtime.getMonth()+1;
            date=nowtime.getDate();
            document.getElementById("mytime").innerText=year+"年"+month+"月"+date+" "+nowtime.toLocaleTimeString();
        }

        setInterval("showTime()",1000);
        setInterval("showNumber()",10000);
        </script>
        
        <style type="text/css">
        .layui-col-xs4 {
	width: 19.999999%
}
	.sss44{
	width: 49.999999%
	}
        </style>
        
</head>
<body>
 <div class="x-body layui-anim layui-anim-up">
 <blockquote class="layui-elem-quote">
			欢迎<span class="x-red">${sessionScope.user.realName}！</span>
			<span id="mytime"></span>
		</blockquote>
<fieldset class="layui-elem-field">
            <legend>待办工作</legend>
            <div class="layui-field-box">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body">
                            <div class="layui-carousel x-admin-carousel x-admin-backlog" lay-anim="" lay-indicator="inside" lay-arrow="none" style="width: 100%; height: 90px;">
                                <div carousel-item="">
                                    <ul class="layui-row layui-col-space10 layui-this">
                                        <li class="layui-col-xs4">
                                            <a href="surveyshow_thq.do"  class="x-admin-backlog-body">
                                                <h3>案件调查</h3>
                                                <p>
                                                    <cite id="number01"></cite></p>
                                            </a>
                                        </li>
                                        <li class="layui-col-xs4">
                                            <a href="surveylookshow_thq.do" class="x-admin-backlog-body">
                                                <h3>现场查勘</h3>
                                                <p>
                                                    <cite id="number02"></cite></p>
                                            </a>
                                        </li>
                                        <li class="layui-col-xs4">
                                            <a href="surveylossone_thq.do" class="x-admin-backlog-body">
                                                <h3>本车定损</h3>
                                                <p>
                                                    <cite id="number03"></cite></p>
                                            </a>
                                        </li>
                                         <li class="layui-col-xs4">
                                            <a href="surveylosstwo_thq.do" class="x-admin-backlog-body">
                                                <h3>三车定损</h3>
                                                <p>
                                                    <cite id="number04"></cite></p>
                                            </a>
                                        </li>
                                         <li class="layui-col-xs4">
                                            <a href="surveypeople_thq.do" class="x-admin-backlog-body">
                                                <h3>人伤调查</h3>
                                                <p>
                                                    <cite id="number05"></cite></p>
                                            </a>
                                        </li>                                    
                                    </ul>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        
        <fieldset class="layui-elem-field">
            <legend>待审核案件</legend>
            <div class="layui-field-box">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body">
                            <div class="layui-carousel x-admin-carousel x-admin-backlog" lay-anim="" lay-indicator="inside" lay-arrow="none" style="width: 100%; height: 90px;">
                                <div carousel-item="">
                                    <ul class="layui-row layui-col-space10 layui-this">
                                       
                                         <li class="layui-col-xs4 sss44">
                                            <a href="surveyshowti_thq.do" class="x-admin-backlog-body">
                                                <h3>已提交待审核案件</h3>
                                                <p><cite id="number06"></cite></p>
                                            </a>
                                        </li>
                                        <li class="layui-col-xs4 sss44">
                                            <a href="surveyshowfu_thq.do" class="x-admin-backlog-body">
                                                <h3>未通过被退回案件</h3>
                                                <p><cite id="number07"></cite></p>
                                            </a>
                                        </li>
                                    </ul>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        
        </div>
</body>
</html>