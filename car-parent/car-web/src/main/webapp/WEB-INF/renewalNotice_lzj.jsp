<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>车险续期交费服务通知单</title>
    
<meta charset="{CHARSET}">
		<title></title>
		<style type="text/css">
			p {
				margin-top: 8px;
			}
		
			
			.totle21{
				margin-top: 30px;
				margin-bottom: 30px;
			}
			.jiaofeidan{
				width: 760px;
				background-color: #EAEAEA;
				margin: 0 auto;
				padding-left: 8%;
				line-height: 30px;
			}
			.totle22{
				font-size: 30px;
				font-family: "黑体";
				margin-left: 26%;
			}
			.totle23{
				margin-left: 34%;
			}
			.totle25{
				margin-left: 28%;
			}
			.totle26{
				margin-left: 68%;
			}
			.totle27{
				margin-left: 66%;
			}
			.totle29{
				margin-top: 30px;
				margin-bottom: 30px;
				font-family: "微软雅黑";
				font-weight: bold;
			}
		</style>
</head>
<body>
 <form id="form_xuqi" action="dayinxufeitongzhidan.do" method="post">
   <button class="layui-btn  layui-btn-radius layui-btn-warm"   id="btn-submit" onclick="xuqi()"><i class="layui-icon"> &#xe63c;</i>导入到Word打印</button>
<div class="jiaofeidan">
			<p class="totle22" style="color: red">中国平安车险有限公司</p>
		<p class="totle23">车险续期交费服务通知单</p> 
		
		<tr>尊敬的&nbsp;&nbsp;&nbsp;&nbsp;先生/女士：
			</br>
			<td class="totle21">&nbsp;&nbsp;您为了家庭幸福在本公司为______________投保的保单号码为___________________的车险，</td>
			</br>
			<td class="totle21">车险费为￥_________,现已到第______次交费时间，今有我公司专职保全员前往为您服务，为避</td>
			</br>
			<td class="totle21">免您因事繁忙忘记缴费，造成保单失效带来不必要的损失，请您于____年____月____日至____日</td>
			</br>
			<td class="totle29"><b>持本通知单到任一农业银行储蓄所(代收费窗口)交纳保险费，同时索取交费收据。</b></td>
			</br>
			<td class="totle21">&nbsp;&nbsp;□咨询服务：如有咨询事项，请于专职展员________联系，以便您提供及时的保全服务。</td>
			</br>
			<td class="totle21">&nbsp;&nbsp;□服务电话：总部：027-66668888&nbsp;&nbsp;&nbsp;&nbsp;地址：珞喻路特1号阳光大厦群楼二楼柜面中心</td>
			</br>
			&nbsp;&nbsp;&nbsp;&nbsp;区部：________________手机:________________传呼：________________
		<p>感谢您对我们工作的支持与合作。</p>
		<p class="totle25"></p>
		<p class="totle26">中国平安车险有限公司</p>
		<p class="totle27">年&nbsp;&nbsp;&nbsp;&nbsp;月&nbsp;&nbsp;&nbsp;&nbsp;日</p>
		</tr>		
		
	</div>
</body>


<script type="text/javascript">
function xuqi() {
    $("#form_xuqi").submit();
}

</script>
</html>