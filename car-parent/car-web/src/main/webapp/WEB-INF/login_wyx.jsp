﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>后台登录-X-admin2.0</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
	<link rel="stylesheet" href="./css/xadmin.css">
	<link rel="stylesheet" href="./css/jigsaw.css">
    <script type="text/javascript" src="./js/jquery-1.12.4.js"></script>
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <script type="text/javascript" src="./js/jigsaw_local.js"></script>
	  
</head>
<body class="login-bg">
    
    <div class="login layui-anim layui-anim-up" style="margin-top: 40px;">
        <div class="message">管理登录</div>
        <p style ="height: 6px;color:red;margin:0px,auto" ><label >${error}</label></p>
        <div id="darkbannerwrap"></div>
        
        <form method="post" action="doLogin_wyx.do" class="layui-form" >
        	
            <input name="userName" id="name" placeholder="用户名"  type="text" lay-verify="required" class="layui-input" >
             <p style ="height: 6px" ><label class="p1"></label></p>
            <hr class="hr15">
            <input name="password" id="password" lay-verify="required" placeholder="密码"  type="password" class="layui-input">
             <p style ="height: 6px" ><label class="p2"></label></p>
            <hr class="hr15">
            
            <input type="hidden" id="slidBlock" name="slidBlock" />
	  			<div>
	  			<div id="captcha" style="position: relative"></div>
	  			<div id="msg"></div>
           		</div>
           		<hr class="hr20" >
            <input value="登录" id="sub" lay-submit lay-filter="login" style="width:100%;" type="submit">
        </form>
    </div>

	
	<script type="text/javascript">
	$(function(){
		
		$("#name").blur(function() {
			reg = /^[a-zA-Z0-9_]{4,16}$/;
			var name = $("#name").val();
			if(name == "") {
				$(".p1").html("账号不能为空");
				$(".p1").css("color", "#7CCD7C");
				return false;
			} else if(!reg.test(name)) {
				$(".p1").html("格式有误!4到16位（字母，数字，下划线）");
				$(".p1").css("color", "#7CCD7C");
				return false;
			} else {
				$(".p1").html("");
			}
		})
		
		$("#password").blur(function() {
					var n= $("#password").val();
					//中文、英文但不包括下划线和数字等符号
						reg = /^(\w){4,20}$/; 
					if(n == "") {
						$(".p2").html("密码不能为空");
						$(".p2").css("color", "#7CCD7C");
						return false;
					}else if(n.length < 4 || n.length > 20) {
						$(".p2").html("长度必须在4-20位之间!");
						$(".p2").css("color", "#7CCD7C");
						return false;
					}else if(!reg.test(n)){
						$(".p2").html("只能输入4-20个字母、数字、下划线  !");
						$(".p2").css("color", "#7CCD7C");
						return false;
					}else {
						$(".p2").html("");
					}
				})
				
		$("#sub").click(function(){
			
			reg = /^[a-zA-Z0-9_]{4,16}$/;
			var name = $("#name").val();
			if(name == "") {
				$(".p1").html("账号不能为空");
				$(".p1").css("color", "#7CCD7C");
				return false;
			} else if(!reg.test(name)) {
				$(".p1").html("格式有误!4到16位（字母，数字，下划线）");
				$(".p1").css("color", "#7CCD7Cd");
				return false;
			} else {
				$(".p1").html("");
			}
			
			
			var n= $("#password").val();
			//中文、英文但不包括下划线和数字等符号
				reg = /^(\w){4,20}$/; 
			if(n == "") {
				$(".p2").html("密码不能为空");
				$(".p2").css("color", "#7CCD7C");
				return false;
			}else if(n.length < 4 || n.length > 20) {
				$(".p2").html("长度必须在4-20位之间!");
				$(".p2").css("color", "#7CCD7C");
				return false;
			}else if(!reg.test(n)){
				$(".p2").html("只能输入4-20个字母、数字、下划线  !");
				$(".p2").css("color", "#7CCD7C");
				return false;
			}else {
				$(".p2").html("");
			}
		})
		
	})
	
			
	
	</script>
		
	
	
	<script type="text/javascript">
  			jigsaw.init(document.getElementById('captcha'), function () {
    		document.getElementById('slidBlock').value="go";
 			 })
	</script>
	 		  
   <!--  <script>
    //百度统计可去掉
    var _hmt = _hmt || [];
    (function() {
      var hm = document.createElement("script");
      hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
      var s = document.getElementsByTagName("script")[0]; 
      s.parentNode.insertBefore(hm, s);
    })();
    </script> -->
</body>
</html>