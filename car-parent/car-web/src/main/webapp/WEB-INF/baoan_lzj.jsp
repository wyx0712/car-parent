<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>报案信息</title>
        <meta name="renderer" content="webkit">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   		<meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
   	    <link rel="stylesheet" href="./css/font.css">
   	    <link rel="stylesheet" href="./css/xadmin.css">
        <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="./js/xadmin.js"></script>
   			
   			
	   
   			
    </head>
    <body class="layui-anim layui-anim-up">
    	 <div class="x-nav">
   
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="baoan.do" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>

    <div class="x-body">
    
      <div class="layui-row">
    
       	 
       	 <div class="layui-row">
       	 
        <form class="layui-form layui-col-md12 x-so" method="post">
           <input type="text" name="reportNo"  id="xiayao1" placeholder="请输入报案号" value="${reportNo}" autocomplete="off" class="layui-input">
          <button type="submit" class="layui-btn"  lay-submit="" id="xiayao2" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
      </form>
     </form>
      <button class="layui-btn  layui-btn-radius layui-btn-warm"   onclick="javascript:beforeSubmit1()"><i class="layui-icon"> &#xe63c;</i>打印数据</button>
           <form id="form_login123" action="/car-web/dayinbaoanxinxi.do" method="post">
	 </form>
     </div>
      </div>
      
    
      <form id="form_getExcel" action="get/excel.html" method="post">
	 </form>
       
     
        <span class="x-right" style="line-height:40px" id ="count" >共有数据：${reportInfo.total} 条</span>
  	 
  	  <table class="layui-table"  >
        <thead>
          <tr>
            
            	
            <th>报案号</th>
            <th>保单号</th>
            <th>报案人</th>
            <th>报案人电话</th>
            <th>查勘人</th>
            <th>报案时间</th>
            <th>报案状态</th>
            <th>核赔人</th>
            <th>详情</th>
        </thead>
        <tbody>
          <c:if test="${not empty reportInfo.list}">
         	<c:forEach  var="reportInfo" items="${reportInfo.list}">
          <tr id="tt">
           
            <td>${reportInfo.reportNo}</td>
            <td>${reportInfo.policyId}</td>
            <td>${reportInfo.reportName}</td>
            <td>${reportInfo.reporTel}</td>
             <td>${reportInfo.surveyId}</td>
       <td><fmt:formatDate value="${reportInfo.reportTime}" pattern="yyyy-MM-dd" /></td> 
            <td>${reportInfo.reportState}</td>
            <td>${reportInfo.serviceId}</td>
         
          
            
            
            
             
            <td class="td-manage">
              
              </a>
             
              <a  title="详情" href="BAxianging.do?id=${reportInfo.reportNo}">
                <i class="layui-icon">&#xe631;</i>
              </a>
              
            </td>
          </tr>
         </c:forEach>
        </c:if>
        </tbody>
      </table>
  	 
      
      <div class="page">
        <div>
        <a class="no1" href="/car-web/baoan.do?pageNo=${reportInfo.pages-reportInfo.pages+1}" style="background-color: #189F92;color: white; border-radius: 3px">首页</a>
          <a class="first" href="/car-web/baoan.do?pageNo=${reportInfo.prePage}" style="background-color: #189F92;color: white; border-radius: 3px">上一页</a>

          <a class="layout" >${reportInfo.pageNum}/${reportInfo.pages}</a>
          <a class="next" href="/car-web/baoan.do?pageNo=${reportInfo.nextPage}" style="background-color: #189F92;color: white; border-radius: 3px">下一页</a>
          <a class="last" href="/car-web/baoan.do?pageNo=${reportInfo.pages}" style="background-color: #189F92;color: white; border-radius: 3px">末页</a>
        </div>
      </div>
    </div>
    
 
   
  </body>
<script type="text/javascript">
function beforeSubmit1(){
    $("#form_login123").submit();
}

</script>

</html>