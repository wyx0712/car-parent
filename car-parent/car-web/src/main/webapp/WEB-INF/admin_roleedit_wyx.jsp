<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>欢迎页面-X-admin2.0</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="./css/font.css">
<link rel="stylesheet" href="./css/xadmin.css">
<script type="text/javascript"
	src="./js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="./lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript" src="./js/xadmin.js"></script>
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<div class="x-body">
		<form class="layui-form" action="#" method="post">
			<div class="layui-form-item">
				<label for="phone" class="layui-form-label"> <span
					class="x-red">*</span>角色名
				</label>
				<div class="layui-input-inline">
					<input type="text" id="type" name="type"  value="管理员"
						required="" lay-verify="type" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label for="L_email" class="layui-form-label"> <span
					class="x-red">*</span>角色code
				</label>
				<div class="layui-input-inline">
					<input type="text"  id="parent" value= "admin"
						name="parent" required="" lay-verify="parent" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label"><span class="x-red">*</span>角色描述</label>
				<div class="layui-input-inline">
					<input type="text"  id="name" value="拥有系统所有权限"
						name="name" required="" lay-verify="name" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			
			<div >
				<label for="L_repass" class="layui-form-label"> </label>
				<input id ="sub" class="layui-btn" lay-filter="add" type="button" value="确定"/>	
			</div>
		</form>
	</div>
	<script>
	$(function(){
		$("#sub").click(function(){
			/* var pkPrivilege = $("#pkPrivilege").val(); */
			var type = $("#type").val();
			var parent = $("#parent").val();
			var url = $("#url").val();
			var name = $("#name").val();
			$.getJSON(
		    		"admin_rule_adds.do",
		    		{"type":type,"parent":parent,"url":url,"name":name},
		    		function(i){
		    			if(i>0){
		       				 layer.msg('增加成功!',{icon:1,time:2000});
		       				window.parent.location.reload(); //刷新父页面
		       				var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
		       				parent.layer.close(index);
		       				 
		       			}else{
		       				layer.msg('操作有误!',{icon:5,time:1000});
		       			}	
		    		}
		  	 ); 		

			
		}
	)
})
	
    </script>
	<script>var _hmt = _hmt || []; (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
      })();</script>
</body>

</html>