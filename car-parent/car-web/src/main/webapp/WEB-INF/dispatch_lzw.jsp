<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>调度派工</title>

<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="./css/font.css">
<link rel="stylesheet" href="./css/xadmin.css">
<script type="text/javascript"
	src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="./lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript" src="./js/xadmin.js"></script>
<script src="./js/xcity.js" type="text/javascript" charset="utf-8"></script>
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<script type="text/javascript">
	$(function() {

		$("#find").click(function() {
			var insuranceId = $("#insuranceId").val();
			if (insuranceId == "") {
				$("#remind").html("保单号不能为空");
				$("#remind").css("color", "red");
			} else {
				$("#remind").html("");
				$.getJSON("getappli.do", {
					"insuranceId" : insuranceId
				}, function(data) {
					if(data.back1 == "报案号不存在"){
						$("#remind").html("报案号不存在");
						$("#remind").css("color", "red");
						$("#reportNo").val("");$("#policyId").val("");$("#address").val("");
					}else{
						var a= data.back2;
						var b= data.back3;
						var c= data.back1;
						$("#reportNo").val(a);
						$("#policyId").val(b);
						$("#address").val(c);
					}
				});                                   
			}
		})
	})
</script>


</head>
<body>
		
	<div class="x-body">
		<table class="layui-table">
			<xblock>
			<button class="layui-btn layui-btn-danger">赔案派工信息</button>
			<a href="desktop_lzw.do" class="layui-btn layui-btn-danger">返回我的桌面</a>
			</xblock>
			<form class="layui-form" action="dispatchdsga_lzw.do" method="post">
			<tr>
					<td style="width: 110px;"><span class="x-red">*</span>保单号</td>
						<td style="width: 400px;">
						<input type="text" id="insuranceId" name="insuranceId" required=""
						class="layui-input"></td>
					<td colspan="4"><input type="button" name="find" id="find"
						value="查询" class="layui-btn layui-btn-radius layui-btn-danger" />&nbsp;&nbsp;<span
						id="remind" style="font-size: large;"></span></td>
				</tr>
					
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>报案号</td>
						<td style="width: 400px;"><input type="text" id="reportNo"
							name="reportNo" required="" class="layui-input" value="" autocomplete="off"/></td>
					

					<td style="width: 110px;"><span class="x-red">*</span>投保人</td>
					<td style="width: 400px;"><input type="text" id="policyId"
						name="policyId" required="" autocomplete="off"
						class="layui-input" value=""></td>
				</tr>
				<tr>
					<td><span class="x-red">*</span>出险地点</td>

					<td colspan="5"><input type="text" class="layui-input" required="" autocomplete="off"  value=""
						 id="address" name="address"></td>
				</tr>
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>公司</td>
					<td style="width: 400px;">
					<input type="text" id="num3"
						name="num3" required="" lay-verify="email" autocomplete="off"
						class="layui-input" value="平安车险有限公司"/></td>
					<td style="width: 110px;"><span class="x-red">*</span>查勘人</td>
					<td colspan="3"><select name="surveyorsId">
							<option value="-1">---请选择查勘人---</option>
							<c:forEach  var="carMessage" items="${requestScope.list}" varStatus="status">
								<option value="${carMessage.realName}" >${carMessage.realName}</option>
							</c:forEach>
					</select></td>
				</tr>

				<tr>
					<td colspan="6"><input type="submit" style="float: right;"
						class="layui-btn layui-btn-radius"  value="保存"></input>
					</td>
				</tr>

			</form>
		</table>
	</div>

	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>



</body>
</html>