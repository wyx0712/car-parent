<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<title>欢迎页面-X-admin2.0</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" href="./css/font.css">
		<link rel="stylesheet" href="./css/xadmin.css">
		<script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
		<script type="text/javascript" src="./js/xadmin.js"></script>
		<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
		<!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>

	<body>
		<div class="x-body">
			<form class="layui-form" id="form1" action="doloss.do" method="post">

				<div class="layui-form-item layui-form-text">

					<div class="layui-input-block">
						<table class="layui-table">
							<tbody>
								
								<tr>
									<td align="right">待定损报案号：</td>
									<td>
										<select id="reportNo" name="reportNo" style="width: 56%;">
										<c:if test="${not empty  carMessage}" >
											
												<c:forEach  var="carMessage" items="${requestScope.carMessage}" varStatus="status">
													<option value="${carMessage.reportNo}" >${carMessage.reportNo}</option>
												</c:forEach>
												
											</c:if>
										
												
													<option value="${lossinfo.reportNo}" >${lossinfo.reportNo}</option>
											
											
										</select>
									</td>
									
									<td>车牌号：</td>
									<td><input type="text" name="carNo" id="" value="${lossinfo.carNo }" class="layui-input" /></td>
									
								</tr>
								<tr>
									<td>修理厂名称：</td>
									<td>
										<input type="text" name="repairShop" id="a" value="${lossinfo.repairShop }" class="layui-input" />
									</td>
									<td>修理厂类型：</td>
									<td>
										<select name="repairType" id="">
											<c:if test="${lossinfo.repairType == '一类修理厂' }">
												<option value="一类修理厂" selected="selected">一类修理厂</option>
											</c:if>
											<c:if test="${lossinfo.repairType != '一类修理厂' || lossinfo.repairType == null }">
												<option value="一类修理厂">一类修理厂</option>
											</c:if>
											<c:if test="${lossinfo.repairType == '二类修理厂' }">
												<option value="二类修理厂" selected="selected">二类修理厂</option>
											</c:if>
											<c:if test="${lossinfo.repairType != '二类修理厂' || lossinfo.repairType == null }">
												<option value="二类修理厂">二类修理厂</option>
											</c:if>
											<c:if test="${lossinfo.repairType == '三类修理厂' }">
												<option value="三类修理厂" selected="selected">三类修理厂</option>
											</c:if>
											<c:if test="${lossinfo.repairType != '三类修理厂' || lossinfo.repairType == null }">
												<option value="三类修理厂">三类修理厂</option>
											</c:if>
										</select>
									</td>
									<td>车主：</td>
									<td><input type="text" name="carOwner" id="" value="${lossinfo.carOwner }" class="layui-input" /></td>
								</tr>
								<tr>
									<td colspan="6" style="height: 20px;background-color: rgb(200,238,238);"></td>
								</tr>
								<tr>
									<td colspan="6">
										<table id="parts">
											<tr>
												<td>配件名称</td>
												<td>数量</td>
												<td>单价</td>
												<td>总定价</td>
												<td>报价</td>
												<td>操作</td>
												<td rowspan="100"><input type="button" value="添 加" id="add" class="layui-btn"/></td>
											</tr>
											<c:if test="${empty carnotoparts}">
												<tr>
													<td><input type="text" name="partsName" id="" class="layui-input" /></td>													
													<td><input type="text" name="partsCount" id="" class="layui-input" /></td>
													<td><input type="text" name="lossFee" id="" class="layui-input" /></td>
													<td><input type="text" name="lossMoney" id="" class="layui-input" /></td>
													<td><input type="text" name="lossPrices" id="" class="layui-input" /></td>
													
													<td><input type='button' value='删 除' id='del0' onclick='del(this)' class='layui-btn' style='background-color: rgb(255 87 34);'/></td>
												</tr>
											</c:if>
											<c:if test="${not empty carnotoparts }">
												<c:forEach items="${carnotoparts }" var="carnotoparts">
													<tr>
														<td><input type="text" name="partsName" id="" value="${carnotoparts.partsName }" class="layui-input" /></td>													
														<td><input type="text" name="partsCount" id="" value="${carnotoparts.partsCount }" class="layui-input" /></td>
														<td><input type="text" name="lossFee" id="" value="${carnotoparts.lossFee }" class="layui-input" /></td>
														<td><input type="text" name="lossMoney" id="" value="${carnotoparts.lossMoney }" class="layui-input" /></td>
														<td><input type="text" name="lossPrices" id="" value="${carnotoparts.lossPrices }" class="layui-input" /></td>
														<td><input type='button' value='删 除' id='del0' onclick='del(this)' class='layui-btn' style='background-color: rgb(255 87 34);'/></td>
													</tr>
												</c:forEach>
											</c:if>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="6" style="height: 20px;background-color: rgb(200,238,238);"></td>
								</tr>
						
								<tr>
									<td>总报损价：</td>
									<td><input type="text" name="carMoney" id="" value="${lossinfo.carMoney }" class="layui-input" /></td>
									<td>报损人：</td>
									<td><input type="text" name="userName" id="" value="${lossinfo.userName }" class="layui-input" /></td>
								
								</tr>
								<tr>
									<td>报损意见：</td>
									<td colspan="5"><textarea name=lossOpinion id="" style="width: 70%; height: 100px;">${lossinfo.lossOpinion }</textarea></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label">
              </label>
              		<input type="hidden" name="operation" value="${operation }" />
              		
					<input type="submit" class="layui-btn"  value="保 存" />
				</div>
			</form>
		</div>
		<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
		<script>
			$(document).ready(function() {
				$("input[name='partsName']").blur(function() {
					var a = $(this).val();
					if(a == "") {
						$("input[name='partsId']").attr("disabled", "disabled");
						$("input[name='partsCount']").attr("disabled", "disabled");
						$("input[name='repairPrices']").attr("disabled", "disabled");
						$("input[name='lossPrices']").attr("disabled", "disabled");
					} else {
						$("input[name='partsId']").removeAttr("disabled");
						$("input[name='partsCount']").removeAttr("disabled");
						$("input[name='repairPrices']").removeAttr("disabled");
						$("input[name='lossPrices']").removeAttr("disabled");
					}

				});
//				看来只能用js了
//				$("input[value='删 除']").click(function(){
//					alert("aa");
//					$(this).parent().parent().remove();
//				});
				var i = 1;
				//真是痛苦啊
				$("#add").click(function(){
						var $tr = $("<tr><td><input type='text' name='partsName' id='' class='layui-input' /></td>"+													
								"<td><input type='text' name='partsCount' id='' class='layui-input' /></td>"+
								"<td><input type='text' name='lossFee' id='' class='layui-input' /></td>"+
								"<td><input type='text' name='lossMoney' id='' class='layui-input' /></td>"+
								"<td><input type='text' name='lossPrices' id='' class='layui-input' /></td><td>"
						+"<input type='button' value='删 除' id='del"+i+"' onclick='del(this)' class='layui-btn' style='background-color: rgb(255 87 34);'/></td></tr>");
						
						$("#parts tr:last-child").after($tr);
						i++;
				});
				
				
			})
		</script>
		<script>
			//终于找到你了
			function del (e) {
				document.getElementById(e.id).parentElement.parentElement.remove();
			}
		</script>
		
		<script>
			layui.use(['form', 'layer'], function() {
				$ = layui.jquery;
				var form = layui.form,
					layer = layui.layer;

				//自定义验证规则
				form.verify({
					nikename: function(value) {
						if(value.length < 5) {
							return '昵称至少得5个字符啊';
						}
					},
					pass: [/(.+){6,12}$/, '密码必须6到12位'],
					repass: function(value) {
						if($('#L_pass').val() != $('#L_repass').val()) {
							return '两次密码不一致';
						}
					}
				});

				//监听提交
				/* form.on('submit(add)', function(data) {
					console.log(data);
					//发异步，把数据提交给php
					layer.alert("增加成功", {
						icon: 6
					}, function() {
						
						// 获得frame索引
						var index = parent.layer.getFrameIndex(window.name);
						//关闭当前frame
						parent.layer.close(index);
					});
					return false;
				}); */

			});
		</script>
		<script>
			var _hmt = _hmt || [];
			(function() {
				var hm = document.createElement("script");
				hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
				var s = document.getElementsByTagName("script")[0];
				s.parentNode.insertBefore(hm, s);
			})();
		</script>
	</body>

</html>