<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    
</head>
 <body class="layui-anim layui-anim-up">
    
    
    <div class="x-body">
      <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so">
          <input type="text" name="username"  placeholder="最后操作人" autocomplete="off" class="layui-input">
          <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
        </form>
      </div>
       <form id="form_login" action="get/excel.html" method="post">
	 </form>
       <button class="layui-btn layui-btn-primary"   id="btn-submit" onclick="beforeSubmit()" ><i class="layui-icon">&#xe642;</i>打印数据</button>
        <span class="x-right" style="line-height:40px" id="count" > </span>
      <table class="layui-table"  id="table222">
        <thead>
          <tr>
          	<th>创建时间</th>
            <th>保单号</th>
            <th>用户名</th>
            <th>手机号</th>
            <th>地址</th>
            <th>险种</th>
            <th>到期时间</th>
            <th>最后操作人</th>
            <th>操作</th>
          </tr>
        </thead>
        <tbody>
         
        </tbody>
      </table>
      
      
      <div class="page">
        <div>
          <a class="prev"  >&lt;&lt;</a>
          <span class="current" >1</span>
          <a class="num">2</a>
          <a class="num">3</a>
          <a class="next" >&gt;&gt;</a>
        </div>
      </div>


    </div>
    
     <script>
    	$(function(){
    		//初始化值
    		init(1);
    	});
    	
    	//转换时间
    	function add0(m){return m<10?'0'+m:m }
    	//时间戳转化成时间格式
    	function timeFormat(timestamp){
    	  //timestamp是整数，否则要parseInt转换,不会出现少个0的情况
    	    var time = new Date(timestamp);
    	    var year = time.getFullYear();
    	    var month = time.getMonth()+1;
    	    var date = time.getDate();
    	    var hours = time.getHours();
    	    var minutes = time.getMinutes();
    	    var seconds = time.getSeconds();
    	    return year+'-'+add0(month)+'-'+add0(date)+' '+add0(hours)+':'+add0(minutes)+':'+add0(seconds);
    	}
    	
    	//初始化从后台调用数据 传入的参数是第几页
    	function init(pageNo){
    		$.getJSON(
    				"manage_member_xh.do",
    				{"pageNo":pageNo},
    				function (page){
    					//1.  删除  所有的  过时数据 ： 除去第一个 和 最后一个  所有  tr	
    					$("table tr:not(:first)").not(":last").remove();
    					
    					$(page.list).each(function(index,obj){

    						//将数据 写道到  创建tr 中 
    						var tr ="<tr>"
    							+"<td>"+timeFormat(obj.createTime)+"</td>"
    							+"<td>"+obj.policyId+"</td>"
    							+"<td>"+obj.policyHolder+"</td>"
    							+"<td>"+obj.phoneNoA+"</td>"
    							+"<td>"+obj.adreessA+"</td>"
    							+"<td>"+obj.insuranceId+"</td>"
    							+"<td>"+timeFormat(obj.endDate)+"</td>"
    							+"<td>"+obj.loadId+"</td>"
    							+"<td class='td-manage'>"
    							+"<button class='layui-btn layui-btn layui-btn-xs'  onclick=\"x_admin_show('编辑','menber_list_b.html?id="+obj.policyId+"')\">"
    							+"<i class='layui-icon'>&#xe642;</i>详情</button></td>"
    							+"</tr>";
    								
    							$("tbody").after(tr);
    						
    					});//循环
    					//分页
    					$(".prev").attr("href","javascript:init("+(pageNo-1)+")");
    					$(".next").attr("href","javascript:init("+(pageNo+1)+")");
    					$("#count").html("共有数据：2条")
    				}//回调函数方法
    		
    		);//ajax方法
    		
    	};
    	//
    	 var loginForm = document.getElementById('form_login');
		    function beforeSubmit() {
		        loginForm.submit();
		    }
    </script>
  </body>

</html>