<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
       
 <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="./css/font.css">
        <link rel="stylesheet" href="./css/xadmin.css">
        <link rel="stylesheet" href="./css/layui.css">
        <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
        <script type="text/javascript"  src="js/echarts.min.js"></script>
        <script type="text/javascript">
        function showTime(){
            nowtime=new Date();
            year=nowtime.getFullYear();
            month=nowtime.getMonth()+1;
            date=nowtime.getDate();
            document.getElementById("mytime").innerText=year+"年"+month+"月"+date+" "+nowtime.toLocaleTimeString();
        }

        setInterval("showTime()",1000);

        </script>
        
        <style type="text/css">
        .layui-col-xs4 {
	width: 50%
}
       .layui-col-xs5 {
	width: 25%
}

        </style>
        
</head>
<body>
 <div class="x-body layui-anim layui-anim-up">
 <blockquote class="layui-elem-quote">
			欢迎<span class="x-red">${sessionScope.user.realName}！</span>
			<span id="mytime"></span>
		</blockquote>
<fieldset class="layui-elem-field">
            <legend>内勤中心</legend>
            <div class="layui-field-box">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body">
                            <div class="layui-carousel x-admin-carousel x-admin-backlog" lay-anim="" lay-indicator="inside" lay-arrow="none" style="width: 100%; height: 90px;">
                                <div carousel-item="">
                                    <ul class="layui-row layui-col-space10 layui-this">
                                        <li class="layui-col-xs4">
                                            <a href="huiyuanguanli222.do" class="x-admin-backlog-body">
                                                <h3>会员管理</h3>
                                                <p>
                                                    <cite>${num1}</cite></p>
                                            </a>
                                        </li>
                                        <li class="layui-col-xs4">
                                            <a href="xufeitongzhidan.do" class="x-admin-backlog-body">
                                                <h3>续费通知单</h3>
                                                <p>
                                                    <cite>${num2}</cite></p>
                                            </a>
                                        </li>
                                       
                                         
                                    </ul>
                                </div>
                                
                                
                                
                                
                                
                                 
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        
        <fieldset class="layui-elem-field">
            <legend>综合管理</legend>
            <div class="layui-field-box">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body">
                            <div class="layui-carousel x-admin-carousel x-admin-backlog" lay-anim="" lay-indicator="inside" lay-arrow="none" style="width: 100%; height: 90px;">
                                <div carousel-item="">
                                    <ul class="layui-row layui-col-space10 layui-this">
                                        <li class="layui-col-xs5">
                                            <a href="baoan.do" class="x-admin-backlog-body">
                                                <h3>报案信息</h3>
                                                <p>
                                                    <cite>${num1}</cite></p>
                                            </a>
                                        </li>
                                        <li class="layui-col-xs5">
                                            <a href="renyuanxinxi.do" class="x-admin-backlog-body">
                                                <h3>人员信息</h3>
                                                <p>
                                                    <cite>${num2}</cite></p>
                                            </a>
                                        </li>
                                        <li class="layui-col-xs5">
                                            <a href="cheliangdingsunxinxi.do" class="x-admin-backlog-body">
                                                <h3>车辆定损信息</h3>
                                                <p>
                                                    <cite>${num3}</cite></p>
                                            </a>
                                        </li>
                                          <li class="layui-col-xs5">
                                            <a href="huishouzhan.do" class="x-admin-backlog-body">
                                                <h3>回收站</h3>
                                                <p>
                                                    <cite>${num4}</cite></p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                
                                
                                
                                
                                
                                 
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        </div>
</body>
</html>