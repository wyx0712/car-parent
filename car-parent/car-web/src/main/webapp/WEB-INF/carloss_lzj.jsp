<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
  	
  	<script type="text/javascript" src="jq/jquery-1.12.4.min.js"></script>
  	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
  	
	


</head>
<body class="layui-anim layui-anim-up">



    <div class="x-nav">
    
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="cheliangdingsunxinxi.do" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    
    <div class="x-body">
      <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" method="post">
           <input type="text" name="carNo"  id="xiayao1" placeholder="请输入车牌号" value="${carNo}" autocomplete="off" class="layui-input">
          <button type="submit" class="layui-btn"  lay-submit="" id="xiayao2" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
         <span class="x-right" style="line-height:40px" id ="count" >共有数据：${lossinfo.total} 条</span>
        </form>
    </form>
        <button class="layui-btn  layui-btn-radius layui-btn-warm"   onclick="javascript:beforeSubmit1()"><i class="layui-icon"> &#xe63c;</i>打印数据</button>
           <form id="form_login123" action="/car-web/dayinchesunxinxi.do" method="post">
	 </form>
      </div>
     
      
      <table class="layui-table"  >
        <thead>
          <tr>
            
           
            <th>报案号</th>
            <th>车主</th>
            <th>车牌号</th>
            <th>定损总价</th>
            <th>定损员</th>
            <th>定损时间</th>
           
            <th>详情</th></tr>
        </thead>
        <tbody>
          <c:if test="${not empty lossinfo.list}">
         	<c:forEach  var="lossinfo" items="${lossinfo.list}">
          <tr id="tt">
           
            <td>${lossinfo.reportNo}</td>
            <td>${lossinfo.carOwner}</td>
            <td>${lossinfo.carNo}</td>
            <td>${lossinfo.carMoney}</td>
            <td>${lossinfo.userName}</td>
          <td><fmt:formatDate value="${lossinfo.lossTime}" pattern="yyyy-MM-dd" /></td>
          
            
            
            
             
            <td class="td-manage">
              
              </a>
             
              <a onclick="" title="详情" href="CarDSxiangqing.do?id=${lossinfo.lossId}">
                <i class="layui-icon">&#xe631;</i>
              </a>
              
            </td>
          </tr>
         </c:forEach>
        </c:if>
        </tbody>
      </table>
      
      
      <div class="page">
        <div>
            <a class="no1" style="background-color: #189F92;color: white; border-radius: 3px" href="/car-web/cheliangdingsunxinxi.do?pageNo=${lossinfo.pages-lossinfo.pages+1}">首页</a>
          <a class="first" style="background-color: #189F92;color: white; border-radius: 3px" href="/car-web/cheliangdingsunxinxi.do?pageNo=${lossinfo.prePage}">上一页</a>

          <a class="layout" >${lossinfo.pageNum}/${lossinfo.pages}</a>
          <a class="next" style="background-color: #189F92;color: white; border-radius: 3px" href="/car-web/cheliangdingsunxinxi.do?pageNo=${lossinfo.nextPage}">下一页</a>
          <a class="last" style="background-color: #189F92;color: white; border-radius: 3px" href="/car-web/cheliangdingsunxinxi.do?pageNo=${lossinfo.pages}">末页</a>
        </div>
      </div>



    </div>
    
    
   
  </body>
<script type="text/javascript">
function beforeSubmit1(){
    $("#form_login123").submit();
}

</script>


</html>