<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
		<title>投诉反馈</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" href="./css/font.css">
		<link rel="stylesheet" href="./css/xadmin.css">
		<script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
		<script type="text/javascript" src="./js/xadmin.js"></script>
		<script src="./js/xcity.js" type="text/javascript" charset="utf-8"></script>
</head>
	<body>

		<div class="x-body">
			<table class="layui-table">
				<xblock>
					<button class="layui-btn layui-btn-danger">客户信息</button>
				</xblock>
				<form class="layui-form" action="userxiugaibaocun.do" method="post">
					<tr>
						<td style="width: 110px;"><span class="x-red">*</span>投保人</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="applicant" required="" lay-verify="email" autocomplete="off" class="layui-input" value="${policy.applicant}"></td>
						<td style="width: 110px;"><span class="x-red">*</span>投保人身份证号</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="aIdcard" required="" lay-verify="email" autocomplete="off" class="layui-input" value="${policy.aIdcard}"></td>
						<td style="width: 110px;"><span class="x-red">*</span>投保人手机号</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="aPhone" required="" lay-verify="email" autocomplete="off" class="layui-input" value="${policy.aPhone}"></td>
					</tr>
					<tr>
						<td style="width: 100px;">受益人</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="beneficiary" required="" lay-verify="email" autocomplete="off" class="layui-input" value="${policy.beneficiary}"></td>
						<td style="width: 100px;">受益人身份证号</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="bIdcard" required="" lay-verify="email" autocomplete="off" class="layui-input" value="${policy.bIdcard}"></td>
						<td style="width: 100px;"><span class="x-red">*</span>受益人手机号</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="bPhone" required="" lay-verify="email" autocomplete="off" class="layui-input" value="${policy.bPhone}"></td>
					</tr>
					<tr>
						<td style="width: 100px;">保费</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="email" required="" lay-verify="email" autocomplete="off" class="layui-input" value="${policy.insuredAmount}" readonly="readonly"></td>
						<td style="width: 100px;">保额</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="email" required="" lay-verify="email" autocomplete="off" class="layui-input" value="${policy.compensateAmount}" readonly="readonly"></td>
						<td style="width: 100px;">险种</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="email" required="" lay-verify="email" autocomplete="off" class="layui-input" value="${policy.insuranceId}" readonly="readonly"></td>
						
						</tr>
					<tr>
						<td style="width: 100px;">保单号</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="policyId" required=""  autocomplete="off" class="layui-input" value="${policy.policyId}" readonly="readonly"></td>
						<td style="width: 100px;">生效时间</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="email" required="" lay-verify="email" autocomplete="off" class="layui-input"
						 value="${startDate1}" readonly="readonly"></td>
						<td style="width: 100px;">结束时间</td>
						<td style="width: 400px;"> <input type="text" id="L_email" name="email" required="" lay-verify="email" autocomplete="off" class="layui-input" value="${endDate1}" readonly="readonly"></td>
						
					</tr>
					
				
					
					
					

					<tr>
						<td ><a _href="www.baidu.com">  <input class="layui-btn layui-btn-danger" type="submit" value="保存" /></a></td>

						</tr>
				</form>
			</table>
		</div>
		
	</body>

</html>

