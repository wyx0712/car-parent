<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="./css/font.css">
<link rel="stylesheet" href="./css/xadmin.css">
<link rel="stylesheet" type="text/css" href="./css/zcity.css">
<script type="text/javascript"
	src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="./lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript" src="./js/xadmin.js"></script>
<script type="text/javascript" src="./js/area.js"></script>
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->

<script type="text/javascript">
		
	$(function(){
		$("#reporTel").blur(function(){
					var reg = /^[1][3,4,5,7,8][0-9]{9}$/;
					var driverTel = $("#reporTel").val();
					
					if(!reg.test(driverTel)) {
						alert("手机号码有误，请重填！");
						return false;
					}
					
				})
	})
		
		</script>
<script type="text/javascript">
	$(function() {

		$("#find").click(function() {
			var carNo = $("#carNo").val();
			if (carNo == "") {
				$("#remind").html("车牌号不能为空");
				$("#remind").css("color", "red");
			} else {
				$("#remind").html("");
				$.getJSON("getPolicy.do", {
					"carNo" : carNo
				}, function(data) {
					var info = data.back1;
					var no = data.back2;
					if (info == "保单有效!") {
						$("#remind").html(info);
						$("#remind").css("color", "green");
						$("#policyId").val(no);
					} else {
						$("#remind").html(info);
						$("#remind").css("color", "red");
						$("#policyId").val("");
					}

				});
			}
		})

		//校验CheckBox,radio
		$("#dosubmit").click(function() {

			if ($("input:radio[name=num2]").is(':checked')) {
				if ($("input:checkbox[name=insurance]").is(':checked')) {
					return true;
				} else {
					alert("请选择'损失类型'");
					return false;
				}
			} else {
				alert("请选择'出险区域'");
				return false;
			}
		})
	})
</script>

<title>报案平台</title>
</head>
<body>


<body onload="setup();">
	<div class="x-body">
		<table class="layui-table">
			<xblock>
			<button class="layui-btn layui-btn-danger">报案受理信息</button>
			<a href="desktop_lzw.do" class="layui-btn layui-btn-danger">返回我的桌面</a>
			</xblock>
			<form class="layui-form" action="reporghjt_lzw.do" method="post">
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>车牌号</td>
					<td><input type="text" id="carNo" name="carNo" required=""
						class="layui-input"></td>
					<td colspan="4"><input type="button" name="find" id="find"
						value="查询" class="layui-btn layui-btn-radius layui-btn-danger" />&nbsp;&nbsp;<span
						id="remind" style="font-size: large;"></span></td>
				</tr>
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>保单号</td>

					<td style="width: 400px;"><input type="text" id="policyId"
						name="policyId" required="" autocomplete="off"
						class="layui-input" value=""></td>
					<td style="width: 110px;"><span class="x-red">*</span>报案人</td>
					<td style="width: 400px;"><input type="text" id="reportName"
						name="reportName" required="" lay-verify="email"
						onchange="select(id)" autocomplete="off" class="layui-input"
						value=""></td>
					<td style="width: 100px;"><span class="x-red">*</span>报案人电话</td>
					<td style="width: 400px;"><input type="text" id="reporTel"
						for="phone" onblur="need1()" name="reporTel" required=""
						lay-verify="email" autocomplete="off" class="layui-input" value=""></td>
				</tr>
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>出险时间</td>
					<td style="width: 400px;"><input type="date" id="dangerTime"
						name="dangerTime" required="" lay-verify="email"
						autocomplete="off" class="layui-input" value=""></td>
					<td style="width: 110px;"><span class="x-red">*</span>驾驶人姓名</td>
					<td style="width: 400px;"><input type="text" id="driverName"
						name="driverName" required="" lay-verify="email"
						autocomplete="off" class="layui-input" value=""></td>
					<td style="width: 110px;"><span class="x-red">*</span>驾驶人电话</td>
					<td style="width: 400px;"><input type="text" id="driverTel"
						for="phone" onblur="need2()" name="driverTel" required=""
						lay-verify="email" autocomplete="off" class="layui-input" value=""></td>

				</tr>
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>出险区域</td>
					<td colspan="5"><input type="radio" name="num2" id="radioo"
						value="市内" />市内&emsp;&emsp;&emsp;&nbsp;&nbsp; <input type="radio"
						name="num2" id="radioo" value="省内" />省内&emsp;&emsp;&emsp;&nbsp;&nbsp;
						<input type="radio" name="num2" id="radioo" value="省外" />省外&emsp;&emsp;&emsp;&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td style="width: 110px;"><span class="x-red">*</span>出险地点</td>
					<td colspan="2"><select class="select" name="province" id="s1">
							<option>请选择省</option>
					</select></td>
					<td><select class="select" name="city" id="s2">
							<option>请选择市</option>
					</select></td>
					<td colspan="2"><select class="select" name="town" id="s3"
						onchange="getTown();">
							<option>请选择区县</option>
					</select> <input id="town1" name="town1" type="hidden" value="" /> <input
						id="address" name="address" type="hidden" value="" /></td>
				</tr>
				<tr>
					<td style="width: 110px;">出险原因</td>
					<td colspan="5"><textarea name="dangerReason"
							id="dangerReason" rows="4" cols="130"></textarea></td>
				</tr>
				<tr>
					<td style="width: 110px;">备注</td>
					<td colspan="5"><textarea name="comment" rows="4" cols="160"></textarea></td>
				</tr>

				<tr>
					<td style="width: 100px;">接案人</td>
					<td colspan="3"><input type="hidden" name="serviceId"
						autocomplete="off" class="layui-input"
						value="${sessionScope.user.realName}"> <input
						type="text" required="" name="serviceName" autocomplete="off"
						class="layui-input" value="${sessionScope.user.realName}"></td>
					<td colspan="2"><input type="submit" style="float: right;"
						class="layui-btn layui-btn-radius"  value="保存"> </td>
						
						
						
				</tr>
			</form>
		</table>
	</div>

	<script type="text/javascript">
		//这个函数是必须的，因为在area.js里每次更改地址时会调用此函数
		function promptinfo() {
			var address = document.getElementById('address');
			var s1 = document.getElementById('s1');
			var s2 = document.getElementById('s2');
			var town1 = document.getElementById('town1');
			if (s1.value != "请选择省" && s2.value == '请选择市') {//若只选择了第一项，则地址就为某某省
				address.value = s1.value;
			}
			if (s1.value != "请选择省" && s2.value != '请选择市') {//若只选择了前两项省和市，则地址就只显示"省|市"
				address.value = s1.value + "|" + s2.value;
			}
			if (s1.value == "请选择省") {//若什么都不选，则隐藏域地址为空串
				address.value = "";
			}
		}
		//获取区县的值（若不写这个触发函数，一直获取不到第三项区县的值，一直是默认值“请选择区县”）
		function getTown() {
			var address = document.getElementById('address');
			var s1 = document.getElementById('s1').value
			var s2 = document.getElementById('s2').value;
			var s3 = document.getElementById('s3').value;
			var town1 = document.getElementById('town1').value;
			town1 = s3;
			if (town1 == "请选择区县") {
				address.value = s1 + "|" + s2;
			} else {
				address.value = s1 + "|" + s2 + "|" + town1;
			}

		}
	</script>
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
	<script type="text/javascript" src="./js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="./js/zcity.js"></script>

	<script type="text/javascript">
		zcityrun('.zcityGroup');
	</script>
</body>







</body>
</html>