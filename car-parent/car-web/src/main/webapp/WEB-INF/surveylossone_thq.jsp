<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%> 
<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="UTF-8">
    <title>本车定损</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
    <div class="x-nav">
      <span class="layui-breadcrumb">
       <a href="survey02_thq.do">返回桌面</a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    <div class="x-body">
      <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so">
          <div class="layui-input-inline">
            <select name="lossType" id="lossType">
              <option value="0">全部</option>
              <option value="1">车牌号</option>
              <option value="2">车主</option>
              <option value="3">模糊查询</option>
            </select>
          </div>
          <input type="hidden" class="pageNo" name="pageNo" />
          <input type="text" name="lossName" id="lossName"  placeholder="请输入查询内容" autocomplete="off" class="layui-input">
          <button type="button" class="layui-btn" id="selectLoss"><i class="layui-icon">&#xe615;</i></button>
        </form>
      </div>
      <xblock>
        <button class="layui-btn" onclick="x_admin_show('添加本车定损','addownloss.do')"><i class="layui-icon"></i>添加</button>
        <span class="x-right" style="line-height:40px" id="rows"></span>
      </xblock>
      <table class="layui-table">
        <thead>
          <tr>
            <th style="width:20%;">车牌号码</th>
            <th style="width:10%;">车主姓名</th>
            <th style="width:10%;">定损价格</th>
            <th style="width:25%;">定损意见</th>
			<th style="width:15%;">状态</th>
            <th style="width:20%;">操作</th>
            </tr>
        </thead>
        <tbody>
        <c:if test="${not empty requestScope.lossinfo}">
         	<c:forEach  var="lossinfo" items="${requestScope.lossinfo}" varStatus="status">
          <tr>
            <td>${lossinfo.carNo}</td>
            <td>${lossinfo.carOwner}</td>
            <td>${lossinfo.carMoney}</td>
            <td>${lossinfo.lossOpinion}</td>
            <td class="td-status">
              <span class="layui-btn layui-btn-normal layui-btn-mini">已提交</span></td>
            <td>
				<button class="layui-btn layui-btn layui-btn-xs"  onclick="x_admin_show('编辑','addownloss.do?surveyId=${lossinfo.reportNo}')" ><i class="layui-icon">&#xe642;</i>编辑</button>
			</td>
          </tr>
            </c:forEach>
        	</c:if>
        </tbody>
      </table>
      <div class="page">
        <div>
          <a class="num1" href="javascript:location.replace(location.href);">首页</a>
          <a class="num2" href="javascript:location.replace(location.href);">上一页</a>
          &nbsp;<span>1/1</span>&nbsp;
          <a class="num3" href="javascript:location.replace(location.href);">下一页</a>
          <a class="num4" href="javascript:location.replace(location.href);">尾页</a>
        </div>
      </div>

    </div>
    
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript">
	    $(document).ready(function() {
	    	
	    	init(1,null,null,null);
	    	
			$("#selectLoss").click(function(){
				
				var lossType = $("#lossType").val(); 
				var lossName = $("#lossName").val();
				
				init(1,lossType,lossName,null);
			});
	    });
	    
	    function init(pageNo,lossType,lossName,id) {
			
	    	$.ajax({
	    		"url":"doLossShow.do",
	    		"data":{"pageNo":pageNo,"lossType":lossType,"lossName":lossName,"id":id},
	    		"dataType":"json",
	    		"success":function(result){
	    			
	    			if(pageNo < 1){
						pageNo = 1;
					}else if(pageNo > result.page.pageTotal){
						pageNo = result.page.pageTotal;
					}
	    			
	    			$("tbody tr").remove();
	    			
	    			$.each(result.page.list, function(index,lossinfo){
	    				if (this.state==3) {
	    					var $tr = $("<tr><td>"+this.carNo+"</td><td>"+this.carOwner+"</td><td>"+this.loadId+"</td><td>"
			    					+this.lossOpinion+"</td><td class='td-status'><span class='layui-btn layui-btn-normal layui-btn-mini'>"+this.state+"</span></td>"
			    					+"<td><button class='layui-btn layui-btn layui-btn-xs'  onclick=\"x_admin_show('编辑','addownloss.html?id="+this.num2+"')\" ><i class='layui-icon'>&#xe642;</i>编辑</button>"
			    					+"<button class='layui-btn-danger layui-btn layui-btn-xs'  onclick=\"member_del(this,"+this.num2+")\" href='javascript:;' ><i class='layui-icon'>&#xe70a;</i>提交</button>"
			    					+"<button class='layui-btn-danger layui-btn layui-btn-xs' id='ziliao' onclick=\"x_admin_show('资料','dataAcquisition2.html?reportNo="+this.num2+"')\" href='javascript:;' ><i class='layui-icon'>&#xe70a;</i>资料</button></td></tr>"
			    					);
						}else{
							var $tr = $("<tr><td>"+this.carNo+"</td><td>"+this.carOwner+"</td><td>"+this.loadId+"</td><td>"
			    					+this.lossOpinion+"</td><td class='td-status'><span class='layui-btn layui-btn-normal layui-btn-mini'>"+this.state+"</span></td>"
			    					+"<td><button class='layui-btn layui-btn layui-btn-xs'  onclick=\"x_admin_show('编辑','addownloss.html?id="+this.num2+"')\" ><i class='layui-icon'>&#xe642;</i>编辑</button>"
			    					+"<button class='layui-btn-danger layui-btn layui-btn-xs'  onclick=\"member_del(this,"+this.num2+")\" href='javascript:;' ><i class='layui-icon'>&#xe70a;</i>提交</button>"
			    					+"<button class='layui-btn-danger layui-btn layui-btn-xs' id='ziliao' onclick=\"x_admin_show('资料','dataAcquisition.html?reportNo="+this.num2+"')\" href='javascript:;' ><i class='layui-icon'>&#xe70a;</i>资料</button></td></tr>"
			    					);
						}
		    			
		    			
		    			$("tbody").append($tr);
	    			});
	    			
		    		$("#rows").html("共有数据："+result.page.rows+"条");
		    		$(".pageNo").val(result.page.pageNo);
		    		
		    		//防止下面的页数跳转时第三个位置什么都没有，像是空字符，导致ajax不能与后台异步交互
		    		if(lossName == ""){
		    			lossName = null;
		    		}
		    		
		    		$(".num1").attr("href","javascript:init(1,"+lossType+","+lossName+",null)");
					$(".num2").attr("href","javascript:init("+(pageNo-1)+","+lossType+","+lossName+",null)");
					$(".num3").attr("href","javascript:init("+(pageNo+1)+","+lossType+","+lossName+",null)");
					$(".num4").attr("href","javascript:init("+result.page.pageTotal+","+lossType+","+lossName+",null)");
		    		
					
					//前端真他妈的坑啊！！！玩不起
			    	var x = document.getElementsByClassName("layui-btn-normal");
			    	
			    	for (var i = 0; i < x.length; i++) {
						if(x[i].innerHTML == 1){
							x[i].innerHTML = "未提交";
							x[i].style.backgroundColor = '#1E9FFF';
						}
						if(x[i].innerHTML == 2){
							x[i].innerHTML = "待审核";
							x[i].style.backgroundColor = '#3BCB48';
						}
						if(x[i].innerHTML == 3){
							x[i].innerHTML = "不通过";
							x[i].style.backgroundColor = '#FFE801';
						}
						if(x[i].innerHTML == 4){
							x[i].innerHTML = "已完成";
							x[i].style.backgroundColor = '#D1C6C6';
						}
					}
	    		}
	    		
	    	});
	    	
		}
	    
	    /*用户-提交*/
        function member_del(obj,id){
            layer.confirm('确认要提交吗？',function(index){
                //发异步提交数据
                var lossType = $("#lossType").val(); 
				var lossName = $("#lossName").val();
				var pageNO = $(".pageNo").val();
                init(pageNO,lossType,lossName,id);
                layer.msg('已提交!',{icon:1,time:1000});
                
            });
        }
    </script>
    
    
    
    
    <script>
      layui.use('laydate', function(){
        var laydate = layui.laydate;
        
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });
	
    </script>
    <script>var _hmt = _hmt || []; (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
      })();</script>
  </body>

</html>
