<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>回收站</title>
        <meta name="renderer" content="webkit">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   		<meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
   	    <link rel="stylesheet" href="./css/font.css">
   	    <link rel="stylesheet" href="./css/xadmin.css">
        <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="./js/xadmin.js"></script>
    </head>
    <body>
    	
 	<div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">管理员管理</a>
        <a>
          <cite>回收站</cite></a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px"></i>
        <i class="layui-icon">&#xe9aa;</i>  
      </a>
    </div>
    <div class="x-body">
     
      <table class="layui-table layui-form" id="table111">
      	
        <button class="layui-btn"  onclick="x_admin_show('信息','admin_adminlist_wyx_b.do')" >
		  <i class="layui-icon">&#xe702;</i>   用户回收信息
		</button>
       			
       <button class="layui-btn"  onclick="x_admin_show('信息','admin_rolelist_wyx_b.do')" >
		  <i class="layui-icon">&#xe702;</i>   角色回收信息
	   </button> 
	   
	   <button class="layui-btn"  onclick="x_admin_show('信息','admin_loglist_wyx_b.do')" >
		  <i class="layui-icon">&#xe702;</i>   日志回收信息
	   </button> 
       
       <!--
       	图
       -->
           <div class="x-body">
           
            <div id="main" style="width: 100%;height:400px;"></div>
            
        </div>
        <script type="text/javascript" src="./js/echarts.min.js" charset="utf-8"></script>
        <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));

        // 指定图表的配置项和数据
        var option = {
        	    title: {
        	        text: '数据删除情况堆叠'
        	    },
        	    tooltip: {
        	        trigger: 'axis'
        	    },
        	    legend: {
        	        data:['用户回收信息','角色回收信息','日志回收信息']
        	    },
        	    grid: {
        	        left: '3%',
        	        right: '4%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    toolbox: {
        	        feature: {
        	            saveAsImage: {}
        	        }
        	    },
        	    xAxis: {
        	        type: 'category',
        	        boundaryGap: false,
        	        data: ['周一','周二','周三','周四','周五','周六','周日']
        	    },
        	    yAxis: {
        	        type: 'value'
        	    },
        	    series: [
        	        {
        	            name:'用户回收信息',
        	            type:'line',
        	            stack: '总量',
        	            data:[0, 2, 0, 0, 5, 0, 1]
        	        },
        	        {
        	            name:'角色回收信息',
        	            type:'line',
        	            stack: '总量',
        	            data:[0, 0, 1, 0, 0, 0, 0]
        	        },
        	        {
        	            name:'日志回收信息',
        	            type:'line',
        	            stack: '总量',
        	            data:[1, 7, 3, 2, 3, 4, 1]
        	        },
        	        
        	    ]
        	};




        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        </script>
       <!--<thead>
          <tr>
            <th width="20">
              <div class="layui-unselect header layui-form-checkbox" lay-skin="primary"><i class="layui-icon">&#xe605;</i></div>
            </th>
            <th width="70">ID</th>
            <th>XX表</th>
            <th width="50">操作人</th>
            <th width="80">删除时间</th>
            <th width="120">操作</th>
        </thead>-->
        <!--<tbody class="x-cate">
          <tr cate-id='1' fid='0' >
            <td>
              <div class="layui-unselect layui-form-checkbox" lay-skin="primary" data-id='2'><i class="layui-icon">&#xe609;</i></div>
            </td>
            <td>1</td>
            <td>
							XX表X条数据被删除
            </td>
            <td>张安</td>
            <td>
							2018-10-09
            </td>
            <td class="td-manage">
              <button class="layui-btn-danger layui-btn layui-btn-xs"  onclick="member_del(this,'要删除的id')" href="javascript:;" ><i class="layui-icon">&#xe609;</i>恢复</button>
            </td>
          </tr>
         
        </tbody>-->      </table>
    </div>
    <style type="text/css">
      
    </style>
    <script>
      layui.use(['form'], function(){
        form = layui.form;
        
      });

      /* /**
       * AJAX测试
       */
	/* function abc(){
		
		$("#table111").load("audit_return.jsp");	
		
		}; */ 
	
      
  </body>

</html>