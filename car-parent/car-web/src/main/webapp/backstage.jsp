<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta charset="UTF-8">
		<title>核赔页面</title>
		<link rel="stylesheet" type="text/css" href="./css/containers.css" />
		<link rel="stylesheet" type="text/css" href="./css/logins.css" />
		<style>

		</style>
	</head>

	<body>
		<div class="zhuti">
			<div class="loginId">
				<ul>
					<li>
						<a href="#">
							<h3 class="disSize">${sessionScope.user.realName}</h3>
						</a>
						<ul>
							<li class="zhendepi">
								<a href="#">百度一下</a>
							</li>
							<li class="zhendepi">
								<a href="loginOut.do">切换账号</a>
							</li>
							<li class="zhendepi">
								<a href="loginOut.do">注销</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="container">
				<img src="img/xxcx.png" class="imsk" />
			</div>

			<div id="menu">
				<ul>

					<li>
						<a href="#" class="hepeiz">核赔中心</a>
						<ul>
							<li>
								<a class="hepeizx">核赔中心</a>
							</li>
							<li>
								<a href="#">核价中心</a>
							</li>
						</ul>
					</li>
					<!--<li>
					<a href="#">垫付申请审核中心</a>
					<ul>
						<li>
							<a href="#">客户申请垫付中心</a>
						</li>
						<li>
							<a href="#">垫付审核处理中心</a>
						</li>
					</ul>
				</li>-->
					<span id="pipixia"> <img src="img/hepei.gif" />
				</span>
				</ul>
			</div>
			<div class="container" style="background: -webkit-linear-gradient(#45494C, black);">
				<a style="text-align: center; color: white;">www.皮皮友情赞助.com</a>
			</div>
			<!--主题操作栏-->
			<div class="subject">
				<!--菜单级别栏-->
				<div class="well">
					&nbsp;&nbsp;
					<a style="text-align: left; color: black; font-size: 20px; font-family: '黑体';">工作页面</a>
				</div>
				<!--欢迎栏-->
				<div class="come">
					<span class="less">welcome ${sessionScope.user.realName}! <span
					id="mytime"></span>
					</p>
					</span>
				</div>
				<div class="PendingWork">
					<p>代办工作</p>
					1,拿首胜</br> 2,不做舔狗
				</div>
				<div class="BulletinBoard">
					<p>公告栏</p>
					1,RNG喜获8强！ 2,ig痛失亚军！
				</div>
			</div>
			<!--核赔中心界面-->
			<div class="hepei">
				<div class="shuju">
					<span>数据统计</span>
					<div class="Agency">
						<a href="javascript:shidayl(${dataChooseVO[0].reportNo})">代审案件</a>
						<a>1</a>
					</div>
					<div class="trial">
						<a href="javascript:queshenA(${dataChooseVO[0].reportNo})">确审案件</a>
						<a>2</a>
					</div>
					<div class="review">
						<a href="javascript:ailisi(${dataChooseVO[0].reportNo})">复审案件</a>
						<a>1</a>
					</div>
				</div>

				

			</div>

			<!--1111待审案件-->
			<div class="daiShen">
				<div class="dsimg">
					<span class="hpcentenr">&nbsp;&nbsp;核赔中心&nbsp;&nbsp;</span><span class="anjianGM">/案件管理</span>&nbsp;&nbsp;<span class="ds">/待审案件
				</span> <img src="img/sx.png" />
				</div>
				<hr />
				<div class="dssousuo">
					&nbsp;&nbsp;&nbsp;<input type="text" value="请输入关键字" style="height: 30px;" /> <img src="img/ss.png" />
				</div>
				<br>
				<!--分页信息-->
				<div>
					<table class="ever1" width=95% border='1px' bordercolor="#CCCCCC" cellspacing="0" style='margin: 0px auto; text-align: center;' id="listTable">
						<tr>
							<td>报案号</td>
							<td>保单号</td>
							<td>报案人</td>
							<td>出险地址</td>
							<td>报案时间</td>
							<td>处理时间</td>
							<td>状态</td>
							<td>操作</td>
						</tr>

						<tr>

						</tr>
					</table>
					<!--记录 首页 尾页-->
					<div class="jilu">

						<span>共1页.共1条记录</span><br> <img src="img/wy.png" /> <img src="img/sy.png" />
					</div>
				</div>
			</div>

			<!--2222确审案件-->
			<div class="quesheng">
				<div class="dsimg">
					<span class="hpcentenr">&nbsp;&nbsp;核赔中心&nbsp;&nbsp;</span><span class="anjianGM">/案件管理</span>&nbsp;&nbsp;<span class="ds">/确审案件
				</span>
					<a href="#"><img src="img/sx.png" /></a>
				</div>
				<hr />
				<div class="dssousuo">
					&nbsp;&nbsp;&nbsp;<input type="text" value="请输入关键字" style="height: 30px;" /> <img src="img/ss.png" />
				</div>
				<br>
				<!--分页信息-->
				<div>
					<table class="ever3" width=95% border='1px' bordercolor="#CCCCCC" cellspacing="0" style='margin: 0px auto; text-align: center;' id="listTable">
						<tr>
							<td>报案号</td>
							<td>保单号</td>
							<td>报案人</td>
							<td>出险地址</td>
							<td>报案时间</td>
							<td>处理时间</td>
							<td>状态</td>
							<td>操作</td>
						</tr>
						<tr>

						</tr>

					</table>
					<!--记录 首页 尾页-->
					<div class="jilu">
						<span>共0页.共0条记录</span><br> <img src="img/wy.png" /> <img src="img/sy.png" />
					</div>
					<div class="gg">

						<DIV class="seriziber" align="bottom">
							<a class="guanbi"><img src="images/xxx1.png"></a><br>
							<tr>
								<h2 class="maseeges1">保单信息</h2>
							</tr>
							<table class="no1" width=95% border='1px' bordercolor="#CCCCCC" cellspacing="0" style='margin: 0px auto; text-align: center;'>
								<tr>
									<td colspan="6">
										<h3>报案受理信息</h3>
									</td>
								</tr>

								<tr>
								</tr>

							</table>

							<br>
							<div class="maseeges2">

								<a class="pipi1" href="javascript:duomos()">查勘信息</a>
							</div>
							<table class="no2" width=95% border='1px' bordercolor="#CCCCCC" cellspacing="0" style='margin: 0px auto; text-align: center;'>
								<tr>

								</tr>
							</table>
							<div class="maseeges3">
								<a class="pipi2" href="javascript:dingsuns()">本车定损信息</a>
							</div>

							<table class="no3" width=95% border='1px' bordercolor="#CCCCCC" cellspacing="0" style='margin: 0px auto; text-align: center;'>
								<tr>

								</tr>
							</table>
							<div class="maseeges4">

								<a class="pipi3" href="javascript:ziliaoxx()">资料信息</a>
							</div>

							<table class="no4" width=95% border='1px' bordercolor="#CCCCCC" cellspacing="0" style='margin: 0px auto; text-align: center;'>
								<tr>

								</tr>
							</table>
						</DIV>

					</div>
				</div>
			</div>

			<!-- 33333复审案件-->
			<div class="fusheng">
				<div class="dsimg">
					<span class="hpcentenr">&nbsp;&nbsp;核赔中心&nbsp;&nbsp;</span><span class="anjianGM">/案件管理</span>&nbsp;&nbsp;<span class="ds">/复审案件
				</span> <img src="img/sx.png" />
				</div>
				<hr />
				<div class="dssousuo">
					&nbsp;&nbsp;&nbsp;<input type="text" value="请输入关键字" style="height: 30px;" /> <img src="img/ss.png" />
				</div>
				<br>
				<!--分页信息-->
				<div>
					<table class="ever2" width=95% border='1px' bordercolor="#CCCCCC" cellspacing="0" style='margin: 0px auto; text-align: center;' id="listTable">
						<tr>
							<td>报案号</td>
							<td>保单号</td>
							<td>报案人</td>
							<td>出险地址</td>
							<td>报案时间</td>
							<td>处理时间</td>
							<td>状态</td>
							<td>操作</td>
						</tr>
						<tr>

						</tr>
					</table>
					<!--记录 首页 尾页-->
					<div class="jilu">
						<span>共1页.共1条记录</span><br> <img src="img/wy.png" /> <img src="img/sy.png" />
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="./js/jquery-1.12.4.js"></script>
		<script type="text/javascript" src="js/important.js"></script>
		<script type="text/javascript" src="js/aligh.js"></script>
		<script type="text/javascript">
			</script>

	</body>

</html>