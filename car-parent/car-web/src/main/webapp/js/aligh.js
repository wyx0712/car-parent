//转换时间戳
function timestampToTime(timestamp) {
	var date = new Date(timestamp);
	Y = date.getFullYear() + "年";
	M = (date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date
			.getMonth() + 1) +
		"月";
	D = (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + "日";

	return Y + M + D;
}

function detail(reportNo) {
	$(".no1 tr:gt(0)").remove();

	$.getJSON("detail.do", // url
		{
			"reportNo": reportNo
		},
		function(dataChooseVO) {
			// 遍历取值，将其加入一行中
			var tr = "<tr><td>报案号</td>" + "<td id='no1_reportNo'>" +
				dataChooseVO.reportNo + "</td>" + "<td>报案人</td>" + "<td>" +
				dataChooseVO.reportInfoList[0].reportName + "</td>" +
				"<td>报案电话</td>" + "<td>" +
				dataChooseVO.reportInfoList[0].reporTel + "</td></tr>" +
				"<tr><td>驾驶人姓名</td>" + "<td>" +
				dataChooseVO.reportInfoList[0].driverName + "</td>" +
				"<td>驾驶人电话</td>" + "<td>" +
				dataChooseVO.reportInfoList[0].driverTel + "</td>" +
				"<td>出险时间</td>" + "<td>" +
				timestampToTime(dataChooseVO.reportInfoList[0].dangerTime) +
				"</td></tr>" + "<tr><td>出险地点</td>" + "<td colspan='5'>" +
				dataChooseVO.reportInfoList[0].dangerAddress + "</td></tr>" +
				"<tr><td>出险原因</td>" + "<td colspan='5'>" +
				dataChooseVO.reportInfoList[0].dangerReason + "</td></tr>" +
				"<tr><td>损失类型</td>" + "<td colspan='5'>" +
				dataChooseVO.reportInfoList[0].lossType + "</td></tr>" +
				"<tr><td>出险区域</td>" + "<td colspan='5'>" +
				dataChooseVO.dangerArea + "</td></tr>" + "<tr><td>接案人</td>" +
				"<td colspan='2'>" + dataChooseVO.surveyName + "</td>" +
				"<td>接案时间</td>" + "<td colspan='2'>" +
				timestampToTime(dataChooseVO.reportInfoList[0].reportTime) +
				"</td></tr>";
			// 将当前数据 依次添加位置
			$(".no1").append(tr);
		});
};

// 刷新查勘表
function duomos(reportNo) {
	var reportNo = $("#no1_reportNo").html();
	$(".no2 tr:gt(0)").remove();
	$.getJSON("duomos.do", // url
		{
			"reportNo": reportNo
		},
		function(dataChooseVO) {
			// 遍历取值，将其加入一行中
			var tr = "<tr><td>报案人</td>" + "<td colspan='2'>" +
				dataChooseVO.reportName + "</td>" + "<td>出险驾驶员</td>" +
				"<td colspan='2'>" + dataChooseVO.driverName + "</td></tr>" +
				"<tr><td>准驾车型</td>" + "<td colspan='2'>" +
				dataChooseVO.carType + "</td>" + "<td>驾驶电话</td>" +
				"<td colspan='2'>" + dataChooseVO.driverTel + "</td></tr>" +
				"<tr><td>道路信息</td>" + "<td colspan='5'>" +
				dataChooseVO.roadInfo + "</td></tr>" + "<tr><td>出险经过</td>" +
				"<td colspan='5'>" + dataChooseVO.dangerThrough +
				"</td></tr>" + "<tr><td>查勘意见</td>" + "<td colspan='5'>" +
				dataChooseVO.surveyOpinion + "</td></tr>" +
				"<tr><td>现场照片</td>" + "<td colspan='5'>" +
				dataChooseVO.imgOne + "</td></tr>" + "<tr><td>查勘员</td>" +
				"<td>" + dataChooseVO.surveyName + "</td>" + "<td>查勘机构</td>" +
				"<td>" + dataChooseVO.companyName + "</td>" + "<td>查勘时间</td>" +
				"<td>" + dataChooseVO.surveyTime + "</td></tr>";
			// 将当前数据 依次添加位置
			$(".no2").append(tr);
		});

};

// 刷新保损表
function dingsuns(reportNo) {
	var reportNo = $("#no1_reportNo").html();
	$(".no3 tr:gt(0)").remove();
	$.getJSON("dingsuns.do", // url
		{
			"reportNo": reportNo
		},
		function(dataChooseVO) {
			// 遍历取值，将其加入一行中
			var tr = "<tr><td>修理厂名</td>" + "<td>" + dataChooseVO.repairType +
				"</td>" + "<td>车主</td>" + "<td>" + dataChooseVO.carOwner +
				"+</td>" + "<td>车型</td>" + "<td>" + dataChooseVO.repairShop +
				"</td></tr>" + "<tr><td>车牌</td>" + "<td>" +
				dataChooseVO.carNo + "</td>" + "<td>发动机号</td>" + "<td>Remax</td>" + "<td>车架号</td>" + "<td>At4388</td></tr>" + "<tr><td>配件名</td>" +
				"<td>" + dataChooseVO.partsName + "</td>" + "<td>报价</td>" +
				"<td>" + dataChooseVO.lossPrices + "</td>" + "<td>系统价</td>" +
				"<td>" + dataChooseVO.lossMoney + "</td></tr>" +
				"<tr><td>本车定损金额</td>" + "<td>" + dataChooseVO.carMoney +
				"</td>" + "<td>本车核损金额</td>" + "<td>" + dataChooseVO.lossFee +
				"</td>" + "<td>报损人</td>" + "<td>" + dataChooseVO.userName +
				"</td></tr>" + "<tr><td>报损意见</td>" + "<td colspan='5'>" +
				dataChooseVO.lossOpinion + "</td></tr>";
			// 将当前数据 依次添加位置
			$(".no3").append(tr);
		});

};

function ziliaoxx(reportNo) {
	var reportNo = $("#no1_reportNo").html();

	$(".no4 tr:gt(0)").remove();
	$.getJSON("ziliaoxx.do", // url
		{
			"reportNo": reportNo
		},
		function(dataChooseVO) {
			// 遍历取值，将其加入一行中
			var tr = "<tr><td>交通事故损害赔偿调解书:</td>" + "<td>" +
				dataChooseVO.daList[0].reparationMediation + "</td>" +
				"<td>修车发票:</td>" + "<td>" +
				dataChooseVO.daList[0].repairInvoice + "</td>" +
				"<td>驾驶员会员证复印件：</td>" + "<td>" +
				dataChooseVO.daList[0].driverVip + "</td></tr>" +
				"<tr><td>医疗费用：</td>" + "<td>" +
				dataChooseVO.daList[0].medicalPrices + "</td>" +
				"<td>死亡证明：</td>" + "<td>" +
				dataChooseVO.daList[0].dieCertificate + "</td>" +
				"<td>交通事故评残证明：</td>" + "<td>" +
				dataChooseVO.daList[0].disabilityCertificate + "</td></tr>" +
				"<tr><td>养路费停缴证明：</td>" + "<td>" +
				dataChooseVO.daList[0].yanglufei + "</td>" +
				"<td>盗抢车辆为侦破证明：</td>" + "<td>" +
				dataChooseVO.daList[0].touqiekanpoCertificate + "</td>" +
				"<td>被盗车钥匙：</td>" + "<td>" + dataChooseVO.daList[0].carKey +
				"</td></tr>" + "<tr><td>车辆丢失登报证明：</td>" + "<td>" +
				dataChooseVO.daList[0].lossVehiclesCertificate + "</td>" +
				"<td>盗抢车辆封档证明：</td>" + "<td>" +
				dataChooseVO.daList[0].touqiefdCertificate + "</td>" +
				"<td>误工证明或护理证名：</td>" + "<td>" +
				dataChooseVO.daList[0].wugongCertificate + "</td></tr>" +
				"<tr><td>权益转让书：</td>" + "<td>" +
				dataChooseVO.daList[0].assignment + "</td>" +
				"<td>火灾证明：</td>" + "<td>" +
				dataChooseVO.daList[0].fireCertificate + "</td>" +
				"<td>暴雨暴风证明：</td>" + "<td>" +
				dataChooseVO.daList[0].rainstormCertificate + "</td></tr>" +
				"<tr><td>驾驶证：</td>" + "<td>" +
				dataChooseVO.doList[0].driverLicense + "</td>" +
				"<td>被保险人营业执照或身份证复印件：</td>" + "<td>" +
				dataChooseVO.doList[0].insurerCertificate + "</td>" +
				"<td>购车发票：</td>" + "<td>" +
				dataChooseVO.doList[0].autobytelInvoice + "</td></tr>" +
				"<tr><td>机动车登记证书原件：</td>" + "<td>" +
				dataChooseVO.doList[0].carCertificate + "</td>" +
				"<td>车辆购置附加费原件：</td>" + "<td>" +
				dataChooseVO.doList[0].additionalPrices + "</td>" +
				"<td>行驶证复件：</td>" + "<td>" +
				dataChooseVO.doList[0].travelLicenseB + "</td></tr>" +
				"<tr><td>索赔申请书：</td>" + "<td>" + dataChooseVO.doList[0].claim +
				"</td>" + "<td>施救费用收据：</td>" + "<td>" +
				dataChooseVO.doList[0].salvagePrices + "</td>" +
				"<td>交通事故损害赔偿调解书：</td>" + "<td>" +
				dataChooseVO.doList[0].reparationMediation + "</td></tr>";

			$(".no4").append(tr);
		});

};

// 待审表
function shidayl(pageNo, reportNo) {
	/* var reportNo=$("#no1_reportNo").html(); */
	$(".ever1 tr:gt(0)").remove();
	$
		.getJSON(
			"shidayl.do", // url
			{
				"pageNo": pageNo,
				"reportNo": reportNo

			},
			function(pageInfo) {

				$(pageInfo.list)
					.each(
						function(index, reportInfo) {

							// 遍历取值，将其加入一行中
							var tr = "<tr><td><input type='checkbox'/></td>" +
								"<td>" +
								reportInfo.reportNo +
								"</td>" +
								"<td>" +
								reportInfo.policyId +
								"</td>" +
								"<td>" +
								reportInfo.reportName +
								"</td>" +
								"<td>" +
								reportInfo.dangerAddress +
								"</td>" +
								"<td>" +
								timestampToTime(reportInfo.reportTime) +
								"</td>" +
								"<td>" +
								timestampToTime(reportInfo.dangerTime) +
								"</td>" +
								"<td>待审案件</td>" +
								"<td><a  onclick='xianshi()' href='javascript:detail(" +
								reportInfo.reportNo +
								")'><img  src='images/xiangxi.png' /></a></td>" +
								"<td><a onclick='tongzhi()' style='color: black;'href='submitda.do?reportNo=" +
								reportInfo.reportNo+
								"'>确审/</a><a  onclick='fushens()'    style='color: black;'href='submitdb.do?reportNo=" +
								reportInfo.reportNo +
								"'>撤回</a></td></tr>";

							$(".ever1").append(tr);
						});
				$("#count").html("共有数据：" + pageInfo.total + "条");

				// 分页
				if(pageNo == 1) {
					$(".first").removeAttr('href');
				} else {
					$(".first").attr("href",
						"javascript:shidayl(" + (pageNo - 1) + ")");
				}
				if(pageNo == pageInfo.pages) {
					$(".next").removeAttr('href');
				} else {
					$(".next").attr("href",
						"javascript:shidayl(" + (pageNo + 1) + ")");
				}

			});

};

// 确审表
function queshenA(reportNo, pageNo) {
	/* var reportNo=$("#no1_reportNo").html(); */
	$(".ever3 tr:gt(0)").remove();
	$
		.getJSON(
			"queshenA.do", // url
			{
				"pageNo": pageNo,
				"reportNo": reportNo

			},
			function(pageInfo) {

				$(pageInfo.list)
					.each(
						function(index, reportInfo) {

							// 遍历取值，将其加入一行中
							var tr = "<tr><td>" +
								reportInfo.reportNo +
								"</td>" +
								"<td>" +
								reportInfo.policyId +
								"</td>" +
								"<td>" +
								reportInfo.reportName +
								"</td>" +
								"<td>" +
								reportInfo.dangerAddress +
								"</td>" +
								"<td>" +
								timestampToTime(reportInfo.reportTime) +
								"</td>" +
								"<td>" +
								timestampToTime(reportInfo.dangerTime) +
								"</td>" +
								"<td>确审案件</td>" +
								"<td><a  onclick='xianshi()' href='javascript:detail(" +
								reportInfo.reportNo +
								")'><img  src='images/xiangxi.png' /></a></td></tr>";

							$(".ever3").append(tr);
						});

				// 分页
				if(pageNo == 1) {
					$(".first").removeAttr('href');
				} else {
					$(".first").attr("href",
						"javascript:queshenA(" + (pageNo - 1) + ")");
				}
				if(pageNo == pageInfo.pages) {
					$(".next").removeAttr('href');
				} else {
					$(".next").attr("href",
						"javascript:queshenA(" + (pageNo + 1) + ")");
				}

			});

};

// 复审表
function ailisi(pageNo, reportNo) {
	/* var reportNo=$("#no1_reportNo").html(); */
	$(".ever2 tr:gt(0)").remove();
	$
		.getJSON(
			"ailisi.do", // url
			{
				"pageNo": pageNo,
				"reportNo": reportNo

			},
			function(pageInfo) {

				$(pageInfo.list)
					.each(
						function(index, reportInfo) {

							// 遍历取值，将其加入一行中
							var tr = "<tr><td><input type='checkbox'/></td>" +
							    "<td>" +
								reportInfo.reportNo +
								"</td>" +
								"<td>" +
								reportInfo.policyId +
								"</td>" +
								"<td>" +
								reportInfo.reportName +
								"</td>" +
								"<td>" +
								reportInfo.dangerAddress +
								"</td>" +
								"<td>" +
								timestampToTime(reportInfo.reportTime) +
								"</td>" +
								"<td>" +
								timestampToTime(reportInfo.dangerTime) +
								"</td>" +
								"<td>撤回案件</td>" +
								"<td><a  onclick='xianshi()' href='javascript:detail(" +
								reportInfo.reportNo +
								")'><img  src='images/xiangxi.png' /></a></td>" +
								"<td><a onclick='tongzhi()' style='color: black;'href='submitda.do?reportNo=" +
								reportInfo.reportNo +
								"'>通过确审</a></td></tr>";

							$(".ever2").append(tr);
						});

				//分页
				if(pageNo == 1) {
					$(".first").removeAttr('href');
				} else {
					$(".first").attr("href",
						"javascript:ailisi(" + (pageNo - 1) + ")");
				}
				if(pageNo == pageInfo.pages) {
					$(".next").removeAttr('href');
				} else {
					$(".next").attr("href",
						"javascript:ailisi(" + (pageNo + 1) + ")");
				}

			});

};