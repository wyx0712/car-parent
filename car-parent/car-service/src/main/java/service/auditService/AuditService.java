/**
 * 
 */
package service.auditService;

import java.util.List;

import pojo.CarMessage;
import pojo.Carnotoparts;
import pojo.Company;
import pojo.DataChoose;
import pojo.DataMust;
import pojo.Lossinfo;
import pojo.Policy;
import pojo.ReportInfo;
import pojo.SurveyInfo;

/**
 * @author 亮亮
 * 2018年10月25日
 * 
 */
public interface AuditService {
	//查询所有案件信息
	List<ReportInfo> getReportInfoList();
    //查找所有查勘信息
	List<SurveyInfo> getSurveyInfo();
	//通过报案号查询查勘信息
	SurveyInfo getSurveyInfo1(Integer reportNo);
	
	//通过报案号查询案件信息的对象
	ReportInfo getReportInfo(Integer reportNo);
	//通过报案号查询案件信息的集合
	List<ReportInfo> getReportInfoList(Integer reportNo);
	
	
	//通过保单号查询保单信息
	Policy getPolicy(String policyId);
	
	//通过案件号看定损信息
	Lossinfo getLossinfo(Integer reportNo);
	
	//通过公司表查找公司
	Company  getCompany(Integer companyId);
	
	//通过service_id寻找查勘员
	SurveyInfo  getSurveyInfo(Integer surveyId);
	
	//通过配件id 寻找配件表
	Carnotoparts getCarnotoparts(Integer partsId);
	
	//通过报案号查找资料信息表A
	DataMust getDataMust(Integer reportNo);
	
	//通过报案号查找资料信息表B
	DataChoose getDataChoose(Integer reportNo);
	
	//通过报案号查找状态表
	CarMessage getCarMessage(Integer reportNo);
	
	//修改字段状态
	int updateByPrimaryKey(CarMessage record);
	
	
}
