/**
 * 
 */
package service.auditService.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.CarMessageMapper;
import mapper.CarnotopartsMapper;
import mapper.CompanyMapper;
import mapper.DataChooseMapper;
import mapper.DataMustMapper;
import mapper.LossinfoMapper;
import mapper.PolicyMapper;
import mapper.ReportInfoMapper;
import mapper.SurveyInfoMapper;
import pojo.CarMessage;
import pojo.CarMessageExample;
import pojo.Carnotoparts;
import pojo.CarnotopartsExample;
import pojo.Company;
import pojo.DataChoose;
import pojo.DataChooseExample;
import pojo.DataMust;
import pojo.DataMustExample;
import pojo.Lossinfo;
import pojo.LossinfoExample;
import pojo.Policy;
import pojo.ReportInfo;
import pojo.ReportInfoExample;
import pojo.ReportInfoExample.Criteria;
import pojo.SurveyInfo;
import pojo.SurveyInfoExample;
import service.auditService.AuditService;

/**
 * @author 亮亮
 * 2018年10月25日
 * 
 */
@Service
public class AuditServiceImpl implements AuditService{
	@Autowired
	ReportInfoMapper reportMapper;
	@Autowired
	PolicyMapper policyMapper;
	@Autowired 
	LossinfoMapper lossinfoMapper; 
	@Autowired
	CompanyMapper companyMapper;
	@Autowired
	SurveyInfoMapper surveyInfoMapper;
	@Autowired
	CarnotopartsMapper carnotopartsMapper;
	@Autowired
	DataChooseMapper dataChooseMapper;
	@Autowired
	DataMustMapper dataMustMapper;
	@Autowired
	CarMessageMapper carMessageMapper;
	
	
	//通过报案号查询案件信息
	@Override
	public ReportInfo getReportInfo(Integer reportNo) {
		
		return reportMapper.selectByPrimaryKey(reportNo);
	}
	//通过保单号查询保单信息
	@Override
	public Policy getPolicy(String policyId) {
		
		return policyMapper.selectByPrimaryKey(policyId);
	}
	//通过案件号看定损信息
	@Override
	public Lossinfo getLossinfo(Integer reportNo) {
		
		LossinfoExample example=new LossinfoExample();
		pojo.LossinfoExample.Criteria criteria=example.createCriteria();
		criteria.andReportNoEqualTo(reportNo);
		
		List<Lossinfo> list=lossinfoMapper.selectByExample(example);
		if(list!=null && list.size()>0){
			return list.get(0);
		}
		
		return null;
	}
	//通过配件id查找配件表
	@Override
	public Carnotoparts getCarnotoparts(Integer partsId) {
	
		CarnotopartsExample example=new CarnotopartsExample();
		pojo.CarnotopartsExample.Criteria criteria=example.createCriteria();
		criteria.andPartsIdEqualTo(partsId);
		
		List<Carnotoparts> list=carnotopartsMapper.selectByExample(example);
		if(list!=null && list.size()>0){
			return list.get(0);
		}
		
		return null;
	}
	//通过 报案号查找信息表B
	@Override
	public DataMust getDataMust(Integer reportNo) {
		DataMustExample example=new DataMustExample();
		pojo.DataMustExample.Criteria criteria=example.createCriteria();
		criteria.andReportNoEqualTo(reportNo);
		
		List<DataMust> list=dataMustMapper.selectByExample(example);
		if(list!=null && list.size()>0){
			return list.get(0);
		}
		
		return null;
	}
	//通过 报案号查找信息表A
	@Override
	public DataChoose getDataChoose(Integer reportNo) {
		DataChooseExample example=new DataChooseExample();
		pojo.DataChooseExample.Criteria criteria=example.createCriteria();
		criteria.andReportNoEqualTo(reportNo);
		
		List<DataChoose> list=dataChooseMapper.selectByExample(example);
		if(list!=null && list.size()>0){
			return list.get(0);
		}
		
		return null;
	}
	//通过报案号查询查勘信息
	@Override
	public SurveyInfo getSurveyInfo1(Integer reportNo) {
		SurveyInfoExample example=new SurveyInfoExample();
		pojo.SurveyInfoExample.Criteria criteria=example.createCriteria();
		criteria.andReportNoEqualTo(reportNo);
		
		List<SurveyInfo> list=surveyInfoMapper.selectByExample(example);
		if(list!=null && list.size()>0){
			return list.get(0);
		}
		
		return null;
	}
	
	
	//查询所有报案信息
	@Override
	public List<ReportInfo> getReportInfoList() {
		// TODO Auto-generated method stub
		return reportMapper.selectByExample(null);
	}
	
	//通过公司表主键 查询公司名
	@Override
	public Company getCompany(Integer companyId) {
		
		return  companyMapper.selectByPrimaryKey(companyId);
		
	}
	
	@Override
	public SurveyInfo getSurveyInfo(Integer surveyId) {
	
		return surveyInfoMapper.selectByPrimaryKey(surveyId);
	}
	/* (non-Javadoc)
	 * @see service.auditService.AuditService#getSurveyInfo()
	 */
	@Override
	public List<SurveyInfo> getSurveyInfo() {
		// TODO Auto-generated method stub
		return surveyInfoMapper.selectByExample(null);
	}
	/* (non-Javadoc)
	 * @see service.auditService.AuditService#getSurveyInfo1(java.lang.Integer)
	 */
	/* (non-Javadoc)
	 * @see service.auditService.AuditService#getReportInfoList(java.lang.Integer)
	 */
	@Override
	public List<ReportInfo> getReportInfoList(Integer reportNo) {
		ReportInfoExample example=new ReportInfoExample();
		pojo.ReportInfoExample.Criteria criteria=example.createCriteria();
		criteria.andReportNoEqualTo(reportNo);
		
		List<ReportInfo> list=reportMapper.selectByExample(example);
		return list;
	}
//通过报案号查找carMessage表
	@Override
	public CarMessage getCarMessage(Integer reportNo) {
		CarMessageExample example=new CarMessageExample();
		pojo.CarMessageExample.Criteria criteria=example.createCriteria();
		criteria.andReportNoEqualTo(reportNo);
		
		List<CarMessage> list=carMessageMapper.selectByExample(example);
		if(list!=null && list.size()>0){
			return list.get(0);
		}
		
		return null;
	}
	
	@Override
	public int updateByPrimaryKey(CarMessage record) {
		
		return carMessageMapper.updateByPrimaryKey(record);
	}
	
	
	
	
	
	

}
