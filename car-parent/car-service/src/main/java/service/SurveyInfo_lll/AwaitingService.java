/**
 * 
 */
package service.SurveyInfo_lll;

import java.util.List;

import org.springframework.stereotype.Service;

import pojo.CarMessage;
import pojo.ReportInfo;
import pojo.SurveyInfo;

/**
 * @author 亮亮
 * 2018年10月28日
 * 
 */

public interface AwaitingService {
	
//通过报案号查询案件级别
	CarMessage getCarMessage(Integer  reportNo);
	
//通过案件级别编号查找对应的表
	List<CarMessage> getCarMessage1(Integer surveyNow);
	

	
	//查询所有案件级别信息表
	List<CarMessage> getCarMessageList();
	
	//通过案件信息表的reportNO查找reportinfo
	ReportInfo getReportInfo(Integer reportNo);
}
