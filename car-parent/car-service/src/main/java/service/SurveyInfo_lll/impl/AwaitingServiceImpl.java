/**
 * 
 */
package service.SurveyInfo_lll.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.ReportInfoMapper;
import pojo.CarMessage;
import pojo.CarMessageExample;
import pojo.Lossinfo;
import pojo.LossinfoExample;
import pojo.ReportInfo;
import pojo.ReportInfoExample;
import service.SurveyInfo_lll.AwaitingService;

/**
 * @author 亮亮
 * 2018年10月28日
 * 
 */
@Service
public class AwaitingServiceImpl implements AwaitingService {

	@Autowired
	mapper.CarMessageMapper carMessageMapper;
	@Autowired
	ReportInfoMapper reportMapper;
	//通过报案号查询   案件级别表
	@Override
	public CarMessage getCarMessage(Integer reportNo) {

		CarMessageExample example=new CarMessageExample();
		pojo.CarMessageExample.Criteria criteria=example.createCriteria();
		criteria.andReportNoEqualTo(reportNo);
		
		List<CarMessage> list=carMessageMapper.selectByExample(example);
		if(list!=null && list.size()>0){
			return list.get(0);
		}
		
		return null;
		
	}


	@Override
	public List<CarMessage> getCarMessage1(Integer surveyNow) {
		CarMessageExample example=new CarMessageExample();
		pojo.CarMessageExample.Criteria criteria=example.createCriteria();
		criteria.andSurveyNowEqualTo(surveyNow);
		
		List<CarMessage> list=carMessageMapper.selectByExample(example);
		
		
		return list;
	}


	
	


	//查找所有carMessage
	@Override
	public List<CarMessage> getCarMessageList() {
		// TODO Auto-generated method stub
		return carMessageMapper.selectByExample(null);
	}


	//通过报案号查找报案信息
	@Override
	public ReportInfo getReportInfo(Integer reportNo) {
	
		return reportMapper.selectByPrimaryKey(reportNo);
	}
	

}
