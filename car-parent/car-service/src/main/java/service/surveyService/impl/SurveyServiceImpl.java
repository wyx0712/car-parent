package service.surveyService.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.CarMessageMapper;
import mapper.CarnotopartsMapper;
import mapper.HurtInfoMapper;
import mapper.HurtrelationMapper;
import mapper.LossinfoMapper;
import mapper.ReportInfoMapper;
import mapper.SurveyInfoMapper;
import pojo.CarMessage;
import pojo.CarMessageExample;
import pojo.Carnotoparts;
import pojo.CarnotopartsExample;
import pojo.HurtInfo;
import pojo.HurtInfoExample;
import pojo.Hurtrelation;
import pojo.HurtrelationExample;
import pojo.Lossinfo;
import pojo.LossinfoExample;
import pojo.ReportInfo;
import pojo.ReportInfoExample;
import pojo.SurveyInfo;
import pojo.SurveyInfoExample;
import service.surveyService.SurveyService;


@Service
public class SurveyServiceImpl implements SurveyService{

	@Autowired
	private CarMessageMapper carMessageMapper;
	
	@Autowired
	private LossinfoMapper lossinfoMapper;
	
	@Autowired
	private  ReportInfoMapper reportInfoMapper;
	
	@Autowired
	private SurveyInfoMapper surveyInfoMapper;
	
	@Autowired
	private CarnotopartsMapper carnotopartsMapper;
	
	@Autowired
	private HurtInfoMapper hurtInfoMapper;
	
	
	
	@Autowired
	private HurtrelationMapper hurtrelationMapper;
	
	public int countByExample(CarMessageExample example){
		
		
		return carMessageMapper.countByExample(example);
	}


	@Override
	public List<CarMessage> selectCarMessage(CarMessageExample example) {
		
		return carMessageMapper.selectByExample(example);
	}


	//根据报案号查询数据
	
	@Override
	public Lossinfo selectlossinfoByreportno(Integer reportNo) {
	
		return lossinfoMapper.selectByPrimaryKey(reportNo);
	}

	
	@Override
	public Lossinfo selectlossByExample(LossinfoExample example) {
		List<Lossinfo> list = lossinfoMapper.selectByExample(example);
		return list.get(0);
	}


	@Override
	public SurveyInfo selectInfoByExample(SurveyInfoExample example) {
		List<SurveyInfo> list = surveyInfoMapper.selectByExample(example);
		return list.get(0);
	}


	@Override
	public SurveyInfo selectInfoById(Integer surveyId) {
		SurveyInfo info = surveyInfoMapper.selectByPrimaryKey(surveyId);
		return info;
	}


	@Override
	public ReportInfo selectreportByreno(ReportInfoExample example) {
		
		List<ReportInfo> list = reportInfoMapper.selectByExample(example);
		return list.get(0);
	}


	@Override
	public int insert(SurveyInfo record) {
		
		return surveyInfoMapper.insert(record);
	}


	@Override
	public int updateByPrimaryKeySelective(SurveyInfo record) {
		
		return surveyInfoMapper.updateByPrimaryKeySelective(record);
	}


	@Override
	public List<SurveyInfo> selectSurveyInfoByExample(SurveyInfoExample example) {
		
		return surveyInfoMapper.selectByExample(example);
	}


	@Override
	public int updateBycarmessKeyselect(CarMessage carMessage,CarMessageExample example) {
		
		return carMessageMapper.updateByExampleSelective(carMessage, example);
	}


	@Override
	public List<Lossinfo> seeLossinfo(LossinfoExample example) {
		
		return lossinfoMapper.selectByExample(example);
	}


	@Override
	public int lossinfoinselt(Lossinfo lossinfo) {
	
		return lossinfoMapper.insert(lossinfo);
	}


	@Override
	public int addcarmessage(CarMessage carMessage) {
		carMessage.setSurveyLook(1);
		carMessage.setSurveyCarone(3);
		carMessage.setSurveyCartwo(3);
		carMessage.setSurveyPeople(3);
		carMessage.setSurveyNow(0);
		carMessage.setTableId(2004);
			
		
		return carMessageMapper.insert(carMessage);
	}


	@Override
	public int addcarnotoparts(Carnotoparts carnotoparts) {
		
		return carnotopartsMapper.insert(carnotoparts);
	}


	@Override
	public List<HurtInfo> selectHurtInfobyExample(HurtInfoExample example) {
		
		return hurtInfoMapper.selectByExample(example);
	}


	@Override
	public int insertHurt(HurtInfo hurtInfo) {
		
		return hurtInfoMapper.insert(hurtInfo);
	}


	@Override
	public int insertHurttion(Hurtrelation hurtrelation) {
		
		return hurtrelationMapper.insert(hurtrelation);
	}


	@Override
	public List<Hurtrelation> selectHurtrelationexampl(HurtrelationExample hurtrelationExample) {
		
		return hurtrelationMapper.selectByExample(hurtrelationExample);
	}


	@Override
	public List<Carnotoparts> selectCarnotoparts(CarnotopartsExample carnotopartsExample) {
		
		return carnotopartsMapper.selectByExample(carnotopartsExample);
	}


	@Override
	public int updatelossKeySelective(Lossinfo lossinfo, LossinfoExample example) {
	
		return lossinfoMapper.updateByExampleSelective(lossinfo, example);
	}


	@Override
	public int deleteByExample(CarnotopartsExample carnotopartsExample) {
		
		return carnotopartsMapper.deleteByExample(carnotopartsExample);
	}


	@Override
	public int deleteHurtrelationByExample(HurtrelationExample hurtrelationExample) {
	
		return hurtrelationMapper.deleteByExample(hurtrelationExample);
	}


	@Override
	public int updatehurtKeySelective(HurtInfo hurtInfo, HurtInfoExample hurtInfoExample) {
		
		return hurtInfoMapper.updateByExampleSelective(hurtInfo, hurtInfoExample);
	}


	@Override
	public int updatereportinfo(ReportInfo reportInfo, ReportInfoExample reportInfoExample) {
		
		return reportInfoMapper.updateByExampleSelective(reportInfo, reportInfoExample);
	}


	

	
	
	
	
}
