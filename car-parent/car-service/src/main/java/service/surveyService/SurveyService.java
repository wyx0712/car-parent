package service.surveyService;

import java.util.List;

import pojo.CarMessage;
import pojo.CarMessageExample;
import pojo.Carnotoparts;
import pojo.CarnotopartsExample;
import pojo.HurtInfo;
import pojo.HurtInfoExample;
import pojo.Hurtrelation;
import pojo.HurtrelationExample;
import pojo.Lossinfo;
import pojo.LossinfoExample;
import pojo.ReportInfo;
import pojo.ReportInfoExample;
import pojo.SurveyInfo;
import pojo.SurveyInfoExample;

public interface SurveyService {
	int updatereportinfo(ReportInfo reportInfo,ReportInfoExample reportInfoExample);
	
	int countByExample(CarMessageExample example);
	List<CarMessage> selectCarMessage(CarMessageExample example);
	int updateBycarmessKeyselect(CarMessage carMessage,CarMessageExample example);
	
	
	Lossinfo selectlossinfoByreportno(Integer reportNo);
	Lossinfo selectlossByExample(LossinfoExample example);
	int lossinfoinselt(Lossinfo lossinfo);
	List<Lossinfo> seeLossinfo(LossinfoExample example);	
	int addcarmessage(CarMessage carMessage);
	int updatelossKeySelective(Lossinfo lossinfo,LossinfoExample example);
	
	
	SurveyInfo selectInfoByExample(SurveyInfoExample example);
	List<SurveyInfo> selectSurveyInfoByExample(SurveyInfoExample example);
	SurveyInfo selectInfoById(Integer surveyId);
	
	
	ReportInfo selectreportByreno(ReportInfoExample example);
	int insert(SurveyInfo record);
	int updateByPrimaryKeySelective(SurveyInfo record);
	
	
	int deleteByExample(CarnotopartsExample carnotopartsExample);
	int addcarnotoparts(Carnotoparts carnotoparts);
	List<Carnotoparts> selectCarnotoparts(CarnotopartsExample carnotopartsExample);
	
	int updatehurtKeySelective(HurtInfo hurtInfo,HurtInfoExample hurtInfoExample);
	List<HurtInfo> selectHurtInfobyExample(HurtInfoExample hurtInfoExample);
	int insertHurt(HurtInfo hurtInfo);
	int deleteHurtrelationByExample(HurtrelationExample hurtrelationExample);
	int insertHurttion(Hurtrelation hurtrelation);
	List<Hurtrelation> selectHurtrelationexampl(HurtrelationExample hurtrelationExample);
	
}
