/**
 * 
 */
package service.userService;

import java.util.List;

import pojo.DatabaseBackup;
import pojo.Role;
import pojo.User;

/**
 * @author 王耀星
 * @date 2018年10月22日
 */
public interface UserService {

	// 根据传递的用户信息查询用户是否存在
	User selectUser(User user);

	// 查询用户名是否重复
	boolean selectUserName(String userName);

	// 查询所有的用户列表
	List<User> getUserList();

	// 根据名字查询用户
	User getUserByName(String userName);

	// 查询可以查询的用户列表(状态为1)
	List<User> getUserListA(Integer state);

	// 根据名字查询可以查询的用户
	List<User> getUserListA1(Integer state, String userName);

	// 添加用户
	Integer addUser(User user);

	// 根据主键删除用户
	Integer deleteUserById(Integer userId);

	// 根据主键查询用户
	User getUserById(Integer userId);

	// 回收用户（更改状态由1变为0）
	Integer recycleUserById(Integer userId);

	// 恢复用户（更改状态由0变为1）
	Integer restoreUserById(Integer userId);

	// 编辑用户
	Integer editUser(User user);

	// 查询角色列表
	List<Role> getRoleList();

	// 根据状态查询角色列表
	List<Role> getRoleList(String back1);

	// 查询角色名是否重复
	Role distinctRoleName(String roleName);

	// 添加角色
	Integer addRole(String roleName, String describe);

	// 回收角色
	Integer recycleRoleById(Integer roleId);

	// 恢复角色
	Integer restoreRoleById(Integer roleId);

	// 根据主键查询角色
	Role getRoleById(Integer roleId);

	// 备份数据库的方法
	Integer backupDatabase(String fileName, String loadName);

	// 还原数据库的方法
	void restoreDatabase(String fileName);

	// 查询数据库备份的信息
	List<DatabaseBackup> getDatabaseBackupList();

	// 根据主键查询数据库备份信息
	DatabaseBackup getDatabaseBackupById(Integer backId);

}
