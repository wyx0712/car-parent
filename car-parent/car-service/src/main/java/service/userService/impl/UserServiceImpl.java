/**
 * 
 */
package service.userService.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.DatabaseBackupMapper;
import mapper.RoleMapper;
import mapper.UserMapper;
import pojo.DatabaseBackup;
import pojo.Role;
import pojo.RoleExample;
import pojo.User;
import pojo.UserExample;
import pojo.UserExample.Criteria;
import service.userService.UserService;
import utils.DatabaseBackupUtil;

/**
 * @author 王耀星
 * @date 2018年10月22日
 */
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private DatabaseBackupMapper databaseBackupMapper;

	// 查询所有用户列表
	@Override
	public List<User> getUserList() {

		return userMapper.selectByExample(null);
	}

	// 根据状态查询用户列表
	@Override
	public List<User> getUserListA(Integer state) {
		UserExample example = new UserExample();
		Criteria criteria = example.createCriteria();
		criteria.andJobStateEqualTo(state);
		List<User> list = userMapper.selectByExample(example);
		return list;
	}

	// 根据名字查询可以查询的用户
	@Override
	public List<User> getUserListA1(Integer state, String userName) {
		UserExample example = new UserExample();
		Criteria criteria = example.createCriteria();
		criteria.andJobStateEqualTo(state);
		criteria.andRealNameLike("%" + userName + "%");
		List<User> list = userMapper.selectByExample(example);
		return list;
	}

	// 查询用户是否存在，登陆时验证使用
	@Override
	public User selectUser(User user) {
		UserExample example = new UserExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserNameEqualTo(user.getUserName());
		criteria.andPasswordEqualTo(user.getPassword());
		List<User> list = userMapper.selectByExample(example);
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	// 根据登录名获取用户
	@Override
	public User getUserByName(String userName) {
		UserExample example = new UserExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserNameEqualTo(userName);
		List<User> list = userMapper.selectByExample(example);
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	// 查询用户名是否可用
	@Override
	public boolean selectUserName(String userName) {

		UserExample example = new UserExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserNameEqualTo(userName);
		List<User> list = userMapper.selectByExample(example);
		if (list == null || list.size() == 0) {
			return true; // true表示用户登录名可用
		}
		return false;// false表示用户登录名不可用
	}

	// 添加用户
	@Override
	public Integer addUser(User user) {
		Integer i = userMapper.insert(user);
		return i;
	}

	// 根据主键删除用户
	@Override
	public Integer deleteUserById(Integer userId) {
		int i = userMapper.deleteByPrimaryKey(userId);
		return i;
	}

	// 根据主键查询用户
	@Override
	public User getUserById(Integer userId) {
		User user = userMapper.selectByPrimaryKey(userId);

		return user;
	}

	// 回收用户（状态由1改为0）
	@Override
	public Integer recycleUserById(Integer userId) {
		User user = getUserById(userId);
		if (user != null) {
			// 将用户的状态由1改为0
			user.setJobState(0);
			Integer i = userMapper.updateByPrimaryKey(user);
			return i;
		}
		return 0;
	}

	// 恢复用户信息，将用户的状态由0改为1
	@Override
	public Integer restoreUserById(Integer userId) {
		User user = getUserById(userId);
		if (user != null) {
			// 将用户的状态由0改为1
			user.setJobState(1);
			Integer i = userMapper.updateByPrimaryKey(user);
			return i;
		}
		return 0;
	}

	// 编辑用户
	@Override
	public Integer editUser(User user) {
		Integer i = userMapper.updateByPrimaryKey(user);
		return i;
	}

	// 查询角色列表
	@Override
	public List<Role> getRoleList() {

		return roleMapper.selectByExample(null);
	}

	// 根据角色名获取角色
	@Override
	public Role distinctRoleName(String roleName) {
		RoleExample example = new RoleExample();
		pojo.RoleExample.Criteria criteria = example.createCriteria();
		criteria.andRoleNameEqualTo(roleName);
		List<Role> list = roleMapper.selectByExample(example);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	// 根据状态获取使用中的角色列表
	@Override
	public List<Role> getRoleList(String back1) {
		RoleExample example = new RoleExample();
		pojo.RoleExample.Criteria criteria = example.createCriteria();
		criteria.andBack1EqualTo(back1);
		List<Role> list = roleMapper.selectByExample(example);
		return list;
	}

	// 添加role
	@Override
	public Integer addRole(String roleName, String describe) {
		Role role = new Role();
		role.setRoleName(roleName);
		role.setBack2(describe);
		return roleMapper.insert(role);
	}

	// 回收角色（更改状态码）
	@Override
	public Integer recycleRoleById(Integer roleId) {
		Role role = getRoleById(roleId);
		if (role != null) {
			// 将角色的状态由1改为0
			role.setBack1("0");
			Integer i = roleMapper.updateByPrimaryKey(role);
			return i;
		}
		return 0;
	}

	// 恢复角色
	@Override
	public Integer restoreRoleById(Integer roleId) {
		Role role = getRoleById(roleId);
		if (role != null) {
			// 将角色的状态由0改为1
			role.setBack1("1");
			Integer i = roleMapper.updateByPrimaryKey(role);
			return i;
		}
		return 0;
	}

	// 根据主键获得角色信息
	@Override
	public Role getRoleById(Integer roleId) {

		return roleMapper.selectByPrimaryKey(roleId);
	}

	// 备份数据库
	@Override
	public Integer backupDatabase(String fileName, String loadName) {
		// 调用工具类进行数据库备份
		DatabaseBackupUtil.backup(fileName);
		// 向备份信息表中添加一条信息
		DatabaseBackup record = new DatabaseBackup();
		record.setFileName(fileName);
		record.setLoadName(loadName);
		record.setCreateTime(new Date());
		Integer i = databaseBackupMapper.insert(record);
		return i;
	}

	// 还原数据库
	@Override
	public void restoreDatabase(String fileName) {
		// 调用工具类方法还原数据库
		DatabaseBackupUtil.restore(fileName);
	}

	// 查询所有备份信息
	@Override
	public List<DatabaseBackup> getDatabaseBackupList() {
		List<DatabaseBackup> list = databaseBackupMapper.selectByExample(null);

		return list;
	}

	// 根据主键获取备份信息
	@Override
	public DatabaseBackup getDatabaseBackupById(Integer backId) {

		return databaseBackupMapper.selectByPrimaryKey(backId);
	}

}
