/**
 * 
 */
package service.userService.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.OperateLogMapper;
import pojo.OperateLog;
import pojo.OperateLogExample;
import pojo.OperateLogExample.Criteria;
import service.userService.OperateLogService;

/**
 * @author 王耀星
 * @date 2018年10月27日
 */
@Service
public class OperateLogServiceImpl implements OperateLogService {
	@Autowired
	private OperateLogMapper logMapper;

	// 保存日志
	@Override
	public Integer saveLog(OperateLog log) {

		return logMapper.insert(log);
	}

	// 查询所有日志信息列表
	@Override
	public List<OperateLog> getAllOperateLog() {

		return logMapper.selectByExample(null);
	}

	// 根据主键查询日志
	@Override
	public OperateLog getOperateLogById(Integer logId) {

		return logMapper.selectByPrimaryKey(logId);
	}

	// 根据主键删除日志
	@Override
	public Integer deleteOperateLog(Integer logId) {

		return logMapper.deleteByPrimaryKey(logId);
	}

	// 查询所有日志列表（状态为1）
	@Override
	public List<OperateLog> getAllOperateLogA(String back1) {
		OperateLogExample example = new OperateLogExample();
		Criteria criteria = example.createCriteria();
		criteria.andBack1EqualTo(back1);
		List<OperateLog> list = logMapper.selectByExample(example);
		return list;
	}

	// 查询所有日志列表（状态为1）且操作人
	@Override
	public List<OperateLog> getAllOperateLogB(String back1, String userName) {
		OperateLogExample example = new OperateLogExample();
		Criteria criteria = example.createCriteria();
		criteria.andBack1EqualTo(back1);
		criteria.andOpNameLike("%" + userName + "%");
		List<OperateLog> list = logMapper.selectByExample(example);
		return list;
	}

	// 根据主键回收日志（更改状态back1）
	@Override
	public Integer recycleOperateLog(Integer logId) {
		OperateLog log = getOperateLogById(logId);
		log.setBack1("0");
		Integer i = logMapper.updateByPrimaryKey(log);
		return i;
	}

	// 根据主键恢复日志
	@Override
	public Integer restoreOperateLog(Integer logId) {
		OperateLog log = getOperateLogById(logId);
		log.setBack1("1");
		Integer i = logMapper.updateByPrimaryKey(log);
		return i;
	}

	// 查询某个时间段内的日志列表
	@Override
	public List<OperateLog> getLogListTime(Date start, Date end) {
		OperateLogExample example = new OperateLogExample();
		Criteria criteria = example.createCriteria();
		criteria.andCreateDateBetween(start, start);
		List<OperateLog> list = logMapper.selectByExample(example);

		return list;
	}

}
