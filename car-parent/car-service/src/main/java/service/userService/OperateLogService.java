/**
 * 
 */
package service.userService;

import java.util.Date;
import java.util.List;

import pojo.OperateLog;

/**
 * @author 王耀星
 * @date 2018年10月27日
 */
public interface OperateLogService {

	// 保存日志的方法
	Integer saveLog(OperateLog log);

	// 查询所有日志列表
	List<OperateLog> getAllOperateLog();

	// 查询所有日志列表（根据状态）
	List<OperateLog> getAllOperateLogA(String back1);

	// 查询所有日志列表（状态为1）且操作人为指定
	List<OperateLog> getAllOperateLogB(String back1, String userName);

	// 根据主键查询日志
	OperateLog getOperateLogById(Integer logId);

	// 根据主键删除日志
	Integer deleteOperateLog(Integer logId);

	// 根据主键回收日志（修改状态）
	Integer recycleOperateLog(Integer logId);

	// 根据主键恢复日志（修改状态）
	Integer restoreOperateLog(Integer logId);

	// 查询某个时间段内的日志信息
	List<OperateLog> getLogListTime(Date start, Date end);
}
