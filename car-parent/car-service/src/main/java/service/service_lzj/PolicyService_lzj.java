package service.service_lzj;

import java.util.List;

import pojo.Policy;
import pojo.PolicyExample;

public interface PolicyService_lzj {
	//查询所有保单集合列表
	List<Policy> selectByExample(PolicyExample policy); 
	//根据保单号查询保单
	Policy selectPolicyById(String policy);
	// 查询可以查询的用户列表
	List<Policy> getUserList();

	Integer updatePolicy(Policy policy);
	
	
	
}
