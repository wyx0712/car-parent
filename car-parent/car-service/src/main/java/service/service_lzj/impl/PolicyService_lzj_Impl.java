package service.service_lzj.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.PolicyMapper;
import pojo.Policy;
import pojo.PolicyExample;
import pojo.PolicyExample.Criteria;
import service.service_lzj.PolicyService_lzj;
@Service
public class PolicyService_lzj_Impl implements PolicyService_lzj {
 
	@Autowired
	private PolicyMapper policyMapper;
	
	@Override
	public List<Policy> selectByExample(PolicyExample example) {
		
		return policyMapper.selectByExample(example);
	}




	@Override
	public Policy selectPolicyById(String policyId) {
		Policy policy =policyMapper.selectByPrimaryKey(policyId);
		return policy;
	}




	@Override
	public List<Policy> getUserList() {
		
		return  policyMapper.selectByExample(null);
	}




	@Override
	public Integer updatePolicy(Policy policy) {
		// TODO Auto-generated method stub
		return policyMapper.updateByPrimaryKey(policy);
	}

}
