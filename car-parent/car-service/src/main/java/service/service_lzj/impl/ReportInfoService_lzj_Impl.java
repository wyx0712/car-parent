package service.service_lzj.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.ReportInfoMapper;
import pojo.ReportInfo;
import pojo.ReportInfoExample;
import service.service_lzj.ReportInfoService_lzj;
@Service
public class ReportInfoService_lzj_Impl implements ReportInfoService_lzj {

	@Autowired
	private ReportInfoMapper reportInfoMapper;
	@Override
	public List<ReportInfo> selectByExample(ReportInfoExample example) {
		List<ReportInfo> list = reportInfoMapper.selectByExample(example);
		return list;
	}
	@Override
	public ReportInfo selectByPrimaryKey(Integer reportNo) {
		ReportInfo reportInfo = reportInfoMapper.selectByPrimaryKey(reportNo);
		return reportInfo;
	}
	//查询总条数
	@Override
	public int countByExample(ReportInfoExample example) {
		int countByExample = reportInfoMapper.countByExample(example);
		return countByExample;
	}
	@Override
	public List<ReportInfo> getUserList() {
	
		return reportInfoMapper.selectByExample(null);
	}

}
