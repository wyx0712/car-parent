package service.service_lzj.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.LossinfoMapper;
import pojo.Lossinfo;
import pojo.LossinfoExample;
import pojo.LossinfoExample.Criteria;
import service.service_lzj.LossinfoService_lzj;
@Service
public class LossinfoService_lzj_Impl implements LossinfoService_lzj {
 @Autowired
	private LossinfoMapper lossinfoMapper;
	
	
	
	@Override
	public List<Lossinfo> selectByExample(LossinfoExample example) {
		List<Lossinfo> list = lossinfoMapper.selectByExample(example);
		return list;
	}



	@Override
	public List<Lossinfo> selectByPrimaryKey1(String carNo) {
		LossinfoExample example = new LossinfoExample();
		Criteria criteria = example.createCriteria();
		criteria.andCarNoEqualTo(carNo);
		
		List<Lossinfo> list = lossinfoMapper.selectByExample(example);
		
		return list;
	}


       //通过主键查询车辆定损信息详情
	@Override
	public Lossinfo selectByPrimaryKey(Integer lossId) {
		Lossinfo lossinfo = lossinfoMapper.selectByPrimaryKey(lossId);
		return lossinfo;
	}



	@Override
	public List<Lossinfo> getUserList() {
		
		return lossinfoMapper.selectByExample(null);
	}



	

}
