package service.service_lzj.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.UserMapper;
import pojo.User;
import pojo.UserExample;
import pojo.UserExample.Criteria;
import service.service_lzj.UserService_lzj;
@Service
public class UserService_lzj_Impl implements UserService_lzj {
			   @Autowired
				private 	UserMapper  userMapper;
				
				
	
	@Override
	public List<User> selectByExample(UserExample example) {
		List<User> list = userMapper.selectByExample(null);
		return list;
	}


	@Override
	public List<User> selectByPrimaryKey1(String realName,Integer jobState) {
		UserExample example=new UserExample();
		Criteria criteria=example.createCriteria();
		criteria.andRealNameEqualTo(realName);
		criteria.andJobStateEqualTo(jobState);
		List<User> list = userMapper.selectByExample(example);
		return list;
	}


	@Override
	public User selectByPrimaryKey(Integer userId) {
		User user = userMapper.selectByPrimaryKey(userId);
		return user;
	}

// 查询可以查询的用户列表
	@Override
	public List<User> getUserList() {
		
		List<User> list = userMapper.selectByExample(null);
		return list;
	}
//根据员工状态查询
	@Override
	public List<User> getUserList1(Integer jobState) {
		UserExample example=new UserExample();
		Criteria criteria=example.createCriteria();
		
		criteria.andJobStateEqualTo(jobState);
		List<User> list = userMapper.selectByExample(example);
		return list;
	}

 //根据在职转台查询
	@Override
	public List<User> selectByJobState(Integer jobState) {
	                UserExample example = new UserExample();
	                Criteria criteria = example.createCriteria();
	                criteria.andJobStateEqualTo(jobState);
	                List<User> list = userMapper.selectByExample(example);
	           
	                
	                
	                
		return list;
	}


	//更改i状态0改为1
	@Override
	public int updateByPrimaryKey(Integer userId) {
		//根据主键查询旧的数据
		User oldUser=userMapper.selectByPrimaryKey(userId);		
		//将查询出的user对象的状态改为1
		oldUser.setJobState(1);
		//调用userMapper的update方法更新数据
		int i=userMapper.updateByPrimaryKey(oldUser);
		return i;
	}


	@Override
	public int xiugaizhuangtai(Integer userId) {
		//根据主键查询旧的数据
				User oldUser=userMapper.selectByPrimaryKey(userId);		
				//将查询出的user对象的状态改为0
				oldUser.setJobState(0);
				//调用userMapper的update方法更新数据
				int i=userMapper.updateByPrimaryKey(oldUser);
		return i;
	}


	

	


}
