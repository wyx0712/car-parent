package service.service_lzj;

import java.util.List;

import pojo.Lossinfo;
import pojo.LossinfoExample;

public interface LossinfoService_lzj {
	List<Lossinfo> selectByExample(LossinfoExample example);
	List<Lossinfo>  selectByPrimaryKey1(String carNo);	
	//通过主键查询信息
	  Lossinfo selectByPrimaryKey(Integer lossId);
	  
		List<Lossinfo> getUserList();
}
