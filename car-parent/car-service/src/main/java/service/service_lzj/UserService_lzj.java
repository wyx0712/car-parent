package service.service_lzj;

import java.util.List;

import pojo.User;
import pojo.UserExample;

public interface UserService_lzj {
	  List<User> selectByExample(UserExample example);
	  
	  List<User> selectByPrimaryKey1(String realName,Integer jobState);
	  //拿到userId
	  User selectByPrimaryKey(Integer userId);
	// 查询可以查询的用户列表
	  List<User> getUserList();
	  
	// 根据员工转台查询
		  List<User> getUserList1(Integer jobState);
	  //根据在职转台查询
	  List<User> selectByJobState(Integer jobState);
	  //修改在职转台 从1变0
	  int updateByPrimaryKey(Integer userId);
	  //修改在职转台从0变1
	  int xiugaizhuangtai(Integer userId);
	  
}
