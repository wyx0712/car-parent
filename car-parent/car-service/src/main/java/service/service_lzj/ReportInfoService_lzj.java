package service.service_lzj;

import java.util.List;

import pojo.ReportInfo;
import pojo.ReportInfoExample;

public interface ReportInfoService_lzj {
	  List<ReportInfo> selectByExample(ReportInfoExample example);
	  ReportInfo selectByPrimaryKey(Integer reportNo);
	  //查询在总条数
	   int countByExample(ReportInfoExample example);
	  List<ReportInfo> getUserList();
}
