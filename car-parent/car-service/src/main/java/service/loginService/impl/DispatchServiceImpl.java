/**
 * 
 */
package service.loginService.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.DispatchMapper;
import mapper.EntrustMapper;
import pojo.Dispatch;
import pojo.DispatchExample;
import service.loginService.DispatchService;

/**
 * @author 李智维
 *
 * 2018年10月28日-上午9:39:28
 */
@Service
public class DispatchServiceImpl implements DispatchService {

	@Autowired
	private DispatchMapper dispatchMapper;
	/* (non-Javadoc)
	 * @see service.loginService.DispatchService#insert(pojo.Dispatch)
	 */
	@Override
	public int insert(Dispatch dispatch) {
		// TODO Auto-generated method stub
		return dispatchMapper.insert(dispatch);
	}

}
