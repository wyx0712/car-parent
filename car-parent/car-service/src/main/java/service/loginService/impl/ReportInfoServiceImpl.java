/**
 * 
 */
package service.loginService.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.ReportInfoMapper;
import pojo.ReportInfo;
import pojo.ReportInfoExample;
import pojo.ReportInfoExample.Criteria;
import service.loginService.ReportInfoService;

/**
 * @author 李智维
 *
 * 2018年10月25日-上午10:43:19
 */
@Service
public class ReportInfoServiceImpl implements ReportInfoService {
	@Autowired
	private ReportInfoMapper reportInfoMapper;
	
	/* (non-Javadoc)
	 * @see service.loginService.ReportInfoService#insert(pojo.ReportInfo)
	 */
	@Override
	public int insert(ReportInfo reportInfo) {
		// TODO Auto-generated method stub
		return reportInfoMapper.insert(reportInfo);
	}

	/* (non-Javadoc)
	 * @see service.loginService.ReportInfoService#getreportNo(java.lang.String)
	 */
	@Override
	public ReportInfo getreportNo(String policyId) {
		ReportInfoExample example = new ReportInfoExample();
		Criteria criteria = example.createCriteria();
		if (policyId != null && policyId != "") {
			criteria.andPolicyIdEqualTo(policyId);
		}
		List<ReportInfo> list = reportInfoMapper.selectByExample(example);
		if (list.size() > 0 && !list.isEmpty()) {
			return list.get(list.size()-1);
		}
		return null;
	}

}
