/**
 * 
 */
package service.loginService.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.CompanyMapper;
import mapper.PolicyMapper;
import pojo.Company;
import pojo.CompanyExample;
import pojo.Policy;
import pojo.PolicyExample;
import pojo.PolicyExample.Criteria;
import service.loginService.CompanyService;

/**
 * @author 李智维
 *
 * 2018年10月30日-下午2:40:06
 */
@Service
public class CompanyServiceImpl implements CompanyService {

	
    @Autowired
    private CompanyMapper companyMapper;
	/* (non-Javadoc)
	 * @see service.loginService.CompanyService#selectByExample(pojo.CompanyExample)
	 */
	@Override
	public List<Company> selectByExample(CompanyExample companyExample) {
		
		return companyMapper.selectByExample(companyExample);
	}

}
