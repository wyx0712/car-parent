/**
 * 
 */
package service.loginService.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.UserMapper;
import pojo.User;
import pojo.UserExample;
import pojo.UserExample.Criteria;
import service.loginService.UserService_lzw;

/**
 * @author 李智维
 *
 * 2018年10月31日-下午3:06:28
 */

@Service
public class UserServiceImpl_lzw implements UserService_lzw {

	
    @Autowired
    private UserMapper userMapper;
	/* (non-Javadoc)
	 * @see service.loginService.UserService#selectByExample(pojo.UserExample)
	 */
    //查询查勘人员列表
	@Override
	public List<User> selectByExample(UserExample userExample) {
		// TODO Auto-generated method stub
		
		UserExample example=new UserExample();
		Criteria criteria=example.createCriteria();
		criteria.andRoleIdEqualTo(3);
		return userMapper.selectByExample(example);
	}

}
