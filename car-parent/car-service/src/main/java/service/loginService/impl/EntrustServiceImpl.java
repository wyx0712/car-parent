/**
 * 
 */
package service.loginService.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.EntrustMapper;
import mapper.ReportInfoMapper;
import pojo.Entrust;
import pojo.ReportInfo;
import service.loginService.EntrustService;

/**
 * @author 李智维
 *
 * 2018年10月27日-下午6:54:40
 */
@Service
public class EntrustServiceImpl implements EntrustService {
	@Autowired
	private EntrustMapper entrustMapper;
	/* (non-Javadoc)
	 * @see service.loginService.EntrustService#insert(pojo.ReportInfo)
	 */
	@Override
	public int insert(Entrust entrust) {
		// TODO Auto-generated method stub
		return entrustMapper.insert(entrust);
	}

}
