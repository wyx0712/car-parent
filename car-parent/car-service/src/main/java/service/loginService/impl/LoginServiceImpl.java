/**
 * 
 */
package service.loginService.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.PolicyMapper;
import mapper.ReportInfoMapper;
import pojo.Policy;
import pojo.PolicyExample;
import pojo.PolicyExample.Criteria;
import pojo.ReportInfo;
import pojo.ReportInfoExample;
import service.loginService.LoginService;

/**
 * @author 李智维
 *
 * 2018年10月24日-上午8:51:59
 */
@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private PolicyMapper policyMapper;
	/* (non-Javadoc)
	 * @see service.loginService.LoginService#getInfo(java.lang.String)
	 */
	//查询保单信息
	@Override
	public Policy getInfo(String carNo) {
		PolicyExample example = new PolicyExample();
		Criteria criteria = example.createCriteria();
		if (carNo != null && carNo != "") {
			criteria.andCarNoEqualTo(carNo);
		}
		List<Policy> list = policyMapper.selectByExample(example);
		if (list.size() > 0 && !list.isEmpty()) {
			return list.get(list.size()-1);
		}
		return null;
	}
	/* (non-Javadoc)
	 * @see service.loginService.LoginService#getapplicant(java.lang.String)
	 */
	@Override
	//查询投保人
	public Policy getapplicant(String insuranceId) {
		PolicyExample example = new PolicyExample();
		Criteria criteria = example.createCriteria();
		if (insuranceId != null && insuranceId != "") {
			criteria.andPolicyIdEqualTo(insuranceId);
		}
		List<Policy> list = policyMapper.selectByExample(example);
		if (list.size() > 0 && !list.isEmpty()) {
			int i = list.size();
			System.out.println(i);
			return list.get(i-1);
		}
		return null;
	}
}
