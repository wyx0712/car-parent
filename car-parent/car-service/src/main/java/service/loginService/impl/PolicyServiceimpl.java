/**
 * 
 */
package service.loginService.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mapper.PolicyMapper;
import pojo.Policy;
import pojo.PolicyExample;
import pojo.PolicyExample.Criteria;
import service.loginService.PolicyService;

/**
 * @author 李智维
 *
 *         2018年10月25日-上午9:39:33
 */
@Service
public class PolicyServiceimpl implements PolicyService {

	@Autowired
	private PolicyMapper policyMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see service.loginService.PolicyService#selectByExample(java.lang.String,
	 * java.lang.Integer)
	 */
	@Override
	public List<Policy> selectByExample(String carNo, Integer PersonnelId) {
		PolicyExample example = new PolicyExample();
		Criteria criteria = example.createCriteria();
		criteria.andCarNoEqualTo(carNo);
		criteria.andLoadIdEqualTo(PersonnelId);
		List<Policy> list = policyMapper.selectByExample(example);
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see service.loginService.PolicyService#getcarNo(int)
	 */
	@Override
	public Policy getcarNo(String policyId) {
		PolicyExample example = new PolicyExample();
		Criteria criteria = example.createCriteria();
		/*
		 * List<Policy> list = policyMapper.selectByExample(example); int i =
		 * list.size(); return list.get(i-1);
		 */
		return policyMapper.selectByPrimaryKey(policyId);

	}

}
