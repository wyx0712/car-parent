/**
 * 
 */
package service.loginService;

import pojo.Dispatch;
import pojo.Entrust;

/**
 * @author 李智维
 *
 * 2018年10月28日-上午9:39:11
 */
public interface DispatchService {
	//新增派工信息
	int insert(Dispatch dispatch);
}
