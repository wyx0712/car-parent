/**
 * 
 */
package service.loginService;

import pojo.ReportInfo;

/**
 * @author 李智维
 *
 * 2018年10月25日-上午10:42:18
 */
public interface ReportInfoService {
	//新增报案信息
	int insert(ReportInfo reportInfo);
	//根据保单号查询报案信息
	ReportInfo getreportNo(String policyId);

}
