/**
 * 
 */
package service.loginService;

import pojo.LossinfoExample;
import pojo.Policy;
import pojo.ReportInfo;

/**
 * @author 李智维
 *
 * 2018年10月24日-上午8:51:06
 */
public interface LoginService {
	//查询保单信息
	Policy getInfo(String carNo);
	//查询投保人
	Policy getapplicant(String insuranceId);
	
	

}
