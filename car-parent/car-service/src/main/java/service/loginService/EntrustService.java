/**
 * 
 */
package service.loginService;

import pojo.Entrust;
import pojo.ReportInfo;

/**
 * @author 李智维
 *
 * 2018年10月27日-下午6:53:40
 */
public interface EntrustService {
	//新增委托信息
	int insert(Entrust entrust);
}
