/**
 * 
 */
package service.loginService;

import java.util.List;

import mapper.CompanyMapper;
import pojo.Company;
import pojo.CompanyExample;

/**
 * @author 李智维
 *
 * 2018年10月30日-下午2:34:19
 */
public interface CompanyService {
	//查询委托公司
	 List<Company> selectByExample(CompanyExample CompanyExample);
}
