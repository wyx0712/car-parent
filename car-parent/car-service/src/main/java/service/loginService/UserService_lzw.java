/**
 * 
 */
package service.loginService;

import java.util.List;

import pojo.CompanyExample;
import pojo.User;
import pojo.UserExample;

/**
 * @author 李智维
 *
 * 2018年10月31日-下午3:01:57
 */
public interface UserService_lzw {
	List<User> selectByExample(UserExample userExample);
}
