/**
 * 
 */
package service.loginService;

import java.util.List;

import pojo.Policy;

/**
 * @author 李智维
 *
 *         2018年10月25日-上午9:38:01
 */
public interface PolicyService {

	// 根据用户提供的车牌查询保单
	List<Policy> selectByExample(String carNo, Integer PersonnelId);

	// 查询车牌号
	Policy getcarNo(String policyId);
}
